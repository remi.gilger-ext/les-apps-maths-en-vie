import AtelierAPI from "./atelierAPI";
import GameAPI from "./gameAPI";
import HelpRequestAPI from "./helpRequestAPI";
import ProgrammationAPI from "./programmationAPI";
import ProfilAPI from "./profilAPI";
import UserAPI from "./userAPI";
import StudentAPI from "./studentAPI";

export type { ApiError, ApiErrorArgs } from "./utils";

export { AtelierAPI, GameAPI, HelpRequestAPI, ProgrammationAPI, ProfilAPI, UserAPI, StudentAPI };
