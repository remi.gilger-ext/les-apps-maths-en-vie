import api from "./api";

export type { ApiError, ApiErrorArgs } from "./apis";

export default api;
