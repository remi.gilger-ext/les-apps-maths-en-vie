enum UserTypeEnum {
  Teacher = "teacher",
  Parent = "parent",
}

export default UserTypeEnum;
