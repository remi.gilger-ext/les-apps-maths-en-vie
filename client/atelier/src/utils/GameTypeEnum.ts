enum GameTypeEnum {
  Demo = "demo",
  Training = "training",
  Evaluation = "evaluation",
}

export default GameTypeEnum;
