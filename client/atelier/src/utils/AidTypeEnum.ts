enum AidTypeEnum {
  SkipProblem = "skipProblem",
  AidNivel1 = "aidNivel1",
  AidNivel2 = "aidNivel2",
}

export default AidTypeEnum;
