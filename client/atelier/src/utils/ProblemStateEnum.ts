enum ProblemStateEnum {
    NOT_DISPLAYED = "notDisplayed",
    ANSWERED_CORRECTLY = "answeredCorrectly",
    ANSWERED_WRONGLY = "answeredWrongly",
    SKIPPED = "skipped",
}

export default ProblemStateEnum;
