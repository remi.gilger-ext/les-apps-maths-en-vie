import BaseLayout from "./BaseLayout";
import { Button, ButtonLink } from "./Buttons";
import { Checkbox, CheckboxArray } from "./Checkbox";
import ErrorPage from "./ErrorPage";
import FormButtonLoading from "./FormButtonLoading";
import Footer from "./Footer";
import Header from "./Header";
import KeypadDisplay from "./KeypadDisplay";
import LayoutPage from "./LayoutPage";
import NumericKeypad from "./NumericKeypad";
import ProgrammationTypeForm from "./ProgrammationTypeForm";
import ProgressBar from "./ProgressBar";
import RituelParametersForm from "./RituelParametersForm";
import ScrollToAnchor from "./ScrollToAnchor";
import ShortcutMenu from "./ShortcutMenu";
import SpeechButton from "./SpeechButton";
import WaitingPage from "./WaitingPage";

// import { lazy } from "react";

// const Chart = lazy(() => import("./Chart"));

export {
  BaseLayout,
  Button,
  ButtonLink,
  Checkbox,
  CheckboxArray,
  ErrorPage,
  FormButtonLoading,
  Footer,
  Header,
  KeypadDisplay,
  LayoutPage,
  NumericKeypad,
  ProgrammationTypeForm,
  ProgressBar,
  RituelParametersForm,
  ScrollToAnchor,
  ShortcutMenu,
  SpeechButton,
  WaitingPage,
};
