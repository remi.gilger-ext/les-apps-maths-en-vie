import { useEffect } from "react";
import { useLocation } from "react-router-dom";

function ScrollToAnchor() {
  const location = useLocation();

  useEffect(() => {
    if (!location.hash) {
      return;
    }

    const hashId = location.hash.slice(1);
    const targetElement = document.getElementById(hashId);

    if (hashId && targetElement) {
      setTimeout(() => {
        targetElement.tabIndex = -1;
        targetElement.focus();
        targetElement.style.scrollMarginTop = "65px";
        targetElement.scrollIntoView({ behavior: "smooth" });
      }, 50);
    }
  }, [location]);

  return null;
}

export default ScrollToAnchor;
