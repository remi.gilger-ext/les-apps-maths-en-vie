import { useRef } from "react";

function ShortcutMenu() {
  const skipToContent = () => {
    const pageContent = document.getElementById("page-content");
    if (pageContent) {
      pageContent.focus();
    }
  };

  const navRef = useRef<HTMLElement>(null);

  const handleKeyPress = (e: React.KeyboardEvent<HTMLElement>) => {
    if (e.key === "Escape" && navRef.current) {
      navRef.current.focus();
      navRef.current.blur();
    }
  };
  return (
    <nav
      ref={navRef}
      tabIndex={-1}
      className="visually-hidden-focusable position-fixed top-0 start-0 border-primary bg-white rounded-4 mt-3 ms-3 p-4"
      style={{ zIndex: 1031, maxWidth: 360, border: "2px solid" }}
      onKeyDown={handleKeyPress}
    >
      <p className="fw-bold mb-2" style={{ fontSize: 14 }}>
        Passer à
      </p>
      <ul className="navbar-nav row-gap-2">
        <li className="nav-item">
          <button tabIndex={0} className="link-primary nav-link link-underline-opacity-0 p-0" onClick={skipToContent}>
            Contenu principal
          </button>
        </li>
      </ul>
    </nav>
  );
}

export default ShortcutMenu;
