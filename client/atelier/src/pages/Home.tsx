import { faPersonDigging, faSchool, faTowerObservation } from "@fortawesome/free-solid-svg-icons";
import { ButtonLink } from "../components";
import { useAuth } from "../contexts/auth";
import { UserTypeEnum } from "../utils";
import { Routes } from "../routes";
import { ButtonHref } from "../components/Buttons";

function Home() {
  const { user } = useAuth();

  const loginURL = new URL(Routes.APPS_LOGIN);
  loginURL.searchParams.append("redirect", window.location.href);

  return (
    <div className="d-flex flex-column justify-content-around h-100 m-auto row-gap-2" style={{ width: "40%", padding: "100px 0" }}>
      <ButtonLink icone={faTowerObservation} path="ATELIER" label="L'atelier flash" fontSize={4} height="12vh" />
      {user ? (
        <ButtonLink icone={faSchool} path="AT_SCHOOL" label="L'atelier dirigé" fontSize={4} height="12vh" disabled={user && user.userType === UserTypeEnum.Parent} />
      ) : (
        <ButtonHref path={loginURL.toString()} icone={faSchool} label="L'atelier dirigé" fontSize={4} height="12vh" />
      )}
      <ButtonLink icone={faPersonDigging} iconeBgColor="red" path="AT_HOME" label="L'atelier focus" fontSize={4} height="12vh" disabled={!user || user.userType === UserTypeEnum.Teacher} />
      <ButtonLink icone={faPersonDigging} iconeBgColor="red" path="AT_HOME" label="L'atelier famille" fontSize={4} height="12vh" disabled={!user || user.userType === UserTypeEnum.Teacher} />
    </div>
  );
}

export default Home;
