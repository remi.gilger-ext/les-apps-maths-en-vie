import Atelier from "./Atelier";
import AtSchool from "./AtSchool";
import EndGame from "./EndGame";
import Game from "./Game";
import Generic from "./Generic";
import Help from "./Help";
import Home from "./Home";
import TeacherDashboard from "./TeacherDashboard";

export { Atelier, AtSchool, EndGame, Game, Generic, Help, Home, TeacherDashboard };
