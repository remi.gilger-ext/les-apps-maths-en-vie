import { FieldValues, Path, RegisterOptions } from "react-hook-form";
import { GameTypeEnum, ProblemStateEnum, SchoolNivelEnum, SchoolZoneEnum, TextProblemTypeEnum } from "./utils";
import AidTypeEnum from "./utils/AidTypeEnum";

// Mutation Data
export type MutationData = {
  message: string;
  variant?: string;
};

// Modal Default Props
export type ModalPropsDefault<T = any> = {
  show: boolean;
  closeModal: () => void;
  saveData: (data: T) => void;
  savedData: T;
};

// FormErrors
type ApiFormError<TFormErrors> = {
  name: keyof TFormErrors | "root";
  message: string;
};

export type FormErrors<TFormErrors = Record<string, string>> = ApiFormError<TFormErrors>[];

export type DefaultErrorsType<T extends FieldValues> = {
  [K in keyof T]: K extends Path<T> ? RegisterOptions<T, K> : never;
};

export type RitualParametersFormValue = {
  schoolZone: SchoolZoneEnum | null;
  classNivel: string[];
};

export type Problem = {
  id: number;
  title: string;
  statement: string;
  type: keyof typeof TextProblemTypeEnum;
  nivel: string;
  response: string;
  responseSentence: string;
  helpSentence1: string;
  helpSentence2: string;
  responseExplication: string;
  state: ProblemStateEnum;
  aidsUsed: AidTypeEnum[];
  responseGiven: string;
};

export type GameData =
  | {
      prePageLabel: string | null;
      nivel: SchoolNivelEnum;
      problems: Problem[];
      nbProblems: number;
      actualProblem: number;
      origin: "local" | "beneylu" | "nathan";
      aidPoints: number;
      aidPointsUsed: number;
      aidsCost: {
        [k in AidTypeEnum]: number;
      };
    } & (
      | {
          type: GameTypeEnum.Demo;
        }
      | {
          type: GameTypeEnum.Evaluation | GameTypeEnum.Training;
          student: Student;
        }
    );

export type Student = {
  id: number;
  nivel: SchoolNivelEnum | null;
  rank: number;
  isEnabled: boolean;
  canModifyNivel: boolean;
  games: Game[];
  helpRequests: HelpRequest[];
};

export type HelpRequest = {
  id: number;
  requestDate: number;
  isTreated: number;
  problem: Problem;
  responseGiven: string;
};

export type Game = {
  id: number;
  period: string;
  score: number;
} & (
  | {
      type: GameTypeEnum.Evaluation;
      evaluationLevel: 1 | 2 | 3;
    }
  | {
      type: GameTypeEnum.Training;
      trainingLevel: 1 | 2 | 3;
    }
  | {
      type: Exclude<GameTypeEnum, GameTypeEnum.Evaluation | GameTypeEnum.Training>;
    }
);

export type Period = 1 | 2 | 3 | 4 | 5 | 1.5 | 2.5 | 3.5 | 4.5 | 5.5;

export type Programmation = { [K in SchoolNivelEnum]?: Record<Period, { f1?: (keyof typeof TextProblemTypeEnum)[]; f2?: number; f3?: number; f4?: number; f5?: boolean }> };
