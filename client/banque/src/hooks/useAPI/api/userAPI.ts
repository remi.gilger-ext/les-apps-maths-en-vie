import axios from "axios";
import { getGetAxiosInstance, getPostPutAxiosInstance, handleError } from "./utils";

class UserAPI {
  async getUserData() {
    try {
      const response = await getGetAxiosInstance().get("/user");
      return response.data;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        handleError(error);
      }
    }
  }

  logout() {
    return getPostPutAxiosInstance().put("/logout");
  }
}

export default UserAPI;
