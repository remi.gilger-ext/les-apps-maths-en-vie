import axios from "axios";
import { getPostPutAxiosInstance, handleError } from "./utils";

class MessageAPI {
  async sendMessage(data: any) {
    try {
      const response = await getPostPutAxiosInstance().post("/message/add", data);
      return response.data;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        handleError(error);
      }
    }
  }
}

export default MessageAPI;
