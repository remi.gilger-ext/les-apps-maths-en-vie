enum ResponseStateEnum {
  NoResponse = "noResponse",
  AnsweredByHuman = "answeredByHuman",
  AnsweredByIa = "answeredByIa",
  Conflict = "conflict",
  Verified = "verified",
}

export default ResponseStateEnum;
