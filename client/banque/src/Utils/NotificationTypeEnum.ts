enum NotificationTypeEnum {
    Like = "likes",
    Comment = "comments",
    Message = "messages",
}

export default NotificationTypeEnum;
