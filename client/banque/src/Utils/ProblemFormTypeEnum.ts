enum ProblemFormTypeEnum {
    Contribute = "contribute",
    Modify = "modify",
    Review = "review",
}

export default ProblemFormTypeEnum;
