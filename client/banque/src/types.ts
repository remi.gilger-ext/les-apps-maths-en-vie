import { FieldValues, Path, RegisterOptions } from "react-hook-form";
import { EnigmaTypeEnum, PhotoTypeEnum, ProblemTypeEnum, ResponseStateEnum, TextProblemTypeEnum } from "./Utils";

// Mutation Data
export type MutationData = {
  message: string;
  variant?: string;
};

// Modal Default Props
export type ModalPropsDefault<T = any> = {
  show: boolean;
  closeModal: () => void;
  saveData: (data: T) => void;
  savedData: T;
};

// FormErrors
type ApiFormError<TFormErrors> = {
  name: keyof TFormErrors | "root";
  message: string;
};

export type FormErrors<TFormErrors = Record<string, string>> = ApiFormError<TFormErrors>[];

export type DefaultErrorsType<T extends FieldValues> = {
  [K in keyof T]: K extends Path<T> ? RegisterOptions<T, K> : never;
};

// Problems

interface ProblemBase {
  id: number;
  counterComment: number;
  counterLike: number;
  discr: ProblemTypeEnum;
  username: string;
  grade: number;
  canModify: boolean;
  canReport: boolean;
  canPublish: boolean;
  canRemove: boolean;
  isPrivateWait: boolean;
  isPrivate: boolean;
  isLiked: boolean;
  email?: string;
}

export type Lang = "french" | "english" | "italian" | "spanish" | "german";

export interface TextProblemType extends ProblemBase {
  discr: ProblemTypeEnum.TextProblem;
  title: string;
  statement: string;
  nivel: string;
  type: keyof typeof TextProblemTypeEnum;
  response: string | null;
  isExclude: boolean;
  responseState: ResponseStateEnum;
  responseSentence: string;
  responseExplication: string;
  helpSentence1: string;
  helpSentence2: string;
  successRate: number;
  traductions: {
    [K in Exclude<Lang, "french">]: {
      title: string | null;
      statement: string | null;
    };
  };
}

export interface PhotoType extends ProblemBase {
  discr: ProblemTypeEnum.Photo;
  attachment: string;
  type: string;
}

export interface PhotoProblemType extends ProblemBase {
  discr: ProblemTypeEnum.PhotoProblem;
  statement: string;
  attachment: string;
  nivel: string;
  type: string;
}

export type ProblemType = TextProblemType | PhotoType | PhotoProblemType;

export type EnigmaType = {
  attachment: string;
  type: keyof typeof EnigmaTypeEnum;
  username: string;
};

type PrimaryType = "step" | "propor" | "frac" | "cart" | "normal";
export type TextProblemFormType = keyof Omit<typeof TextProblemTypeEnum, PrimaryType> | "";

export type TextProblemFormValue = {
  primaryType: PrimaryType;
  title: string;
  statement: string;
  nivel: string;
  type: TextProblemFormType;
  public: "yes" | "no";
  response: string | null;
  isExclude: boolean;
};

export type PhotoFormValue = {
  attachment: (File | string)[];
  type: (keyof typeof PhotoTypeEnum)[];
  imageData: `${number};${number};${number};${number}`;
  imageRotation: number;
};

export type PhotoProblemFormValue = {
  attachment: (File | string)[];
  type: (keyof typeof PhotoTypeEnum)[];
  statement: string;
  nivel: string;
  public: "yes" | "no";
  imageData: `${number};${number};${number};${number}`;
  imageRotation: number;
  addToPhoto: boolean;
  photoType: (keyof typeof PhotoTypeEnum)[];
};

export type ProblemFormValue = TextProblemFormValue | PhotoFormValue | PhotoProblemFormValue;

export type ProblemCommentType = {
  id: number;
  content: string;
  createdAt: number;
  username: string;
  canRemove: boolean;
  problemId: number;
};

export type EditionType = {
  problemType: ProblemTypeEnum;
  editionType: ["pdf" | "proj", number];
};

export type TextProblemConsultFormValues = {
  tag: ["COMMUNITY_PRIVATE"?, "COMMUNITY_PUBLIC"?];
  type: (keyof typeof TextProblemTypeEnum)[];
  nivel: string[];
  textResearch: string;
  responseState: ResponseStateEnum | "all";
  isEdition: boolean;
  problemType: ProblemTypeEnum.TextProblem;
};

export type PhotoConsultFormValues = {
  tag: ["COMMUNITY_PRIVATE"?, "COMMUNITY_PUBLIC"?];
  type: (keyof typeof PhotoTypeEnum)[];
  problemType: ProblemTypeEnum.Photo;
};

export type PhotoProblemConsultFormValues = {
  tag: ["COMMUNITY_PRIVATE"?, "COMMUNITY_PUBLIC"?];
  type: (keyof typeof PhotoTypeEnum)[];
  nivel: string[];
  textResearch: string;
  isEdition: boolean;
  problemType: ProblemTypeEnum.PhotoProblem;
};

export type ProblemConsultFormValues = TextProblemConsultFormValues | PhotoConsultFormValues | PhotoProblemConsultFormValues;

export type EnigmaFormValues = {
  type: (keyof typeof EnigmaTypeEnum)[];
  nivel: string[];
};

export type EditionBasketProblem = {
  id: number;
  title?: string;
  attachment?: string;
  statement: string;
};
