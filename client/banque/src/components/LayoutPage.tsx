import { Outlet } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { Header, Footer, ShortcutMenu } from ".";

const LayoutPage = () => {
  const headerHeight = 50;

  return (
    <>
      <ShortcutMenu />
      <Header height={headerHeight} />
      <div id="page-content" tabIndex={-1} className="container-fluid" style={{ marginTop: headerHeight }}>
        <div style={{ padding: "20px 0px" }}>
          <Outlet />
        </div>
      </div>
      <ToastContainer autoClose={5000} position="bottom-right" hideProgressBar draggable={false} limit={2} />
      <Footer />
    </>
  );
};

export default LayoutPage;
