import ProtectedConsult from "./ProtectedConsult";
import ProtectedContribute from "./ProtectedContribute";
import ProtectedEdition from "./ProtectedEdition";
import ProtectedEnigma from "./ProtectedEnigma";
import ProtectedNotifications from "./ProtectedNotifications";
import ProtectedRoute from "./ProtectedRoute";

export { ProtectedConsult, ProtectedContribute, ProtectedEdition, ProtectedEnigma, ProtectedNotifications, ProtectedRoute };
