import { useQuery } from "@tanstack/react-query";
import { useAPI } from "../hooks";
import { Bar } from "react-chartjs-2";
import { Chart as ChartJS, BarElement, CategoryScale, LinearScale, Legend, Tooltip, ChartOptions, ChartData } from "chart.js";
import { AdminChartsData, AdminChartsKey } from "../pages/Admin/query";
import { useAuth } from "../contexts/auth";
ChartJS.register(BarElement, CategoryScale, LinearScale, Legend, Tooltip);

export const CHART_COLORS = {
  red: "rgb(255, 99, 132)",
  red_t: "rgb(255, 99, 132, 0.5)",
  orange: "rgb(255, 159, 64)",
  orange_t: "rgb(255, 159, 64, 0.5)",
  yellow: "rgb(255, 205, 86)",
  yellow_t: "rgb(255, 205, 86, 0.8)",
  green: "rgb(75, 192, 192)",
  green_t: "rgb(75, 192, 192, 0.5)",
  blue: "rgb(54, 162, 235)",
  blue_t: "rgb(54, 162, 235, 0.5)",
  purple: "rgb(153, 102, 255)",
  purple_t: "rgb(153, 102, 255, 0.5)",
  grey: "rgb(201, 203, 207)",
  grey_t: "rgb(201, 203, 207, 0.5)",
  brown: "rgb(165, 42, 42)",
  brown_t: "rgb(165, 42, 42, 0.5)",
};

function Chart() {
  const api = useAPI();
  const { user } = useAuth();

  const date30DaysAgo = new Date(Date.now() - 29 * 24 * 60 * 60 * 1000);
  const { data: chartsData } = useQuery<AdminChartsData>(AdminChartsKey, () =>
    api.admin.chart("week", date30DaysAgo.getUTCDate() + "-" + (date30DaysAgo.getUTCMonth() + 1) + "-" + date30DaysAgo.getUTCFullYear())
  );

  const options: ChartOptions<"bar"> = {
    scales: {
      y: {
        beginAtZero: true,
        ticks: {
          stepSize: 1,
        },
      },
    },
    plugins: {
      tooltip: {
        mode: "index",
        intersect: false,
      },
    },
  };

  const data: ChartData<"bar"> = {
    labels: chartsData?.labels,
    datasets: [
      {
        label: "Visites journalières",
        data: chartsData?.data.visits ?? [null],
        borderWidth: 1,
        borderColor: CHART_COLORS.yellow,
        backgroundColor: CHART_COLORS.yellow_t,
        hidden: !user?.isSuperAdmin,
      },
      {
        label: "Utilisateurs journaliers",
        data: chartsData?.data.user_visits ?? [null],
        borderWidth: 1,
        borderColor: CHART_COLORS.orange,
        backgroundColor: CHART_COLORS.orange_t,
        hidden: true,
      },
      {
        label: "Emails envoyés",
        data: chartsData?.data.emails_sent ?? [null],
        borderWidth: 1,
        borderColor: CHART_COLORS.blue,
        backgroundColor: CHART_COLORS.blue_t,
        hidden: true,
      },
      {
        label: "Nouvelles inscriptions",
        data: chartsData?.data.registrations ?? [null],
        borderWidth: 1,
        borderColor: CHART_COLORS.red,
        backgroundColor: CHART_COLORS.red_t,
        hidden: !user?.isSuperAdmin,
      },
      {
        label: "Clics page rituel",
        data: chartsData?.data.rituel_clicks ?? [null],
        borderWidth: 1,
        borderColor: CHART_COLORS.green,
        backgroundColor: CHART_COLORS.green_t,
        hidden: true,
      },
      {
        label: "Problèmes ajoutés",
        data: chartsData?.data.problems_added ?? [null],
        borderWidth: 1,
        borderColor: CHART_COLORS.purple,
        backgroundColor: CHART_COLORS.purple_t,
        hidden: true,
      },
      {
        label: "Parties jouées",
        data: chartsData?.data.games_played ?? [null],
        borderWidth: 1,
        borderColor: CHART_COLORS.brown,
        backgroundColor: CHART_COLORS.brown_t,
        hidden: true,
      },
    ],
  };

  return <div>{chartsData && <Bar options={options} data={data} />}</div>;
}

export default Chart;
