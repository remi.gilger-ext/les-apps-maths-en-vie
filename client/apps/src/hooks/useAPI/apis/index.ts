import ProfilAPI from "./profilAPI";
import ProgrammationAPI from "./programmationAPI";
import RituelAPI from "./rituelAPI";
import UserAPI from "./userAPI";

export type { ApiError } from "./utils";

export { ProfilAPI, ProgrammationAPI, RituelAPI, UserAPI };
