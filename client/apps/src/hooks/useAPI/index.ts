import api from "./api";

export type { ApiError } from "./apis";

export default api;
