import { FieldValues, Path, RegisterOptions } from "react-hook-form";
import { ProblemTypeEnum, ProgrammationTypeEnum, SchoolNivelEnum, SchoolZoneEnum, TextProblemTypeEnum } from "./utils";

// Mutation Data
export type MutationData = {
  message: string;
  variant?: string;
};

// Modal Default Props
export type ModalPropsDefault<T = any> = {
  show: boolean;
  closeModal: () => void;
  saveData: (data: T) => void;
  savedData: T;
};

// FormErrors
type ApiFormError<TFormErrors> = {
  name: keyof TFormErrors | "root";
  message: string;
};

export type FormErrors<TFormErrors = Record<string, string>> = ApiFormError<TFormErrors>[];

export type DefaultErrorsType<T extends FieldValues> = {
  [K in keyof T]: K extends Path<T> ? RegisterOptions<T, K> : never;
};

export type RitualParametersFormValue = {
  schoolZone: SchoolZoneEnum | null;
  classNivel: string[];
};

interface ProblemBase {
  id: number;
  counterComment: number;
  counterLike: number;
  discr: ProblemTypeEnum;
  username: string;
  grade: number;
  canModify: boolean;
  canReport: boolean;
  canPublish: boolean;
  canRemove: boolean;
  isPrivateWait: boolean;
  isPrivate: boolean;
  isLiked: boolean;
  email?: string;
}

export interface TextProblemType extends ProblemBase {
  discr: ProblemTypeEnum.TextProblem;
  title: string;
  statement: string;
  nivel: string;
  type: keyof typeof TextProblemTypeEnum;
  response: string | null;
  isExclude: boolean;
  responseSentence: string;
  responseExplication: string;
  helpSentence1: string;
  helpSentence2: string;
}

export type Period = 1 | 2 | 3 | 4 | 5 | 1.5 | 2.5 | 3.5 | 4.5 | 5.5;

export type Programmation = { [K in SchoolNivelEnum]?: Record<Period, { f1?: (keyof typeof TextProblemTypeEnum)[]; f2?: number; f3?: number; f4?: number; f5?: boolean }> };
