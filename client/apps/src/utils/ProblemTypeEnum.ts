enum ProblemTypeEnum {
  TextProblem = "textProblem",
  Photo = "photo",
  PhotoProblem = "photoProblem",
}

export default ProblemTypeEnum;
