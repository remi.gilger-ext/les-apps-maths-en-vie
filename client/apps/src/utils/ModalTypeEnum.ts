enum ModalTypeEnum {
  DeleteAccount = "deleteAccount",
  EditProgrammation = "editProgrammation",
  Message = "message",
  Programmation = "programmation",
  RituelParameters = "rituelParameters",
}

export default ModalTypeEnum;
