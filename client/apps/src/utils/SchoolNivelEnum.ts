enum SchoolNivelEnum {
  CP = "cp",
  CE1 = "ce1",
  CE2 = "ce2",
  CM1 = "cm1",
  CM2 = "cm2",
}

export default SchoolNivelEnum;
