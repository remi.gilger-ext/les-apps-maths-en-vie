import { useEffect, useRef, useState } from "react";
import { Link, matchPath, useLocation } from "react-router-dom";
import { Routes } from "../routes";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons";

function ShortcutMenu() {
  const skipToContent = () => {
    const pageContent = document.getElementById("page-content");
    if (pageContent) {
      pageContent.focus();
    }
  };

  const location = useLocation();
  const isHomePage = matchPath({ path: Routes.HOME }, location.pathname);

  const [focusIndex, setFocusIndex] = useState(-1);
  const navRef = useRef<HTMLElement>(null);

  useEffect(() => {
    if (navRef.current && focusIndex >= 0) {
      const element = navRef.current.querySelectorAll("ul li>a, li>button")[focusIndex] as HTMLElement;
      element.focus();
    }
  }, [focusIndex]);

  const handleKeyPress = (e: React.KeyboardEvent<HTMLElement>) => {
    const size = navRef.current?.querySelectorAll("li").length ?? 0;
    if (e.key === "ArrowDown") {
      setFocusIndex((old) => (old + 1 + size) % size);
    } else if (e.key === "ArrowUp") {
      setFocusIndex((old) => (old - 1 + size) % size);
    } else if (e.key === "Escape") {
      if (navRef.current) {
        navRef.current.focus();
        navRef.current.blur();
      }
    }
  };
  return (
    <nav
      ref={navRef}
      tabIndex={-1}
      className="visually-hidden-focusable position-fixed top-0 start-0 border-primary bg-white rounded-4 mt-3 ms-3 p-4"
      style={{ zIndex: 1031, maxWidth: 360, border: "2px solid" }}
      onKeyDown={handleKeyPress}
    >
      <p className="fw-bold mb-2" style={{ fontSize: 14 }}>
        Passer à
      </p>
      <ul className="navbar-nav row-gap-2">
        <li className="nav-item">
          <button tabIndex={0} onFocus={() => setFocusIndex(0)} className="link-primary nav-link link-underline-opacity-0 p-0" onClick={skipToContent}>
            Contenu principal
          </button>
        </li>
      </ul>
      {isHomePage && (
        <>
          <hr />
          <p className="fw-bold mb-2" style={{ fontSize: 14 }}>
            Ressources
          </p>
          <ul className="navbar-nav row-gap-2">
            <li className="nav-item">
              <a tabIndex={-1} className="link-primary nav-link link-underline-opacity-0 p-0" href={process.env.REACT_APP_ATELIER_URL}>
                L'atelier des problèmes
              </a>
            </li>
            <li className="nav-item">
              <Link tabIndex={-1} className="link-primary nav-link link-underline-opacity-0 p-0" to={Routes.RITUEL}>
                Le rituel de problèmes
              </Link>
            </li>
            <li className="nav-item">
              <a tabIndex={-1} className="link-primary nav-link link-underline-opacity-0 p-0" href={process.env.REACT_APP_BANQUE_URL}>
                La banque de problèmes
              </a>
            </li>
          </ul>
          <div className="bg-primary-subtle mt-3 rounded-3 p-2 d-flex">
            <FontAwesomeIcon className="me-2 mt-1 text-secondary" icon={faCircleInfo} />
            <span className="" style={{ fontSize: 14 }}>
              Pour vous déplacer, utilisez les flèches vers le haut ou vers le bas de votre clavier.
            </span>
          </div>
        </>
      )}
    </nav>
  );
}

export default ShortcutMenu;
