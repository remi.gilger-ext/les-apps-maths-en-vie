import BaseLayout from "./BaseLayout";
import ChangePasswordForm from "./ChangePasswordForm";
import { Checkbox, CheckboxArray } from "./Checkbox";
import Dice from "./Dice";
import FakeProtectedRoute from "./FakeProtectedRoute";
import Footer from "./Footer";
import FormButtonLoading from "./FormButtonLoading";
import GrantedAdherentForm from "./GrantedAdherentForm";
import GrantedVipForm from "./GrantedVipForm";
import Header from "./Header";
import LayoutPage from "./LayoutPage";
import ProgrammationTypeForm from "./ProgrammationTypeForm";
import ProtectedRoute from "./ProtectedRoute";
import RituelParametersForm from "./RituelParametersForm";
import ShortcutMenu from "./ShortcutMenu";
import Tooltip from "./ToolTip";

// import { lazy } from "react";

// const Chart = lazy(() => import("./Chart"));

export {
  BaseLayout,
  ChangePasswordForm,
  Checkbox,
  CheckboxArray,
  Dice,
  FakeProtectedRoute,
  Footer,
  FormButtonLoading,
  GrantedAdherentForm,
  GrantedVipForm,
  Header,
  LayoutPage,
  ProgrammationTypeForm,
  ProtectedRoute,
  RituelParametersForm,
  ShortcutMenu,
  Tooltip,
};
