import { faDoorOpen, faGear, faHouse, faLightbulb, faRightFromBracket, faSpinner, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link, matchPath, NavLink, useLocation, useSearchParams } from "react-router-dom";
import { Routes } from "../routes";
import { useAuth } from "../contexts/auth";

type HeaderProps = {
  height: number;
};

function Header({ height }: Readonly<HeaderProps>) {
  const location = useLocation();
  const isRituelPage = matchPath({ path: Routes.RITUEL }, location.pathname);
  const isRituelHelpPage = matchPath({ path: Routes.HELP_RITUEL }, location.pathname);
  const isRituelPages = isRituelPage || isRituelHelpPage;

  const [searchParams] = useSearchParams();
  const rituelDisplayed = searchParams.has("displayed");

  return (
    <header className="bg-ternary fixed-top" style={{ height: height }}>
      <div id="navbar">
        <nav className="navbar navbar-expand navbar-dark bg-ternary d-flex justify-content-between">
          <div className="d-flex column-gap-4 justify-content-arround w-100" style={{ height: height }} id="navbarPrincipal">
            <ul className="navbar-nav me-auto">
              {!isRituelPages && (
                <li className="nav-item me-2">
                  <a href="https://www.mathsenvie.fr/" className="d-flex h-100 p-2">
                    <img className="align-self-center" src="img/logo_small.webp" alt="Site M@ths en-vie" style={{ maxHeight: 30 }} />
                  </a>
                </li>
              )}
              {isRituelPages && (
                <li className="nav-item">
                  <Link className="d-flex btn btn-ternary text-primary rounded-0 h-100" to={Routes.HOME} aria-label="Quitter l'application">
                    <FontAwesomeIcon className="bg-secondary p-2 rounded-5 align-self-center" icon={faDoorOpen} />
                  </Link>
                </li>
              )}
              {(!isRituelPages || rituelDisplayed) && (
                <li className="nav-item">
                  <Link className="d-flex btn btn-ternary text-primary rounded-0 h-100" to={rituelDisplayed ? Routes.RITUEL : Routes.HOME} aria-label="Page d'accueil">
                    <FontAwesomeIcon className="bg-secondary p-2 rounded-5 align-self-center" icon={faHouse} />
                  </Link>
                </li>
              )}
            </ul>
            {isRituelPages && (
              <div className="h-100 w-100 text-center justify-content-center d-flex ms-md-5 text-white">
                <div className="align-self-center d-lg-none d-flex flex-column me-2" style={{ height: "75%" }}>
                  <img className="h-50" src="img/header_rituel.webp" alt="Illustration le rituel de problèmes" />
                  <img className="h-50" src="img/header_problem.webp" alt="Illustration le rituel de problèmes" />
                </div>
                <img className="h-100 d-lg-block d-none" src="img/header_rituel.webp" alt="Illustration le rituel de problèmes" />
                <img className="h-100 d-lg-block d-none mx-lg-3" src="img/header_problem.webp" alt="Illustration le rituel de problèmes" />
                <img className="align-self-center d-sm-block d-none" style={{ height: "100%" }} src="img/header_mathsenvie.webp" alt="Illustration le rituel de problèmes" />
              </div>
            )}
            <ul className="navbar-nav ms-auto">
              {isRituelPages && (
                <li className="nav-item">
                  <Link className="d-flex btn btn-ternary text-primary rounded-0 h-100" to={Routes.HELP_RITUEL} aria-label="Aide">
                    <FontAwesomeIcon className="bg-secondary p-2 rounded-5 align-self-center" icon={faLightbulb} />
                  </Link>
                </li>
              )}
              <UserHeader />
            </ul>
          </div>
        </nav>
      </div>
    </header>
  );
}

function UserHeader() {
  const { user, logout, isInitialLoading } = useAuth();

  return (
    <>
      {isInitialLoading && <FontAwesomeIcon icon={faSpinner} spin size="lg" className="text-white mx-3" style={{ marginTop: 15 }} />}
      {user && (
        <li className="nav-item dropdown">
          <button className="d-flex btn btn-ternary text-primary rounded-0 h-100" data-bs-toggle="dropdown" aria-expanded="false" aria-label="Profile">
            <FontAwesomeIcon className="bg-secondary p-2 rounded-5 align-self-center" icon={faUser} />
          </button>
          <ul className="dropdown-menu dropdown-menu-end bg-ternary no-dropdown-hover">
            <p className="p-1 mb-0 text-center">{user.username}</p>
            <li>
              <hr className="dropdown-divider" />
            </li>
            <li>
              <NavLink className="dropdown-item" to={Routes.PROFIL_PARAMETERS}>
                <FontAwesomeIcon icon={faGear} /> Paramètres
              </NavLink>
            </li>
            <li>
              <hr className="dropdown-divider" />
            </li>
            <li>
              <button className="dropdown-item" id="logout" onClick={logout}>
                <FontAwesomeIcon icon={faRightFromBracket} /> Déconnexion
              </button>
            </li>
          </ul>
        </li>
      )}
      {!user && !isInitialLoading && (
        <>
          <li className="nav-item">
            <NavLink to={Routes.LOGIN} className="nav-link">
              Se&nbsp;connecter
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to={Routes.REGISTER} className="nav-link">
              S'inscrire
            </NavLink>
          </li>
        </>
      )}
    </>
  );
}

export default Header;
