import { faArrowUpRightFromSquare, faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import { Routes } from "../routes";

function HelpRituel() {
  return (
    <div className="my-0 mx-auto" style={{ maxWidth: 960 }}>
      <h1>Aide « Rituel de problèmes »</h1>

      <p className="mt-3">
        Ce module permet de proposer à vos élèves un rituel de problèmes s'appuyant sur une programmation et une progression sur les 5 périodes de l'année et ce, du CP au CM2. Cet entraînement vient
        en complément d'un enseignement explicite et structuré de la résolution de problèmes en lien avec une progression (progression personnelle, progression de{" "}
        <a href="https://www.mathsenvie.fr/la-methode" rel="noreferrer" target="_blank">
          la méthode M@ths en-vie <FontAwesomeIcon icon={faArrowUpRightFromSquare} size="xs" />
        </a>{" "}
        ou progression de{" "}
        <a href="https://tandem.nathan.fr/" rel="noreferrer" target="_blank">
          la méthode Tandem Maths <FontAwesomeIcon icon={faArrowUpRightFromSquare} size="xs" />
        </a>{" "}
        )
      </p>

      <div className="mt-5 d-flex w-100 text-center mt-3 column-gap-3 justify-content-around fw-bold" style={{ height: 220 }}>
        <div className="d-flex flex-column" style={{ maxWidth: "30%" }}>
          <span>Programmation personnelle</span>
          <img className="h-100 object-fit-contain" src="img/customProgrammation.webp" alt="Programmation personnelle" />
        </div>
        <div className="d-flex flex-column" style={{ maxWidth: "30%" }}>
          <span>Programmation Maths en-vie</span>
          <img className="h-100 object-fit-contain" src="img/mathsenvieProgrammation.webp" alt="Programmation Maths en-vie" />
        </div>
        <div className="d-flex flex-column" style={{ maxWidth: "30%" }}>
          <span>Programmation Tandem Maths</span>
          <img className="h-100 object-fit-contain" src="img/tandemProgrammation.webp" alt="Programmation Tandem Maths" />
        </div>
      </div>

      <p className="mt-3">Avant de commencer, paramétrez dans votre profil :</p>
      <ul>
        <li>votre zone de vacances afin que les problèmes du jour correspondent à la programmation par période,</li>
        <li>les niveaux des élèves de votre classe permettant d'afficher 1, 2 ou 3 problèmes par jour selon le nombre de niveaux,</li>
        <li>La programmation que vous suivez.</li>
      </ul>

      <p className="mt-3">Chaque jour, un problème par niveau est tiré au sort dans la banque de problèmes collaborative en fonction de la programmation choisie.</p>

      <p className="mt-3">Vous pouvez :</p>
      <ul>
        <li>afficher ou non le niveau des problèmes,</li>
        <li>afficher ou non la difficulté des problèmes (de 1 à 3 *, selon les niveaux choisis),</li>
        <li>afficher ou non le type de problème,</li>
        <li>faire un autre tirage parmi le type enseigné si le problème du jour ne vous convient pas.</li>
      </ul>

      <p className="mt-3">
        Cet outil ne vit que des contributions de ses utilisateurs. S'il vous est utile, vous pouvez contribuer à son fonctionnement en proposant à votre tour 3 énoncés de problèmes. Cela vous
        permettra alors d'accéder à l'intégralité de la banque pour éditer et vidéoprojeter les problèmes de votre choix !
      </p>
      <div className="text-center mt-3">
        <Link className="btn btn-primary p-2 m-1" to={Routes.CONTRIBUTE}>
          <FontAwesomeIcon icon={faPen} size="4x" />
          <h3 className="mt-2 fw-semibold">Contribuer</h3>
          <p>Partagez vos problèmes avec la communauté.</p>
        </Link>
      </div>
    </div>
  );
}

export default HelpRituel;
