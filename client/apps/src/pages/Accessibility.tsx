import { Link } from "react-router-dom";
import { Routes } from "../routes";

function Accessibility() {
  return (
    <div className="my-0 mx-auto" style={{ maxWidth: 960 }}>
      <h1 className="text-center">Accessibilité</h1>

      <p className="mt-3">
        Nous nous engageons à rendre accessible notre application à tous les utilisateurs, quelle que soit leur situation. Nous avons à cœur de rendre l'éducation inclusive, et c'est pourquoi nous
        améliorons continuellement notre plateforme pour la rendre accessible à tous les élèves, les enseignants et les parents, y compris ceux ayant des besoins spécifiques.
      </p>

      {/* Qu'est-ce que l'accessibilité numérique&nbsp;? =========================== */}

      <h2 className="mt-3">Qu'est-ce que l'accessibilité numérique&nbsp;?</h2>

      <p className="mt-3">
        L'accessibilité numérique consiste à rendre les contenus et services en ligne utilisables par tous, y compris les personnes en situation de handicap. Elle vise à garantir une égalité d'accès à
        l'information et aux services numériques grâce à diverses bonnes pratiques et réglementations.
      </p>

      <p className="mt-3">Elle vise notamment à&nbsp;:</p>
      <ul>
        <li>rendre l'information et les composants de l'interface accessibles aux sens (visuel, auditif, tactile)&nbsp;;</li>
        <li>permettre une navigation fluide et intuitive, notamment avec un clavier ou un lecteur d'écran&nbsp;;</li>
        <li>assurer une clarté des contenus et des interactions&nbsp;;</li>
        <li>garantir la compatibilité avec les technologies d'assistance et les différents supports numériques.</li>
      </ul>

      <p className="mt-3">
        L'accessibilité numérique est encadrée par le <a href="https://accessibilite.numerique.gouv.fr/">référentiel RGAA</a>.
      </p>

      <p className="mt-3">
        Conformément à la loi française, notamment l'article 47 de la loi n°2005-102 du 11 février 2005 pour l'égalité des droits et des chances, nous avons pris des mesures pour garantir
        l'accessibilité de notre contenu.
      </p>

      {/* Les mesures mises en oeuvre ========================================== */}

      <h2 className="mt-3">Les mesures mises en oeuvre</h2>

      <p className="mt-3">Nous avons conçu notre plateforme en tenant compte des recommandations d'accessibilité pour garantir une expérience utilisateur fluide et inclusive.</p>
      <ul className="mt-3">
        <li>Nous avons veillé à utiliser des contrastes de couleurs suffisamment élevés pour assurer une lisibilité optimale.</li>
        <li>Oralisation disponible pour tous les contenus (énoncés de problèmes, aides, correction) via un bouton intégré à l'application</li>
        <li>Utilisation de polices de caractères sans-serif avec des tailles adaptées&nbsp;; pas de polices fantaisies ou scriptes.</li>
        <li>Les textes ne sont pas justifiés. Ils sont alignés à gauche, avec parfois des éléments en gras (pas d'utilisation de l'italique ou du soulignement).</li>
        <li>Toutes les fonctionnalités de l'application peuvent être utilisées uniquement à l'aide du clavier.</li>
        <li>L'application est compatible avec les technologies d'assistance, telles que les lecteurs d'écran.</li>
        <li>Toutes les images, graphiques et éléments visuels importants disposent d'un texte alternatif pour aider les utilisateurs ayant une déficience visuelle.</li>
        <li>Une structure logique de navigation assure une utilisation simple de l'application pour les utilisateurs ayant des troubles cognitifs.</li>
        <li>L'interface est épurée, sans distracteurs, avec un nombre de couleurs limité.</li>
        <li>Mode plein écran disponible.</li>
      </ul>

      {/* Les contenus non accessibles ======================================== */}

      <h2 className="mt-3">Les contenus non accessibles</h2>

      <ul className="mt-3">
        <li>Les PDF ne sont pas accessibles car ils ont vocation à être imprimés et non lus en ligne.</li>
        <li>Les photos de la banque de photos mathématiques ne disposent pas de description.</li>
      </ul>

      {/* Droit au recours ==================================================== */}

      <h2 className="mt-3">Droit au recours</h2>

      <p className="mt-3">
        En vertu de l'article 11 de la loi de février 2005 : « la personne handicapée a droit à la compensation des conséquences de son handicap, quels que soient l'origine et la nature de sa
        déficience, son âge ou son mode de vie. »
      </p>

      <p className="mt-3">
        M@ths'n Co. s'engage à prendre les moyens nécessaires afin de donner accès, dans un délai raisonnable, aux informations et fonctionnalités recherchées par la personne handicapée, que le
        contenu fasse l'objet d'une dérogation ou non.
      </p>

      <p className="mt-3">
        Nous sommes à l'écoute de vos retours pour améliorer l'accessibilité du site. Si vous constatez un problème d'accessibilité qui vous empêche d'accéder à un contenu ou à une fonctionnalité,
        vous pouvez nous contacter afin qu'une assistance puisse être apportée (alternative accessible, information et contenu donnés sous une autre forme).
      </p>

      <p className="mt-3">
        Formulaire de contact&nbsp;: <Link to={{ pathname: Routes.CONTACT, search: "?type=accessibility" }}>cliquez ici</Link>{" "}
      </p>
    </div>
  );
}

export default Accessibility;
