import Accessibility from "./Accessibility";
import Cgu from "./Cgu";
import Contact from "./Contact";
import Credits from "./Credits";
import HelpRituel from "./HelpRituel";
import Home from "./Home";
import LegalNotices from "./LegalNotices";
import Login from "./Login";
import PasswordReset from "./PasswordReset";
import PasswordResetRequest from "./PasswordResetRequest";
import Privacy from "./Privacy";
import Register from "./Register";
import ResendVerifyEmail from "./ResendVerifyEmail";
import Rituel from "./Rituel";
import UnsubscribeEmails from "./UnsubscribeEmails";

export { Accessibility, Cgu, Contact, Credits, HelpRituel, Home, LegalNotices, Login, PasswordReset, PasswordResetRequest, Privacy, Register, ResendVerifyEmail, Rituel, UnsubscribeEmails };
