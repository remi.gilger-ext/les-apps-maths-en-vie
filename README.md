# Les apps M@ths en-vie

Une banque collaborative qui s'appuie sur une typologie avec des outils de recherche, d'édition et de vidéoprojection : https://banque.appenvie.fr

Un logiciel pour s'entraîner à résoudre des problèmes avec des aides tutorielles et un suivi individuel des élèves : https://atelier.appenvie.fr

En collaboration avec l'association M@ths-en-vie (https://www.mathsenvie.fr/association).

L´application se nourrit des problèmes de la banque et propose des problèmes selon une programmation établie et une typologie de problème bien définie.

https://appenvie.fr

Toute suggestion d'amélioration ou toute participation est la bienvenue.

L'application est développé en php avec le framework symfony pour le backend et react, scss, bootstrap pour le frontend.

## Installation

### Préprequis

Installer [docker](https://www.docker.com/), [composer](https://getcomposer.org/download/) et [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

### Docker initialization

À faire une seule fois

```bash
docker compose build --no-cache
```

Dans le dossier /server :
```bash
  composer install
  composer dump-autoload
```

Dans les dossier /client/apps /client/atelier /client/banque :
```bash
  npm install
```

## Usage

Pour lancer l'application :
```bash
docker compose up -d
```

Le site de présentation est disponible dans le navigateur à l'adresse http://localhost:3000
L'atelier est disponible dans le navigateur à l'adresse http://localhost:3001
La banque est disponible dans le navigateur à l'adresse http://localhost:3002

PhpMyAdmin pour la base de donnée est accesible à http://localhost:8080

Pour arrêter l'application :
```bash
docker compose down --remove-orphans
```

Pour générer une base de donnée de base (se mettre dans le container docker):
```bash
php -d memory_limit=-1 bin/console doctrine:fixtures:load
```

## Authors

TODO

## Contact

Rémi GILGER remi.gilger@mathsenvie.fr

M@ths en vie contact@mathsenvie.fr
