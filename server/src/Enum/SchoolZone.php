<?php

declare(strict_types=1);

namespace Shared\Enum;

enum SchoolZone: string
{
  case ZoneA = "zoneA";
  case ZoneB = "zoneB";
  case ZoneC = "zoneC";

  case Guadeloupe = "guadeloupe";
  case Martinique = "martinique";
  case Guyane = "guyane";
  case Réunion = "reunion";
  case Mayotte = "mayotte";
  case Corse = "corse";
  case SaintPierreEtMiquelon = "pierreEtMiquelon";
  case Polynésie = "polynesie";

  case Default = "default";

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getTimeZone(): string
  {
    return match ($this) {
      self::ZoneA, self::ZoneB, self::ZoneC, self::Corse => "Europe/Paris",
      self::Guadeloupe => "America/Guadeloupe",
      self::Martinique => "America/Martinique",
      self::Guyane => "America/Guyana",
      self::Réunion => "Indian/Reunion",
      self::Mayotte => "Indian/Mayotte",
      self::SaintPierreEtMiquelon => "America/Miquelon",
      self::Polynésie => "Pacific/Gambier",
      self::Default => "UTC"
    };
  }

  #================================================================#

  public function getDisplayName(): string
  {
    return match ($this) {
      self::ZoneA => "Zone A",
      self::ZoneB => "Zone B",
      self::ZoneC => "Zone C",
      self::Guadeloupe => "Guadeloupe",
      self::Martinique => "Martinique",
      self::Guyane => "Guyane",
      self::Réunion => "Réunion",
      self::Mayotte => "Mayotte",
      self::Corse => "Corse",
      self::SaintPierreEtMiquelon => "Saint Pierre et Miquelon",
      self::Polynésie => "Polynésie",
      self::Default => "Zone par défaut"
    };
  }
}
