<?php

declare(strict_types=1);

namespace Shared\Enum;

enum ProgrammationType: string
{
  case MathsEnVie = "mathsenvie";
  case Tandem = "tandem";
  case Custom = "custom";
}
