<?php

declare(strict_types=1);

namespace Shared\Enum;

enum UserTypeEnum: string
{
  case TEACHER = 'teacher';
  case PARENT = 'parent';
}
