<?php

declare(strict_types=1);

namespace Shared\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\Rituel;

/** @extends BaseRepository<Rituel> */
class RituelRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, Rituel::class);
  }
}
