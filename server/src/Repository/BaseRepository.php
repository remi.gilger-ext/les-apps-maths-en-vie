<?php

declare(strict_types=1);

namespace Shared\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

/** @template T of object */
abstract class BaseRepository
{
  /** @var ObjectRepository<T> */
  protected ObjectRepository $repository;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  /** @param class-string<T> $className */
  public function __construct(protected EntityManagerInterface $entityManager, string $className)
  {
    $this->repository = $this->entityManager->getRepository($className);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /** @param T $entity */
  public function add(object $entity, bool $flush = false): void
  {
    $this->entityManager->persist($entity);

    if ($flush) {
      $this->entityManager->flush();
    }
  }

  #================================================================#

  /** @param T $entity */
  public function remove(object $entity, bool $flush = false): void
  {
    $this->entityManager->remove($entity);

    if ($flush) {
      $this->entityManager->flush();
    }
  }

  #================================================================#

  /** @return ?T */
  public function find(int $id): ?object
  {
    return $this->repository->find($id);
  }

  #================================================================#

  /**
   * @param array<string, mixed> $criteria
   * @param array<string, 'asc'|'desc'|'ASC'|'DESC'>|null $orderBy
   *
   * @return T[]
   */
  public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
  {
    return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
  }

  #================================================================#

  /** @return array<int, T> */
  public function findAll(): array
  {
    return $this->repository->findAll();
  }

  #================================================================#

  /**
   * @param array<string, mixed> $criteria
   *
   * @return ?T
   */
  public function findOneBy(array $criteria): ?object
  {
    return $this->repository->findOneBy($criteria);
  }

  #================================================================#
  /**
   * Creates a new QueryBuilder instance that is prepopulated for this entity name.
   *
   * @param string      $alias
   * @param string|null $indexBy The index for the from.
   *
   * @return QueryBuilder
   */
  public function createQueryBuilder($alias, $indexBy = null)
  {
    return $this->entityManager->createQueryBuilder()
      ->select($alias)
      ->from($this->repository->getClassName(), $alias, $indexBy);
  }

  #================================================================#

  /** @param array<string, mixed> $criteria */
  public function count(array $criteria): int
  {
    return $this->entityManager->getUnitOfWork()->getEntityPersister($this->repository->getClassName())->count($criteria);
  }
}
