<?php

declare(strict_types=1);

namespace Shared\Repository;

use Banque\Entity\Problem;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;

/**
 * @extends BaseRepository<User>
 */
class UserRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, User::class);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /** @return array{nbProblem: int, username: string}[] */
  public function getTopUsers(int $limit): array
  {
    $qb = $this->createQueryBuilder('u');

    $qb->select('COUNT(u.id) as nbProblem, u.username')
      ->leftJoin('u.problems', 'p')
      ->where('p.tag IN (:tag)')
      ->andWhere('u.username NOT IN (:bannedUser)')
      ->setParameter('tag', [Problem::TAG_COMMUNITY_PUBLIC])
      ->setParameter('bannedUser', ['Eduscol', 'Administrateur', 'Maths_en_vie', 'MHM', '4RMC', 'SdM2024'])
      ->groupBy('u.id')
      ->orderBy('nbProblem', 'DESC')
      ->setMaxResults($limit);

    return $qb->getQuery()
      ->getResult();
  }

  #================================================================#

  /**
   * @return iterable<int, User>
   */
  public function userIterator(): iterable
  {
    $qb = $this->createQueryBuilder('u');

    return $qb->getQuery()->toIterable();
  }
}
