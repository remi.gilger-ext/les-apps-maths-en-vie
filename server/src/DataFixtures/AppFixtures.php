<?php

namespace Shared\DataFixtures;

use Banque\Entity\Problem;
use Banque\Factory\ProblemCommentFactory;
use Banque\Factory\ProblemFactory;
use Banque\Factory\ProblemLikeFactory;
use Banque\Service\GradeService;
use Shared\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private GradeService $gradeService
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function load(
    ObjectManager $manager
  ): void {
    ProblemCommentFactory::truncate();
    ProblemLikeFactory::truncate();
    ProblemFactory::truncate();
    UserFactory::truncate();

    UserFactory::createOne([
      'roles' => ['ROLE_SUPER_ADMIN'],
      'username' => 'Administrateur',
      'password' => 'admin',
      'email' => 'admin@admin.fr',
    ]);
    UserFactory::createOne([
      'roles' => ['ROLE_ADMIN'],
      'username' => 'Maths_en_vie',
      'password' => 'admin',
      'email' => 'mev@mev.fr',
      'programmation' => '{"cp":[]}'
    ]);
    UserFactory::createOne([
      'roles' => [],
      'username' => 'tandem',
      'password' => 'admin',
      'email' => 'tandem@tandem.fr',
      'programmation' => '{"cp":[]}'
    ]);
    UserFactory::createMany(20);

    ProblemFactory::new()->createMany(
      20,
      function () {
        return [
          'user' => UserFactory::find(['username' => 'Administrateur']),
          'tag' => Problem::TAG_SAMPLE
        ];
      }
    );

    ProblemFactory::new()->privateProblem()->createMany(
      100,
      function () {
        return [
          'user' => UserFactory::random(['isVerified' => 1])
        ];
      }
    );

    ProblemFactory::new()->verified()->publicProblem()->createMany(
      500,
      function () {
        return [
          'problemComments' => ProblemCommentFactory::new([
            'user' => UserFactory::random(['isVerified' => 1])
          ])->many(0, 10),
          'problemLikes' => ProblemLikeFactory::new([
            'user' => UserFactory::random(['isVerified' => 1])
          ])->many(0, 10),
          'user' => UserFactory::random(['isVerified' => 1]),
        ];
      }
    );

    foreach (UserFactory::all() as $user) {
      $this->gradeService->updateGrade($user->_real());
    }
  }
}
