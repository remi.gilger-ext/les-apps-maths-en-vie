<?php

declare(strict_types=1);

namespace Shared\Controller;

use Banque\Enum\DailyCounterTypeEnum;
use Banque\Service\DailyCounterService;
use Banque\Service\Log\UserLog;
use Banque\Service\MessageService;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Form\ContactModel;
use Shared\Form\RegistrationModel;
use Shared\Form\ResetPassword;
use Shared\Repository\UserRepository;
use Shared\Security\EmailVerifier;
use Shared\Service\Log\AuthLog;
use Shared\Service\Log\ErrorsLog;
use Shared\Service\MailerService;
use Shared\Service\ParseToArrayService;
use Shared\Service\ProgrammationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

#[Route('/api')]
class SecurityController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private AuthLog $authLog,
    private EntityManagerInterface $em,
    private ParseToArrayService $parseToArrayService,
    private RequestStack $requestStack,
    private MailerService $mailerService,
    private UserRepository $userRepository,
    private ValidatorInterface $validator
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/user', name: 'app.user', methods: 'GET')]
  /**
   * Gather data of the user
   *
   * @return Response
   */
  public function userData(): Response
  {
    /** @var ?User $user */
    $user = $this->getUser();

    if ($user === null) {
      return new JsonResponse();
    }

    return new JsonResponse($this->parseToArrayService->parseUser($user));
  }

  #================================================================#

  #[Route('/login', name: 'app.login', methods: 'PUT')]
  public function login(): void
  {
    throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
  }

  #================================================================#

  #[IsGranted('ROLE_USER')]
  #[Route('/logout', name: 'app.logout', methods: 'PUT')]
  public function logout(
    Security $security
  ): Response {
    $security->logout(false);
    $this->requestStack->getSession()->invalidate();

    return new JsonResponse([
      'logout' => 'Success'
    ], status: 200);
  }

  #================================================================#

  #[Route('/register', name: 'app.register', methods: 'POST')]
  public function register(
    AuthLog $authLog,
    Request $request,
    MessageService $messageService,
    UserPasswordHasherInterface $userPasswordHasher
  ): Response {
    $user = new User();

    $registrationValidator = new RegistrationModel();

    $registrationValidator->fillInstance($request->getPayload());
    $errors = $this->validator->validate($registrationValidator);

    if ($errors->count() === 0) {
      // encode the plain password
      $user->setPassword(
        $userPasswordHasher->hashPassword(
          $user,
          $registrationValidator->getPasswordFirst()
        )
      );
      $user->setEmail($registrationValidator->getEmail());
      $user->setUsername($registrationValidator->getUsername());
      $user->setAddressIp($request->getClientIp());
      $user->setType($registrationValidator->getUserType());

      $errors = $this->validator->validate($user);

      if ($errors->count() !== 0) {
        return new JsonResponse([
          'formErrors' => $registrationValidator->getFormatErrors($errors)
        ], status: 400);
      }

      $this->em->persist($user);
      $this->em->flush();

      $admin = $this->userRepository->findOneBy(['username' => 'Administrateur']);

      if ($admin === null) {
        throw new \LogicException("Can't find Administrateur user");
      }

      $messageService->sendMessage($admin, $user, 'Bienvenue !', 'Bienvenue dans la banque collaborative M@ths en-vie ! Pour accéder à la banque collaborative, rien de plus simple ! Proposez via le menu "Contribuer" 3 problèmes, photos ou photo-problèmes de votre création. Lorsqu\'ils seront validés par un administrateur, votre accès en intégralité à la banque sera activé.');

      # Log to the Auth file for the registration of the user
      $authLog->registerSuccess($user, $request->getClientIp());

      $this->mailerService->verifyEmail($user);

      return new JsonResponse([
        'message' => 'Un courriel vous a été envoyé pour confirmer votre adresse.'
      ], status: 200);
    }

    return new JsonResponse([
      'formErrors' => $registrationValidator->getFormatErrors($errors)
    ], status: 400);
  }

  #================================================================#

  #[Route('/verify-email', name: 'app.verifyEmail', methods: 'GET')]
  public function verifyUserEmail(
    DailyCounterService $dailyCounterService,
    EmailVerifier $emailVerifier,
    Request $request
  ): Response {
    $id = $request->query->getInt('id') ?: null;

    if ($id === null) {
      return $this->redirect($this->getParameter('app.apps_url') . '/login?variant=danger&message=Lien invalide.');
    }

    $user = $this->userRepository->find($id);
    if (!$user) {
      return $this->redirect($this->getParameter('app.apps_url') . '/login?variant=danger&message=Lien invalide.');
    }

    if ($user->isVerified()) {
      return $this->redirect($this->getParameter('app.apps_url') . '/login?variant=success&message=Votre email a déjà été verifiée, vous pouvez désormais vous connecter.');
    }

    // validate email confirmation link, sets User::isVerified=true and persists
    try {
      $emailVerifier->handleEmailConfirmation($request, $user);
    } catch (VerifyEmailExceptionInterface) {
      return $this->redirect($this->getParameter('app.apps_url') . '/login?variant=danger&message=Le lien pour vérifier votre email est incorrecte, vous pouvez, si besoin, renvoyer un email de confirmation.');
    }

    $this->mailerService->welcome($user);

    $this->authLog->registerConfirmation($user, $request->getClientIp());

    $dailyCounterService->add(DailyCounterTypeEnum::REGISTRATIONS);

    return $this->redirect($this->getParameter('app.apps_url') . '/login?variant=success&message=Votre adresse mail a été vérifiée, vous pouvez maintenant vous connecter.');
  }

  #================================================================#

  #[Route("/resend-verify-email", name: "app.resendVerifyEmail", methods: 'POST')]
  public function resendVerifyEmail(
    Request $request
  ): Response {
    // We get the email passed to the form
    $payload = $request->getPayload();
    $email = $payload->getString('email') ?: null;

    $user = $this->userRepository->findOneBy(['email' => $email]);

    if ($email === null || $user === null) {
      return new JsonResponse([
        'message' => 'Aucun compte n\'a été trouvé pour cette adresse mail.'
      ], status: 400);
    }

    if ($user->isVerified()) {
      return new JsonResponse([
        'message' => 'Votre compte a déjà été vérifié.',
        'variant' => 'info'
      ], status: 400);
    }

    // We send an email
    $this->mailerService->verifyEmail($user);

    $this->authLog->resendVerifyEmail($user->getUserIdentifier(), $request->getClientIp());

    return new JsonResponse([
      'message' => 'Email envoyé avec succès.'
    ], status: 200);
  }

  #================================================================#

  #[Route("/password-reset-request", name: "app.passwordResetRequest", methods: 'POST')]
  public function passwordResetRequest(
    Request $request,
    TokenGeneratorInterface $tokenGeneratorInterface
  ): Response {
    // We get the email passed to the form
    $payload = $request->getPayload();
    $email = $payload->getString('email');

    if ($email !== '') {
      // We get the user associated
      $user = $this->userRepository->findOneBy(['email' => $email]);

      if ($user !== null) {
        // We generate a reinitialisation token
        $token = $tokenGeneratorInterface->generateToken();

        $user->setResetToken($token);
        $user->setResetExpireDate(new \DateTime('+1 hours', new \DateTimeZone('UTC')));
        $this->em->flush();

        // We send an email
        $this->mailerService->resetPassword($user, $token);

        $this->authLog->passwordResetRequest($user->getUserIdentifier(), $request->getClientIp());

        return new JsonResponse([
          'message' => "Email envoyé avec succès à l'adresse {$email}."
        ], status: 200);
      }
    }

    return new JsonResponse([
      'message' => 'Aucun compte n\'a été trouvé pour cette adresse mail.'
    ], status: 400);
  }

  #================================================================#

  #[Route("/password-reset", name: "app.passwordReset", methods: 'POST')]
  public function passwordReset(
    Request $request,
    UserPasswordHasherInterface $userPasswordHasher,
  ): Response {
    // We change the password of the user
    $resetPasswordValidator = new ResetPassword();
    $resetPasswordValidator->fillInstance($request->getPayload());
    $errors = $this->validator->validate($resetPasswordValidator);

    if ($errors->count() === 0) {
      // We get the user from the token passed
      $user = $this->userRepository->findOneBy(['resetToken' => $resetPasswordValidator->getToken()]);

      if ($user && $user->getResetExpireDate() > new \DateTime('now', new \DateTimeZone('UTC'))) {
        $user->setResetToken(null);
        $user->setResetExpireDate(null);
        $user->setPassword(
          $userPasswordHasher->hashPassword(
            $user,
            $resetPasswordValidator->getNewPasswordFirst()
          )
        );

        $this->em->flush();

        $this->authLog->passwordReset($user->getUserIdentifier(), $request->getClientIp());

        return new JsonResponse([
          'message' => 'Votre mot de passe a bien été modifié, vous pouvez vous connecter avec votre nouveau mot de passe.'
        ], status: 200);
      }

      return new JsonResponse([
        'message' => 'Jeton invalide ou expiré, veuillez renvoyer un email de réinitialisation.'
      ], status: 400);
    }

    return new JsonResponse([
      'formErrors' => $resetPasswordValidator->getFormatErrors($errors)
    ], status: 400);
  }

  #================================================================#

  #[Route("/error-report", name: "app.errorReport", methods: 'POST')]
  public function errorReport(
    Request $request,
    ErrorsLog $errorsLog,
  ): Response {
    // TODO: Do something in case someone send too much request
    $errorsLog->reportError($request->getPayload()->getString('error', 'No error message was provided :('), $request->getPayload()->getString('stack', 'No stack trace was provided :('));

    return new JsonResponse(status: 200);
  }

  #================================================================#

  #[Route('/unsubscribe-emails', name: 'app.unsubscribeEmails', methods: 'PUT')]
  public function unsubscribeEmails(
    UserLog $userLog,
    Request $request
  ): Response {
    $hmacSecret = $this->getParameter('hmacSecret');

    if (empty($hmacSecret)) {
      throw new \Exception("The HMAC secret need to be defined", 1);
    }

    $payload = $request->getPayload();
    $id = $payload->getInt('id');
    $hashProvided = $payload->getString('token');
    $reSubscribe = $payload->getBoolean('reSubscribe');

    $user = $this->userRepository->find($id);
    if (!$user) {
      return new JsonResponse([
        'message' => 'Lien invalide, si vous avez copiez le lien assurez-vous que l\'url a été copiée en entier.',
        'variant' => 'warning'
      ], status: 400);
    }

    $hashNeeded = \hash_hmac('sha256', (string) $id, $hmacSecret);

    if (!\hash_equals($hashNeeded, $hashProvided)) {
      return new JsonResponse([
        'message' => 'Lien invalide, si vous avez copiez le lien assurez-vous que l\'url a été copiée en entier.',
        'variant' => 'warning'
      ], status: 400);
    }

    if ($reSubscribe) {
      $user->setIsEmailsSubscribed(true);
      $this->em->flush();

      $userLog->subscribedToEmails($user);

      return new JsonResponse([
        'message' => 'Nous vous confirmons votre réinscription.',
        'variant' => 'info',
        'isUnsubscribe' => false
      ]);
    }

    if (!$user->isEmailsSubscribed()) {
      return new JsonResponse([
        'message' => 'Vous êtes déjà désinscrits.',
        'variant' => 'info',
        'isUnsubscribe' => true
      ]);
    }

    $user->setIsEmailsSubscribed(false);
    $this->em->flush();

    $userLog->unsubscribedToEmails($user);

    return new JsonResponse([
      'message' => 'Nous vous confirmons votre désinscription.',
      'isUnsubscribe' => true
    ]);
  }

  #================================================================#

  #[Route('/contact', name: 'app.contact', methods: 'POST')]
  public function contact(
    Request $request,
  ): JsonResponse {
    $contactValidator = new ContactModel();

    $contactValidator->fillInstance($request->getPayload());
    $errors = $this->validator->validate($contactValidator);

    if ($errors->count() > 0) {
      return new JsonResponse([
        'formErrors' => $contactValidator->getFormatErrors($errors)
      ], status: 400);
    }

    $this->mailerService->contact(
      $contactValidator->getFirstName() . " " . $contactValidator->getLastName(),
      $contactValidator->getEmail(),
      $contactValidator->getSubject(),
      $contactValidator->getContent(),
      $contactValidator->getDemandType()
    );

    return new JsonResponse([
      'message' => 'Votre demande à bien envoyé.',
    ]);
  }
}
