<?php

declare(strict_types=1);

namespace Shared\Controller;

use Banque\Service\Log\UserLog;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Enum\ProgrammationType;
use Shared\Form\ChangePassword;
use Shared\Form\RituelParameters;
use Shared\Repository\UserRepository;
use Shared\Service\Log\AuthLog;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/profil')]
#[IsGranted('ROLE_USER')]
class ProfilController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private EntityManagerInterface $em,
    private UserRepository $userRepository,
    private UserLog $userLog
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/change-password', name: 'profil.changePassword', methods: 'PUT')]
  public function changePassword(
    AuthLog $authLog,
    Request $request,
    UserPasswordHasherInterface $userPasswordHasher,
    ValidatorInterface $validator
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    $changePasswordValidator = new ChangePassword();

    $changePasswordValidator->fillInstance($request->getPayload());
    $errors = $validator->validate($changePasswordValidator);

    if ($errors->count() === 0) {
      $user->setPassword(
        $userPasswordHasher->hashPassword(
          $user,
          $changePasswordValidator->getNewPasswordFirst()
        )
      );
      $this->em->flush();

      $authLog->passwordChanged($user->getUserIdentifier(), $request->getClientIp());

      return new JsonResponse([
        'message' => 'Votre mot de passe a bien été changé.'
      ], status: 200);
    }

    return new JsonResponse([
      'formErrors' => $changePasswordValidator->getFormatErrors($errors)
    ], status: 400);
  }

  #================================================================#

  #[Route('/become-adherent', name: 'profil.becomeAdherent', methods: 'PUT')]
  public function becomeAdherent(
    Request $request,
  ): Response {
    /** @var User*/
    $user = $this->getUser();

    if (!$this->isGranted('ROLE_ADHERENT')) {
      $code = $request->getPayload()->getString('codeAdherent');

      if ($code === $this->getParameter('app.passwordAdherent')) {
        $roles = array_diff($user->getRoles(), ['ROLE_USER']);
        $roles[] = 'ROLE_ADHERENT';
        $user->setRoles($roles);

        $this->userLog->granted($user, 'ROLE_ADHERENT');

        $this->em->flush();

        return new JsonResponse([
          'message' => 'Vous êtez maintenant reconnu comme adhérent, vous allez être automatiquement déconnecté, merci de vous reconnecter.'
        ], status: 200);
      }

      return new JsonResponse([
        'message' => 'Le code que vous avez saisi est incorrect.'
      ], status: 400);
    }

    return new JsonResponse([
      'message' => 'Vous êtes déja authentifié comme adhérent.'
    ], status: 400);
  }

  #================================================================#

  #[Route('/become-vip', name: 'profil.becomeVip', methods: 'PUT')]
  public function becomeVip(
    Request $request,
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    if (!$this->isGranted('ROLE_VIP')) {
      $code = $request->getPayload()->getString('codeVip');

      if ($code === $this->getParameter('app.passwordVip') || $code === $this->getParameter('app.passwordVip2')) {
        $roles = array_diff($user->getRoles(), ['ROLE_USER']);
        $roles[] = 'ROLE_VIP';
        $user->setRoles($roles);

        $this->userLog->granted($user, 'ROLE_VIP');

        $this->em->flush();

        return new JsonResponse([
          'message' => 'Vous êtez maintenant reconnu comme VIP, vous allez être automatiquement déconnecté, merci de vous reconnecter.'
        ], status: 200);
      }

      return new JsonResponse([
        'message' => 'Le code que vous avez saisi est incorrect.'
      ], status: 400);
    }

    return new JsonResponse([
      'message' => 'Vous êtes déja authentifié comme VIP.'
    ], status: 400);
  }

  #================================================================#

  #[Route('/delete-account', name: 'profil.deleteAccount', methods: 'PUT')]
  public function deleteAccount(
    AuthLog $authLog,
    Request $request,
    UserPasswordHasherInterface $passwordHasher
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    $plainPassword = $request->getPayload()->getString('password');

    if ($passwordHasher->isPasswordValid($user, $plainPassword)) {
      // Allow to not create a 500 error
      $this->container->get('security.token_storage')->setToken(null);

      $this->userRepository->remove($user, true);

      $authLog->removeAccount($user->getUserIdentifier(), $request->getClientIp());

      return new JsonResponse([
        'message' => 'Votre compte a été définitivement supprimé, vous allez être automatiquement déconnecté.'
      ], status: 200);
    }

    return new JsonResponse([
      'message' => 'Votre mot de passe est incorrect.',
      'variant' => 'danger'
    ], status: 400);
  }

  #================================================================#

  #[Route('/update-rituel-parameters', name: 'profil.updateRituelParameters', methods: 'PUT')]
  public function updateRituelParameters(
    Request $request,
    ValidatorInterface $validator
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    $rituelParametersValidator = new RituelParameters();

    $rituelParametersValidator->fillInstance($request->getPayload());
    $errors = $validator->validate($rituelParametersValidator);

    if ($errors->count() === 0) {
      $user->setSchoolZone($rituelParametersValidator->getSchoolZone());
      $user->setClassNivel($rituelParametersValidator->getClassNivel());

      $this->em->flush();

      return new JsonResponse([
        'message' => 'Les informations ont été misent à jour.',
        'parameters' => [
          'schoolZone' => $user->getSchoolZone(),
          'classNivel' => $user->getClassNivel()
        ]
      ], status: 200);
    }

    return new JsonResponse([
      'formErrors' => $rituelParametersValidator->getFormatErrors($errors)
    ], status: 400);
  }

  #================================================================#

  #[Route('/update-programmation-type', name: 'profil.updateProgrammationType', methods: 'PUT')]
  public function updateProgrammationType(
    Request $request
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    $programmationType = $request->getPayload()->getEnum('programmationType', ProgrammationType::class, ProgrammationType::MathsEnVie);

    if (
      !($this->isGranted('ROLE_ADHERENT') || $this->isGranted('ROLE_VIP')) &&
      $programmationType === ProgrammationType::Custom
    ) {
      return new JsonResponse([
        'message' => 'Vous devez être adhérent pour pouvoir modifier la programmation'
      ], 400);
    }

    $user->setProgrammationType($programmationType);
    $this->em->flush();

    return new JsonResponse([
      'programmation' => \json_decode($user->getProgrammation(), true),
      'programmationType' => $programmationType
    ]);
  }
}
