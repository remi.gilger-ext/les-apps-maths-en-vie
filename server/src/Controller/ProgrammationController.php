<?php

declare(strict_types=1);

namespace Shared\Controller;

use Atelier\Enum\SchoolNivel;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Enum\ProgrammationType;
use Shared\Form\ProgrammationModel;
use Shared\Service\ProgrammationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/programmation')]
class ProgrammationController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private EntityManagerInterface $em,
    private ProgrammationService $programmationService
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/update/{nivel}/{period}', name: 'programmation.update', methods: 'PUT')]
  public function update(
    Request $request,
    ValidatorInterface $validator,
    SchoolNivel $nivel,
    string $period
  ): Response {
    if (!$this->isGranted('ROLE_ADHERENT') && !$this->isGranted('ROLE_VIP')) {
      return new Response(status: 403);
    }
    if (!\in_array($period, ["1", "1.5", "2", "2.5", "3", "3.5", "4", "4.5", "5", "5.5"])) {
      return new Response(status: 400);
    }

    /** @var User */
    $user = $this->getUser();

    if ($user->getProgrammationType() !== ProgrammationType::Custom) {
      return new Response(status: 400);
    }

    $programmationValidator = new ProgrammationModel();

    $programmationValidator->fillInstance($request->getPayload());
    $errors = $validator->validate($programmationValidator);

    if ($errors->count() > 0) {
      return new JsonResponse([
        'formErrors' => $programmationValidator->getFormatErrors($errors)
      ], status: 400);
    }

    $newProg = $this->programmationService->updateFilters($nivel, $period, $user, $programmationValidator);
    $user->setProgrammation($newProg);
    $this->em->flush();

    return new JsonResponse([
      'message' => "La programmation a bien été enregistré.",
      'newProgrammation' => \json_decode($user->getProgrammation(), true)
    ]);
  }

  #================================================================#

  #[Route('/reset/{nivel}', name: 'programmation.reset', methods: 'PUT')]
  public function reset(
    SchoolNivel $nivel
  ): Response {
    if (!$this->isGranted('ROLE_ADHERENT') && !$this->isGranted('ROLE_VIP')) {
      return new Response(status: 403);
    }
    /** @var User */
    $user = $this->getUser();

    $newProg = $this->programmationService->resetFilters($nivel, $user);

    $user->setProgrammation($newProg);
    $this->em->flush();

    return new JsonResponse([
      'newProgrammation' => \json_decode($newProg, true)
    ]);
  }
}
