<?php

declare(strict_types=1);

namespace Shared\Controller;

use Atelier\Enum\SchoolNivel;
use Banque\Entity\Problem;
use Banque\Entity\TextProblem;
use Banque\Enum\DailyCounterTypeEnum;
use Banque\Enum\ProblemTypeEnum;
use Banque\Enum\TextProblemTypeEnum;
use Banque\Repository\ProblemRepository;
use Banque\Service\DailyCounterService;
use Banque\Service\ParseToArrayService;
use Banque\Service\RightsManagement;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\Rituel;
use Shared\Entity\User;
use Shared\Enum\ProgrammationType;
use Shared\Enum\SchoolZone;
use Shared\Repository\RituelRepository;
use Shared\Service\ProgrammationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/rituel')]
class RituelController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private EntityManagerInterface $em,
    private ProblemRepository $problemRepository,
    private ProgrammationService $programmationService
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/{schoolZone}/{classNivel}', name: 'rituel.get', methods: 'GET')]
  public function rituel(
    ParseToArrayService $parseToArrayService,
    RightsManagement $rightsManagement,
    RituelRepository $rituelRepository,
    DailyCounterService $dailyCounterService,
    SchoolZone $schoolZone,
    string $classNivel
  ): Response {
    if ($this->programmationService->getPeriod($schoolZone) === null) {
      return new JsonResponse([
        'message' => 'Aujourd\'hui, aucun problème car il n\'y a pas d\'école.',
        'variant' => 'success'
      ], 400);
    }

    /** @var ?User*/
    $user = $this->getUser();

    $actualDate = new \DateTime('now', new \DateTimeZone($schoolZone->getTimeZone()));

    $actualDate = $actualDate->format("Y/m/d");

    $problems = [];
    $classNivel = \explode(";", $classNivel);
    foreach ($classNivel as $nivel) {
      if (($nivel = SchoolNivel::tryFrom($nivel)) === null) {
        return new Response(status: 400);
      }

      if ($user) {
        $programmationType = $user->getProgrammationType();
      } else {
        $programmationType = ProgrammationType::MathsEnVie;
      }

      // If the programmation type is not custom we search for a problem in the database
      if ($programmationType !== ProgrammationType::Custom) {
        $rituel = $rituelRepository->findOneBy([
          "day" => $actualDate,
          "nivel" => $nivel->value,
          "schoolZone" => $schoolZone->value,
          "programmationType" => $programmationType
        ]);
      } else {
        $rituel = null;
      }

      if ($rituel === null) {
        $problem = $this->createRituelProblem($nivel, $schoolZone);

        if ($problem === null) {
          return new JsonResponse([
            'message' => 'Aucun problème n\'a pu être trouvé, veuillez vérifier que votre programmation n\'est pas trop restrictif.',
            'variant' => 'success'
          ], 400);
        }
      } else {
        $problem = $rituel->getProblem();
      }

      $problems[] = $parseToArrayService->parseProblem($problem);
    }

    $dailyCounterService->add(DailyCounterTypeEnum::RITUEL_CLICKS);

    return new JsonResponse([
      "problems" => $problems,
      "canSeePublicProblems" => !$rightsManagement->isRestrictedForPublicProblem(ProblemTypeEnum::TextProblem)
    ]);
  }

  #================================================================#

  #[Route('/refetch/{schoolZone}/{nivel}', name: 'rituel.refetch', methods: 'GET')]
  public function rituelProblemNivel(
    ParseToArrayService $parseToArrayService,
    SchoolZone $schoolZone,
    SchoolNivel $nivel
  ): Response {
    $problem = $this->createRituelProblem($nivel, $schoolZone, false);

    $problem = $parseToArrayService->parseProblem($problem);

    return new JsonResponse([
      "problem" => $problem,
    ]);
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  private function createRituelProblem(SchoolNivel $nivel, SchoolZone $zone, bool $createInDdb = true): ?TextProblem
  {
    /** @var ?User*/
    $user = $this->getUser();

    $filters = $this->programmationService->getFilters($zone, $nivel, $user);

    if ($filters === null) {
      return null;
    }

    $paramQuery = [
      [
        "p.tag" => [Problem::TAG_COMMUNITY_PUBLIC, Problem::TAG_SAMPLE],
        "p.type" => $filters[ProgrammationService::FILTER_TYPE] ?? TextProblemTypeEnum::casesName(),
        "p.nivel" => [$nivel->value]
      ],
      []
    ];

    $allProblems = $this->problemRepository->getProblemsQuery(TextProblem::class, $paramQuery, false);

    $allProblems = $this->programmationService->filtersProblems($allProblems, $filters);

    \srand(\time());

    if (empty($allProblems)) {
      return null;
    }

    $problem = $allProblems[array_rand($allProblems)];

    if (!$createInDdb) {
      return $problem;
    }

    // If it's a custom programmation the problem is not save in the ddb
    if ($user && $user->getProgrammationType() === ProgrammationType::Custom) {
      return $problem;
    }

    if ($user) {
      $programmationType = $user->getProgrammationType();
    } else {
      $programmationType = ProgrammationType::MathsEnVie;
    }

    $rituel = new Rituel();
    $rituel->setDay((new \DateTime('now', new \DateTimeZone($zone->getTimeZone())))->format("Y/m/d"));
    $rituel->setNivel($nivel);
    $rituel->setSchoolZone($zone);
    $rituel->setProgrammationType($programmationType);
    $rituel->setProblem($problem);

    $this->em->persist($rituel);
    $this->em->flush();

    return $problem;
  }
}
