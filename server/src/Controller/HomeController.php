<?php

declare(strict_types=1);

namespace Shared\Controller;

use Banque\Entity\Problem;
use Banque\Repository\ProblemRepository;
use Shared\Entity\User;
use Shared\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api')]
class HomeController extends AbstractController
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/stats', name: 'home.stats', methods: 'GET')]
  public function stats(
    ProblemRepository $problemRepository,
    UserRepository $userRepository
  ): Response {
    /** @var ?User $user */
    $user = $this->getUser();

    $userRegisteredAmout = $userRepository->count([
      'isVerified' => true
    ]);

    if (!$user) {
      return new JsonResponse([
        'userRegisteredAmout' => $userRegisteredAmout,
        'contributions' => '...',
        'problemsResolvedAmouts' => '...',
        'zone' => '...',
      ]);
    }

    $contributionsAmount = $problemRepository->count([
      'user' => $user,
      'tag' => Problem::TAG_COMMUNITY_PUBLIC
    ]);

    // TODO: replace this by a single database request
    $gamesAmout = 0;
    foreach ($user->getStudents() as $student) {
      $gamesAmout += \count($student->getGames());
    }
    $problemsResolvedAmouts = $gamesAmout * 5;

    return new JsonResponse([
      'userRegisteredAmout' => $userRegisteredAmout,
      'contributions' => $contributionsAmount,
      'problemsResolvedAmouts' => $problemsResolvedAmouts,
      'zone' => $user->getSchoolZone()?->getDisplayName() ?? "Non défini",
    ]);
  }
}
