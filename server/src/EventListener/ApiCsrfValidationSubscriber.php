<?php

declare(strict_types=1);

namespace Shared\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class ApiCsrfValidationSubscriber
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function onKernelRequest(RequestEvent $event): void
  {
    $request = $event->getRequest();

    // no validation needed on safe methods
    if ($request->isMethodSafe()) {
      return;
    }

    if (!($request->headers->has('X-CSRF-PROTECTION') && $request->headers->get('X-CSRF-PROTECTION') === '1')) {
      $event->setResponse(new JsonResponse([
        'message' => 'Missing CSRF Protection Header'
      ], 401));
      $event->stopPropagation();

      return;
    }

    if ($request->getMethod() === "POST" && $request->headers->get('Content-Type') !== 'application/json') {
      $event->setResponse(new JsonResponse([
        'message' => 'Invalid Content-Type'
      ], 415));
      $event->stopPropagation();
    }
  }
}
