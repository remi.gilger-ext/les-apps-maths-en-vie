<?php

declare(strict_types=1);

namespace Shared\EventListener;

use Banque\Entity\Problem;
use Banque\Repository\ProblemRepository;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PostLoadEventArgs;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;
use Shared\Entity\User;
use Shared\Repository\UserRepository;
use Shared\Service\ProgrammationService;

#[AsEntityListener(event: Events::preRemove, method: 'preRemove', entity: User::class)]
#[AsEntityListener(event: Events::postLoad, method: 'postLoad', entity: User::class)]
class UserNotifier
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private ProblemRepository $problemRepository,
    private UserRepository $userRepository,
    private ProgrammationService $programmationService
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function preRemove(User $user, PreRemoveEventArgs $event): void
  {
    $mathsenvie = $this->userRepository->findOneBy([
      'username' => 'Maths_en_vie'
    ]);

    if ($mathsenvie === null) {
      $mathsenvie = $this->userRepository->findOneBy([]);
    }

    if ($mathsenvie === null) {
      throw new \RuntimeException('No user was found');
    }

    foreach ($user->getProblems() as $problem) {
      if ($problem->getTag() === Problem::TAG_COMMUNITY_PRIVATE) {
        $this->problemRepository->remove($problem, true);
      } else {
        $problem->setUser($mathsenvie);
      }
    }
  }

  #================================================================#

  public function postLoad(User $user, PostLoadEventArgs $event): void
  {
    $user->setMathsenvieProgrammation($this->programmationService->getMathsenvieProgrammation());
    $user->setTandemProgrammation($this->programmationService->getTandemProgrammation());
  }
}
