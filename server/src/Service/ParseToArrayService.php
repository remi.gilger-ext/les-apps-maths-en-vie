<?php

declare(strict_types=1);

namespace Shared\Service;

use Shared\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

class ParseToArrayService
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private Security $security
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Parse a problem to an array
   *
   * @return array<string, mixed>
   */
  public function parseUser(User $user): array
  {
    $isSuperAdmin = false;
    $isAdmin = false;
    $isAdherent = false;
    $isVip = false;

    if ($this->security->isGranted("ROLE_SUPER_ADMIN")) {
      $isSuperAdmin = true;
    }
    if ($this->security->isGranted("ROLE_ADMIN")) {
      $isAdmin = true;
    }
    if ($this->security->isGranted("ROLE_VIP")) {
      $isVip = true;
    }
    if ($this->security->isGranted("ROLE_ADHERENT")) {
      $isAdherent = true;
    }

    return [
      "username" => $user->getUsername(),
      "email" => $user->getEmail(),
      "grade" => $user->getGrade(),
      "schoolZone" => $user->getSchoolZone(),
      "classNivel" => $user->getClassNivel(),
      "isEmailsSubscribed" => $user->isEmailsSubscribed(),
      "programmationType" => $user->getProgrammationType(),
      "programmation" => \json_decode($user->getProgrammation(), true),
      "isSuperAdmin" => $isSuperAdmin,
      "isAdmin" => $isAdmin,
      "isAdherent" => $isAdherent,
      "isVip" => $isVip,
      "userType" => $user->getType(),
    ];
  }
}
