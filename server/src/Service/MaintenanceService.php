<?php

declare(strict_types=1);

namespace Shared\Service;

use Shared\Kernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Twig\Environment;

class MaintenanceService
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private string $lockFilePath,
    private string $autorizedIp,
    private Environment $twig
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function onKernelRequest(RequestEvent $event): void
  {
    if (!file_exists($this->lockFilePath)) {
      return;
    }

    $pathInfo = $event->getRequest()->getPathInfo();

    if ($event->getRequest()->getClientIp() === $this->autorizedIp && $pathInfo !== "/maintenance") {
      return;
    }

    if ($pathInfo === "/maintenance" && $event->getRequest()->getClientIp() === $this->autorizedIp) {
      $action = $event->getRequest()->query->get("action");

      $input = new ArrayInput([]);

      if ($action === "cache:clear") {
        $input = new ArrayInput([
          'command' => 'cache:clear'
        ]);
      }
      if ($action === "doctrine:schema:update") {
        $input = new ArrayInput([
          'command' => 'doctrine:schema:update',
          '--dump-sql' => true,
          '--complete' => true
        ]);
      }
      if ($action === "doctrine:schema:update:force") {
        $input = new ArrayInput([
          'command' => 'doctrine:schema:update',
          '--force' => true,
          '--complete' => true
        ]);
      }

      $kernel = new Kernel('prod', false);

      $application = new Application($kernel);
      $application->setAutoExit(false);

      $output = new BufferedOutput();

      $application->run($input, $output);

      $event->setResponse(
        new Response('<pre>' . $output->fetch() . '</pre>')
      );
      $event->stopPropagation();
    } else {
      $page = $this->twig->render('ressources/maintenance.html.twig');

      $event->setResponse(
        new Response(
          $page,
          Response::HTTP_SERVICE_UNAVAILABLE
        )
      );
      $event->stopPropagation();
    }
  }
}
