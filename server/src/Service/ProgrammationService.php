<?php

declare(strict_types=1);

namespace Shared\Service;

use Atelier\Enum\SchoolNivel;
use Banque\Entity\TextProblem;
use Banque\Enum\ResponseStateEnum;
use Banque\Enum\TextProblemTypeEnum;
use Shared\Form\ProgrammationModel;
use Shared\Entity\User;
use Shared\Enum\SchoolZone;
use Shared\Repository\UserRepository;

class ProgrammationService
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  // All numbers are used as striclty upper bound
  public const FILTER_TYPE = "f1";
  public const FILTER_MAX_NUMBER = "f2";
  public const FILTER_CARACTERS_MAX = "f3";
  public const FILTER_STEP_PROBLEM_MAX_NUMBERS = "f4";
  public const FILTER_ALLOW_DECIMALS = "f5";

  public string $mathsenvieProgrammation;
  public string $tandemProgrammation;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private string $schoolZoneDatesFilePath,
    private string $environnement,
    private UserRepository $userRepository
  ) {
    $mathsenvie = $this->userRepository->findOneBy([
      'username' => 'Maths_en_vie'
    ]);

    $tandem = $this->userRepository->findOneBy([
      'username' => 'tandem'
    ]);

    if ($mathsenvie === null) {
      throw new \RuntimeException("User Maths_en_vie doesn't exist");
    }

    if ($tandem === null) {
      throw new \RuntimeException("User tandem doesn't exist");
    }

    $mathsenvieProgrammation = $mathsenvie->getProgrammation(true);
    $tandemProgrammation = $tandem->getProgrammation(true);

    if ($mathsenvieProgrammation === null) {
      throw new \RuntimeException("User Maths_en_vie doesn't have a programmation");
    }

    if ($tandemProgrammation === null) {
      throw new \RuntimeException("User tandem doesn't have a programmation");
    }

    $this->mathsenvieProgrammation = $mathsenvieProgrammation;
    $this->tandemProgrammation = $tandemProgrammation;
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getMathsenvieProgrammation(): string
  {
    return $this->mathsenvieProgrammation;
  }

  #================================================================#

  public function getTandemProgrammation(): string
  {
    return $this->tandemProgrammation;
  }

  #================================================================#

  /**
   * Return null when not in a period
   * @return mixed[]|null
   */
  public function getFilters(SchoolZone $zone, SchoolNivel $nivel, ?User $user = null): ?array
  {
    if ($this->environnement === 'dev') {
      return [];
    }

    $period = $this->getPeriod($zone, true);

    if ($period === null) {
      return null;
    }

    if (!$user) {
      $programmation = \json_decode($this->mathsenvieProgrammation, true);
    } else {
      $programmation = \json_decode($user->getProgrammation(), true);
    }

    if (!isset($programmation[$nivel->value])) {
      return [];
    }

    if (!isset($programmation[$nivel->value][$period])) {
      $period = (string) \intval($period);
    }

    if (!isset($programmation[$nivel->value][$period])) {
      return [];
    }

    return $programmation[$nivel->value][$period];
  }

  #================================================================#

  public function updateFilters(SchoolNivel $nivel, string $period, User $user, ProgrammationModel $model): string
  {
    $programmation = \json_decode($user->getProgrammation(), true);

    $filters = [];

    if (!empty($model->getF1())) {
      $filters[self::FILTER_TYPE] = $model->getF1();
    }

    if ($model->getF2() !== null) {
      $filters[self::FILTER_MAX_NUMBER] = $model->getF2();
    }

    if ($model->getF3() !== null) {
      $filters[self::FILTER_CARACTERS_MAX] = $model->getF3();
    }

    if ($model->getF4() !== null) {
      $filters[self::FILTER_STEP_PROBLEM_MAX_NUMBERS] = $model->getF4();
    }

    $filters[self::FILTER_ALLOW_DECIMALS] = $model->getF5();

    $programmation[$nivel->value][$period] = $filters;

    return \json_encode($programmation);
  }

  #================================================================#

  public function resetFilters(SchoolNivel $nivel, User $user): string
  {
    $programmation = \json_decode($user->getProgrammation(), true);

    $programmation[$nivel->value] = [];

    return \json_encode($programmation);
  }

  #================================================================#

  /**
   * Filters problems based on conditions
   *
   * @param TextProblem[] $problems
   * @param mixed[] $filters
   *
   * @return TextProblem[]
   */
  public function filtersProblems(array $problems, array $filters): array
  {
    return \array_filter($problems, function (TextProblem $problem) use ($filters) {
      if (isset($filters[self::FILTER_CARACTERS_MAX])) {
        $caracterMax = $filters[self::FILTER_CARACTERS_MAX];

        if (\mb_strlen($problem->getStatement()) > $caracterMax) {
          return false;
        }
      }

      if (isset($filters[self::FILTER_MAX_NUMBER])) {
        \preg_match_all('/\b\d+[,.]?\d*\b/', $problem->getStatement(), $matches);

        $maxNumber = $filters[self::FILTER_MAX_NUMBER];
        foreach ($matches[0] as $number) {
          $number = \doubleval($number);

          if ($number > $maxNumber) {
            return false;
          }
        }
      }

      if (
        isset($filters[self::FILTER_STEP_PROBLEM_MAX_NUMBERS]) && (
          $problem->getType() === TextProblemTypeEnum::add->name ||
          $problem->getType() === TextProblemTypeEnum::mult->name ||
          $problem->getType() === TextProblemTypeEnum::mixed->name)
      ) {
        \preg_match_all('/\b\d+[,.]?\d*\b/', $problem->getStatement(), $matches);

        $maxNumbers = $filters[self::FILTER_STEP_PROBLEM_MAX_NUMBERS];

        if (\count($matches[0]) > $maxNumbers) {
          return false;
        }
      }

      if (isset($filters[self::FILTER_ALLOW_DECIMALS]) && $filters[self::FILTER_ALLOW_DECIMALS] !== true) {
        \preg_match_all('/\b\d+[,.]\d*\b/', $problem->getStatement(), $matchesStatement);

        $exclusif = $filters[self::FILTER_ALLOW_DECIMALS] === 'exclusif';

        if ($problem->getResponse() !== null && $problem->getResponseState() !== ResponseStateEnum::Conflict) {
          \preg_match_all('/\b\d+[,.]\d*\b/', $problem->getResponse(), $matchesResponse);

          if ($exclusif !== \count($matchesStatement[0]) + \count($matchesResponse[0]) > 0) {
            return false;
          }
        } else {
          if ($exclusif !== \count($matchesStatement[0]) > 0) {
            return false;
          }
        }
      }

      return true;
    });
  }

  #================================================================#

  public function getPeriod(SchoolZone $zone, bool $allowOnWeekend = false): ?string
  {
    $actualDate = new \DateTime('now', new \DateTimeZone($zone->getTimeZone()));

    if ($this->environnement === 'dev') {
      $zone = SchoolZone::Default;
    } else {
      // No period if it's saturday or sunday
      if (!$allowOnWeekend && \in_array($actualDate->format("w"), [0, 6])) {
        return null;
      }
    }

    $actualDate = $actualDate->format("Y/m/d");

    $content = \file_get_contents($this->schoolZoneDatesFilePath);
    if ($content === false) {
      return null;
    }

    $content = \json_decode($content, true);
    if ($content === null) {
      return null;
    }

    // We iterate over each row to determine the period
    foreach ($content[$zone->value] as $period => $dates) {
      if ($actualDate >= \trim($dates[0]) && $actualDate <= \trim($dates[1])) {
        return \strval($period);
      }
    }

    return null;
  }
}
