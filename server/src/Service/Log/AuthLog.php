<?php

declare(strict_types=1);

namespace Shared\Service\Log;

use Shared\Entity\User;
use Psr\Log\LoggerInterface;

class AuthLog
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private LoggerInterface $logger
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function registerSuccess(User $user, ?string $ip): void
  {
    $this->logger->info("[REGISTER] : {$ip} {$user->getUserIdentifier()}");
  }

  #================================================================#

  public function registerConfirmation(User $user, ?string $ip): void
  {
    $this->logger->info("[REGISTER-CONFIRMATION] : {$ip} {$user->getUserIdentifier()}");
  }

  #================================================================#

  public function loginSuccess(User $user, string $ip): void
  {
    $this->logger->info("[LOGIN-SUCCESS] : {$ip} {$user->getUserIdentifier()}");
  }

  #================================================================#

  public function loginFailure(string $email, string $msg, string $ip): void
  {
    $this->logger->warning("[LOGIN-FAILURE] : {$ip} {$email}, {$msg}");
  }

  #================================================================#

  public function removeAccount(string $email, ?string $ip): void
  {
    $this->logger->warning("[REMOVE-ACCOUNT] : {$ip} {$email}");
  }

  #================================================================#

  public function passwordResetRequest(string $email, ?string $ip): void
  {
    $this->logger->warning("[RESET-PASSWORD-REQUEST] : {$ip} {$email}");
  }

  #================================================================#

  public function passwordReset(string $email, ?string $ip): void
  {
    $this->logger->warning("[RESET-PASSWORD] : {$ip} {$email}");
  }

  #================================================================#

  public function resendVerifyEmail(string $email, ?string $ip): void
  {
    $this->logger->warning("[RESEND-VERIFICATION] : {$ip} {$email}");
  }

  #================================================================#

  public function passwordChanged(string $email, ?string $ip): void
  {
    $this->logger->warning("[PASSWORD-CHANGED] : {$ip} {$email}");
  }
}
