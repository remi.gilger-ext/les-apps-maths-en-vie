<?php

declare(strict_types=1);

namespace Shared\Service\Log;

use Psr\Log\LoggerInterface;

class ErrorsLog
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private LoggerInterface $logger
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function reportError(string $reason, string $stack): void
  {
    $this->logger->info("[Error] : {$reason}, call stack : {$stack}");
  }
}
