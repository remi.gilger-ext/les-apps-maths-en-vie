<?php

declare(strict_types=1);

namespace Shared\Service;

use Banque\Entity\Problem;
use Banque\Enum\ProblemTypeEnum;
use Shared\Entity\User;
use Shared\Security\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Messenger\SendEmailMessage;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class MailerService
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  private string $emailSenderName = 'M@ths en-vie';

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private string $emailSender,
    private string $emailAdmin,
    private string $emaildev,
    private string $emailContact,
    private MailerInterface $mailer,
    private MessageBusInterface $bus,
    private EmailVerifier $emailVerifier,
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function reportProblem(?User $user, Problem $problem, string $comment): void
  {
    $email = (new TemplatedEmail())
      ->from(new Address($this->emailSender, $this->emailSenderName))
      ->to($this->emailAdmin)
      ->subject('[Report] Signalement de problème');

    $email->htmlTemplate('@Banque/emails/reportProblemEmail.html.twig');

    $email->context([
      'userEmail' => $user !== null ? $user->getEmail() : 'Anonymous',
      'problemId' => $problem->getId(),
      'reason' => $comment
    ]);

    $this->mailer->send($email);
  }

  #================================================================#

  public function verifyEmail(User $user): void
  {
    $this->emailVerifier->sendEmailConfirmation(
      'app.verifyEmail',
      $user,
      (new TemplatedEmail())
        ->from(new Address($this->emailSender, $this->emailSenderName))
        ->to($user->getEmail())
        ->subject('Veuillez confirmer votre email')
        ->htmlTemplate('emails/registerConfirmationEmail.html.twig')
    );
  }

  #================================================================#

  public function resetPassword(User $user, string $token): void
  {
    $email = (new TemplatedEmail())
      ->from(new Address($this->emailSender, $this->emailSenderName))
      ->to($user->getEmail())
      ->subject('Réinitialisation de mot de passe')

      // path of the Twig template to render
      ->htmlTemplate('emails/resetPasswordEmail.html.twig')

      ->context([
        'token' => $token,
        'user' => $user
      ]);

    $this->mailer->send($email);
  }

  #================================================================#

  public function grantedOnPublicProblem(User $user, ProblemTypeEnum $type): void
  {
    $email = (new TemplatedEmail())
      ->from(new Address($this->emailSender, $this->emailSenderName))
      ->to($user->getEmail())
      ->subject('Accès à la banque collaborative');

    if ($type === ProblemTypeEnum::TextProblem) {
      $email->htmlTemplate('@Banque/emails/grantedOnPublicTextProblemEmail.html.twig');
    } elseif ($type === ProblemTypeEnum::Photo) {
      $email->htmlTemplate('@Banque/emails/grantedOnPublicPhotoEmail.html.twig');
    } elseif ($type === ProblemTypeEnum::PhotoProblem) {
      $email->htmlTemplate('@Banque/emails/grantedOnPublicPhotoProblemEmail.html.twig');
    } else {
      throw new \LogicException("Problem type impossible");
    }

    $email->context([
      'user' => $user
    ]);

    $this->mailer->send($email);
  }

  #================================================================#

  public function welcome(User $user): void
  {
    $email = (new TemplatedEmail())
      ->from(new Address($this->emailSender, $this->emailSenderName))
      ->to($user->getEmail())
      ->subject('Bienvenue')

      // path of the Twig template to render
      ->htmlTemplate('emails/welcomeEmail.html.twig')

      ->context([
        'user' => $user
      ]);

    $this->mailer->send($email);
  }

  #================================================================#

  /**
   * Send an email to all users specified
   *
   * @param User[] $users
   * @param int $offsetDelay offset delay to send an email every 10 sec, 0 to not have any delay
   * @param array<string, mixed>|array<array<string, mixed>> $contexts array of one context per user or
   *                                                    just one context if isSameContext = true
   * @param string $template html template
   * @param string $subject subject of the mail
   * @param boolean $isSameContext same context for each emails
   * @return string|boolean
   */
  public function sendEmailToList(
    array $users,
    int $offsetDelay,
    array $contexts,
    string $template,
    string $subject,
    bool $isSameContext = false
  ): string|bool {
    if (!$isSameContext && \count($users) !== \count($contexts)) {
      return "The number of users need to be the same length as the number of bodys html";
    }

    $usersKeys = array_keys($users);
    $contextsKeys = array_keys($contexts);

    $delay = $offsetDelay * 1000 * 10;
    for ($i = 0; $i < \count($users); $i++) {
      $email = (new TemplatedEmail())
        ->from(new Address($this->emailSender, $this->emailSenderName))
        ->subject($subject)
        ->to($users[$usersKeys[$i]]->getEmail())
        ->htmlTemplate($template);

      if ($isSameContext) {
        $email->context($contexts);
      } else {
        $email->context($contexts[$contextsKeys[$i]]);
      }

      $this->bus->dispatch(new SendEmailMessage($email), [new DelayStamp($delay)]);
      $delay += 1000 * 10; // A email is sent every 10 sec
    }

    return true;
  }

  #================================================================#

  public function confirmUser(User $user): void
  {
    $email = (new TemplatedEmail())
      ->from(new Address($this->emaildev, $this->emailSenderName))
      ->to($user->getEmail())
      ->subject('Problème d\'inscription aux apps M@ths en-vie')

      // path of the Twig template to render
      ->htmlTemplate('@Banque/emails/confirmUser.html.twig')

      ->context([
        'user' => $user
      ]);

    $this->mailer->send($email);
  }

  #================================================================#

  /**
   * Send an email to notify a new problem to the admin
   *
   * @return void
   */
  public function notifyAdminNewProblem(ProblemTypeEnum $problemType, string $email): void
  {
    $date = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
    $email = (new Email())
      ->from(new Address($this->emailSender, $this->emailSenderName))
      ->subject("[NEW] Banque M@ths en-vie - Nouveau problème publié")
      ->to($this->emailAdmin)
      ->text("Nouveau {$problemType->value} - " . $email . " - " . $date->format("d-m-Y H-i-s"));

    $this->mailer->send($email);
  }

  #================================================================#

  /** @param "pedagogique"|"technique"|"accessibility" $demandType */
  public function contact(string $fullname, string $email, string $subject, string $content, string $demandType): void
  {
    $preSubject = "[Contact] ";
    if ($demandType === "accessibility") {
      $preSubject .= "[Accessibilité] ";
    }

    $email = (new TemplatedEmail())
      ->from(new Address($this->emailSender, $this->emailSenderName))
      ->to($demandType === "pedagogique" ? $this->emailContact : $this->emaildev)
      ->subject($preSubject . $subject)

      // path of the Twig template to render
      ->htmlTemplate('emails/contact.html.twig')

      ->context([
        'fullname' => $fullname,
        'destEmail' => $email,
        'subject' => $subject,
        'content' => $content,
      ]);

    $this->mailer->send($email);
  }

  #================================================================#

  public function getEmailSender(): string
  {
    return $this->emailSender;
  }

  #================================================================#

  public function setEmailSender(string $emailSender): self
  {
    $this->emailSender = $emailSender;

    return $this;
  }

  #================================================================#

  public function getEmailSenderName(): string
  {
    return $this->emailSenderName;
  }

  #================================================================#

  public function setEmailSenderName(string $emailSenderName): self
  {
    $this->emailSenderName = $emailSenderName;

    return $this;
  }
}
