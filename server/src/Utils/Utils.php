<?php

declare(strict_types=1);

namespace Shared\Utils;

use Atelier\Enum\AidType;
use Atelier\Enum\GameType;
use Atelier\Enum\ProblemStateEnum;
use Atelier\Enum\SchoolNivel;
use Banque\Enum\ResponseStateEnum;

class Utils
{
  public static function toBool(int|bool|string|null|float $value): bool|null
  {
    return match (is_string($value) ? strtolower(trim($value)) : $value) {
      1, 1.0, true, '1', '1.0', 'true', 'on', 'yes', 'y' => true,
      0, 0.0, false, '0', '0.0', 'false', 'off', 'no', 'n' => false,
      default => null,
    };
  }

  /**
   * Convert a english month to french
   *
   * @param string $month
   * @return string|null
   */
  public static function convertMonthToFrench(string $month): string|null
  {
    return match ($month) {
      'January' => 'de janvier',
      'February' => 'de février',
      'March' => 'de mars',
      'April' => 'd\'avril',
      'May' => 'de mai',
      'June' => 'de juin',
      'July' => 'de juillet',
      'August' => 'd\'août',
      'September' => 'de septembre',
      'October' => 'd\'octobre',
      'November' => 'de novembre',
      'December' => 'de décembre',
      default => null,
    };
  }

  #================================================================#

  /**
   * To check if the file really exists (safer method)
   *
   * NOTE: The file is created if not existing
   *
   * @param string $file
   * @return boolean
   */
  public static function fileExistsSafe(string $file): bool
  {
    try {
      if (!$fd = fopen($file, 'x')) {
        return true;
      }
    } catch (\Throwable $th) {
      return false;
    }

    fclose($fd);
    return false;
  }

  #================================================================#

  /**
   * @param scalar[] $values
   * @phpstan-assert-if-true int[] $values
   */
  public static function isArrayOfInt(array $values): bool
  {
    foreach ($values as $value) {
      if (!is_int($value)) {
        return false;
      }
    }

    return true;
  }

  #================================================================#

  /**
   * Get the score for a problem answered in a game
   */
  public static function getScore(GameType $gameType, ProblemStateEnum $problemState, array $aidUsed, SchoolNivel $gameNivel, ?SchoolNivel $problemNivel): float|null
  {
    $score = 0.;

    if (
      $problemState !== ProblemStateEnum::ANSWERED_CORRECTLY &&
      $problemState !== ProblemStateEnum::ANSWERED_WRONGLY
    ) {
      return null;
    }

    if ($gameType === GameType::DEMO) {
      if ($problemState === ProblemStateEnum::ANSWERED_CORRECTLY) {
        if (\in_array(AidType::AID_NIVEL_1->value, $aidUsed)) {
          $score = 0.75;
        } elseif (\in_array(AidType::AID_NIVEL_2->value, $aidUsed)) {
          $score = 0.25;
        } else {
          $score = 1.;
        }
      } else {
        $score = 0.;
      }
    } elseif ($gameType === GameType::EVALUATION) {
      if ($problemState === ProblemStateEnum::ANSWERED_CORRECTLY) {
        $score = 1.;
      } else {
        $score = 0.;
      }
    } elseif ($gameType === GameType::TRAINING) {
      if ($problemState === ProblemStateEnum::ANSWERED_CORRECTLY) {
        if (\in_array(AidType::AID_NIVEL_1->value, $aidUsed)) {
          $score = $problemNivel === $gameNivel ? 0.66 : 0.75;
        } elseif (\in_array(AidType::AID_NIVEL_2->value, $aidUsed)) {
          $score = $problemNivel === $gameNivel ? 0.33 : 0.50;
        } else {
          $score = 1.;
        }
      } else {
        $score = 0.;
      }
    }

    return $score;
  }
}
