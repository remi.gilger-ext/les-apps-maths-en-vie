<?php

declare(strict_types=1);

namespace Shared\Entity;

use Atelier\Entity\Student;
use Banque\Entity\Message;
use Banque\Entity\Problem;
use Banque\Entity\ProblemComment;
use Banque\Entity\ProblemLike;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Shared\Enum\ProgrammationType;
use Shared\Enum\SchoolZone;
use Shared\Enum\UserTypeEnum;
use Shared\Service\ProgrammationService;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity]
#[ORM\Table(name: '`tt_user`')]
#[UniqueEntity(fields: ['email'], message: 'Un compte est déjà existant avec cet email.')]
#[UniqueEntity(fields: ['username'], message: 'Ce pseudo est déjà utilisé.')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
  #================================================================#
  # Constants                                                      #
  #================================================================#

  public const ROLES = [
    "ROLE_USER",
    "ROLE_ADHERENT",
    "ROLE_VIP",
    "ROLE_ADMIN",
    "ROLE_SUPER_ADMIN"
  ];

  #================================================================#
  # Properties                                                     #
  #================================================================#

  #================================================================#
  # Shared User

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_user')]
  private int $id;

  /** @var value-of<User::ROLES>[] */
  #[ORM\Column(name: 'user_roles')]
  private array $roles = [];

  #[ORM\Column(name: 'user_username', length: 32, unique: true)]
  #[Assert\Regex(
    pattern: '/^[a-zA-Z0-9_]+$/',
    message: 'Votre pseudo ne doit contenir que des caractères alphanumériques, des chiffres ou un underscore.',
  )]
  private string $username;

  #[ORM\Column(name: 'user_email', length: 255, unique: true)]
  private string $email;

  #[ORM\Column(name: 'user_password')]
  private string $password;

  #[ORM\Column(name: 'user_isVerified', options: ["default" => false])]
  private bool $isVerified = false;

  #[ORM\Column(name: 'user_registerDate', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  private \DateTimeInterface $registerDate;

  #[ORM\Column(name: 'user_addressIp', length: 40, nullable: true)]
  private ?string $addressIp = null;

  #[ORM\Column(name: 'user_lastLogin', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  private \DateTimeInterface $lastLogin;

  #[ORM\Column(name: 'user_resetToken', length: 100, nullable: true)]
  private ?string $resetToken = null;

  #[ORM\Column(name: 'user_resetExpireDate', type: Types::DATETIME_MUTABLE, nullable: true, options: ["default" => "CURRENT_TIMESTAMP"])]
  private ?\DateTimeInterface $resetExpireDate = null;

  #[ORM\Column(name: 'user_schoolZone', type: 'string', length: 16, enumType: SchoolZone::class, nullable: true)]
  private ?SchoolZone $schoolZone = null;

  #[ORM\Column(name: 'user_type', type: 'string', length: 16, enumType: UserTypeEnum::class)]
  private UserTypeEnum $type;

  #[ORM\Column(name: 'user_programmationType', type: 'string', length: 16, enumType: ProgrammationType::class)]
  private ProgrammationType $programmationType = ProgrammationType::MathsEnVie;

  #[ORM\Column(name: 'user_programmation', type: Types::TEXT, length: 65535, nullable: true)]
  private ?string $programmation = null;

  private string $mathsenvieProgrammation;
  private string $tandemProgrammation;

  #================================================================#
  # Banque User

  #[ORM\Column(name: 'user_grade', options: ["default" => 0])]
  private int $grade = 0;

  /** @var Collection<int, Problem> */
  #[ORM\OneToMany(mappedBy: 'user', targetEntity: Problem::class)]
  private Collection $problems;

  /** @var Collection<int, ProblemLike> */
  #[ORM\OneToMany(mappedBy: 'user', targetEntity: ProblemLike::class, orphanRemoval: true)]
  private Collection $problemLikes;

  /** @var Collection<int, ProblemComment> */
  #[ORM\OneToMany(mappedBy: 'user', targetEntity: ProblemComment::class, orphanRemoval: true)]
  private Collection $problemComments;

  #[ORM\Column(name: 'user_lastNotificationChecked', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  private \DateTimeInterface $lastNotificationChecked;

  #[ORM\Column(name: 'user_lastActivity', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  private \DateTimeInterface $lastActivity;

  /** @var Collection<int, Message> */
  #[ORM\OneToMany(mappedBy: 'sender', targetEntity: Message::class, orphanRemoval: true)]
  private Collection $messagesSent;

  /** @var Collection<int, Message> */
  #[ORM\OneToMany(mappedBy: 'receiver', targetEntity: Message::class, orphanRemoval: true)]
  private Collection $messagesReceived;

  #[ORM\Column(name: 'user_isEmailsSubscribed', options: ["default" => true])]
  private bool $isEmailsSubscribed = true;

  #[ORM\Column(name: 'user_classNivel', length: 16, nullable: true)]
  private ?string $classNivel = null;

  #================================================================#
  # App User

  /** @var Collection<int, Student> */
  #[ORM\OneToMany(targetEntity: Student::class, mappedBy: 'tutor', orphanRemoval: true)]
  private Collection $students;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->problems = new ArrayCollection();
    $this->problemLikes = new ArrayCollection();
    $this->problemComments = new ArrayCollection();
    $this->messagesSent = new ArrayCollection();
    $this->messagesReceived = new ArrayCollection();

    $this->students = new ArrayCollection();

    $now = new \DateTime('now', new \DateTimeZone('UTC'));

    $this->registerDate = $now;
    $this->lastLogin = $now;
    $this->lastNotificationChecked = $now;
    $this->lastActivity = $now;
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  /**
   * A visual identifier that represents this user.
   *
   * @see UserInterface
   */
  public function getUserIdentifier(): string
  {
    return $this->email;
  }

  #================================================================#

  /** @return value-of<User::ROLES>[] */
  public function getRoles(): array
  {
    $roles = $this->roles;
    // guarantee every user at least has ROLE_USER
    $roles[] = 'ROLE_USER';

    return array_unique($roles);
  }

  /** @param value-of<User::ROLES>[] $roles */
  public function setRoles(array $roles): static
  {
    $this->roles = $roles;

    return $this;
  }

  #================================================================#

  public function getUsername(): string
  {
    return $this->username;
  }

  public function setUsername(string $username): static
  {
    $this->username = $username;

    return $this;
  }

  #================================================================#

  public function getEmail(): string
  {
    return $this->getUserIdentifier();
  }

  public function setEmail(string $email): static
  {
    $this->email = $email;

    return $this;
  }

  #================================================================#

  /**
   * @see PasswordAuthenticatedUserInterface
   */
  public function getPassword(): string
  {
    return $this->password;
  }

  public function setPassword(string $password): static
  {
    $this->password = $password;

    return $this;
  }

  #================================================================#

  /**
   * @see UserInterface
   */
  public function eraseCredentials(): void
  {
    // If you store any temporary, sensitive data on the user, clear it here
  }

  #================================================================#

  public function isVerified(): bool
  {
    return $this->isVerified;
  }

  public function setIsVerified(bool $isVerified): static
  {
    $this->isVerified = $isVerified;

    return $this;
  }

  #================================================================#

  public function getRegisterDate(): \DateTimeInterface
  {
    return $this->registerDate;
  }

  public function setRegisterDate(\DateTimeInterface $registerDate): static
  {
    $this->registerDate = $registerDate;

    return $this;
  }

  #================================================================#

  public function getAddressIp(): ?string
  {
    return $this->addressIp;
  }

  public function setAddressIp(?string $addressIp): static
  {
    $this->addressIp = $addressIp;

    return $this;
  }

  #================================================================#

  public function getLastLogin(): \DateTimeInterface
  {
    return $this->lastLogin;
  }

  public function setLastLogin(\DateTimeInterface $lastLogin): static
  {
    $this->lastLogin = $lastLogin;

    return $this;
  }

  #================================================================#

  public function getResetToken(): ?string
  {
    return $this->resetToken;
  }

  public function setResetToken(?string $resetToken): static
  {
    $this->resetToken = $resetToken;

    return $this;
  }

  #================================================================#

  public function getResetExpireDate(): ?\DateTimeInterface
  {
    return $this->resetExpireDate;
  }

  public function setResetExpireDate(?\DateTimeInterface $resetExpireDate): static
  {
    $this->resetExpireDate = $resetExpireDate;

    return $this;
  }

  #================================================================#

  public function getSchoolZone(): ?SchoolZone
  {
    return $this->schoolZone;
  }

  public function setSchoolZone(?SchoolZone $schoolZone): static
  {
    $this->schoolZone = $schoolZone;

    return $this;
  }

  #================================================================#

  public function getType(): UserTypeEnum
  {
    return $this->type;
  }

  public function setType(UserTypeEnum $type): static
  {
    $this->type = $type;

    return $this;
  }

  #================================================================#

  public function getProgrammationType(): ProgrammationType
  {
    return $this->programmationType;
  }

  public function setProgrammationType(ProgrammationType $programmationType): static
  {
    $this->programmationType = $programmationType;

    return $this;
  }

  #================================================================#

  /**
   * @return ($forceCustom is true ? ?string : string)
   */
  public function getProgrammation(bool $forceCustom = false): ?string
  {
    if ($forceCustom) {
      return $this->programmation;
    }

    return match ($this->programmationType) {
      ProgrammationType::Custom => $this->programmation ?? \json_encode([]),
      ProgrammationType::MathsEnVie => $this->mathsenvieProgrammation,
      ProgrammationType::Tandem => $this->tandemProgrammation,
    };
  }

  public function setProgrammation(?string $programmation): static
  {
    $this->programmation = $programmation;

    return $this;
  }

  #================================================================#

  public function getGrade(): int
  {
    return $this->grade;
  }

  public function setGrade(int $grade): static
  {
    $this->grade = $grade;

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, Problem>
   */
  public function getProblems(): Collection
  {
    return $this->problems;
  }

  public function addProblem(Problem $problem): static
  {
    if (!$this->problems->contains($problem)) {
      $this->problems->add($problem);
      $problem->setUser($this);
    }

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, ProblemLike>
   */
  public function getProblemLikes(): Collection
  {
    return $this->problemLikes;
  }

  public function addProblemLike(ProblemLike $problemLike): static
  {
    if (!$this->problemLikes->contains($problemLike)) {
      $this->problemLikes->add($problemLike);
      $problemLike->setUser($this);
    }

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, ProblemComment>
   */
  public function getProblemComments(): Collection
  {
    return $this->problemComments;
  }

  public function addProblemComment(ProblemComment $problemComment, Problem $problem): static
  {
    if (!$this->problemComments->contains($problemComment)) {
      $this->problemComments->add($problemComment);
      $problemComment->setUser($this);

      $problem->addProblemComment($problemComment);
    }

    return $this;
  }

  public function removeProblemComment(ProblemComment $problemComment): static
  {
    if (
      $this->problemComments->removeElement($problemComment) &&
      $problemComment->getUser() === $this
    ) {
      $problemComment->getProblem()->removeProblemComment($problemComment);
    }

    return $this;
  }

  #================================================================#

  public function getLastNotificationChecked(): \DateTimeInterface
  {
    return $this->lastNotificationChecked;
  }

  public function setLastNotificationChecked(\DateTimeInterface $lastNotificationChecked): static
  {
    $this->lastNotificationChecked = $lastNotificationChecked;

    return $this;
  }

  #================================================================#

  public function getLastActivity(): \DateTimeInterface
  {
    return $this->lastActivity;
  }

  public function setLastActivity(\DateTimeInterface $lastActivity): static
  {
    $this->lastActivity = $lastActivity;

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, Message>
   */
  public function getSentMessages(): Collection
  {
    return $this->messagesSent;
  }

  public function sendMessage(Message $message): static
  {
    if (!$this->messagesSent->contains($message)) {
      $this->messagesSent->add($message);
      $message->setSender($this);
    }

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, Message>
   */
  public function getReceivedMessages(): Collection
  {
    return $this->messagesReceived;
  }

  public function addReceivedMessage(Message $message): static
  {
    if (!$this->messagesReceived->contains($message)) {
      $this->messagesReceived->add($message);
      $message->setReceiver($this);
    }

    return $this;
  }

  #================================================================#

  public function isEmailsSubscribed(): bool
  {
    return $this->isEmailsSubscribed;
  }

  public function setIsEmailsSubscribed(bool $isEmailsSubscribed): static
  {
    $this->isEmailsSubscribed = $isEmailsSubscribed;

    return $this;
  }

  #================================================================#

  /** @return string[] */
  public function getClassNivel(): array
  {
    return $this->classNivel !== null ? \explode(";", $this->classNivel) : [];
  }

  /** @param ?string[] $classNivel */
  public function setClassNivel(?array $classNivel): static
  {
    $this->classNivel = $classNivel !== null ? \implode(";", $classNivel) : null;

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, Student>
   */
  public function getStudents(): Collection
  {
    return $this->students;
  }

  public function getStudent(int $rank): ?Student
  {
    foreach ($this->getStudents() as $student) {
      if ($student->getRank() === $rank) {
        return $student;
      }
    }

    return null;
  }

  public function addStudent(Student $student): static
  {
    if (!$this->students->contains($student)) {
      $this->students->add($student);
      $student->setTutor($this);
    }

    return $this;
  }

  #================================================================#

  public function setMathsenvieProgrammation(string $programmation): void
  {
    $this->mathsenvieProgrammation = $programmation;
  }

  #================================================================#

  public function setTandemProgrammation(string $programmation): void
  {
    $this->tandemProgrammation = $programmation;
  }
}
