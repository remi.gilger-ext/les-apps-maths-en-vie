<?php

declare(strict_types=1);

namespace Shared\Entity;

use Atelier\Enum\SchoolNivel;
use Banque\Entity\TextProblem;
use Doctrine\ORM\Mapping as ORM;
use Shared\Enum\ProgrammationType;
use Shared\Enum\SchoolZone;

#[ORM\Entity]
#[ORM\Table(name: '`rituel`')]
class Rituel
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_rituel')]
  private int $id;

  #[ORM\Column(name: 'rituel_day', length: 10)]
  private string $day;

  #[ORM\Column(name: 'rituel_nivel', type: 'string', length: 16, enumType: SchoolNivel::class)]
  private SchoolNivel $nivel;

  #[ORM\Column(name: 'rituel_schoolZone', type: 'string', length: 16, enumType: SchoolZone::class)]
  private SchoolZone $schoolZone;

  #[ORM\Column(name: 'rituel_programmationType', type: 'string', length: 16, enumType: ProgrammationType::class)]
  private ProgrammationType $programmationType;

  #[ORM\ManyToOne(targetEntity: TextProblem::class)]
  #[ORM\JoinColumn(name: 'id_problem', referencedColumnName: 'id_problem', nullable: false, onDelete: "CASCADE")]
  private TextProblem $problem;

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getDay(): string
  {
    return $this->day;
  }

  public function setDay(string $day): static
  {
    $this->day = $day;

    return $this;
  }

  #================================================================#

  public function getNivel(): SchoolNivel
  {
    return $this->nivel;
  }

  public function setNivel(SchoolNivel $nivel): static
  {
    $this->nivel = $nivel;

    return $this;
  }

  #================================================================#

  public function getSchoolZone(): SchoolZone
  {
    return $this->schoolZone;
  }

  public function setSchoolZone(SchoolZone $schoolZone): static
  {
    $this->schoolZone = $schoolZone;

    return $this;
  }

  #================================================================#

  public function getProgrammationType(): ProgrammationType
  {
    return $this->programmationType;
  }

  public function setProgrammationType(ProgrammationType $programmationType): static
  {
    $this->programmationType = $programmationType;

    return $this;
  }

  #================================================================#

  public function getProblem(): TextProblem
  {
    return $this->problem;
  }

  public function setProblem(TextProblem $problem): static
  {
    $this->problem = $problem;

    return $this;
  }
}
