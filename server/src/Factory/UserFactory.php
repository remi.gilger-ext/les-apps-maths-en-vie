<?php

declare(strict_types=1);

namespace Shared\Factory;

use Shared\Entity\User;
use Shared\Enum\SchoolZone;
use Shared\Enum\UserTypeEnum;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<User>
 */
final class UserFactory extends PersistentProxyObjectFactory
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private UserPasswordHasherInterface $hasher
  ) {
    parent::__construct();
  }

  #================================================================#
  # Protected Methods                                              #
  #================================================================#

  /** @return array<string, mixed> */
  protected function defaults(): array
  {
    return [
      'roles' => self::faker()->randomElement([
        ['ROLE_ADHERENT'],
        ['ROLE_VIP'],
        []
      ]),
      'username' => self::faker()->unique()->firstName(),
      'email' => self::faker()->unique()->email(),
      'password' => 'password',
      'addressIp' => self::faker()->ipv4(),
      'isVerified' => self::faker()->boolean(90),
      'schoolZone' => self::faker()->randomElement([
        self::faker()->randomElement(SchoolZone::cases()),
        null
      ]),
      'isEmailsSubscribed' => self::faker()->boolean(),
      'classNivel' => self::faker()->randomElement([
        self::faker()->randomElements(['cp', 'ce1', 'ce2', 'cm1', 'cm2'], 2),
        null
      ]),
      'type' => self::faker()->randomElement(UserTypeEnum::cases()),
      'programmation' => null
    ];
  }

  #================================================================#

  protected function initialize(): self
  {
    return $this
      ->afterInstantiate(function (User $user) {
        $user->setPassword($this->hasher->hashPassword($user, $user->getPassword()));
      });
  }

  #================================================================#

  public static function class(): string
  {
    return User::class;
  }
}
