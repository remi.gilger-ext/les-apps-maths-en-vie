<?php

declare(strict_types=1);

namespace Shared\Form;

use Shared\Enum\UserTypeEnum;
use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class RegistrationModel extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\NotBlank(
    message: 'Veuillez saisir un email.'
  )]
  #[Assert\Email(
    message: 'Veuillez saisir un email valide.'
  )]
  #[Assert\Length(
    max: 255,
    maxMessage: 'Merci de saisir moins de {{ limit }} caractères.'
  )]
  private string $email = "";

  #[Assert\NotBlank(
    message: 'Veuillez saisir un pseudo.'
  )]
  #[Assert\Length(
    max: 16,
    maxMessage: 'Le pseudo doit contenir moins de {{ limit }} caratères.'
  )]
  private string $username = "";

  #[Assert\IsTrue(
    message: 'Vous devez accepter les termes.'
  )]
  private bool $agreeTerms = false;

  #[Assert\Length(
    min: 6,
    max: 4096,
    // max length allowed by Symfony for security reasons
    minMessage: 'Le mot de passe doit contenir au moins {{ limit }} caractères.'
  )]
  private string $passwordFirst = "";

  #[Assert\IdenticalTo(
    propertyPath: "passwordFirst",
    message: "Les deux mots de passe doivent être identiques."
  )]
  private string $passwordSecond = "";

  #[Assert\NotNull(
    message: 'Veuillez choisir le type de compte.'
  )]
  private ?UserTypeEnum $userType = null;

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getEmail(): string
  {
    return $this->email;
  }

  #================================================================#

  public function getUsername(): string
  {
    return $this->username;
  }

  #================================================================#

  public function getAgreeTerms(): bool
  {
    return $this->agreeTerms;
  }

  #================================================================#

  public function getPasswordFirst(): string
  {
    return $this->passwordFirst;
  }

  #================================================================#

  public function getPasswordSecond(): string
  {
    return $this->passwordSecond;
  }

  #================================================================#

  public function getUserType(): UserTypeEnum
  {
    if ($this->userType === null) {
      throw new \LogicException("usertype property can not be null");
    }

    return $this->userType;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->email = $params->getString("email");
    $this->username = $params->getString("username");
    $this->agreeTerms = $params->getBoolean("agreeTerms", false);
    $this->passwordFirst = $params->getString("passwordFirst");
    $this->passwordSecond = $params->getString("passwordSecond");
    $this->userType = $params->getEnum("userType", UserTypeEnum::class);
  }
}
