<?php

declare(strict_types=1);

namespace Shared\Form;

use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class ContactModel extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\NotBlank(
    message: 'Veuillez saisir votre prénom.'
  )]
  #[Assert\Length(
    max: 32,
    maxMessage: 'Votre prénom doit contenir moins de {{ limit }} caratères.'
  )]
  private string $firstName = "";

  #[Assert\NotBlank(
    message: 'Veuillez saisir votre nom.'
  )]
  #[Assert\Length(
    max: 32,
    maxMessage: 'Votre nom doit contenir moins de {{ limit }} caratères.'
  )]
  private string $lastName = "";

  #[Assert\NotBlank(
    message: 'Veuillez saisir un email.'
  )]
  #[Assert\Email(
    message: 'Veuillez saisir un email valide.'
  )]
  #[Assert\Length(
    max: 128,
    maxMessage: 'Merci de saisir moins de {{ limit }} caractères.'
  )]
  private string $email = "";

  #[Assert\NotBlank(
    message: 'Veuillez saisir le sujet de votre demande.'
  )]
  #[Assert\Length(
    max: 128,
    maxMessage: 'Le sujet doit contenir moins de {{ limit }} caratères.'
  )]
  private string $subject = "";

  #[Assert\NotBlank(
    message: 'Veuillez saisir le contenu de votre demande.'
  )]
  #[Assert\Length(
    max: 1024,
    maxMessage: 'Le contenu doit contenir moins de {{ limit }} caratères.'
  )]
  private string $content = "";

  #[Assert\Choice(
    choices: ['pedagogique', 'technique', 'accessibility'],
    message: 'Veuillez saisir le type de votre demande.',
  )]
  private ?string $demandType = null;

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getFirstName(): string
  {
    return $this->firstName;
  }

  #================================================================#

  public function getLastName(): string
  {
    return $this->lastName;
  }

  #================================================================#

  public function getEmail(): string
  {
    return $this->email;
  }

  #================================================================#

  public function getSubject(): string
  {
    return $this->subject;
  }

  #================================================================#

  public function getContent(): string
  {
    return $this->content;
  }

  #================================================================#

  public function getDemandType(): string
  {
    return $this->demandType;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->firstName = $params->getString('firstName');
    $this->lastName = $params->getString('lastName');
    $this->email = $params->getString('email');
    $this->subject = $params->getString('subject');
    $this->content = $params->getString('content');
    $this->demandType = $params->getString('demandType');
  }
}
