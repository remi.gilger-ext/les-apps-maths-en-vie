<?php

declare(strict_types=1);

namespace Shared\Form;

use Banque\Enum\TextProblemTypeEnum;
use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class ProgrammationModel extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\Choice(
    callback: 'getTypesCallback',
    message: 'Les types sélectionnés ne sont pas valides.',
    multiple: true,
  )]
  private array $f1 = [];

  #[Assert\GreaterThanOrEqual(
    value: 5,
    message: 'La valeur minimal est 5.',
  )]
  #[Assert\LessThanOrEqual(
    value: 100_000_000_000,
    message: 'La valeur maximal est cent milliards.',
  )]
  private ?int $f2 = null;

  #[Assert\GreaterThanOrEqual(
    value: 150,
    message: 'La valeur minimal est 150.',
  )]
  #[Assert\LessThanOrEqual(
    value: 500,
    message: 'La valeur maximal est 500.',
  )]
  private ?int $f3 = null;

  #[Assert\GreaterThanOrEqual(
    value: 3,
    message: 'La valeur minimum est 3.',
  )]
  #[Assert\LessThanOrEqual(
    value: 10,
    message: 'La valeur maximal est 10.',
  )]
  private ?int $f4 = null;

  private bool $f5 = true;

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getF1(): array
  {
    return \array_unique($this->f1);
  }

  #================================================================#

  public function getF2(): ?int
  {
    return $this->f2;
  }

  #================================================================#

  public function getF3(): ?int
  {
    return $this->f3;
  }

  #================================================================#

  public function getF4(): ?int
  {
    return $this->f4;
  }

  #================================================================#

  public function getF5(): bool
  {
    return $this->f5;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->f1 = $params->all('f1');

    $this->f2 = $params->getBoolean('f2Enable') ? $params->getInt('f2') : null;
    $this->f3 = $params->getBoolean('f3Enable') ? $params->getInt('f3') : null;
    $this->f4 = $params->getBoolean('f4Enable') ? $params->getInt('f4') : null;

    $this->f5 = $params->getBoolean("f5", true);
  }

  #================================================================#

  public function getTypesCallback(): array
  {
    return array_column(TextProblemTypeEnum::cases(), 'name');
  }
}
