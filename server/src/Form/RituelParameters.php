<?php

declare(strict_types=1);

namespace Shared\Form;

use Shared\Enum\SchoolZone;
use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class RituelParameters extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\NotNull(
    message: 'Veuillez saisir votre zone scolaire.'
  )]
  private ?SchoolZone $schoolZone = null;

  /** @var array<'cp'|'ce1'|'ce2'|'cm1'|'cm2'> */
  #[Assert\Choice(
    choices: ['cp', 'ce1', 'ce2', 'cm1', 'cm2'],
    message: 'Les niveaux sélectionnés ne sont pas valides.',
    multiple: true,
    min: 1,
    max: 3,
    minMessage: 'Vous devez saisir au moins un niveau.',
    maxMessage: 'Vous ne pouvez choisir au maximum que trois niveaux.'
  )]
  private array $classNivel = [];

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getSchoolZone(): SchoolZone
  {
    if ($this->schoolZone === null) {
      throw new \LogicException("schoolZone property can not be null");
    }

    return $this->schoolZone;
  }

  #================================================================#

  /** @return array<'cp'|'ce1'|'ce2'|'cm1'|'cm2'> */
  public function getClassNivel(): array
  {
    return \array_unique($this->classNivel);
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->schoolZone = $params->getEnum("schoolZone", SchoolZone::class);
    $this->classNivel = $params->all('classNivel');
  }
}
