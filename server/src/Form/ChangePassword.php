<?php

declare(strict_types=1);

namespace Shared\Form;

use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

class ChangePassword extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[SecurityAssert\UserPassword(
    message: 'Votre mot de passe actuel est incorrect.'
  )]
  private string $oldPassword = '';

  #[Assert\Length(
    min: 6,
    max: 4096,
    // max length allowed by Symfony for security reasons
    minMessage: 'Le mot de passe doit contenir au moins {{ limit }} caractères.'
  )]
  private string $newPasswordFirst = '';

  #[Assert\IdenticalTo(
    propertyPath: 'newPasswordFirst',
    message: 'Les deux mots de passe doivent être identiques.'
  )]
  private string $newPasswordSecond = '';

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getOldPassword(): string
  {
    return $this->oldPassword;
  }

  #================================================================#

  public function getNewPasswordFirst(): string
  {
    return $this->newPasswordFirst;
  }

  #================================================================#

  public function getNewPasswordSecond(): string
  {
    return $this->newPasswordSecond;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->oldPassword = $params->getString('oldPassword');
    $this->newPasswordFirst = $params->getString('newPasswordFirst');
    $this->newPasswordSecond = $params->getString('newPasswordSecond');
  }
}
