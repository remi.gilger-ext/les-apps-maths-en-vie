<?php

declare(strict_types=1);

namespace Shared\Form;

use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class ResetPassword extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\Length(
    min: 6,
    max: 4096,
    // max length allowed by Symfony for security reasons
    minMessage: 'Le mot de passe doit contenir au moins {{ limit }} caractères.'
  )]
  private string $newPasswordFirst = "";

  #[Assert\IdenticalTo(
    propertyPath: "newPasswordFirst",
    message: "Les deux mots de passe doivent être identiques."
  )]
  private string $newPasswordSecond = "";

  private string $token = "";

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getNewPasswordFirst(): string
  {
    return $this->newPasswordFirst;
  }

  #================================================================#

  public function getNewPasswordSecond(): string
  {
    return $this->newPasswordSecond;
  }

  #================================================================#

  public function getToken(): string
  {
    return $this->token;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->newPasswordFirst = $params->getString('newPasswordFirst');
    $this->newPasswordSecond = $params->getString('newPasswordSecond');
    $this->token = $params->getString('token');
  }
}
