<?php

declare(strict_types=1);

namespace Shared\Form;

use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

abstract class AbstractModel
{
  #================================================================#
  # Abstract Methods                                               #
  #================================================================#

  /** @param InputBag<bool|float|int|string> $params */
  abstract public function fillInstance(InputBag $params): void;

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Transform a list of violations to use in react
   *
   * @param ConstraintViolationListInterface $errors
   * @return array{name: string, message: string|\Stringable}[]
   */
  public function getFormatErrors(ConstraintViolationListInterface $errors): array
  {
    $formErrors = [];

    /** @var ConstraintViolationInterface $error */
    foreach ($errors as $error) {
      $formErrors[] = [
        'name' => $error->getPropertyPath(),
        'message' => $error->getMessage(),
      ];
    }

    return $formErrors;
  }

  #================================================================#
  # Protected Methods                                              #
  #================================================================#

  // /**
  //  * @param array<string, scalar|array<string|int, scalar>> $params
  //  */
  // abstract public function getString(array $params): void;
  // {

  // }
}
