<?php

declare(strict_types=1);

namespace Shared\Security;

use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Service\Log\AuthLog;
use Shared\Service\ParseToArrayService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

class LoginAuthenticator extends AbstractLoginFormAuthenticator
{
  use TargetPathTrait;

  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const LOGIN_ROUTE = 'app.login';

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private AuthenticationUtils $authenticationUtils,
    private AuthLog $authLog,
    private EntityManagerInterface $em,
    private ParseToArrayService $parseToArrayService,
    private UrlGeneratorInterface $urlGenerator,
    private TranslatorInterface $translator
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function authenticate(Request $request): Passport
  {
    $payload = $request->getPayload();
    $identifier = $payload->getString("identifier");
    $password = $payload->getString("password");

    $request->attributes->set('_remember_me', $payload->getBoolean('_remember_me'));

    return new Passport(
      new UserBadge($identifier),
      new PasswordCredentials($password),
      [
        new RememberMeBadge(),
        //new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
      ]
    );
  }

  #================================================================#

  public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): Response
  {
    /** @var ?User $user */
    $user = $token->getUser();

    if (!$user) {
      throw new UserNotFoundException();
    }

    $user->setLastLogin(new \DateTime('now', new \DateTimeZone('UTC')));
    if ($ip = $request->getClientIp()) {
      $user->setAddressIp($ip);
    }

    $this->em->persist($user);
    $this->em->flush();

    $this->authLog->loginSuccess($user, $request->getClientIp() ?? "No IP provided");

    return new JsonResponse($this->parseToArrayService->parseUser($user));
  }

  #================================================================#

  public function start(Request $request, AuthenticationException $authException = null): Response
  {
    return new Response(status: 403);
  }

  #================================================================#

  public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
  {
    $this->authLog->loginFailure(
      $this->authenticationUtils->getLastUsername(),
      $exception->getMessage(),
      $request->getClientIp() ?? 'No IP provided'
    );

    $error = $this->translator->trans($exception->getMessageKey(), $exception->getMessageData(), 'security');

    return new JsonResponse([
      'message' => $error
    ], status: 403);
  }

  #================================================================#

  public function supports(Request $request): bool
  {
    return $request->isMethod('PUT') && $this->getLoginUrl($request) === $request->getBaseUrl() . $request->getPathInfo();
  }

  #================================================================#
  # Protected Methods                                              #
  #================================================================#

  protected function getLoginUrl(Request $request): string
  {
    return $this->urlGenerator->generate(self::LOGIN_ROUTE);
  }
}
