<?php

declare(strict_types=1);

namespace Shared\Security;

use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class EmailVerifier
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private EntityManagerInterface $em,
    private MailerInterface $mailer,
    private VerifyEmailHelperInterface $verifyEmailHelper
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function sendEmailConfirmation(
    string $verifyEmailRouteName,
    User $user,
    TemplatedEmail $email
  ): void {
    $signatureComponents = $this->verifyEmailHelper->generateSignature(
      $verifyEmailRouteName,
      \strval($user->getId()),
      $user->getEmail(),
      ['id' => $user->getId()]
    );

    $context = $email->getContext();
    $context['signedUrl'] = $signatureComponents->getSignedUrl();
    $context['expiresAtMessageKey'] = $signatureComponents->getExpirationMessageKey();
    $context['expiresAtMessageData'] = $signatureComponents->getExpirationMessageData();

    $email->context($context);

    $this->mailer->send($email);
  }

  #================================================================#

  /**
   * @throws VerifyEmailExceptionInterface
   */
  public function handleEmailConfirmation(
    Request $request,
    User $user
  ): void {
    $this->verifyEmailHelper->validateEmailConfirmationFromRequest(
      $request,
      \strval($user->getId()),
      $user->getEmail()
    );

    $user->setIsVerified(true);

    $this->em->flush();
  }
}
