<?php

declare(strict_types=1);

namespace Shared\Security;

use Shared\Entity\User;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function checkPreAuth(UserInterface $user): void
  {
    if (!$user instanceof User) {
      return;
    }

    if (!$user->isVerified()) {
      throw new CustomUserMessageAccountStatusException('Vous devez confirmer votre compte grâce à l\'email envoyé.');
    }
  }

  #================================================================#

  public function checkPostAuth(UserInterface $user): void
  {
    if (!$user instanceof User) {
      throw new CustomUserMessageAccountStatusException('Erreur lors de la connexion.');
    }
  }
}
