<?php

declare(strict_types=1);

namespace Shared\Security;

use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Repository\UserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/** @implements UserProviderInterface<User> */
class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private UserRepository $userRepository,
    private EntityManagerInterface $em
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Used to upgrade (rehash) the user's password automatically over time.
   */
  public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
  {
    if (!$user instanceof User) {
      throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
    }

    $user->setPassword($newHashedPassword);

    $this->userRepository->add($user, true);
  }

  #================================================================#

  public function loadUserByIdentifier(string $usernameOrEmail): User
  {
    $user = $this->em->createQuery(
      'SELECT u
      FROM Shared\Entity\User u
      WHERE u.username = :query
      OR u.email = :query'
    )
      ->setParameter('query', $usernameOrEmail)
      ->getOneOrNullResult();

    if ($user === null) {
      throw new UserNotFoundException();
    }

    return $user;
  }

  #================================================================#

  /**
   * Refreshes the user after being reloaded from the session.
   *
   * When a user is logged in, at the beginning of each request, the
   * User object is loaded from the session and then this method is
   * called. Your job is to make sure the user's data is still fresh by,
   * for example, re-querying for fresh User data.
   *
   * If your firewall is "stateless: true" (for a pure API), this
   * method is not called.
   */
  public function refreshUser(UserInterface $user): UserInterface
  {
    if (!$user instanceof User) {
      throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
    }

    $freshUser = $this->userRepository->find($user->getId());

    if ($freshUser === null) {
      throw new UserNotFoundException();
    }

    if ($freshUser->getRoles() !== $user->getRoles()) {
      throw new UserNotFoundException();
    }

    return $freshUser;
  }

  #================================================================#

  /**
   * Tells Symfony to use this provider for this User class.
   */
  public function supportsClass(string $class): bool
  {
    return User::class === $class || is_subclass_of($class, User::class);
  }
}
