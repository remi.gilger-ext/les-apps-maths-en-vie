<?php

declare(strict_types=1);

namespace Atelier\EventListener;

use Atelier\Entity\Student;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::preRemove, method: 'preRemove', entity: Student::class)]
class StudentNotifier
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function preRemove(Student $student, PreRemoveEventArgs $event): void
  {
    foreach ($student->getGames() as $game) {
      $game->setStudent(null);
    }
  }
}
