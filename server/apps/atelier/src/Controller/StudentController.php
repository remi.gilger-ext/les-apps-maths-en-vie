<?php

declare(strict_types=1);

namespace Atelier\Controller;

use Atelier\Entity\Student;
use Atelier\Form\StudentModel;
use Atelier\Service\ParseToArrayService;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api')]
#[IsGranted("ROLE_USER")]
class StudentController extends AbstractController
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const MAX_STUDENT_PER_USER = 32;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private ParseToArrayService $parseToArrayService,
    private EntityManagerInterface $em
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/student/{id?0}', name: 'student.get', requirements: ['id' => '\d+'], methods: 'GET')]
  public function get(
    Student $student = null
  ): Response {
    if ($student === null) {
      return new Response(status: 404);
    }

    // TODO: only the tutor can see the child data
    // if (!$this->isGranted(ProblemVoter::VIEW, $problem)) {
    //   return new Response(status: 403);
    // }

    return new JsonResponse($this->parseToArrayService->parseStudent($student));
  }

  #================================================================#

  #[Route('/students', name: 'students.get', methods: 'GET')]
  public function getStudents(): Response
  {
    /** @var User $tutor */
    $tutor = $this->getUser();

    $students = [];
    for ($i = 0; $i < self::MAX_STUDENT_PER_USER; $i++) {
      if ($student = $tutor->getStudent($i)) {
        $students[] = $this->parseToArrayService->parseStudent($student);
      } else {
        $students[] = Student::createFakeStudent($i);
      }
    }

    return new JsonResponse($students);
  }

  #================================================================#

  #[Route('/student/add', name: 'student.add', methods: 'POST')]
  public function add(
    Request $request,
    ValidatorInterface $validator
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    $studentValidator = new StudentModel();

    $studentValidator->fillInstance($request->getPayload());

    $errors = $validator->validate($studentValidator);

    if ($errors->count() > 0) {
      return new JsonResponse([
        'formErrors' => $studentValidator->getFormatErrors($errors)
      ], status: 400);
    }

    foreach ($studentValidator->getRanks() as $rank) {
      if ($student = $user->getStudent($rank)) {
        if ($student->canModifyNivel()) {
          // Update the current student
          $student->setNivel($studentValidator->getNivel());
        }
      } else {
        // Add a student
        $student = new Student();
        $student->setNivel($studentValidator->getNivel());
        $student->setRank($rank);

        $user->addStudent($student);
        $this->em->persist($student);
      }
    }

    $this->em->flush();

    return new Response(status: 200);
  }
}
