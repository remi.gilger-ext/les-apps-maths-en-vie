<?php

declare(strict_types=1);

namespace Atelier\Controller;

use Atelier\Entity\HelpRequest;
use Banque\Entity\TextProblem;
use Banque\Enum\ProblemTypeEnum;
use Banque\Repository\ProblemRepository;
use Doctrine\ORM\EntityManagerInterface;
use LDAP\Result;
use Shared\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/help-request')]
#[IsGranted("ROLE_USER")]
class HelpRequestController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private EntityManagerInterface $em
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/add', name: 'helpRequest.add', methods: 'POST')]
  public function add(
    Request $request,
    ProblemRepository $problemRepository
  ): Response {
    /** @var User */
    $user = $this->getUser();

    $responseGiven = $request->getPayload()->getString('responseGiven');
    $rank = $request->getPayload()->getInt('rank');
    $problemId = $request->getPayload()->getInt('problemId');

    $student = $user->getStudent($rank);

    if ($student === null) {
      return new Response(status: 400);
    }

    $problem = $problemRepository->find($problemId);

    if ($problem === null || !$problem instanceof TextProblem) {
      return new Response(status: 400);
    }

    $helpRequest = new HelpRequest();
    $helpRequest->setResponseGiven($responseGiven);
    $helpRequest->setStudent($student);
    $helpRequest->setProblem($problem);

    $this->em->persist($helpRequest);
    $this->em->flush();

    return new Response();
  }

  #================================================================#

  #[Route('/treat/{id?0}', name: 'helpRequest.treat', requirements: ['id' => '\d+'], methods: 'PUT')]
  public function treat(
    HelpRequest $helpRequest = null
  ): Response {
    if ($helpRequest === null) {
      return new Response(status: 404);
    }

    /** @var User */
    $user = $this->getUser();

    if ($user !== $helpRequest->getStudent()->getTutor()) {
      return new Response(status: 403);
    }

    $helpRequest->setIsTreated(true);
    $this->em->flush();

    return new JsonResponse([
      'id' => $helpRequest->getId(),
    ]);
  }
}
