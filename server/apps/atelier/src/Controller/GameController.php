<?php

declare(strict_types=1);

namespace Atelier\Controller;

use Atelier\Entity\Game;
use Atelier\Enum\AidType;
use Atelier\Enum\GameOrigin;
use Atelier\Enum\GameType;
use Atelier\Enum\ProblemStateEnum;
use Atelier\Enum\SchoolNivel;
use Atelier\Repository\GameRepository;
use Atelier\Repository\StudentRepository;
use Atelier\Service\ParseToArrayService;
use Banque\Entity\Problem;
use Banque\Entity\TextProblem;
use Banque\Enum\DailyCounterTypeEnum;
use Banque\Enum\ResponseStateEnum;
use Banque\Enum\TextProblemTypeEnum;
use Banque\Repository\ProblemRepository;
use Banque\Service\DailyCounterService;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Enum\SchoolZone;
use Shared\Service\ProgrammationService;
use Shared\Utils\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/game')]
class GameController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private EntityManagerInterface $em,
    private GameRepository $gameRepository,
    private ParseToArrayService $parseToArrayService,
    private ProblemRepository $problemRepository,
    private ProgrammationService $programmationService,
    private StudentRepository $studentRepository,
    private DailyCounterService $dailyCounterService
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/start/{nivelOrRank}', name: 'game.start', methods: 'GET')]
  public function start(
    RequestStack $requestStack,
    Request $request,
    string $nivelOrRank
  ): Response {
    /** @var ?User $tutor */
    $tutor = $this->getUser();

    $gameType = GameType::DEMO;
    $prePageLabel = null;

    /** @var array{problemsAmout: int, levelEvaluation?: int<1,3>} */
    $param = [
      "problemsAmout" => 8
    ];

    if (SchoolNivel::tryFrom($nivelOrRank) === null) {
      if (!\is_numeric($nivelOrRank)) {
        return new JsonResponse(status: 400);
      } elseif (!$tutor) {
        return new JsonResponse(status: 400);
      } elseif ($tutor->getSchoolZone() === null) {
        return new JsonResponse([
          'message' => 'Veuillez spécifier votre zone scolaire dans les paramètres.'
        ], status: 400);
      }

      $student = $tutor->getStudent(\intval($nivelOrRank));

      if (!$student) {
        return new Response(status: 400);
      }

      $schoolZone = $tutor->getSchoolZone();
      $nivel = $student->getNivel();

      $period = $this->programmationService->getPeriod($schoolZone, $nivelOrRank === "31");

      if (!$period && $nivelOrRank === "31") {
        $period = $this->programmationService->getPeriod(SchoolZone::Default, true);
        $schoolZone = SchoolZone::Default;
      }

      if ($period === null) {
        return new JsonResponse([
          'message' => 'Aujourd\'hui pas d\'école.',
          'args' => [
            'gameType' => GameType::TRAINING
          ]
        ], 400);
      }

      $period = intval($period);

      $trainingGames = $this->gameRepository->findBy([
        "student" => $student->getId(),
        "period" => [(string) $period, (string) ($period + 0.5)],
        "type" => GameType::TRAINING
      ]);

      if (\count($trainingGames) >= 10) {
        return new JsonResponse([
          'message' => 'Tu as fais dix parties cette période, reviens après les prochaines vacances pour la suite...',
          'variant' => 'success',
          'args' => [
            'gameType' => GameType::TRAINING
          ]
        ], 400);
      }

      if (Game::getDailyGames($trainingGames, $schoolZone) >= 3) {
        return new JsonResponse([
          'message' => 'Tu as fais trois parties aujourd\'hui ! Reviens à partir de demain pour la suite...',
          'variant' => 'success',
          'args' => [
            'gameType' => GameType::TRAINING
          ]
        ], 400);
      }

      $evalGame = $this->gameRepository->findOneBy([
        "student" => $student->getId(),
        "period" => [(string) $period, (string) ($period + 0.5)],
        "type" => GameType::EVALUATION
      ]);

      if ($evalGame !== null) {
        $gameType = GameType::TRAINING;

        if (empty($trainingGames)) {
          $prePageLabel = "Je m'entraine";
        }

        $level = $evalGame->getEvaluationLevel();
        $param["levelEvaluation"] = $level;
      } else {
        $gameType = GameType::EVALUATION;
        $prePageLabel = "Je m'évalue";

        $param["problemsAmout"] = 3;
      }
    } else {
      $nivel = SchoolNivel::from($nivelOrRank);
      $schoolZone = $tutor?->getSchoolZone() ?? SchoolZone::Default;

      if (!$this->programmationService->getPeriod($schoolZone, true)) {
        $schoolZone = SchoolZone::Default;
      }
    }

    $origin = GameOrigin::Local;
    $filters = $this->programmationService->getFilters($schoolZone, $nivel, $tutor);

    $originToken = $request->query->getString('origin', '');
    if ($originToken === $this->getParameter('beneyluToken')) {
      $origin = GameOrigin::Beneylu;
      $filters = $this->extractFilters($request->query);
    }

    // Set the origin
    $requestStack->getSession()->set("gameOrigin", $origin);

    $problems = $this->getProblems($nivel, $filters, $param);

    if ($problems === null) {
      return new JsonResponse([
        'message' => "Aucun problème n'a pu être trouvé.",
        'args' => [
          'gameType' => $gameType
        ]
      ], status: 400);
    }

    if (($numberOfProblem = \count($problems)) < $param["problemsAmout"]) {
      for ($i = 0; $i < $param["problemsAmout"] - $numberOfProblem; $i++) {
        $problems[] = $problems[\array_rand($problems)];
      }
    }

    $problems = \array_map(function (TextProblem $problem): array {
      return $this->parseToArrayService->parseApplicationProblem($problem);
    }, $problems);

    $problems = \array_map(function ($problem): array {
      $problem["state"] = ProblemStateEnum::NOT_DISPLAYED;
      $problem["aidsUsed"] = [];
      $problem["responseGiven"] = "";
      return $problem;
    }, $problems);

    $gameData = [
      "prePageLabel" => $prePageLabel,
      "nivel" => $nivel,
      "problems" => $problems,
      "type" => $gameType,
      "nbProblems" => 5,
      "actualProblem" => 0,
      "aidPoints" => 3,
      "aidPointsUsed" => 0,
      "aidsCost" => [
        AidType::SKIP_PROBLEM->value => 1,
        AidType::AID_NIVEL_1->value => 1,
        AidType::AID_NIVEL_2->value => 2
      ],
      "origin" => $origin
    ];

    if (isset($student)) {
      $gameData["student"] = $this->parseToArrayService->parseStudent($student);
    }

    // Set an id for the game to know that we received values for the same user
    $uniqueId = \uniqid("", true);
    $requestStack->getSession()->set("gameId", $uniqueId);
    $gameData["id"] = $uniqueId;

    return new JsonResponse($gameData);
  }

  #================================================================#

  #[Route('/end', name: 'game.end', methods: 'POST')]
  public function end(
    Request $request,
  ): Response {
    $payload = $request->getPayload();

    $gameId = $payload->getString("id");

    if ($existingGame = $this->gameRepository->findOneBy(["uniqueID" => $gameId])) {
      return new JsonResponse($this->parseToArrayService->parseGame($existingGame));
    }

    // Fix for Beneylu
    if ($payload->getEnum("origin", GameOrigin::class, GameOrigin::Local) !== GameOrigin::Beneylu) {
      if (!$request->getSession()->has("gameId") || $gameId !== $request->getSession()->get("gameId", "")) {
        return new Response(status: 400);
      }
      $gameOrigin = $request->getSession()->get("gameOrigin", GameOrigin::Local);
    } else {
      $gameOrigin = GameOrigin::Beneylu;
    }

    $request->getSession()->remove("gameId");
    $request->getSession()->remove("gameOrigin");

    $problems = $payload->all("problems");
    $type = $payload->getEnum("type", GameType::class);

    $gameData = [];
    foreach ($problems as $problem) {
      $gameData[] = [
        "id" => $problem["id"],
        "state" => $problem["state"],
        "aidsUsed" => $problem["aidsUsed"],
        "responseGiven" => $problem["responseGiven"]
      ];
    }

    $game = new Game();

    if ($type !== GameType::DEMO) {
      $student = $this->studentRepository->find($payload->all('student')["id"]);
      if ($student === null) {
        return new JsonResponse([
          'args' => [
            'gameType' => $type
          ]
        ], status: 400);
      }

      $student->addGame($game);
      $period = $this->programmationService->getPeriod($student->getTutor()->getSchoolZone(), true);
      if (!$period && $student->getRank() === 31) {
        $period = $this->programmationService->getPeriod(SchoolZone::Default, true);
      }
      $game->setPeriod($period);
    }

    $nivel = $payload->getEnum("nivel", SchoolNivel::class);
    $game->setNivel($nivel);
    $game->setOrigin($gameOrigin);
    $game->setUniqueID($gameId);
    $game->setType($type);
    $game->setData($gameData);

    $this->em->persist($game);

    $this->dailyCounterService->add(DailyCounterTypeEnum::GAMES_PLAYED);

    foreach ($gameData as $data) {
      $problem = $this->problemRepository->find($data["id"]);
      if (!$problem || !$problem instanceof TextProblem) {
        continue;
      }

      $score = Utils::getScore($type, ProblemStateEnum::from($data["state"]), $data["aidsUsed"], $nivel, SchoolNivel::from($problem->getNivel()));

      if ($score === null) {
        continue;
      }

      $problem->setTotalScore($problem->getTotalScore() + $score);
      $problem->setAmoutAnswered($problem->getAmoutAnswered() + 1);
    }

    $this->em->flush();

    return new JsonResponse($this->parseToArrayService->parseGame($game));
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  /**
   * Get problems for a complete game
   *
   * @param array{problemsAmout: int, levelEvaluation?: int<1,3>} $param
   * @param
   *
   * @return TextProblem[]|null
   */
  public function getProblems(SchoolNivel $nivel, ?array $filters, array $param = [
    "problemsAmout" => 8
  ]): ?array
  {
    if (isset($param["levelEvaluation"])) {
      $nextNivel = $nivel->next();
      switch ($param["levelEvaluation"]) {
        case 1:
          unset($param["levelEvaluation"]);
          return $this->getProblems($nivel, $filters, $param);
        case 2:
          unset($param["levelEvaluation"]);
          $actualNivelProblem = $this->getProblems($nivel, $filters, [...$param, "problemsAmout" => 7]);
          $nextNivelProblem = $this->getProblems($nextNivel, $filters, [...$param, "problemsAmout" => 1]);
          return \array_merge(
            \array_slice($actualNivelProblem, 0, 4),
            $nextNivelProblem,
            \array_slice($actualNivelProblem, 4),
          );
        case 3:
          unset($param["levelEvaluation"]);
          $actualNivelProblem = $this->getProblems($nivel, $filters, [...$param, "problemsAmout" => 5]);
          $nextNivelProblem = $this->getProblems($nextNivel, $filters, [...$param, "problemsAmout" => 3]);

          return \array_merge(
            \array_slice($actualNivelProblem, 0, 2),
            $nextNivelProblem,
            \array_slice($actualNivelProblem, 2),
          );
        default:
          throw new \LogicException("This level for the evaluation does not exist");
      }
    }

    if ($filters === null) {
      return null;
    }

    $paramQuery = [
      [
        "p.tag" => [Problem::TAG_COMMUNITY_PUBLIC, Problem::TAG_SAMPLE],
        "p.type" => $filters[ProgrammationService::FILTER_TYPE] ?? TextProblemTypeEnum::casesName(),
        "p.nivel" => [$nivel->value],
        "p.isExclude" => [false],
        'p.responseState' => [ResponseStateEnum::Verified->value]
      ],
      []
    ];

    $allProblems = $this->problemRepository->getProblemsQuery(TextProblem::class, $paramQuery, false);

    $allProblems = $this->programmationService->filtersProblems($allProblems, $filters);

    \srand(\time());

    $numberProblemsWanted = $param["problemsAmout"];

    if (empty($allProblems)) {
      return null;
    }

    $idProblems = array_rand($allProblems, \count($allProblems) >= $numberProblemsWanted ? $numberProblemsWanted : \count($allProblems));

    if (\is_scalar($idProblems)) {
      $problems = [$allProblems[$idProblems]];
    } else {
      $problems = \array_values(\array_intersect_key($allProblems, \array_flip($idProblems)));
    }

    return $problems;
  }

  #================================================================#

  public function extractFilters(InputBag $query)
  {
    $filters = [];

    if ($query->has('f1')) {
      $types = $query->all('f1');

      foreach ($types as $type) {
        if (TextProblemTypeEnum::tryFromName($type)) {
          $filters[ProgrammationService::FILTER_TYPE][] = $type;
        }
      }
    }

    if ($query->has('f4')) {
      $filters[ProgrammationService::FILTER_STEP_PROBLEM_MAX_NUMBERS] = $query->getInt('f4');
    }

    if ($query->has('f5')) {
      if ($query->getString('f5') === "exclusif") {
        $filters[ProgrammationService::FILTER_ALLOW_DECIMALS] = "exclusif";
      } elseif ($query->getBoolean('f5') === false) {
        $filters[ProgrammationService::FILTER_ALLOW_DECIMALS] = false;
      }
    }

    return $filters;
  }
}
