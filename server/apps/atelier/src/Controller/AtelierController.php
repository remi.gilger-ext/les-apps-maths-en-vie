<?php

declare(strict_types=1);

namespace Atelier\Controller;

use Atelier\Repository\StudentRepository;
use Atelier\Service\CacheService;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/atelier')]
class AtelierController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private EntityManagerInterface $em,
    private StudentRepository $studentRepository,
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/stats', name: 'atelier.stats', methods: 'GET')]
  public function stats(
    CacheService $cacheService
  ): Response {
    $stats = $cacheService->getAtelierStats();

    return new JsonResponse([
      'classesCount' => $stats['classesCount'],
      'studentsCount' => $stats['studentsCount'],
      'problemsResolved' => $stats['problemsResolved'],
    ]);
  }

  #================================================================#

  #[IsGranted('ROLE_USER')]
  #[Route('/reset-year', name: 'atelier.resetYear', methods: 'PUT')]
  public function resetYear(): Response
  {
    /** @var User $tutor */
    $tutor = $this->getUser();

    foreach ($tutor->getStudents() as $student) {
      $this->studentRepository->remove($student);
    }

    $this->em->flush();

    return new Response();
  }
}
