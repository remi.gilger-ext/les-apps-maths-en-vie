<?php

declare(strict_types=1);

namespace Atelier\Enum;

enum GameOrigin: string
{
  case Local = 'local';
  case Beneylu = 'beneylu';
  case Nathan = 'nathan';
}
