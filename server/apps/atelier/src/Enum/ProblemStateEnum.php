<?php

declare(strict_types=1);

namespace Atelier\Enum;

enum ProblemStateEnum: string
{
  case NOT_DISPLAYED = 'notDisplayed';
  case ANSWERED_CORRECTLY = 'answeredCorrectly';
  case ANSWERED_WRONGLY = 'answeredWrongly';
  case SKIPPED = 'skipped';
}
