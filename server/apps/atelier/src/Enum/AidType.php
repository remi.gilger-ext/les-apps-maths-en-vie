<?php

declare(strict_types=1);

namespace Atelier\Enum;

enum AidType: string
{
  case SKIP_PROBLEM = 'skipProblem';
  case AID_NIVEL_1 = 'aidNivel1';
  case AID_NIVEL_2 = 'aidNivel2';
}
