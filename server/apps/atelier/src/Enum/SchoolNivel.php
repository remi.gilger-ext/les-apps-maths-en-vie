<?php

declare(strict_types=1);

namespace Atelier\Enum;

enum SchoolNivel: string
{
  case CP = 'cp';
  case CE1 = 'ce1';
  case CE2 = 'ce2';
  case CM1 = 'cm1';
  case CM2 = 'cm2';

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function next(): static
  {
    switch ($this) {
      case self::CP:
        return self::CE1;
      case self::CE1:
        return self::CE2;
      case self::CE2:
        return self::CM1;
      case self::CM1:
        return self::CM2;
      case self::CM2:
        return self::CM2;
      default:
        throw new \LogicException("Impossible to get here");
    }
  }
}
