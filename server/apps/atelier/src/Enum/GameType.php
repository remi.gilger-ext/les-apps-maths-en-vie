<?php

declare(strict_types=1);

namespace Atelier\Enum;

enum GameType: string
{
  case DEMO = 'demo';
  case TRAINING = 'training';
  case EVALUATION = 'evaluation';
}
