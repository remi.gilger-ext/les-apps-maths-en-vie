<?php

declare(strict_types=1);

namespace Atelier\Form;

use Atelier\Controller\StudentController;
use Atelier\Enum\SchoolNivel;
use Shared\Form\AbstractModel;
use Shared\Utils\Utils;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class StudentModel extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\NotNull(
    message: 'Veuillez renseigner le niveau.'
  )]
  private ?SchoolNivel $nivel = null;

  /** @var int[] */
  #[Assert\All([
    new Assert\PositiveOrZero(),
    new Assert\LessThan(StudentController::MAX_STUDENT_PER_USER)
  ])]
  #[Assert\Count(min: 1)]
  private array $ranks = [];

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getNivel(): SchoolNivel
  {
    if ($this->nivel === null) {
      throw new \LogicException("nivel property can not be null");
    }

    return $this->nivel;
  }

  #================================================================#

  /** @return int[] */
  public function getRanks(): array
  {
    return $this->ranks;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->nivel = $params->getEnum("nivel", SchoolNivel::class);

    $ranks = $params->all('ranks');
    if (Utils::isArrayOfInt($ranks)) {
      $this->ranks = $ranks;
    }
  }
}
