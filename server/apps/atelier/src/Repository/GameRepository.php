<?php

declare(strict_types=1);

namespace Atelier\Repository;

use Atelier\Entity\Game;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Repository\BaseRepository;

/** @extends BaseRepository<Game> */
class GameRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, Game::class);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function numberOfGamesByDay(\DateTime $day): int
  {
    return $this->createQueryBuilder("g")
      ->select('COUNT(g.id)')
      ->where('DATE(g.playingDate) = DATE(:date)')
      ->setParameter('date', $day)
      ->getQuery()
      ->getSingleScalarResult();
  }
}
