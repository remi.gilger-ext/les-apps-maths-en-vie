<?php

declare(strict_types=1);

namespace Atelier\Repository;

use Atelier\Entity\Student;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Repository\BaseRepository;

/** @extends BaseRepository<Student> */
class StudentRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, Student::class);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getNumberOfClass(): int
  {
    return \count($this->createQueryBuilder('s')
      ->select('DISTINCT u.id')
      ->leftJoin('s.tutor', 'u')
      ->getQuery()->getResult());
  }
}
