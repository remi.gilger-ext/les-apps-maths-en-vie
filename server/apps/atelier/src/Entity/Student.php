<?php

declare(strict_types=1);

namespace Atelier\Entity;

use Atelier\Enum\SchoolNivel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Entity\User;

#[ORM\Entity]
#[ORM\Table(name: '`student`')]
#[ORM\UniqueConstraint(fields: ["tutor", "rank"])]
class Student
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_student')]
  private int $id;

  #[ORM\Column(name: 'student_nivel', type: 'string', length: 16, enumType: SchoolNivel::class, nullable: true)]
  private ?SchoolNivel $nivel = null;

  #[ORM\ManyToOne(inversedBy: 'students', targetEntity: User::class)]
  #[ORM\JoinColumn(name: 'id_user', referencedColumnName: 'id_user', nullable: false)]
  private User $tutor;

  #[ORM\Column(name: "student_rank")]
  private int $rank;

  /** @var Collection<int, Game> */
  #[ORM\OneToMany(targetEntity: Game::class, mappedBy: 'student', cascade: ["persist"])]
  private Collection $games;

  /** @var Collection<int, HelpRequest> */
  #[ORM\OneToMany(targetEntity: HelpRequest::class, mappedBy: 'student', orphanRemoval: true, cascade: ["persist"])]
  private Collection $helpRequests;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->games = new ArrayCollection();
    $this->helpRequests = new ArrayCollection();
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getNivel(): ?SchoolNivel
  {
    return $this->nivel;
  }

  public function setNivel(SchoolNivel $nivel): static
  {
    $this->nivel = $nivel;

    return $this;
  }

  #================================================================#

  public function getTutor(): User
  {
    return $this->tutor;
  }

  public function setTutor(User $tutor): static
  {
    $this->tutor = $tutor;

    return $this;
  }

  #================================================================#

  public function getRank(): int
  {
    return $this->rank;
  }

  public function setRank(int $rank): static
  {
    $this->rank = $rank;

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, Game>
   */
  public function getGames(): Collection
  {
    return $this->games;
  }

  public function addGame(Game $game): static
  {
    if (!$this->games->contains($game)) {
      $this->games->add($game);
      $game->setStudent($this);
    }

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, HelpRequest>
   */
  public function getHelpRequests(): Collection
  {
    return $this->helpRequests;
  }

  public function addHelpRequest(HelpRequest $helpRequest): static
  {
    if (!$this->helpRequests->contains($helpRequest)) {
      $this->helpRequests->add($helpRequest);
      $helpRequest->setStudent($this);
    }

    return $this;
  }

  #================================================================#
  # Utilities Methods                                              #
  #================================================================#

  /** @phpstan-assert-if-true !null $this->getNivel() */
  public function isEnabled(): bool
  {
    return $this->nivel !== null;
  }

  #================================================================#
  public function canModifyNivel(): bool
  {
    return $this->games->count() === 0;
  }

  #================================================================#
  /**
   * @return array<string, mixed>
   */
  public static function createFakeStudent(int $rank): array
  {
    return [
      "id" => -1,
      "nivel" => null,
      "rank" => $rank,
      "isEnabled" => false,
      "canModifyNivel" => true,
      "games" => [],
      "helpRequests" => [],
    ];
  }
}
