<?php

declare(strict_types=1);

namespace Atelier\Entity;

use Banque\Entity\TextProblem;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class HelpRequest
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_helpRequest')]
  private int $id;

  #[ORM\ManyToOne(inversedBy: 'helpRequests', targetEntity: Student::class)]
  #[ORM\JoinColumn(name: 'id_student', referencedColumnName: 'id_student', nullable: false)]
  private Student $student;

  #[ORM\Column(name: 'helpRequest_requestDate', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  private \DateTimeInterface $requestDate;

  #[ORM\ManyToOne(inversedBy: 'helpRequests', targetEntity: TextProblem::class)]
  #[ORM\JoinColumn(name: 'id_problem', referencedColumnName: "id_problem", nullable: false)]
  private TextProblem $problem;

  #[ORM\Column(name: 'helpRequest_isTreated', options: ["default" => false])]
  private bool $isTreated = false;

  #[ORM\Column(name: 'helpRequest_responseGiven', length: 16)]
  private string $responseGiven;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->requestDate = new \DateTime('now', new \DateTimeZone('UTC'));
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getStudent(): Student
  {
    return $this->student;
  }

  public function setStudent(Student $student): static
  {
    $this->student = $student;

    return $this;
  }

  #================================================================#

  public function getRequestDate(): \DateTimeInterface
  {
    return $this->requestDate;
  }

  public function setRequestDate(\DateTimeInterface $requestDate): static
  {
    $this->requestDate = $requestDate;

    return $this;
  }

  #================================================================#

  public function getProblem(): TextProblem
  {
    return $this->problem;
  }

  public function setProblem(TextProblem $problem): static
  {
    $this->problem = $problem;

    return $this;
  }

  #================================================================#

  public function isTreated(): bool
  {
    return $this->isTreated;
  }

  public function setIsTreated(bool $isTreated): static
  {
    $this->isTreated = $isTreated;

    return $this;
  }

  #================================================================#

  public function getResponseGiven(): string
  {
    return $this->responseGiven;
  }

  public function setResponseGiven(string $responseGiven): static
  {
    $this->responseGiven = $responseGiven;

    return $this;
  }
}
