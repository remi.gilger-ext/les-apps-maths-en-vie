<?php

declare(strict_types=1);

namespace Atelier\Entity;

use Atelier\Enum\AidType;
use Atelier\Enum\GameOrigin;
use Atelier\Enum\GameType;
use Atelier\Enum\ProblemStateEnum;
use Atelier\Enum\SchoolNivel;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Shared\Enum\SchoolZone;

#[ORM\Entity]
#[ORM\Table(name: '`game`')]
class Game
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_game')]
  private int $id;

  #[ORM\ManyToOne(inversedBy: 'games', targetEntity: Student::class)]
  #[ORM\JoinColumn(name: 'id_student', referencedColumnName: 'id_student')]
  private ?Student $student = null;

  #[ORM\Column(name: 'game_playingDate', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  private \DateTimeInterface $playingDate;

  #[ORM\Column(name: 'game_period', nullable: true)]
  private ?string $period = null;

  #[ORM\Column(name: 'game_type', type: 'string', length: 16, enumType: GameType::class)]
  private GameType $type;

  #[ORM\Column(name: 'game_data', length: 1024)]
  private string $data;

  #[ORM\Column(name: 'game_uniqueID', length: 23)]
  private string $uniqueID;

  #[ORM\Column(name: 'game_nivel', type: 'string', length: 16, enumType: SchoolNivel::class)]
  private SchoolNivel $nivel;

  #[ORM\Column(name: 'game_origin', type: 'string', length: 16, enumType: GameOrigin::class)]
  private GameOrigin $origin = GameOrigin::Local;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->playingDate = new \DateTime('now', new \DateTimeZone('UTC'));
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getStudent(): ?Student
  {
    return $this->student;
  }

  public function setStudent(?Student $student): static
  {
    $this->student = $student;

    return $this;
  }

  #================================================================#

  public function getPlayingDate(): \DateTimeInterface
  {
    return $this->playingDate;
  }

  public function setPlayingDate(\DateTimeInterface $playingDate): static
  {
    $this->playingDate = $playingDate;

    return $this;
  }

  #================================================================#

  public function getPeriod(): ?string
  {
    return $this->period;
  }

  public function setPeriod(string $period): static
  {
    $this->period = $period;

    return $this;
  }

  #================================================================#

  public function getType(): GameType
  {
    return $this->type;
  }

  public function setType(GameType $type): static
  {
    $this->type = $type;

    return $this;
  }

  #================================================================#

  /** @return array{id: int,
   *                state: value-of<ProblemTypeEnum>,
   *                aidsUsed: value-of<AidType>[],
   *                responseGiven: string}[] */
  public function getData(): array
  {
    return \json_decode($this->data, true);
  }

  /** @param array{id: int, state: value-of<ProblemStateEnum>, aidsUsed: value-of<AidType>[], responseGiven: string}[] $data */
  public function setData(array $data): static
  {
    $this->data = \json_encode($data) ?: "";

    return $this;
  }

  #================================================================#

  public function getUniqueID(): string
  {
    return $this->uniqueID;
  }

  public function setUniqueID(string $uniqueID): static
  {
    $this->uniqueID = $uniqueID;

    return $this;
  }

  #================================================================#

  public function getNivel(): SchoolNivel
  {
    return $this->nivel;
  }

  public function setNivel(SchoolNivel $nivel): static
  {
    $this->nivel = $nivel;

    return $this;
  }

  #================================================================#

  public function getOrigin(): GameOrigin
  {
    return $this->origin;
  }

  public function setOrigin(GameOrigin $origin): static
  {
    $this->origin = $origin;

    return $this;
  }

  #================================================================#
  # Utilities Methods                                              #
  #================================================================#

  /**
   * @return int<1,3>
   */
  public function getEvaluationLevel(): int
  {
    if ($this->type !== GameType::EVALUATION) {
      throw new \ErrorException("The level can be computed, only for evaluation games");
    }

    $data = $this->getData();

    if (\count($data) !== 3) {
      throw new \RuntimeException("The number of problems for the evaluation need to be 3.");
    }

    $correctResponseAmout = 0;
    foreach ($data as $problem) {
      if ($problem["state"] === ProblemStateEnum::ANSWERED_CORRECTLY->value) {
        $correctResponseAmout++;
      }
    }

    switch ($correctResponseAmout) {
      case 0:
      case 1:
        return 1;
      case 2:
        return 2;
      case 3:
        return 3;
      default:
        throw new \LogicException("Can't have more than 3 correct response ");
        break;
    }
  }

  #================================================================#

  public function getScore(): int
  {
    $data = $this->getData();

    $score = 30; // We start with 30 points based on the aidsUsed
    foreach ($data as $problem) {
      if ($problem["state"] === ProblemStateEnum::ANSWERED_CORRECTLY->value) {
        $score += 10;
      }

      $score -= (10 * \count($problem["aidsUsed"]));
    }

    return $score;
  }

  #================================================================#

  /**
   * @return int<1,3>
   */
  public function getTrainingLevel(): int
  {
    if ($this->type !== GameType::TRAINING) {
      throw new \ErrorException("The level can be computed, only for training games");
    }

    $data = $this->getData();

    if (\count($data) !== 8) {
      throw new \RuntimeException("The number of problems for the training need to be 8.");
    }

    $score = $this->getScore();

    if ($score > 0 && $score <= 40) {
      return 1;
    } elseif ($score <= 60) {
      return 2;
    } else {
      return 3;
    }
  }

  #================================================================#

  /**
   * The games need to come from the same student and need to be traning games
   *
   * @param Game[] $games
   */
  public static function getDailyGames(array $games, SchoolZone $schoolZone): int
  {
    $dailyGamesAmout = 0;

    $today = (new \DateTime('now', new \DateTimeZone($schoolZone->getTimeZone())))->format("Y/m/d");
    foreach ($games as $game) {
      $gameTime = \DateTime::createFromInterface($game->getPlayingDate());
      $gameTime->setTimezone(new \DateTimeZone($schoolZone->getTimeZone()));

      if ($today === $gameTime->format("Y/m/d")) {
        $dailyGamesAmout++;
      }
    }

    return $dailyGamesAmout;
  }
}
