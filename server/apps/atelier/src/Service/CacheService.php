<?php

declare(strict_types=1);

namespace Atelier\Service;

use Atelier\Repository\GameRepository;
use Atelier\Repository\StudentRepository;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class CacheService
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private GameRepository $gameRepository,
    private StudentRepository $studentRepository,
    private TagAwareCacheInterface $cache
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * To get the atelier stats
   *
   * @param boolean $remove
   * @return ($remove is true ? null : array<string, mixed>)
   */
  public function getAtelierStats(bool $remove = false): ?array
  {
    if ($remove) {
      $this->cache->delete('atelier-stats');
      return null;
    }

    return $this->cache->get('atelier-stats', function (ItemInterface $item) {
      $item->expiresAfter(300); // 5 min

      $classesCount = $this->studentRepository->getNumberOfClass();
      $studentsCount = $this->studentRepository->count([]);
      $problemsResolved = $this->gameRepository->count([]) * 5;

      return [
        'classesCount' => $classesCount,
        'studentsCount' => $studentsCount,
        'problemsResolved' => $problemsResolved,
      ];
    });
  }
}
