<?php

declare(strict_types=1);

namespace Atelier\Service;

use Atelier\Entity\Game;
use Atelier\Entity\HelpRequest;
use Atelier\Entity\Student;
use Atelier\Enum\GameType;
use Banque\Entity\TextProblem;

class ParseToArrayService
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Parse a Student to an array
   *
   * @return array<string, mixed>
   */
  public function parseStudent(Student $student): array
  {
    return [
      "id" => $student->getId(),
      "nivel" => $student->getNivel()?->value,
      "rank" => $student->getRank(),
      "isEnabled" => $student->isEnabled(),
      "canModifyNivel" => $student->canModifyNivel(),
      'games' => \array_map(function (Game $game) {
        return $this->parseGame($game);
      }, $student->getGames()->toArray()),
      'helpRequests' => \array_map(function (HelpRequest $helpRequest) {
        return $this->parseHelpRequest($helpRequest);
      }, [...\array_filter(
        $student->getHelpRequests()->toArray(),
        function (HelpRequest $helpRequest) {
          return !$helpRequest->isTreated();
        }
      )]),
    ];
  }

  #================================================================#

  /**
   * Parse a Game to an array
   *
   * @return array<string, mixed>
   */
  public function parseGame(Game $game): array
  {
    $data = [
      "id" => $game->getId(),
      "period" => $game->getPeriod(),
      "type" => $game->getType(),
      "score" => $game->getScore()
    ];

    if ($game->getType() === GameType::EVALUATION) {
      $data["evaluationLevel"] = $game->getEvaluationLevel();
    }

    if ($game->getType() === GameType::TRAINING) {
      $data["trainingLevel"] = $game->getTrainingLevel();
    }

    return $data;
  }

  #================================================================#

  /**
   * Parse a HelpRequest to an array
   *
   * @return array<string, mixed>
   */
  public function parseHelpRequest(HelpRequest $helpRequest): array
  {
    return [
      "id" => $helpRequest->getId(),
      "requestDate" => $helpRequest->getRequestDate()->getTimestamp(),
      "isTreated" => $helpRequest->isTreated(),
      "problem" => $this->parseApplicationProblem($helpRequest->getProblem()),
      "responseGiven" => $helpRequest->getResponseGiven(),
    ];
  }

  #================================================================#

  /**
   * Parse a problem application to an array
   *
   * @return array<string, mixed>
   */
  public function parseApplicationProblem(TextProblem $problem): array
  {
    return [
      'id' => $problem->getId(),
      'title' => $problem->getTitle(),
      'statement' => $problem->getStatement(),
      'nivel' => $problem->getNivel(),
      'type' => $problem->getType(),
      'response' => $problem->getResponse(),
      'responseSentence' => $problem->getResponseSentence(),
      'helpSentence1' => $problem->getHelpSentence1(),
      'helpSentence2' => $problem->getHelpSentence2(),
      'responseExplication' => $problem->getResponseExplication(),
    ];
  }
}
