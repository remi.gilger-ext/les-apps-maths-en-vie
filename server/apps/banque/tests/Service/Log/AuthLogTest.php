<?php

declare(strict_types=1);

namespace Banque\Tests\Service\Log;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Shared\Entity\User;
use Shared\Service\Log\AuthLog;

class AuthLogTest extends TestCase
{
  private User $user;
  private MockObject & LoggerInterface $mockLogger;

  protected function setUp(): void
  {
    $this->user = new User();
    $this->user->setEmail("remi@exemple.com");

    $this->mockLogger = $this->createMock(LoggerInterface::class);
  }

  #================================================================#

  public function testRegisterSuccessLog(): void
  {
    $this->mockLogger->expects($this->once())
      ->method('info')
      ->with($this->stringContains('[REGISTER] : 1.2.3.4 remi@exemple.com'));

    $authLogger = new AuthLog($this->mockLogger);

    $authLogger->registerSuccess($this->user, "1.2.3.4");
  }

  #================================================================#

  public function testRegisterConfirmationLog(): void
  {
    $this->mockLogger->expects($this->once())
      ->method('info')
      ->with($this->stringContains("[REGISTER-CONFIRMATION] : 1.2.3.4 remi@exemple.com"));

    $authLogger = new AuthLog($this->mockLogger);

    $authLogger->registerConfirmation($this->user, "1.2.3.4");
  }

  #================================================================#

  public function testLoginSuccessLog(): void
  {
    $this->mockLogger->expects($this->once())
      ->method('info')
      ->with($this->stringContains("[LOGIN-SUCCESS] : 1.2.3.4 remi@exemple.com"));

    $authLogger = new AuthLog($this->mockLogger);

    $authLogger->loginSuccess($this->user, "1.2.3.4");
  }

  #================================================================#

  public function testLoginFailureLog(): void
  {
    $this->mockLogger->expects($this->once())
      ->method('warning')
      ->with($this->stringContains("[LOGIN-FAILURE] : 1.2.3.4 remi@exemple.com, invalide content"));

    $authLogger = new AuthLog($this->mockLogger);

    $authLogger->loginFailure("remi@exemple.com", "invalide content", "1.2.3.4");
  }

  #================================================================#

  public function testRemoveAccountLog(): void
  {
    $this->mockLogger->expects($this->once())
      ->method('warning')
      ->with($this->stringContains("[REMOVE-ACCOUNT] : 1.2.3.4 remi@exemple.com"));

    $authLogger = new AuthLog($this->mockLogger);

    $authLogger->removeAccount("remi@exemple.com", "1.2.3.4");
  }

  #================================================================#

  public function testPasswordResetRequestLog(): void
  {
    $this->mockLogger->expects($this->once())
      ->method('warning')
      ->with($this->stringContains("[RESET-PASSWORD-REQUEST] : 1.2.3.4 remi@exemple.com"));

    $authLogger = new AuthLog($this->mockLogger);

    $authLogger->passwordResetRequest("remi@exemple.com", "1.2.3.4");
  }

  #================================================================#

  public function testPasswordResetLog(): void
  {
    $this->mockLogger->expects($this->once())
      ->method('warning')
      ->with($this->stringContains("[RESET-PASSWORD] : 1.2.3.4 remi@exemple.com"));

    $authLogger = new AuthLog($this->mockLogger);

    $authLogger->passwordReset("remi@exemple.com", "1.2.3.4");
  }

  #================================================================#

  public function testResendVerifyEmailLog(): void
  {
    $this->mockLogger->expects($this->once())
      ->method('warning')
      ->with($this->stringContains("[RESEND-VERIFICATION] : 1.2.3.4 remi@exemple.com"));

    $authLogger = new AuthLog($this->mockLogger);

    $authLogger->resendVerifyEmail("remi@exemple.com", "1.2.3.4");
  }

  #================================================================#

  public function testPasswordChangedLog(): void
  {
    $this->mockLogger->expects($this->once())
      ->method('warning')
      ->with($this->stringContains("[PASSWORD-CHANGED] : 1.2.3.4 remi@exemple.com"));

    $authLogger = new AuthLog($this->mockLogger);

    $authLogger->passwordChanged("remi@exemple.com", "1.2.3.4");
  }
}
