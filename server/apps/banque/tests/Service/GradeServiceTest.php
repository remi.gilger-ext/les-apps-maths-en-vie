<?php

declare(strict_types=1);

namespace Banque\Tests\Service;

use Banque\Enum\GradeEnum;
use Banque\Service\CacheService;
use Banque\Service\GradeService;
use Banque\Service\MessageService;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Repository\UserRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GradeServiceTest extends TestCase
{
  /** @return array{int, GradeEnum}[] */
  public static function paramsGetGrade(): array
  {
    return [
      [-1, GradeEnum::NoGrade],
      [0, GradeEnum::NoGrade],
      [1, GradeEnum::NoGrade],
      [3, GradeEnum::Novice],
      [99, GradeEnum::Expert],
      [100, GradeEnum::Certifié],
      [101, GradeEnum::Certifié],
    ];
  }

  #================================================================#

  /** @return array{int[], GradeEnum, GradeEnum, bool, string}[] */
  public static function paramsUpdateGrade(): array
  {
    return [
      [[0], GradeEnum::NoGrade, GradeEnum::NoGrade, false, "undefined"],
      [[3], GradeEnum::NoGrade, GradeEnum::Novice, true, GradeEnum::Novice->name],
      [[3], GradeEnum::Novice, GradeEnum::Novice, false, "undefined"],
      [[99], GradeEnum::Expert, GradeEnum::Expert, false, "undefined"],
      [[100], GradeEnum::Expert, GradeEnum::Certifié, true, GradeEnum::Certifié->name],
      [[100], GradeEnum::Débutant, GradeEnum::Certifié, true, GradeEnum::Certifié->name],
      [[101], GradeEnum::Certifié, GradeEnum::Certifié, false, "undefined"],
    ];
  }

  #================================================================#

  /** @dataProvider paramsGetGrade */
  public function testGettingGradeFromNUmberOfPublicProblems(int $numberOfPublicProblems, GradeEnum $expectedGrade): void
  {
    $gradeService = new GradeService($this->createMock(CacheService::class), $this->createMock(EntityManagerInterface::class), $this->createMock(MessageService::class), $this->createMock(UserRepository::class));

    $this->assertSame($expectedGrade->value, $gradeService->getGrade($numberOfPublicProblems));
  }

  #================================================================#

  /**
   * @param int[] $numberProblems
   *
   * @dataProvider paramsUpdateGrade
   */
  public function testUpdateGrade(array $numberProblems, GradeEnum $actualGrade, GradeEnum $expectedGrade, bool $expectedGradeUpdated, string $newGrade): void
  {
    $user = new User();
    $user->setGrade($actualGrade->value);
    $user->setUsername("remi");

    $adminUser = new User();

    /** @var MockObject&CacheService $cacheServiceMock */
    $cacheServiceMock = $this->createMock(CacheService::class);
    $cacheServiceMock->method("nbProblemPublic")
      ->willReturn($numberProblems);

    /** @var MockObject&UserRepository $userRepositoryMock */
    $userRepositoryMock = $this->createMock(UserRepository::class);
    $userRepositoryMock->method("findOneBy")
      ->willReturn($adminUser);

    /** @var MockObject&MessageService $messageServiceMock */
    $messageServiceMock = $this->createMock(MessageService::class);
    $messageServiceMock->expects($expectedGradeUpdated ? $this->once() : $this->never())
      ->method("sendMessage")
      ->with($adminUser, $user, $this->anything(), $this->stringContains($newGrade));

    $gradeService = new GradeService($cacheServiceMock, $this->createMock(EntityManagerInterface::class), $messageServiceMock, $userRepositoryMock);

    $gradeService->updateGrade($user);

    $this->assertSame($expectedGrade->value, $user->getGrade());
  }
}
