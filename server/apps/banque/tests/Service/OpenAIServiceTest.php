<?php

declare(strict_types=1);

namespace Banque\Tests\Service;

use Banque\Entity\TextProblem;
use Banque\Enum\GenerateAiTypeEnum;
use Banque\Enum\ResponseStateEnum;
use Banque\Service\OpenAIService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class OpenAIServiceTest extends TestCase
{
  public static function paramsGenerateIA(): array
  {
    return [
      [[], null, null, ResponseStateEnum::Conflict, null, null, ResponseStateEnum::Conflict],
      [[], null, null, ResponseStateEnum::AnsweredByIa, null, null, ResponseStateEnum::AnsweredByIa],
      [[], ["response" => "2", "responseSentence" => "responseSentence"], null, ResponseStateEnum::Verified, "responseSentence", null, ResponseStateEnum::Verified],
      [["2"], ["response" => "2", "responseSentence" => "responseSentence"], null, ResponseStateEnum::NoResponse, "responseSentence", "2", ResponseStateEnum::AnsweredByIa],
      [["2"], ["response" => null, "responseSentence" => "responseSentence"], null, ResponseStateEnum::NoResponse, null, null, ResponseStateEnum::NoResponse],
      [[null], ["response" => "2", "responseSentence" => "responseSentence"], null, ResponseStateEnum::NoResponse, null, null, ResponseStateEnum::NoResponse],
      [[null], ["response" => null, "responseSentence" => "responseSentence"], null, ResponseStateEnum::NoResponse, null, null, ResponseStateEnum::NoResponse],
      [["2", "2", "2", "2"], ["response" => "3", "responseSentence" => "responseSentence"], null, ResponseStateEnum::NoResponse, "responseSentence", "2", ResponseStateEnum::AnsweredByIa],
      [["2", "2", "5", "2"], ["response" => "3", "responseSentence" => "responseSentence"], null, ResponseStateEnum::NoResponse, null, null, ResponseStateEnum::NoResponse],
      // When IA is agree with human --> problem become verified
      [["2"], ["response" => "2", "responseSentence" => "responseSentence"], "2", ResponseStateEnum::AnsweredByHuman, "responseSentence", "2", ResponseStateEnum::Verified],
      [["3"], ["response" => "3", "responseSentence" => "responseSentence"], "2", ResponseStateEnum::AnsweredByHuman, "responseSentence", "2;3", ResponseStateEnum::Conflict],
      // To test that 2.1 and 2.10 will be equal for the response
      [["2,10"], ["response" => "2,10", "responseSentence" => "responseSentence"], "2,1", ResponseStateEnum::AnsweredByHuman, "responseSentence", "2,1", ResponseStateEnum::Verified],
    ];
  }

  #================================================================#

  public static function paramsResponsesEqual(): array
  {
    return [
      ["2", "2", true],
      ["2.0", "2", true],
      ["2.50", "2.5", true],
      ["2.1", "2,1", true],
      ["2.1", "2,2", false],
      ["3", "9/3", true],
      ["3", "8/3", false],
      ["1/3", "3/9", true],
      ["1/3", "0.3", false],
      ["1/3", "0.33", true],
      ["1/4", "0.25", true],
      [null, "0.25", false],
      ["1 000", "1000", true],
      ["1 000", "1 000", true],
      ["0", "0", true],
    ];
  }

  #================================================================#

  /** @dataProvider paramsGenerateIA */
  public function testGenerateIA(array $responses, ?array $responseSentence, ?string $response, ResponseStateEnum $responseState, ?string $responseSentenceExpected, ?string $responseExpected, ResponseStateEnum $responseStateExpected): void
  {
    $problem = new TextProblem();
    $problem->setStatement("This is the problem content");
    $problem->setResponseState($responseState);
    $problem->setResponse($response);
    $problem->setTag(TextProblem::TAG_COMMUNITY_PUBLIC);

    /** @var MockObject&OpenAIService $openAIServiceMock */
    $openAIServiceMock = $this->getMockBuilder(OpenAIService::class)->onlyMethods(['generateResponseSentence', 'generateResponse', 'generateResponseExplication'])->setConstructorArgs(["", $this->createMock(EntityManagerInterface::class)])->getMock();

    $openAIServiceMock->method("generateResponse")->willReturnOnConsecutiveCalls(...$responses);
    $openAIServiceMock->method("generateResponseSentence")->willReturn($responseSentence);

    $openAIServiceMock->generateIA($problem, GenerateAiTypeEnum::Response);

    $this->assertSame($responseSentenceExpected, $problem->getResponseSentence());
    $this->assertSame($responseExpected, $problem->getResponse());
    $this->assertSame($responseStateExpected, $problem->getResponseState());
  }

  #================================================================#

  /** @dataProvider paramsResponsesEqual */
  public function testResponsesEqual(?string $response1, ?string $response2, bool $isEqual): void
  {
    $openAIService = new OpenAIService("", $this->createMock(EntityManagerInterface::class));

    $result = $openAIService->responsesEqual($response1, $response2);

    $this->assertSame($isEqual, $result);
  }
}
