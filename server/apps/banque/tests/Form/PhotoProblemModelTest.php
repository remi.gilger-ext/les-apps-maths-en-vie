<?php

declare(strict_types=1);

namespace Banque\Tests\Form;

use Banque\Entity\Problem;
use Banque\Entity\PhotoProblem;
use Banque\Form\PhotoProblemModel;
use Banque\Service\ImageService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;

class PhotoProblemModelTest extends TestCase
{
  private MockObject & ImageService $mockImageService;

  protected function setUp(): void
  {
    $this->mockImageService = $this->createMock(ImageService::class);
  }

  #================================================================#

  public static function tearDownAfterClass(): void
  {
    $files = \glob('/tmp/sf*');
    foreach ($files as $file) {
      if (\is_file($file)) {
        \unlink($file);
      }
    }
    $files = \glob('/tmp/*.png');
    foreach ($files as $file) {
      if (\is_file($file)) {
        \unlink($file);
      }
    }
  }

  #================================================================#

  public static function paramsProvider(): array
  {
    return [
      [[ // Normal PhotoProblem
        "statement" => "Statement exemple",
        "nivel" => "ce1",
        "public" => "yes",
        "attachment" => ["data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII"],
        "type" => ["meas_1", "numb_3", "geom_1"],
        "imageData" => "12.23;2.2;34.2344;0.0001",
        "imageRotation" => 90,
        "addToPhoto" => false,
        "photoType" => []
      ], []],
      [[ // Normal PhotoProblem
        "statement" => "Statement exemple",
        "nivel" => "ce1",
        "public" => "yes",
        "attachment" => ["data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII"],
        "type" => ["meas_1", "numb_3", "geom_1"],
        "imageData" => "12.23;2.2;34.2344;0.0001",
        "imageRotation" => 90,
        "addToPhoto" => true,
        "photoType" => ["meas_1", "numb_3"]
      ], []],
    ];
  }

  #================================================================#

  /** @dataProvider paramsProvider */
  public function testValidation(array $params, array $errorsName): void
  {
    $problemValidator = new PhotoProblemModel($this->mockImageService, "/tmp");

    $problemValidator->fillInstance(new InputBag($params));

    $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

    $errors = $validator->validate($problemValidator);

    /** @var ConstraintViolation $error */
    foreach ($errors as $error) {
      $this->assertContains($error->getPropertyPath(), $errorsName);
    }

    $this->assertSame(\count($errorsName), $errors->count());
  }

  #================================================================#

  public function testRemovingModifyError(): void
  {
    $problemValidator = new PhotoProblemModel($this->mockImageService, "/tmp");

    $listErrors = new ConstraintViolationList([
      new ConstraintViolation("The value is invalid", null, [], null, "attachment", "invalid value"),
      new ConstraintViolation("The value is invalid", null, [], null, "attachment", "another invalid value"),
      new ConstraintViolation("The value is invalid", null, [], null, "public", "invalid value"),
      new ConstraintViolation("The value is invalid", null, [], null, "public", "another invalid value"),
      new ConstraintViolation("This another value is invalid", null, [], null, "type", "another invalid value")
    ]);

    $problemValidator->removeModifyingErrors($listErrors);

    $this->assertSame(1, \count($listErrors));
    $this->assertArrayHasKey(4, $listErrors);
  }

  #================================================================#

  public function testCreateInstance(): void
  {
    $problemValidator = new PhotoProblemModel($this->mockImageService, "/tmp");

    $problemValidator->fillInstance(new InputBag([ // Normal problem
      "statement" => "Statement exemple",
      "nivel" => "ce1",
      "attachment" => ["data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII"],
      "type" => ["meas_1", "numb_3", "geom_1"],
      "public" => "yes",
    ]));

    $this->mockImageService->expects($this->once())
      ->method("saveImage");

    $problem = $problemValidator->createInstance();

    $this->assertSame("Statement exemple", $problem->getStatement());
    $this->assertSame("ce1", $problem->getNivel());
    $this->assertSame("meas_1;numb_3;geom_1", $problem->getType());
    $this->assertSame(Problem::TAG_COMMUNITY_PUBLIC_WAIT, $problem->getTag());

    $this->assertSame(false, $problem->isVerified());
  }

  #================================================================#

  public function testUpdateInstance(): void
  {
    $problem = new PhotoProblem();
    $problem->setStatement("old statement");
    $problem->setNivel("cm1");
    $problem->setType("geom_9;geom_8");

    $problemValidator = new PhotoProblemModel($this->mockImageService, "/tmp");

    $this->mockImageService->expects($this->never())
      ->method("saveImage");

    $problemValidator->fillInstance(new InputBag([ // Propor problem
      "statement" => "new statement",
      "nivel" => "ce1",
      "type" => ["meas_1", "geom_2"]
    ]));

    $problemValidator->updateInstance($problem);

    $this->assertSame("new statement", $problem->getStatement());
    $this->assertSame("ce1", $problem->getNivel());
    $this->assertSame("meas_1;geom_2", $problem->getType());
  }

  #================================================================#

  public function testUpdateInstanceInReview(): void
  {
    $problem = new PhotoProblem();
    $problem->setStatement("old statement");
    $problem->setNivel("cm1");
    $problem->setType("geom_9;geom_8");
    $problem->setAttachment("image.png");

    $problemValidator = new PhotoProblemModel($this->mockImageService, "/tmp");

    $this->mockImageService->expects($this->once())
      ->method("saveImage")
      ->with("image.png");

    $problemValidator->fillInstance(new InputBag([ // Propor problem
      "statement" => "new statement",
      "nivel" => "ce1",
      "type" => ["meas_1", "geom_2"]
    ]));

    $problemValidator->updateInstance($problem, true);

    $this->assertSame("new statement", $problem->getStatement());
    $this->assertSame("ce1", $problem->getNivel());
    $this->assertSame("meas_1;geom_2", $problem->getType());
  }
}
