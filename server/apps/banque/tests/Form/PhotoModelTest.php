<?php

declare(strict_types=1);

namespace Banque\Tests\Form;

use Banque\Entity\Problem;
use Banque\Entity\Photo;
use Banque\Form\PhotoModel;
use Banque\Service\ImageService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;

class PhotoModelTest extends TestCase
{
  private MockObject & ImageService $mockImageService;

  protected function setUp(): void
  {
    $this->mockImageService = $this->createMock(ImageService::class);
  }

  #================================================================#

  public static function tearDownAfterClass(): void
  {
    $files = \glob('/tmp/sf*');
    foreach ($files as $file) {
      if (\is_file($file)) {
        \unlink($file);
      }
    }
    $files = \glob('/tmp/*.png');
    foreach ($files as $file) {
      if (\is_file($file)) {
        \unlink($file);
      }
    }
  }

  #================================================================#

  public static function paramsProvider(): array
  {
    return [
      [[ // Normal photo
        "attachment" => ["data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII"],
        "type" => ["meas_1", "numb_3", "geom_1"],
        "imageData" => "12.23;2.2;34.2344;0.0001",
        "imageRotation" => 90
      ], []],
      [[
        "attachment" => ["sdfsdffdsfsdf"],
        "type" => ["ds", "dsc"],
        "imageData" => "qsfdqfdq",
        "imageRotation" => "2"
      ], ["attachment", "type", "imageData", "imageRotation"]],
    ];
  }

  #================================================================#

  /** @dataProvider paramsProvider */
  public function testValidation(array $params, array $errorsName): void
  {
    $problemValidator = new PhotoModel($this->mockImageService, "/tmp");

    $problemValidator->fillInstance(new InputBag($params));

    $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

    $errors = $validator->validate($problemValidator);

    /** @var ConstraintViolation $error */
    foreach ($errors as $error) {
      $this->assertContains($error->getPropertyPath(), $errorsName);
    }

    $this->assertSame(\count($errorsName), $errors->count());
  }

  #================================================================#

  public function testRemovingModifyError(): void
  {
    $problemValidator = new PhotoModel($this->mockImageService, "/tmp");

    $listErrors = new ConstraintViolationList([
      new ConstraintViolation("The value is invalid", null, [], null, "attachment", "invalid value"),
      new ConstraintViolation("The value is invalid", null, [], null, "attachment", "another invalid value"),
      new ConstraintViolation("This another value is invalid", null, [], null, "type", "another invalid value")
    ]);

    $problemValidator->removeModifyingErrors($listErrors);

    $this->assertSame(1, \count($listErrors));
    $this->assertArrayHasKey(2, $listErrors);
  }

  #================================================================#

  public function testCreateInstance(): void
  {
    $problemValidator = new PhotoModel($this->mockImageService, "/tmp");

    $problemValidator->fillInstance(new InputBag([ // Normal problem
      "attachment" => ["data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII"],
      "type" => ["meas_1", "numb_3", "geom_1"],
    ]));

    $this->mockImageService->expects($this->once())
      ->method("saveImage");

    $problem = $problemValidator->createInstance();

    $this->assertSame("meas_1;numb_3;geom_1", $problem->getType());
    $this->assertSame(Problem::TAG_COMMUNITY_PUBLIC_WAIT, $problem->getTag());

    $this->assertSame(false, $problem->isVerified());
  }

  #================================================================#

  public function testUpdateInstance(): void
  {
    $problem = new Photo();
    $problem->setType("geom_9;geom_8");

    $problemValidator = new PhotoModel($this->mockImageService, "/tmp");

    $this->mockImageService->expects($this->never())
      ->method("saveImage");

    $problemValidator->fillInstance(new InputBag([ // Propor problem
      "type" => ["meas_1", "geom_2"]
    ]));

    $problemValidator->updateInstance($problem);

    $this->assertSame("meas_1;geom_2", $problem->getType());
  }

  #================================================================#

  public function testUpdateInstanceInReview(): void
  {
    $problem = new Photo();
    $problem->setType("geom_9;geom_8");
    $problem->setAttachment("image.png");

    $problemValidator = new PhotoModel($this->mockImageService, "/");

    $this->mockImageService->expects($this->once())
      ->method("saveImage")
      ->with("image.png");

    $problemValidator->fillInstance(new InputBag([ // Propor problem
      "type" => ["meas_1", "geom_2"]
    ]));

    $problemValidator->updateInstance($problem, true);

    $this->assertSame("meas_1;geom_2", $problem->getType());
  }
}
