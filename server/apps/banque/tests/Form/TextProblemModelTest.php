<?php

declare(strict_types=1);

namespace Banque\Tests\Form;

use Banque\Entity\Problem;
use Banque\Entity\TextProblem;
use Banque\Enum\TextProblemTypeEnum;
use Banque\Form\TextProblemModel;
use Banque\Service\OpenAIService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;

class TextProblemModelTest extends TestCase
{
  public static function paramsProvider(): array
  {
    return [
      [[ // Normal problem
        "primaryType" => "normal",
        "title" => "Title exemple",
        "statement" => "Statement exemple",
        "nivel" => "ce1",
        "type" => TextProblemTypeEnum::rdp->name,
        "public" => "yes",
        "response" => "12.3",
        "isExclude" => false
      ], []],
      [[ // Step problem
        "primaryType" => "step",
        "title" => "Title exemple",
        "statement" => "Statement exemple",
        "nivel" => "ce1",
        "type" => TextProblemTypeEnum::mult->name,
        "public" => "yes",
        "response" => "12.3",
        "isExclude" => false
      ], []],
      [[ // Step problem with wrong type
        "primaryType" => "step",
        "title" => "Title exemple",
        "statement" => "Statement exemple",
        "nivel" => "ce1",
        "type" => TextProblemTypeEnum::rdp->name,
        "public" => "yes",
        "response" => "12.3",
        "isExclude" => false
      ], ["type"]],
      [[ // Frac problem
        "primaryType" => "frac",
        "title" => "Title exemple",
        "statement" => "Statement exemple",
        "nivel" => "ce1",
        "type" => TextProblemTypeEnum::rdp->name,
        "public" => "no",
        "response" => "2/3",
        "isExclude" => true
      ], []],
      [[
        "primaryType" => "normal",
        "title" => "Title exemple",
        "statement" => "Statement exemple",
        "nivel" => "ce1",
        "type" => TextProblemTypeEnum::frac->name, // type canno't be a primary type
        "public" => "no",
        "response" => "12.3",
        "isExclude" => false
      ], ["type"]],
      [[ // Normal problem without a type
        "primaryType" => "normal",
        "title" => "Title exemple",
        "statement" => "Statement exemple",
        "nivel" => "ce1",
        "public" => "no",
        "response" => "12.3",
        "isExclude" => false
      ], ["type"]],
      [[], ["primaryType", "title", "statement", "nivel", "public"]],
      [[ // Step problem without a type
        "primaryType" => "normal",
        "title" => "Title exemple",
        "statement" => "Statement exemple",
        "nivel" => "ce1",
        "public" => "no",
        "response" => "12.3",
        "isExclude" => false
      ], ["type"]],
    ];
  }

  #================================================================#

  /** @dataProvider paramsProvider */
  public function testValidation(array $params, array $errorsName): void
  {
    $problemValidator = new TextProblemModel($this->createMock(OpenAIService::class));

    $problemValidator->fillInstance(new InputBag($params));

    $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

    $errors = $validator->validate($problemValidator);

    /** @var ConstraintViolation $error */
    foreach ($errors as $error) {
      $this->assertContains($error->getPropertyPath(), $errorsName);
    }

    $this->assertSame(\count($errorsName), $errors->count());
  }

  #================================================================#

  public function testRemovingModifyError(): void
  {
    $problemValidator = new TextProblemModel($this->createMock(OpenAIService::class));

    $listErrors = new ConstraintViolationList([
      new ConstraintViolation("The value is invalid", null, [], null, "public", "invalid value"),
      new ConstraintViolation("The value is invalid", null, [], null, "public", "another public invalid value"),
      new ConstraintViolation("This another value is invalid", null, [], null, "type", "another invalid value")
    ]);

    $problemValidator->removeModifyingErrors($listErrors);

    $this->assertSame(1, \count($listErrors));
    $this->assertArrayHasKey(2, $listErrors);
  }

  #================================================================#

  public function testGetTypeCallback(): void
  {
    $problemValidator = new TextProblemModel($this->createMock(OpenAIService::class));

    $types = $problemValidator->getTypesCallback();

    $this->assertContainsOnly(\string::class, $types);
    $this->assertNotContains(TextProblemTypeEnum::frac->name, $types);
    $this->assertNotContains(TextProblemTypeEnum::propor->name, $types);
  }

  #================================================================#

  public function testCreateInstancePublicAndNormal(): void
  {
    $problemValidator = new TextProblemModel($this->createMock(OpenAIService::class));

    $problemValidator->fillInstance(new InputBag([ // Normal problem
      "primaryType" => "normal",
      "title" => "Title exemple",
      "statement" => "Statement exemple",
      "nivel" => "ce1",
      "type" => TextProblemTypeEnum::rdp->name,
      "public" => "yes",
      "response" => "2/3",
      "isExclude" => true
    ]));

    $problem = $problemValidator->createInstance();

    $this->assertSame("Title exemple", $problem->getTitle());
    $this->assertSame("Statement exemple", $problem->getStatement());
    $this->assertSame("ce1", $problem->getNivel());
    $this->assertSame("rdp", $problem->getType());
    $this->assertSame(Problem::TAG_COMMUNITY_PUBLIC_WAIT, $problem->getTag());
    $this->assertSame(null, $problem->getResponse());
    $this->assertSame(false, $problem->isExclude());

    $this->assertSame(false, $problem->isVerified());
  }

  #================================================================#

  public function testCreateInstancePrivateAndPropor(): void
  {
    $problemValidator = new TextProblemModel($this->createMock(OpenAIService::class));

    $problemValidator->fillInstance(new InputBag([ // Propor problem
      "primaryType" => "propor",
      "title" => "Title exemple",
      "statement" => "Statement exemple",
      "nivel" => "cm2",
      "public" => "no"
    ]));

    $problem = $problemValidator->createInstance();

    $this->assertSame("Title exemple", $problem->getTitle());
    $this->assertSame("Statement exemple", $problem->getStatement());
    $this->assertSame("cm2", $problem->getNivel());
    $this->assertSame("propor", $problem->getType());
    $this->assertSame(Problem::TAG_COMMUNITY_PRIVATE, $problem->getTag());

    $this->assertSame(false, $problem->isVerified());
  }

  #================================================================#

  public function testUpdateInstance(): void
  {
    $problem = new TextProblem();
    $problem->setType("rdt");
    $problem->setTitle("old title");
    $problem->setStatement("old statement");
    $problem->setNivel("ce1");
    $problem->setResponse("2/3");
    $problem->isExclude(false);

    $problemValidator = new TextProblemModel($this->createMock(OpenAIService::class));

    $problemValidator->fillInstance(new InputBag([ // Propor problem
      "primaryType" => "propor",
      "title" => "new title",
      "statement" => "new statement",
      "nivel" => "cm2",
      "response" => "2.3",
      "isExclude" => true

    ]));

    $problemValidator->updateInstance($problem);

    $this->assertSame("new title", $problem->getTitle());
    $this->assertSame("new statement", $problem->getStatement());
    $this->assertSame("cm2", $problem->getNivel());
    $this->assertSame("propor", $problem->getType());
    $this->assertSame(null, $problem->getResponse());
    $this->assertSame(true, $problem->isExclude());
  }
}
