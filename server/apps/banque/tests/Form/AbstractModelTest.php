<?php

declare(strict_types=1);

namespace Banque\Tests\Form;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shared\Form\AbstractModel;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class AbstractModelTest extends TestCase
{
  public function testGettingFormatErrors(): void
  {
    /** @var MockObject&AbstractModel */
    $mock = $this->getMockForAbstractClass(AbstractModel::class);

    $listErrors = new ConstraintViolationList([
      new ConstraintViolation("The value is invalid", null, [], null, "propertyExemple", "invalid value"),
      new ConstraintViolation("This another value is invalid", null, [], null, "anotherPropertyExemple", "another invalid value")
    ]);
    $errors = $mock->getFormatErrors($listErrors);

    $this->assertIsArray($errors);
    $this->assertSame(\count($listErrors), \count($errors), "The list of formatted errors need to be the same length as the input list");
    foreach ($errors as $key => $error) {
      $this->assertIsArray($error);
      $this->assertArrayHasKey("message", $error);
      $this->assertSame($listErrors->get($key)->getMessage(), $error["message"]);
      $this->assertArrayHasKey("name", $error);
      $this->assertSame($listErrors->get($key)->getPropertyPath(), $error["name"]);
    }
  }
}
