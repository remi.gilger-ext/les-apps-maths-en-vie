<?php

declare(strict_types=1);

namespace Banque\Tests;

use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
  public function testTestAreWorking(): void
  {
    $this->assertSame(2, 1 + 1);
  }
}
