<?php

declare(strict_types=1);

namespace Banque\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Shared\Entity\User;

#[ORM\Entity]
class ProblemLike
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column]
  private int $id;

  #[ORM\ManyToOne(inversedBy: 'problemLikes')]
  #[ORM\JoinColumn(name: 'id_user', referencedColumnName: "id_user", nullable: false)]
  private User $user;

  #[ORM\ManyToOne(inversedBy: 'problemLikes')]
  #[ORM\JoinColumn(name: 'id_problem', referencedColumnName: "id_problem", nullable: false)]
  private Problem $problem;

  #[ORM\Column(name: 'problemLike_createdAt', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  private \DateTimeInterface $createdAt;

  #[ORM\Column(name: 'problemLike_value')]
  private int $value = 1;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->createdAt = new \DateTime('now', new \DateTimeZone('UTC'));
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getUser(): User
  {
    return $this->user;
  }

  public function setUser(User $user): static
  {
    $this->user = $user;

    return $this;
  }

  #================================================================#

  public function getProblem(): Problem
  {
    return $this->problem;
  }

  public function setProblem(Problem $problem): static
  {
    $this->problem = $problem;

    return $this;
  }

  #================================================================#

  public function getCreatedAt(): \DateTimeInterface
  {
    return $this->createdAt;
  }

  public function setCreatedAt(\DateTimeInterface $createdAt): static
  {
    $this->createdAt = $createdAt;

    return $this;
  }

  #================================================================#

  public function getValue(): int
  {
    return $this->value;
  }

  public function setValue(int $value): static
  {
    $this->value = $value;

    return $this;
  }
}
