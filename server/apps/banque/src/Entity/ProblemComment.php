<?php

declare(strict_types=1);

namespace Banque\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Shared\Entity\User;

#[ORM\Entity]
class ProblemComment
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column]
  private int $id;

  #[ORM\ManyToOne(inversedBy: 'problemComments')]
  #[ORM\JoinColumn(name: 'id_user', referencedColumnName: 'id_user', nullable: false)]
  private User $user;

  #[ORM\ManyToOne(inversedBy: 'problemComments')]
  #[ORM\JoinColumn(name: 'id_problem', referencedColumnName: 'id_problem', nullable: false)]
  private Problem $problem;

  #[ORM\Column(name: 'problemComment_createdAt', type: Types::DATETIME_MUTABLE)]
  private \DateTimeInterface $createdAt;

  #[ORM\Column(name: 'problemComment_content', length: 512)]
  private string $content = '';

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->createdAt = new \DateTime('now', new \DateTimeZone('UTC'));
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getUser(): User
  {
    return $this->user;
  }

  public function setUser(User $user): static
  {
    $this->user = $user;

    return $this;
  }

  #================================================================#

  public function getProblem(): Problem
  {
    return $this->problem;
  }

  public function setProblem(Problem $problem): static
  {
    $this->problem = $problem;

    return $this;
  }

  #================================================================#

  public function getCreatedAt(): \DateTimeInterface
  {
    return $this->createdAt;
  }

  public function setCreatedAt(\DateTimeInterface $createdAt): static
  {
    $this->createdAt = $createdAt;

    return $this;
  }

  #================================================================#

  public function getContent(): string
  {
    return $this->content;
  }

  public function setContent(string $content): static
  {
    $this->content = $content;

    return $this;
  }
}
