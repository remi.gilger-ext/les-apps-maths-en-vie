<?php

declare(strict_types=1);

namespace Banque\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class ProblemTraduction
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_problemTraduction')]
  private int $id;

  #[ORM\Column(name: 'problemTraduction_englishTitle', length: 64, nullable: true)]
  private ?string $englishTitle = null;

  #[ORM\Column(name: 'problemTraduction_englishStatement', length: 1024, nullable: true)]
  private ?string $englishStatement = null;

  #[ORM\Column(name: 'problemTraduction_italianTitle', length: 64, nullable: true)]
  private ?string $italianTitle = null;

  #[ORM\Column(name: 'problemTraduction_italianStatement', length: 1024, nullable: true)]
  private ?string $italianStatement = null;

  #[ORM\Column(name: 'problemTraduction_spanishTitle', length: 64, nullable: true)]
  private ?string $spanishTitle = null;

  #[ORM\Column(name: 'problemTraduction_spanishStatement', length: 1024, nullable: true)]
  private ?string $spanishStatement = null;

  #[ORM\Column(name: 'problemTraduction_germanTitle', length: 64, nullable: true)]
  private ?string $germanTitle = null;

  #[ORM\Column(name: 'problemTraduction_germanStatement', length: 1024, nullable: true)]
  private ?string $germanStatement = null;

  #[ORM\OneToOne(inversedBy: 'traductions')]
  #[ORM\JoinColumn(name: 'id_problem', referencedColumnName: 'id_problem', nullable: false)]
  private TextProblem $problem;

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getEnglishTitle(): ?string
  {
    return $this->englishTitle;
  }

  public function setEnglishTitle(?string $englishTitle): static
  {
    $this->englishTitle = $englishTitle;

    return $this;
  }

  #================================================================#

  public function getEnglishStatement(): ?string
  {
    return $this->englishStatement;
  }

  public function setEnglishStatement(?string $englishStatement): static
  {
    $this->englishStatement = $englishStatement;

    return $this;
  }

  #================================================================#

  public function getItalianTitle(): ?string
  {
    return $this->italianTitle;
  }

  public function setItalianTitle(?string $italianTitle): static
  {
    $this->italianTitle = $italianTitle;

    return $this;
  }

  #================================================================#

  public function getItalianStatement(): ?string
  {
    return $this->italianStatement;
  }

  public function setItalianStatement(?string $italianStatement): static
  {
    $this->italianStatement = $italianStatement;

    return $this;
  }

  #================================================================#

  public function getSpanishTitle(): ?string
  {
    return $this->spanishTitle;
  }

  public function setSpanishTitle(?string $spanishTitle): static
  {
    $this->spanishTitle = $spanishTitle;

    return $this;
  }

  #================================================================#

  public function getSpanishStatement(): ?string
  {
    return $this->spanishStatement;
  }

  public function setSpanishStatement(?string $spanishStatement): static
  {
    $this->spanishStatement = $spanishStatement;

    return $this;
  }

  #================================================================#

  public function getGermanTitle(): ?string
  {
    return $this->germanTitle;
  }

  public function setGermanTitle(?string $germanTitle): static
  {
    $this->germanTitle = $germanTitle;

    return $this;
  }

  #================================================================#

  public function getGermanStatement(): ?string
  {
    return $this->germanStatement;
  }

  public function setGermanStatement(?string $germanStatement): static
  {
    $this->germanStatement = $germanStatement;

    return $this;
  }

  #================================================================#

  public function getProblem(): TextProblem
  {
    return $this->problem;
  }

  public function setProblem(TextProblem $problem): static
  {
    $this->problem = $problem;

    return $this;
  }
}
