<?php

declare(strict_types=1);

namespace Banque\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Shared\Entity\User;

#[ORM\Entity]
class Message
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_message')]
  private int $id;

  #[ORM\Column(name: 'message_createdAt', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  private \DateTimeInterface $createdAt;

  #[ORM\ManyToOne(inversedBy: 'messagesSent', targetEntity: User::class)]
  #[ORM\JoinColumn(name: 'id_userSender', referencedColumnName: "id_user", nullable: false)]
  private User $sender;

  #[ORM\ManyToOne(inversedBy: 'messagesReceived', targetEntity: User::class)]
  #[ORM\JoinColumn(name: 'id_userReceiver', referencedColumnName: "id_user")]
  private ?User $receiver = null;

  #[ORM\Column(name: 'message_title', length: 128)]
  private string $title;

  #[ORM\Column(name: 'message_content', length: 1024)]
  private string $content;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->createdAt = new \DateTime('now', new \DateTimeZone('UTC'));
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getCreatedAt(): \DateTimeInterface
  {
    return $this->createdAt;
  }

  public function setCreatedAt(\DateTimeInterface $createdAt): static
  {
    $this->createdAt = $createdAt;

    return $this;
  }

  #================================================================#

  public function getSender(): User
  {
    return $this->sender;
  }

  public function setSender(User $sender): static
  {
    $this->sender = $sender;

    return $this;
  }

  #================================================================#

  public function getReceiver(): ?User
  {
    return $this->receiver;
  }

  public function setReceiver(?User $receiver): static
  {
    $this->receiver = $receiver;

    return $this;
  }

  #================================================================#

  public function getTitle(): string
  {
    return $this->title;
  }

  public function setTitle(string $title): static
  {
    $this->title = $title;

    return $this;
  }

  #================================================================#

  public function getContent(): string
  {
    return $this->content;
  }

  public function setContent(string $content): static
  {
    $this->content = $content;

    return $this;
  }
}
