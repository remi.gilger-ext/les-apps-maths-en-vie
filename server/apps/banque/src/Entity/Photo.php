<?php

declare(strict_types=1);

namespace Banque\Entity;

use Banque\Enum\ProblemTypeEnum;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: '`photo`')]
class Photo extends Problem
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Column(name: 'photo_attachment', length: 64)]
  private string $attachment;

  #[ORM\Column(name: 'photo_type', length: 255)]
  private string $type;

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getAttachment(): string
  {
    return $this->attachment;
  }

  public function setAttachment(string $attachment): static
  {
    $this->attachment = $attachment;

    return $this;
  }

  #================================================================#

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): static
  {
    $this->type = $type;

    return $this;
  }

  #================================================================#

  public function getDiscr(): ProblemTypeEnum
  {
    return ProblemTypeEnum::Photo;
  }
}
