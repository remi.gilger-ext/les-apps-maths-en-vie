<?php

declare(strict_types=1);

namespace Banque\Entity;

use Banque\Enum\ProblemTypeEnum;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: '`photoProblem`')]
class PhotoProblem extends Problem
{
  #================================================================#
  # Constants                                                      #
  #================================================================#

  public const MAX_STATEMENT_LENGTH = 500;

  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Column(name: 'photoProblem_attachment', length: 64)]
  private string $attachment;

  #[ORM\Column(name: 'photoProblem_statement', length: 1024)]
  private string $statement;

  #[ORM\Column(name: 'photoProblem_type', length: 255)]
  private string $type;

  #[ORM\Column(name: 'photoProblem_nivel', length: 16)]
  private string $nivel;

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getAttachment(): string
  {
    return $this->attachment;
  }

  public function setAttachment(string $attachment): static
  {
    $this->attachment = $attachment;

    return $this;
  }

  #================================================================#

  public function getStatement(): string
  {
    return $this->statement;
  }

  public function setStatement(string $statement): static
  {
    $this->statement = $statement;

    return $this;
  }

  #================================================================#

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): static
  {
    $this->type = $type;

    return $this;
  }

  #================================================================#

  public function getNivel(): string
  {
    return $this->nivel;
  }

  public function setNivel(string $nivel): static
  {
    $this->nivel = $nivel;

    return $this;
  }

  #================================================================#

  public function getDiscr(): ProblemTypeEnum
  {
    return ProblemTypeEnum::PhotoProblem;
  }
}
