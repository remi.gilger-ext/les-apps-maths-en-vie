<?php

declare(strict_types=1);

namespace Banque\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class MathWeekProblem
{
  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_mathsWeek')]
  private int $id;

  #[ORM\Column(name: 'mathWeek_schoolNameAndCity', type: "string", length: 64)]
  private string $schoolNameAndCity;

  #[ORM\Column(name: 'mathWeek_nivel', type: "string", length: 16)]
  private string $nivel;

  #[ORM\Column(name: 'mathWeek_statement', type: "string", length: 512)]
  private string $statement;

  #[ORM\Column(name: 'mathWeek_photoId')]
  private int $photoId;

  #[ORM\Column(name: 'mathWeek_sendingDate', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  protected \DateTimeInterface $sendingDate;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->sendingDate = new \DateTime('now', new \DateTimeZone('UTC'));
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getSchoolNameAndCity(): string
  {
    return $this->schoolNameAndCity;
  }

  public function setSchoolNameAndCity(string $schoolNameAndCity): static
  {
    $this->schoolNameAndCity = $schoolNameAndCity;

    return $this;
  }

  #================================================================#

  public function getNivel(): string
  {
    return $this->nivel;
  }

  public function setNivel(string $nivel): static
  {
    $this->nivel = $nivel;

    return $this;
  }

  #================================================================#

  public function getStatement(): string
  {
    return $this->statement;
  }

  public function setStatement(string $statement): static
  {
    $this->statement = $statement;

    return $this;
  }

  #================================================================#

  public function getPhotoId(): int
  {
    return $this->photoId;
  }

  public function setPhotoId(int $photoId): static
  {
    $this->photoId = $photoId;

    return $this;
  }

  #================================================================#

  public function getSendingDate(): \DateTimeInterface
  {
    return $this->sendingDate;
  }

  public function setSendingDate(\DateTimeInterface $sendingDate): static
  {
    $this->sendingDate = $sendingDate;

    return $this;
  }
}
