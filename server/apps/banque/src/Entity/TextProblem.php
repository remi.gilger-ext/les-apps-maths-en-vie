<?php

declare(strict_types=1);

namespace Banque\Entity;

use Atelier\Entity\HelpRequest;
use Banque\Enum\ProblemTypeEnum;
use Banque\Enum\ResponseStateEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: '`textProblem`')]
class TextProblem extends Problem
{
  #================================================================#
  # Constants                                                      #
  #================================================================#

  public const MAX_STATEMENT_LENGTH = 500;

  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Column(name: 'textProblem_title', length: 64)]
  private string $title;

  #[ORM\Column(name: 'textProblem_statement', length: 1024)]
  private string $statement;

  #[ORM\Column(name: 'textProblem_nivel', length: 16)]
  private string $nivel;

  #[ORM\Column(name: 'textProblem_type', length: 16)]
  private string $type;

  #[ORM\Column(name: 'textProblem_response', length: 16, nullable: true)]
  private ?string $response = null;

  #[ORM\Column(name: 'textProblem_responseState', type: 'string', length: 16, enumType: ResponseStateEnum::class, options: ["default" => ResponseStateEnum::NoResponse])]
  private ResponseStateEnum $responseState = ResponseStateEnum::NoResponse;

  #[ORM\Column(name: 'textProblem_isExclude', options: ["default" => false])]
  private bool $isExclude = false;

  #[ORM\Column(name: 'textProblem_responseSentence', length: 128, nullable: true)]
  private ?string $responseSentence = null;

  #[ORM\Column(name: 'textProblem_helpSentence1', length: 1024, nullable: true)]
  private ?string $helpSentence1 = null;

  #[ORM\Column(name: 'textProblem_helpSentence2', length: 1024, nullable: true)]
  private ?string $helpSentence2 = null;

  #[ORM\Column(name: 'textProblem_responseExplication', length: 1024, nullable: true)]
  private ?string $responseExplication = null;

  /** @var Collection<int, HelpRequest> */
  #[ORM\OneToMany(targetEntity: HelpRequest::class, mappedBy: 'problem', orphanRemoval: true)]
  private Collection $helpRequests;

  #[ORM\Column(name: 'textProblem_totalScore', options: ["default" => 0])]
  private float $totalScore = 0.;

  #[ORM\Column(name: 'textProblem_amoutAnswered', options: ["default" => 0])]
  private int $amoutAnswered = 0;

  #[ORM\OneToOne(targetEntity: ProblemTraduction::class, mappedBy: 'problem', orphanRemoval: true, cascade: ['persist'])]
  private ?ProblemTraduction $traductions = null;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    parent::__construct();

    $this->helpRequests = new ArrayCollection();
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getTitle(): string
  {
    return $this->title;
  }

  public function setTitle(string $title): static
  {
    $this->title = $title;

    return $this;
  }

  #================================================================#

  public function getStatement(): string
  {
    return $this->statement;
  }

  public function setStatement(string $statement): static
  {
    $this->statement = $statement;

    return $this;
  }

  #================================================================#

  public function getNivel(): string
  {
    return $this->nivel;
  }

  public function setNivel(string $nivel): static
  {
    $this->nivel = $nivel;

    return $this;
  }

  #================================================================#

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): static
  {
    $this->type = $type;

    return $this;
  }

  #================================================================#

  public function getResponse(): ?string
  {
    return $this->response;
  }

  public function setResponse(?string $response): static
  {
    $this->response = $response;

    return $this;
  }

  #================================================================#

  public function getResponseState(): ResponseStateEnum
  {
    return $this->responseState;
  }

  public function setResponseState(ResponseStateEnum $responseState): static
  {
    $this->responseState = $responseState;

    return $this;
  }

  #================================================================#

  public function isExclude(): bool
  {
    return $this->isExclude;
  }

  public function setIsExclude(bool $isExclude): static
  {
    $this->isExclude = $isExclude;

    return $this;
  }

  #================================================================#

  public function getResponseSentence(): ?string
  {
    return $this->responseSentence;
  }

  public function setResponseSentence(?string $responseSentence): static
  {
    $this->responseSentence = $responseSentence;

    return $this;
  }

  #================================================================#

  public function getHelpSentence1(): ?string
  {
    return $this->helpSentence1;
  }

  public function setHelpSentence1(?string $helpSentence1): static
  {
    $this->helpSentence1 = $helpSentence1;

    return $this;
  }

  #================================================================#

  public function getHelpSentence2(): ?string
  {
    return $this->helpSentence2;
  }

  public function setHelpSentence2(?string $helpSentence2): static
  {
    $this->helpSentence2 = $helpSentence2;

    return $this;
  }

  #================================================================#

  public function getResponseExplication(): ?string
  {
    return $this->responseExplication;
  }

  public function setResponseExplication(?string $responseExplication): static
  {
    $this->responseExplication = $responseExplication;

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, HelpRequest>
   */
  public function getHelpRequests(): Collection
  {
    return $this->helpRequests;
  }

  public function addHelpRequest(HelpRequest $helpRequest): static
  {
    if (!$this->helpRequests->contains($helpRequest)) {
      $this->helpRequests->add($helpRequest);
      $helpRequest->setProblem($this);
    }

    return $this;
  }

  #================================================================#

  public function getTotalScore(): float
  {
    return $this->totalScore;
  }

  public function setTotalScore(float $totalScore): static
  {
    $this->totalScore = $totalScore;

    return $this;
  }

  #================================================================#

  public function getAmoutAnswered(): int
  {
    return $this->amoutAnswered;
  }

  public function setAmoutAnswered(int $amoutAnswered): static
  {
    $this->amoutAnswered = $amoutAnswered;

    return $this;
  }

  #================================================================#

  public function getTraductions(): ?ProblemTraduction
  {
    return $this->traductions;
  }

  public function setTraductions(ProblemTraduction $traductions): static
  {
    // set the owning side of the relation if necessary
    if ($traductions->getProblem() !== $this) {
      $traductions->setProblem($this);
    }

    $this->traductions = $traductions;

    return $this;
  }

  #================================================================#

  public function getDiscr(): ProblemTypeEnum
  {
    return ProblemTypeEnum::TextProblem;
  }
}
