<?php

declare(strict_types=1);

namespace Banque\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Viewer
{
  #================================================================#
  # Constants                                                      #
  #================================================================#

  # Value in sec
  public const TIME_MAX_VISIT = 60 * 60 * 2; // 2h

  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_viewer')]
  private int $id;

  #[ORM\Column(name: 'viewer_addressIp', length: 40)]
  private string $addressIp;

  #[ORM\Column(name: 'viewer_viewingDate', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  private \DateTimeInterface $viewingDate;

  #[ORM\Column(name: 'viewer_userId', nullable: true)]
  private ?int $userId = null;

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getAddressIp(): string
  {
    return $this->addressIp;
  }

  public function setAddressIp(string $addressIp): static
  {
    $this->addressIp = $addressIp;

    return $this;
  }

  #================================================================#

  public function getViewingDate(): \DateTimeInterface
  {
    return $this->viewingDate;
  }

  public function setViewingDate(\DateTimeInterface $viewingDate): static
  {
    $this->viewingDate = $viewingDate;

    return $this;
  }

  #================================================================#

  public function getUserId(): ?int
  {
    return $this->userId;
  }

  public function setUserId(?int $userId): static
  {
    $this->userId = $userId;

    return $this;
  }
}
