<?php

declare(strict_types=1);

namespace Banque\Entity;

use Banque\Enum\ProblemTypeEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Shared\Entity\User;

#[ORM\Entity]
#[ORM\Table(name: '`problem`')]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'discr', type: 'string', length: 16)]
#[ORM\DiscriminatorMap([
  "textProblem" => TextProblem::class,
  "photo" => Photo::class,
  "photoProblem" => PhotoProblem::class
])]
abstract class Problem
{
  #================================================================#
  # Constants                                                      #
  #================================================================#

  public const TAG_SAMPLE = 'SAMPLE';
  public const TAG_COMMUNITY_PRIVATE = 'COMMUNITY_PRIVATE';
  public const TAG_COMMUNITY_PUBLIC = 'COMMUNITY_PUBLIC';
  public const TAG_COMMUNITY_PRIVATE_WAIT = 'COMMUNITY_PRIVATE_WAIT';
  public const TAG_COMMUNITY_PUBLIC_WAIT = 'COMMUNITY_PUBLIC_WAIT';

  #================================================================#
  # Properties                                                     #
  #================================================================#

  #[ORM\Id]
  #[ORM\GeneratedValue]
  #[ORM\Column(name: 'id_problem')]
  protected int $id;

  #[ORM\ManyToOne(inversedBy: 'problems', targetEntity: User::class)]
  #[ORM\JoinColumn(name: 'id_user', referencedColumnName: 'id_user', nullable: false)]
  protected User $user;

  #[ORM\Column(name: 'problem_sendingDate', type: Types::DATETIME_MUTABLE, options: ["default" => "CURRENT_TIMESTAMP"])]
  protected \DateTimeInterface $sendingDate;

  #[ORM\Column(name: 'problem_isVerified', options: ["default" => false])]
  protected bool $isVerified = false;

  #[ORM\Column(name: 'problem_verifiedDate', type: Types::DATETIME_MUTABLE, nullable: true, options: ["default" => "CURRENT_TIMESTAMP"])]
  protected ?\DateTimeInterface $verifiedDate = null;

  #[ORM\Column(name: 'problem_downloadCount', options: ["default" => 0])]
  protected int $downloadCount = 0;

  #[ORM\Column(name: 'problem_tag', length: 32)]
  protected string $tag;

  /** @var Collection<int, ProblemLike> */
  #[ORM\OneToMany(mappedBy: 'problem', targetEntity: ProblemLike::class, orphanRemoval: true, cascade: ["persist"])]
  protected Collection $problemLikes;

  #[ORM\Column(name: 'problem_counterLike', options: ["default" => 0])]
  protected int $counterLike = 0;

  /** @var Collection<int, ProblemComment> */
  #[ORM\OneToMany(mappedBy: 'problem', targetEntity: ProblemComment::class, orphanRemoval: true, cascade: ["persist"])]
  protected Collection $problemComments;

  #[ORM\Column(name: 'problem_counterComment', options: ["default" => 0])]
  protected int $counterComment = 0;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->problemLikes = new ArrayCollection();
    $this->problemComments = new ArrayCollection();

    $this->sendingDate = new \DateTime('now', new \DateTimeZone('UTC'));
  }

  #================================================================#
  # Getters|Setters Methods                                        #
  #================================================================#

  public function getId(): int
  {
    return $this->id;
  }

  #================================================================#

  public function getUser(): User
  {
    return $this->user;
  }

  public function setUser(User $user): static
  {
    $this->user = $user;

    return $this;
  }

  #================================================================#

  public function getSendingDate(): \DateTimeInterface
  {
    return $this->sendingDate;
  }

  public function setSendingDate(\DateTimeInterface $sendingDate): static
  {
    $this->sendingDate = $sendingDate;

    return $this;
  }

  #================================================================#

  public function isVerified(): bool
  {
    return $this->isVerified;
  }

  public function setIsVerified(bool $isVerified): static
  {
    $this->isVerified = $isVerified;

    return $this;
  }

  #================================================================#

  public function getVerifiedDate(): ?\DateTimeInterface
  {
    return $this->verifiedDate;
  }

  public function setVerifiedDate(?\DateTimeInterface $verifiedDate): static
  {
    $this->verifiedDate = $verifiedDate;

    return $this;
  }

  #================================================================#

  public function getDownloadCount(): int
  {
    return $this->downloadCount;
  }

  public function setDownloadCount(int $downloadCount): static
  {
    $this->downloadCount = $downloadCount;

    return $this;
  }

  #================================================================#

  public function getTag(): string
  {
    return $this->tag;
  }

  public function setTag(string $tag): static
  {
    $this->tag = $tag;

    return $this;
  }

  #================================================================#

  public function getCounterLike(): int
  {
    return $this->counterLike;
  }

  public function setCounterLike(int $counterLike): static
  {
    $this->counterLike = $counterLike;

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, ProblemLike>
   */
  public function getProblemLikes(): Collection
  {
    return $this->problemLikes;
  }

  public function addProblemLike(ProblemLike $problemLike): static
  {
    if (!$this->problemLikes->contains($problemLike)) {
      $this->problemLikes->add($problemLike);
      $problemLike->setProblem($this);
    }

    return $this;
  }

  public function removeProblemLike(ProblemLike $problemLike): static
  {
    $this->problemLikes->removeElement($problemLike);

    return $this;
  }

  #================================================================#

  /**
   * @return Collection<int, ProblemComment>
   */
  public function getProblemComments(): Collection
  {
    return $this->problemComments;
  }

  public function addProblemComment(ProblemComment $problemComment): static
  {
    if (!$this->problemComments->contains($problemComment)) {
      $this->problemComments->add($problemComment);
      $problemComment->setProblem($this);
    }

    return $this;
  }

  public function removeProblemComment(ProblemComment $problemComment): static
  {
    $this->problemComments->removeElement($problemComment);

    return $this;
  }

  #================================================================#

  public function getCounterComment(): int
  {
    return $this->counterComment;
  }

  public function setCounterComment(int $counterComment): static
  {
    $this->counterComment = $counterComment;

    return $this;
  }

  #================================================================#

  /**
   * @param Problem[] $problems
   *
   * @return ArrayCollection<int|string, TextProblem>
   */
  public static function getTextProblem(array $problems, bool $isVerified = false): ArrayCollection
  {
    $problems = new ArrayCollection($problems);
    return $problems->filter(function (Problem $item) use ($isVerified) {
      return $item instanceof TextProblem && ($isVerified ? $item->isVerified() : true);
    });
  }

  #================================================================#

  /**
   * @param Problem[] $problems
   *
   * @return ArrayCollection<int|string, Photo>
   */
  public static function getPhoto(array $problems, bool $isVerified = false)
  {
    $problems = new ArrayCollection($problems);
    return $problems->filter(function (Problem $item) use ($isVerified) {
      return $item instanceof Photo && ($isVerified ? $item->isVerified() : true);
    });
  }

  #================================================================#

  /**
   * @param Problem[] $problems
   *
   * @return ArrayCollection<int|string, PhotoProblem>
   */
  public static function getPhotoProblem(array $problems, bool $isVerified = false)
  {
    $problems = new ArrayCollection($problems);
    return $problems->filter(function (Problem $item) use ($isVerified) {
      return $item instanceof PhotoProblem && ($isVerified ? $item->isVerified() : true);
    });
  }

  #================================================================#

  /**
   * @template T of Problem
   *
   * @param class-string<T> $problemType
   */
  public static function getClassFromType(ProblemTypeEnum $type): string
  {
    if ($type === ProblemTypeEnum::TextProblem) {
      return TextProblem::class;
    } elseif ($type === ProblemTypeEnum::Photo) {
      return Photo::class;
    } elseif ($type === ProblemTypeEnum::PhotoProblem) {
      return PhotoProblem::class;
    }
  }

  #================================================================#

  abstract public function getDiscr(): ProblemTypeEnum;
}
