<?php

declare(strict_types=1);

namespace Banque\Factory;

use Banque\Entity\ProblemLike;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<ProblemLike>
 */
final class ProblemLikeFactory extends PersistentProxyObjectFactory
{
  #================================================================#
  # Protected Methods                                              #
  #================================================================#

  /** @return array<string, mixed> */
  protected function defaults(): array
  {
    return [
      'createdAt' => self::faker()->dateTimeBetween('-1 year', 'now', 'UTC'),
      'value' => 1
    ];
  }

  #================================================================#

  protected function initialize(): self
  {
    return $this;
  }

  #================================================================#

  public static function class(): string
  {
    return ProblemLike::class;
  }
}
