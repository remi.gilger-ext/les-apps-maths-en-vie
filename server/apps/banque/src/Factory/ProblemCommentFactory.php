<?php

declare(strict_types=1);

namespace Banque\Factory;

use Banque\Entity\ProblemComment;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<ProblemComment>
 */
final class ProblemCommentFactory extends PersistentProxyObjectFactory
{
  #================================================================#
  # Protected Methods                                              #
  #================================================================#

  /** @return array<string, mixed> */
  protected function defaults(): array
  {
    return [
      'content' => self::faker()->paragraph(),
      'createdAt' => self::faker()->dateTimeBetween('-1 year', 'now', 'UTC')
    ];
  }

  #================================================================#

  protected function initialize(): self
  {
    return $this;
  }

  #================================================================#

  public static function class(): string
  {
    return ProblemComment::class;
  }
}

// UPDATE problem p SET p.problem_counterComment = if (( SELECT COUNT(c.id_problem) FROM problem_comment c WHERE c.id_problem = p.id_problem GROUP BY c.id_problem) is null, 0, ( SELECT COUNT(c.id_problem) FROM problem_comment c WHERE c.id_problem = p.id_problem GROUP BY c.id_problem));
