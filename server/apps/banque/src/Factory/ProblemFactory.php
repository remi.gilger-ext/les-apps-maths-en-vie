<?php

declare(strict_types=1);

namespace Banque\Factory;

use Banque\Entity\Problem;
use Banque\Entity\TextProblem;
use Banque\Enum\ResponseStateEnum;
use Banque\Enum\TextProblemTypeEnum;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<TextProblem>
 */
final class ProblemFactory extends PersistentProxyObjectFactory
{
  #================================================================#
  # Constants                                                      #
  #================================================================#

  public const PROBLEMS = [
    "Dans la classe de CE1-CM1 de Maitresse Bene Docere, il y a 21 élèves dont 7 CM1. Indique la part des CM1 dans la classe par une fraction.",
    "Sur la carte, les élèves mesurent 10 cm entre deux villages et ils concluent que les villages sont distants de 5 km. S'ils mesurent 25 cm entre deux villes sur la carte, quelle est la distance réelle ?",
    "M. Bing veut clôturer sa piscine rectangulaire fait 12,4 mètres par 7,5 mètres. Il souhaite laisser 1 mètre entre la piscine et la clôture. Quelle sera la longueur de la clôture ?",
    "Albert doit entourer son jardin avec 45,3 mètres de clôture. Le grillage qu'il a choisi coûte 1,75 € du mètre. Il doit aussi acheter 23 piquets à 4,90 € l'unité et 3 rouleaux de fil de 50 m à 7,99 € le rouleau. Quel va être le montant de sa facture ?",
  ];

  #================================================================#
  # Protected Methods                                              #
  #================================================================#

  /** @return array<string, mixed> */
  protected function defaults(): array
  {
    $isExclude = self::faker()->boolean(10);
    $responseState = $this->faker()->randomElement(ResponseStateEnum::cases());

    return [
      'title' => self::faker()->sentence(3, true),
      'statement' => self::faker()->randomElement(self::PROBLEMS),
      'nivel' => self::faker()->randomElement(['cp', 'ce1', 'ce2', 'cm1', 'cm2', '6eme']),
      'type' => self::faker()->randomElement(array_map(function ($o) {
        return $o->name;
      }, TextProblemTypeEnum::cases())),
      'tag' => self::faker()->randomElement([Problem::TAG_COMMUNITY_PUBLIC, Problem::TAG_COMMUNITY_PRIVATE]),
      'isExclude' => $isExclude,
      'response' => $isExclude ? null : ($responseState === ResponseStateEnum::Conflict ? "10;12" : ($responseState === ResponseStateEnum::NoResponse ? null : "10")),
      'responseState' => $isExclude ? ResponseStateEnum::NoResponse : $responseState,
      'responseSentence' => $isExclude ? null : self::faker()->sentence(3, true) . " [] " . self::faker()->sentence(3, true),
      'helpSentence1' => $isExclude ? null : self::faker()->realText(300),
      'helpSentence2' => $isExclude ? null : self::faker()->realText(300),
      'responseExplication' => $isExclude ? null : self::faker()->realText(400)
    ];
  }

  #================================================================#

  public function verified(): self
  {
    return $this->with(['isVerified' => 1])
      ->with(function () {
        return ['sendingDate' => self::faker()->dateTimeBetween('-1 month', '-3 days', 'UTC')];
      })
      ->with(function () {
        return ['verifiedDate' => self::faker()->dateTimeBetween('-2 days', 'now', 'UTC')];
      })
      ->with(function () {
        return ['downloadCount' => self::faker()->randomNumber(3)];
      });
  }

  #================================================================#

  public function privateProblem(): self
  {
    return $this->with(['tag' => Problem::TAG_COMMUNITY_PRIVATE]);
  }

  #================================================================#

  public function publicProblem(): self
  {
    return $this->with(['tag' => Problem::TAG_COMMUNITY_PUBLIC]);
  }

  #================================================================#

  protected function initialize(): self
  {
    return $this->afterInstantiate(function (TextProblem $problem) {
      $problem->setCounterLike($problem->getProblemLikes()->count());
    });
  }

  #================================================================#

  public static function class(): string
  {
    return TextProblem::class;
  }
}
