<?php

declare(strict_types=1);

namespace Banque\Form;

use Banque\Enum\EnigmaTypeEnum;
use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class EnigmaFilters extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\Choice(
    callback: 'getTypesCallback',
    message: 'Les types sélectionnés ne sont pas valides.',
    multiple: true
  )]
  private array $type = [];

  #[Assert\Choice(
    choices: ['1', '2', '3'],
    message: 'Les niveaux sélectionnés ne sont pas valides.',
    multiple: true
  )]
  private array $nivel = [];

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getType(): array
  {
    return $this->type;
  }

  #================================================================#

  public function getNivel(): array
  {
    return $this->nivel;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->type = $params->all('type');
    $this->nivel = $params->all('nivel');
  }

  #================================================================#

  public function updateRequiredParam(array &$requiredParam): void
  {
    if (!empty($this->getType())) {
      $requiredParam['enigmaType'] = $this->getType();
    }

    if (!empty($this->getNivel())) {
      $requiredParam['enigmaNivel'] = $this->getNivel();
    }
  }

  #================================================================#

  public function getTypesCallback(): array
  {
    return array_column(EnigmaTypeEnum::cases(), 'name');
  }
}
