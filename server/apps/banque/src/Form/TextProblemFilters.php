<?php

declare(strict_types=1);

namespace Banque\Form;

use Banque\Enum\ResponseStateEnum;
use Banque\Enum\TextProblemTypeEnum;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class TextProblemFilters extends AbstractProblemFilters
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\Choice(
    callback: 'getTypesCallback',
    message: 'Les types sélectionnés ne sont pas valides.',
    multiple: true
  )]
  private array $type = [];

  #[Assert\Choice(
    choices: ['cp', 'ce1', 'ce2', 'cm1', 'cm2', '6eme'],
    message: 'Les niveaux sélectionnés ne sont pas valides.',
    multiple: true
  )]
  private array $nivel = [];

  private string $textResearch = '';

  private ?ResponseStateEnum $responseState = null;

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getType(): array
  {
    return $this->type;
  }

  #================================================================#

  public function getNivel(): array
  {
    return $this->nivel;
  }

  #================================================================#

  public function getTextResearch(): string
  {
    return $this->textResearch;
  }

  #================================================================#

  public function getResponseState(): ?ResponseStateEnum
  {
    return $this->responseState;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->type = $params->all('type');
    $this->nivel = $params->all('nivel');
    $this->textResearch = $params->getString('textResearch');
    if ($params->getString("responseState") !== "all") {
      $this->responseState = $params->getEnum("responseState", ResponseStateEnum::class);
    }

    parent::fillInstance($params);
  }

  #================================================================#

  public function updateRequiredParam(array &$requiredParam): void
  {
    if (!empty($this->getType())) {
      $requiredParam['p.type'] = $this->getType();
    }

    if (!empty($this->getNivel())) {
      $requiredParam['p.nivel'] = $this->getNivel();
    }

    if (!empty($this->getTextResearch())) {
      $requiredParam['textResearch'] = $this->getTextResearch();
    }

    if ($this->getResponseState() !== null) {
      $requiredParam['p.responseState'] = [$this->getResponseState()->value];
      $requiredParam['p.isExclude'] = [false];
    }
  }

  #================================================================#

  public function getTypesCallback(): array
  {
    return array_column(TextProblemTypeEnum::cases(), 'name');
  }
}
