<?php

declare(strict_types=1);

namespace Banque\Form;

use Banque\Entity\Photo;
use Banque\Entity\Problem;
use Banque\Enum\PhotoTypeEnum;
use Banque\Service\ImageService;
use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class PhotoModel extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\Choice(
    callback: 'getTypesCallback',
    multiple: true,
    multipleMessage: "Les mots-clés sélectionnés sont invalide.",
  )]
  #[Assert\NotBlank(
    message: "Veuillez choisir le ou les mots-clés de votre photo."
  )]
  private array $type = [];

  #[Assert\File(
    mimeTypes: ['image/jpg', 'image/jpeg', 'image/png'],
    mimeTypesMessage: "Le type du fichier est invalide {{ type }}. Les types autorisés sont png, jpg et jpeg",
    maxSize: '2Mi'
  )]
  #[Assert\NotNull(
    message: "Veuillez choisir une photo."
  )]
  private ?File $attachment = null;

  #[Assert\Regex(
    pattern: '/(-?([0-9]+\.)?[0-9]{1,4};){3}(-?([0-9]+\.)?[0-9]{1,4})/',
  )]
  private string $imageData = "0;0;1;1";

  #[Assert\Choice(
    choices: [0, 90, 180, 270],
  )]
  private int $imageRotation = 0;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private ImageService $imageService,
    private string $photoDirectory
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getType(): array
  {
    return $this->type;
  }

  #================================================================#

  public function getAttachment(): ?File
  {
    return $this->attachment;
  }

  #================================================================#

  public function getImageData(): string
  {
    return $this->imageData;
  }

  #================================================================#

  public function getImageRotation(): int
  {
    return $this->imageRotation;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->type = $params->all('type');

    $attachements = $params->all('attachment');
    if (!empty($attachements) && \is_string($attachements[0])) {
      $tmpPath = \sys_get_temp_dir() . '/sf_upload' . uniqid();
      file_put_contents($tmpPath, \base64_decode(preg_replace('/^data:image\/\w+;base64,/i', '', $attachements[0])));
      $uploadedFile = new File($tmpPath);
      $this->attachment = $uploadedFile;
    }

    $this->imageData = $params->getString('imageData');
    $this->imageRotation = $params->getInt('imageRotation', 0);
  }

  #================================================================#

  public function removeModifyingErrors(ConstraintViolationListInterface $errors): void
  {
    /** @var ConstraintViolationInterface $error */
    foreach ($errors as $index => $error) {
      if (\in_array($error->getPropertyPath(), ["attachment"])) {
        $errors->remove($index);
      }
    }
  }

  #================================================================#

  public function clear(): void
  {
    if ($this->attachment !== null && \file_exists($this->attachment->getFilename())) {
      \unlink($this->attachment->getFilename());
    }
  }

  #================================================================#

  public function createInstance(): Photo
  {
    $problem = new Photo();

    $date = new \DateTime('now', new \DateTimeZone('UTC'));

    $extension = $this->attachment->guessExtension();
    $fichier = md5(\uniqid()) . $date->format("d-m-Y") . '.' . $extension;

    $this->attachment->move(
      $this->photoDirectory,
      $fichier
    );
    $problem->setAttachment($fichier);

    $this->imageService->setImage($fichier);
    $this->imageService->rotate($this->imageRotation);
    $this->imageService->resize($this->imageData);
    $this->imageService->saveImage($fichier);

    $problem->setType(\implode(";", $this->type));
    $problem->setTag(Problem::TAG_COMMUNITY_PUBLIC_WAIT);

    return $problem;
  }

  #================================================================#

  public function updateInstance(Photo $problem, bool $isReview = false): void
  {
    $problem->setType(\implode(";", $this->type));

    if ($isReview) {
      $this->imageService->setImage($problem->getAttachment());
      $this->imageService->rotate($this->imageRotation);
      $this->imageService->resize($this->imageData);
      $this->imageService->addLogo();
      $this->imageService->saveImage($problem->getAttachment());
    }
  }

  #================================================================#

  public function getTypesCallback(): array
  {
    return array_column(PhotoTypeEnum::cases(), 'name');
  }
}
