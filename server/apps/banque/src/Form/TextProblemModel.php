<?php

declare(strict_types=1);

namespace Banque\Form;

use Banque\Entity\Problem;
use Banque\Entity\TextProblem;
use Banque\Enum\GenerateAiTypeEnum;
use Banque\Enum\ResponseStateEnum;
use Banque\Enum\TextProblemTypeEnum;
use Banque\Service\OpenAIService;
use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class TextProblemModel extends AbstractModel
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private OpenAIService $openAIService,
  ) {}

  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\Choice(
    choices: ['normal', 'step', 'propor', 'frac', 'cart'],
    message: 'Veuillez indiquer le type de problème.',
  )]
  private string $primaryType = '';

  #[Assert\NotBlank(
    message: 'Veuillez renseigner le titre du problème.'
  )]
  #[Assert\Length(
    max: 64,
    maxMessage: 'Merci de saisir moins de 64 caractères.'
  )]
  private string $title = '';

  #[Assert\NotBlank(
    message: 'Veuillez renseigner l\'énoncé du problème.'
  )]
  #[Assert\Length(
    max: TextProblem::MAX_STATEMENT_LENGTH,
    maxMessage: 'Merci de saisir moins de {{ limit }} caractères.'
  )]
  private string $statement = '';

  #[Assert\Choice(
    choices: ['cp', 'ce1', 'ce2', 'cm1', 'cm2', '6eme'],
    message: 'Veuillez renseigner le niveau du problème.',
  )]
  private string $nivel = '';

  #[Assert\Choice(
    callback: 'getTypesCallback',
    message: 'Le type sélectionné est invalide.',
  )]
  private ?string $type = null;

  #[Assert\NotNull(
    message: 'Veuillez choisir la visibilité de votre problème.'
  )]
  private ?bool $public = null;

  #[Assert\Length(
    max: 16,
    maxMessage: 'Merci de saisir moins de 16 caractères.'
  )]
  private ?string $response = null;

  private bool $isExclude = false;

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getPrimaryType(): string
  {
    return $this->primaryType;
  }

  #================================================================#

  public function getTitle(): string
  {
    return $this->title;
  }

  #================================================================#

  public function getStatement(): string
  {
    return $this->statement;
  }

  #================================================================#

  public function getNivel(): string
  {
    return $this->nivel;
  }

  #================================================================#

  public function getType(): ?string
  {
    return $this->type;
  }

  #================================================================#

  public function getPublic(): bool
  {
    if ($this->public === null) {
      throw new \LogicException('public property can not be null');
    }

    return $this->public;
  }

  #================================================================#

  public function getResponse(): ?string
  {
    return $this->response;
  }

  #================================================================#

  public function getIsExclude(): bool
  {
    return $this->isExclude;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->primaryType = $params->getString('primaryType');
    $this->title = $params->getString('title');
    $this->statement = $params->getString('statement');
    $this->nivel = $params->getString('nivel');
    $this->type = $params->getString('type') ?: null;

    if ($params->has('public')) {
      $this->public = $params->getBoolean('public', true);
    }

    $this->response = \trim(\str_replace([".", " "], [",", ""], $params->getString('response'))) ?: null;
    $this->isExclude = $params->getBoolean('isExclude');
  }

  #================================================================#

  public function removeModifyingErrors(ConstraintViolationListInterface $errors): void
  {
    /** @var ConstraintViolationInterface $error */
    foreach ($errors as $index => $error) {
      if (\in_array($error->getPropertyPath(), ["public"])) {
        $errors->remove($index);
      }
    }
  }

  #================================================================#

  public function clear(): void
  {
    // Anything to clear for a text problem
  }

  #================================================================#

  public function createInstance(): TextProblem
  {
    $problem = new TextProblem();

    if ($this->primaryType !== 'normal' && $this->primaryType !== 'step') {
      $problem->setType($this->primaryType);
    } else {
      $problem->setType($this->type);
    }

    if ($this->public) {
      $problem->setTag(Problem::TAG_COMMUNITY_PUBLIC_WAIT);
    } else {
      $problem->setTag(Problem::TAG_COMMUNITY_PRIVATE);
    }

    $problem->setTitle($this->title);
    $problem->setStatement($this->statement);
    $problem->setNivel($this->nivel);

    return $problem;
  }

  #================================================================#

  public function updateInstance(TextProblem $problem, bool $isReview = false): void
  {
    if ($this->primaryType !== 'normal' && $this->primaryType !== 'step') {
      $problem->setType($this->primaryType);
    } else {
      $problem->setType($this->type);
    }

    $problem->setTitle($this->title);
    $problem->setStatement($this->statement);

    $oldNivel = $problem->getNivel();
    $problem->setNivel($this->nivel);

    if (!$isReview && $oldNivel !== $this->nivel) {
      $this->openAIService->generateIA($problem, GenerateAiTypeEnum::HelpSentence1);
      $this->openAIService->generateIA($problem, GenerateAiTypeEnum::HelpSentence2);
      $this->openAIService->generateIA($problem, GenerateAiTypeEnum::ResponseExplication);
    }

    if (!$isReview) {
      $this->openAIService->generateIA($problem, GenerateAiTypeEnum::English);
      $this->openAIService->generateIA($problem, GenerateAiTypeEnum::Italian);
      $this->openAIService->generateIA($problem, GenerateAiTypeEnum::Spanish);
      $this->openAIService->generateIA($problem, GenerateAiTypeEnum::German);
    }

    $problem->setIsExclude($this->isExclude);

    if (
      $this->response !== $problem->getResponse() && (
        $problem->getResponseState() === ResponseStateEnum::NoResponse ||
        $problem->getResponseState() === ResponseStateEnum::AnsweredByHuman ||
        $problem->getResponseState() === ResponseStateEnum::Verified)
    ) {
      if ($problem->getResponseState() === ResponseStateEnum::Verified) {
        $problem->setResponseExplication(null);
      }

      $problem->setResponse($this->response);
      $problem->setResponseState($this->response === null ? ResponseStateEnum::NoResponse : ResponseStateEnum::AnsweredByHuman);
    }

    if ($this->isExclude) {
      $problem->setResponse(null);
      $problem->setResponseState(ResponseStateEnum::NoResponse);
      $problem->setResponseSentence(null);
      $problem->setHelpSentence1(null);
      $problem->setHelpSentence2(null);
      $problem->setResponseExplication(null);
    }
  }

  #================================================================#

  public function getTypesCallback(): array
  {
    return array_diff(
      array_column(TextProblemTypeEnum::cases(), 'name'),
      [
        TextProblemTypeEnum::frac->name,
        TextProblemTypeEnum::propor->name,
        TextProblemTypeEnum::cart->name,
      ]
    );
  }

  #================================================================#

  #[Assert\Callback]
  public function validateTypeMissing(ExecutionContextInterface $context, mixed $payload): void
  {
    if (($this->getPrimaryType() === 'normal' || $this->getPrimaryType() === 'step') && empty($this->getType())) {
      $context->buildViolation('Veuillez spécifier le type du problème.')
        ->atPath('type')
        ->addViolation();
    }
  }

  #================================================================#

  #[Assert\Callback]
  public function validateWrongType(ExecutionContextInterface $context, mixed $payload): void
  {
    if (
      $this->getPrimaryType() === 'step' &&
      !\in_array($this->getType(), [
        TextProblemTypeEnum::add->name,
        TextProblemTypeEnum::mult->name,
        TextProblemTypeEnum::mixed->name,
      ])
    ) {
      $context->buildViolation('Le type sélectionné est invalide.')
        ->atPath('type')
        ->addViolation();
    }
  }
}
