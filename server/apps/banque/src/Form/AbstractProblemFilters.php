<?php

declare(strict_types=1);

namespace Banque\Form;

use Banque\Entity\Problem;
use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractProblemFilters extends AbstractModel
{
  #================================================================#
  # Abstract Methods                                               #
  #================================================================#

  abstract public function updateRequiredParam(array &$requiredParam): void;

  #================================================================#
  # Private Attributs                                            #
  #================================================================#

  #[Assert\Choice(
    choices: [Problem::TAG_COMMUNITY_PRIVATE, Problem::TAG_COMMUNITY_PUBLIC],
    message: 'Les tags sélectionnés ne sont pas valides.',
    multiple: true
  )]
  private array $tag = [];

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getTag(): array
  {
    return $this->tag;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->tag = $params->all('tag');
  }
}
