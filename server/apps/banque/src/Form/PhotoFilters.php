<?php

declare(strict_types=1);

namespace Banque\Form;

use Banque\Enum\PhotoTypeEnum;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class PhotoFilters extends AbstractProblemFilters
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\Choice(
    callback: 'getTypesCallback',
    message: 'Les types sélectionnés ne sont pas valides.',
    multiple: true
  )]
  private array $type = [];

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getType(): array
  {
    return $this->type;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->type = $params->all('type');

    parent::fillInstance($params);
  }

  #================================================================#

  public function updateRequiredParam(array &$requiredParam): void
  {
    if (!empty($this->getType())) {
      $requiredParam['photoType'] = $this->getType();
    }
  }

  #================================================================#

  public function getTypesCallback(): array
  {
    return array_column(PhotoTypeEnum::cases(), 'name');
  }
}
