<?php

declare(strict_types=1);

namespace Banque\Form;

use Banque\Enum\PhotoTypeEnum;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class PhotoProblemFilters extends AbstractProblemFilters
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\Choice(
    callback: 'getTypesCallback',
    message: 'Les types sélectionnés ne sont pas valides.',
    multiple: true
  )]
  private array $type = [];

  #[Assert\Choice(
    choices: ['c1', 'cp', 'ce1', 'ce2', 'cm1', 'cm2', '6eme'],
    message: 'Les niveaux sélectionnés ne sont pas valides.',
    multiple: true
  )]
  private array $nivel = [];

  private string $textResearch = '';

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getType(): array
  {
    return $this->type;
  }

  #================================================================#

  public function getNivel(): array
  {
    return $this->nivel;
  }

  #================================================================#

  public function getTextResearch(): string
  {
    return $this->textResearch;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->type = $params->all('type');
    $this->nivel = $params->all('nivel');
    $this->textResearch = $params->getString('textResearch');

    parent::fillInstance($params);
  }

  #================================================================#

  public function updateRequiredParam(array &$requiredParam): void
  {
    if (!empty($this->getType())) {
      $requiredParam['photoType'] = $this->getType();
    }

    if (!empty($this->getNivel())) {
      $requiredParam['p.nivel'] = $this->getNivel();
    }

    if (!empty($this->getTextResearch())) {
      $requiredParam['textResearch'] = $this->getTextResearch();
    }
  }

  #================================================================#

  public function getTypesCallback(): array
  {
    return array_column(PhotoTypeEnum::cases(), 'name');
  }
}
