<?php

declare(strict_types=1);

namespace Banque\Form;

use Banque\Entity\Problem;
use Banque\Entity\PhotoProblem;
use Banque\Enum\PhotoTypeEnum;
use Banque\Service\ImageService;
use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class PhotoProblemModel extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\Choice(
    callback: 'getTypesCallback',
    multiple: true,
    multipleMessage: "Les mots-clés sélectionnés sont invalide.",
  )]
  #[Assert\NotBlank(
    message: "Veuillez choisir le ou les mots-clés de votre photo-problème."
  )]
  private array $type = [];

  #[Assert\File(
    mimeTypes: ['image/jpg', 'image/jpeg', 'image/png'],
    mimeTypesMessage: "Le type du fichier est invalide {{ type }}. Les types autorisés sont png, jpg et jpeg",
    maxSize: '2Mi'
  )]
  #[Assert\NotNull(
    message: "Veuillez choisir une photo."
  )]
  private ?File $attachment = null;

  #[Assert\NotBlank(
    message: 'Veuillez renseigner l\'énoncé du photo-problème'
  )]
  #[Assert\Length(
    max: PhotoProblem::MAX_STATEMENT_LENGTH,
    maxMessage: 'Merci de saisir moins de {{ limit }} caractères.'
  )]
  private string $statement = '';

  #[Assert\Choice(
    choices: ['c1', 'cp', 'ce1', 'ce2', 'cm1', 'cm2', '6eme'],
    message: 'Le niveau sélectionné est invalide.',
  )]
  #[Assert\NotBlank(
    message: 'Veuillez renseigner le niveau du photo-problème.'
  )]
  private string $nivel = '';

  #[Assert\NotNull(
    message: 'Veuillez choisir la visibilité de votre photo-problème.'
  )]
  private ?bool $public = null;

  #[Assert\Regex(
    pattern: '/(-?([0-9]+\.)?[0-9]{1,4};){3}(-?([0-9]+\.)?[0-9]{1,4})/',
  )]
  private string $imageData = "0;0;1;1";

  #[Assert\Choice(
    choices: [0, 90, 180, 270],
  )]
  private int $imageRotation = 0;

  private bool $addToPhoto = false;

  #[Assert\Choice(
    callback: 'getTypesCallback',
    multiple: true,
    multipleMessage: "Les mots-clés sélectionnés sont invalide.",
  )]
  private array $photoType = [];

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private ImageService $imageService,
    private string $photoDirectory
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getType(): array
  {
    return $this->type;
  }

  #================================================================#

  public function getAttachment(): ?File
  {
    return $this->attachment;
  }

  #================================================================#

  public function getStatement(): string
  {
    return $this->statement;
  }

  #================================================================#

  public function getNivel(): string
  {
    return $this->nivel;
  }

  #================================================================#

  public function getPublic(): ?bool
  {
    return $this->public;
  }

  #================================================================#

  public function getImageData(): string
  {
    return $this->imageData;
  }

  #================================================================#

  public function getImageRotation(): int
  {
    return $this->imageRotation;
  }

  #================================================================#

  public function getAddToPhoto(): bool
  {
    return $this->addToPhoto;
  }

  #================================================================#

  public function getPhotoType(): array
  {
    return $this->photoType;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->type = $params->all('type');

    $attachements = $params->all('attachment');
    if (!empty($attachements) && \is_string($attachements[0])) {
      $tmpPath = \sys_get_temp_dir() . '/sf_upload' . uniqid();
      file_put_contents($tmpPath, \base64_decode(preg_replace('/^data:image\/\w+;base64,/i', '', $attachements[0])));
      $uploadedFile = new File($tmpPath);
      $this->attachment = $uploadedFile;
    }

    $this->statement = $params->getString('statement');
    $this->nivel = $params->getString('nivel');

    if ($params->has('public')) {
      $this->public = $params->getBoolean('public', true);
    }

    $this->imageData = $params->getString('imageData');
    $this->imageRotation = $params->getInt('imageRotation', 0);

    $this->addToPhoto = $params->getBoolean('addToPhoto');
    $this->photoType = $params->all('photoType');
  }

  #================================================================#

  public function removeModifyingErrors(ConstraintViolationListInterface $errors): void
  {
    /** @var ConstraintViolationInterface $error */
    foreach ($errors as $index => $error) {
      if (\in_array($error->getPropertyPath(), ["attachment", "public"])) {
        $errors->remove($index);
      }
    }
  }

  #================================================================#

  public function clear(): void
  {
    if ($this->attachment !== null && \file_exists($this->attachment->getFilename())) {
      \unlink($this->attachment->getFilename());
    }
  }

  #================================================================#

  public function createInstance(): PhotoProblem
  {
    $problem = new PhotoProblem();

    /**
     * @var UploadedFile $image
     */
    $date = new \DateTime('now', new \DateTimeZone('UTC'));

    $extension = $this->attachment->guessExtension();
    $fichier = md5(\uniqid()) . $date->format("d-m-Y") . '.' . $extension;

    $this->attachment->move(
      $this->photoDirectory,
      $fichier
    );
    $problem->setAttachment($fichier);

    $this->imageService->setImage($fichier);
    $this->imageService->rotate($this->imageRotation);
    $this->imageService->resize($this->imageData);
    $this->imageService->saveImage($fichier);

    if ($this->public) {
      $problem->setTag(Problem::TAG_COMMUNITY_PUBLIC_WAIT);
    } else {
      $problem->setTag(Problem::TAG_COMMUNITY_PRIVATE);
    }

    $problem->setType(\implode(";", $this->type));
    $problem->setStatement($this->statement);
    $problem->setNivel($this->nivel);

    return $problem;
  }

  #================================================================#

  public function updateInstance(PhotoProblem $problem, bool $isReview = false): void
  {
    $problem->setType(\implode(";", $this->type));
    $problem->setStatement($this->statement);
    $problem->setNivel($this->nivel);

    if ($isReview) {
      $this->imageService->setImage($problem->getAttachment());
      $this->imageService->rotate($this->imageRotation);
      $this->imageService->resize($this->imageData);
      $this->imageService->addLogo();
      $this->imageService->saveImage($problem->getAttachment());
    }
  }

  #================================================================#

  public function getTypesCallback(): array
  {
    return array_column(PhotoTypeEnum::cases(), 'name');
  }

  #================================================================#

  #[Assert\Callback]
  public function validate(ExecutionContextInterface $context, mixed $payload): void
  {
    if ($this->getAddToPhoto() && empty($this->getPhotoType())) {
      $context->buildViolation('Veuillez choisir le ou les mots-clés de votre photo.')
        ->atPath('photoType')
        ->addViolation();
    }
  }
}
