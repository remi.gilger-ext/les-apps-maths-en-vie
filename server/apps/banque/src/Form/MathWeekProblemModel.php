<?php

declare(strict_types=1);

namespace Banque\Form;

use Banque\Entity\MathWeekProblem;
use Shared\Form\AbstractModel;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\Constraints as Assert;

class MathWeekProblemModel extends AbstractModel
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  #[Assert\NotBlank(
    message: 'Veuillez renseigner le nom de votre école et de votre ville.'
  )]
  #[Assert\Length(
    max: 64,
    maxMessage: 'Veuillez saisir moins de {{ limit }} caractères.'
  )]
  private string $schoolNameAndCity = '';

  #[Assert\NotBlank(
    message: 'Veuillez renseigner le niveau de votre classe.'
  )]
  #[Assert\Length(
    max: 16,
    maxMessage: 'Veuillez saisir moins de {{ limit }} caractères.'
  )]
  private string $nivel = '';

  #[Assert\NotBlank(
    message: 'Veuillez renseigner l\'énoncé du problème.'
  )]
  #[Assert\Length(
    max: 500,
    maxMessage: 'Merci de saisir moins de {{ limit }} caractères.'
  )]
  private string $statement = '';

  private int $photoId = 0;

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getSchoolNameAndCity(): string
  {
    return $this->schoolNameAndCity;
  }

  #================================================================#

  public function getNivel(): string
  {
    return $this->nivel;
  }

  #================================================================#

  public function getStatement(): string
  {
    return $this->statement;
  }

  #================================================================#

  public function getPhotoId(): int
  {
    return $this->photoId;
  }

  #================================================================#

  public function fillInstance(InputBag $params): void
  {
    $this->schoolNameAndCity = $params->getString('schoolNameAndCity');
    $this->nivel = $params->getString('nivel');
    $this->statement = $params->getString('statement');
    $this->photoId = $params->getInt('photoId');
  }

  #================================================================#

  public function createInstance(): MathWeekProblem
  {
    $problem = new MathWeekProblem();

    $problem->setSchoolNameAndCity($this->schoolNameAndCity);
    $problem->setNivel($this->nivel);
    $problem->setStatement($this->statement);
    $problem->setPhotoId($this->photoId);

    return $problem;
  }
}
