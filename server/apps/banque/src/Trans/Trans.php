<?php

declare(strict_types=1);

namespace Banque\Trans;

use Banque\Enum\ProblemTypeEnum;

class Trans
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  #= AdminController ==============================================#

  public const ADMIN_GRANTED_PUBLIC_PROBLEM = 'adminGrantedPublicProblem';

  #= ProblemCommentController =====================================#

  public const PROBLEM_COMMENT_COMMENT_NOT_GRANTED = 'problemCommentCommentNotGranted';

  #= ProblemController ============================================#

  public const PROBLEM_ADD_PUBLIC = 'problemAddPublic';
  public const PROBLEM_ADD_PRIVATE = 'problemAddPrivate';

  #================================================================#
  # Static Methods                                                 #
  #================================================================#

  public static function trans(string $const, ProblemTypeEnum $type): string
  {
    return call_user_func([Trans::class, $const], $type);
  }

  #= AdminController =======================================================#

  public static function adminGrantedPublicProblem(ProblemTypeEnum $type): string
  {
    return match ($type) {
      ProblemTypeEnum::TextProblem => 'Félicitation ! Vous avez désormais accès à l\'intégralité de la banque collaborative des problèmes. N\'oubliez pas que cette banque ne vit que grâce à vous ! C\'est pourquoi, si vous l\'utilisez, nous vous invitons à continuer de l\'alimenter régulièrement de vos problèmes ou photos.',

      ProblemTypeEnum::Photo => 'Félicitation ! Vous avez désormais accès à l\'intégralité de la banque collaborative des photos. N\'oubliez pas que cette banque ne vit que grâce à vous ! C\'est pourquoi, si vous l\'utilisez, nous vous invitons à continuer de l\'alimenter régulièrement de vos problèmes ou photos.',

      ProblemTypeEnum::PhotoProblem => 'Félicitation ! Vous avez désormais accès à l\'intégralité de la banque collaborative des photo-problèmes. N\'oubliez pas que cette banque ne vit que grâce à vous ! C\'est pourquoi, si vous l\'utilisez, nous vous invitons à continuer de l\'alimenter régulièrement de vos problèmes ou photos.'
    };
  }

  #= ProblemCommentController =====================================#

  public static function problemCommentCommentNotGranted(ProblemTypeEnum $type): string
  {
    return match ($type) {
      ProblemTypeEnum::TextProblem => 'Vous devez être connecté et avoir contribué à au moins 3 problèmes pour pouvoir ajouter un commentaire.',
      ProblemTypeEnum::Photo => 'Vous devez être connecté et avoir contribué à au moins 3 photos pour pouvoir ajouter un commentaire.',
      ProblemTypeEnum::PhotoProblem => 'Vous devez être connecté et avoir contribué à au moins 3 photo-problèmes pour pouvoir ajouter un commentaire.'
    };
  }

  #= ProblemController ============================================#

  public static function problemAddPublic(ProblemTypeEnum $type): string
  {
    return match ($type) {
      ProblemTypeEnum::TextProblem => 'Le problème a été soumis à un administrateur (en attente de validation).',
      ProblemTypeEnum::Photo => 'La photo a été soumise à un administrateur (en attente de validation).',
      ProblemTypeEnum::PhotoProblem => 'Le photo-problème a été soumis à un administrateur (en attente de validation).'
    };
  }

  #================================================================#

  public static function problemAddPrivate(ProblemTypeEnum $type): string
  {
    return match ($type) {
      ProblemTypeEnum::TextProblem => 'Le problème a été ajouté avec succès.',
      ProblemTypeEnum::Photo => 'La photo a été ajoutée avec succès.',
      ProblemTypeEnum::PhotoProblem => 'Le photo-problème a été ajouté avec succès.'
    };
  }
}
