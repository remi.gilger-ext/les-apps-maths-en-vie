<?php

declare(strict_types=1);

namespace Banque\Repository;

use Banque\Entity\ProblemLike;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Repository\BaseRepository;

/** @extends BaseRepository<ProblemLike> */
class ProblemLikeRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, ProblemLike::class);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Get all comments of the user
   *
   * @param User $user
   * @return ProblemLike[]
   */
  public function findUserNotificationProblemsLiked(User $user): array
  {
    return $this->createQueryBuilder('l')
      ->leftJoin('l.problem', 'p')
      ->where('p.counterLike > 0')
      ->andWhere('p.user = :user')
      ->andWhere('l.value = 1')
      ->setParameter('user', $user->getId())
      ->orderBy('l.createdAt', 'DESC')
      ->getQuery()
      ->getResult();
  }
}
