<?php

declare(strict_types=1);

namespace Banque\Repository;

use Banque\Entity\ProblemComment;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Repository\BaseRepository;

/** @extends BaseRepository<ProblemComment> */
class ProblemCommentRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, ProblemComment::class);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Get all comments of the user
   *
   * @param User $user
   * @return ProblemComment[]
   */
  public function findUserNotificationProblemsCommented(User $user): array
  {
    return $this->createQueryBuilder('c')
      ->leftJoin('c.problem', 'p')
      ->where('p.counterComment > 0')
      ->andWhere('p.user = :user')
      ->setParameter('user', $user->getId())
      ->orderBy('c.createdAt', 'DESC')
      ->getQuery()
      ->getResult();
  }

  #================================================================#

  /**
   * Get last comments of the week
   *
   * @return ProblemComment[]
   */
  public function lastComments(): array
  {
    return $this->createQueryBuilder('c')
      ->where('c.createdAt > :fromCreationTime')
      ->setParameter('fromCreationTime', new \DateTime("-1 week", new \DateTimeZone("UTC")))
      ->orderBy('c.createdAt', 'DESC')
      ->getQuery()
      ->getResult();
  }
}
