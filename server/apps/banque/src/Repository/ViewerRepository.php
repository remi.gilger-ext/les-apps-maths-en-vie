<?php

declare(strict_types=1);

namespace Banque\Repository;

use Banque\Entity\Viewer;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Repository\BaseRepository;

/** @extends BaseRepository<Viewer> */
class ViewerRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, Viewer::class);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function findViewerByIp(string $ip): ?Viewer
  {
    $date = new \DateTime();
    $date->setTimestamp(\time() - Viewer::TIME_MAX_VISIT);

    return $this->createQueryBuilder('v')
      ->where('v.addressIp = :ip')
      ->andWhere('v.viewingDate > :date')
      ->setParameter('ip', $ip)
      ->setParameter('date', $date)
      ->orderBy('v.id', 'DESC')
      ->getQuery()
      ->setMaxResults(1)
      ->getOneOrNullResult();
  }

  #================================================================#

  public function removeOldConnexions(\DateTime $date): void
  {
    $this->createQueryBuilder('v')
      ->delete()
      ->where('v.viewingDate < :date')
      ->setParameter('date', $date->modify('-' . Viewer::TIME_MAX_VISIT . ' seconds'))
      ->getQuery()
      ->execute();
  }
}
