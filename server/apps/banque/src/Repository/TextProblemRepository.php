<?php

declare(strict_types=1);

namespace Banque\Repository;

use Banque\Entity\Problem;
use Banque\Entity\TextProblem;
use Banque\Enum\GenerateAiTypeEnum;
use Banque\Enum\ResponseStateEnum;

class TextProblemRepository extends ProblemRepository
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * @return int[]
   */
  public function getIdProblemsNeedToGenerateResponse(GenerateAiTypeEnum $type): array
  {
    $traductionType = $type === GenerateAiTypeEnum::English || $type === GenerateAiTypeEnum::Italian || $type === GenerateAiTypeEnum::Spanish || $type === GenerateAiTypeEnum::German;

    $qb = $this->entityManager->createQueryBuilder()
      ->select('p.id')
      ->from(TextProblem::class, 'p')
      ->where('p.responseState IN (:responseState)')
      ->andWhere('p.tag IN (:tag)')
      ->andWhere('p.isVerified = 1');

    if (!$traductionType) {
      $qb->andWhere('p.isExclude = 0');
    }

    if ($type === GenerateAiTypeEnum::Response) {
      $qb->setParameters([
        'responseState' => [ResponseStateEnum::AnsweredByHuman, ResponseStateEnum::NoResponse],
        'tag' => [Problem::TAG_COMMUNITY_PUBLIC, Problem::TAG_SAMPLE],
      ]);
    } elseif ($type === GenerateAiTypeEnum::ResponseExplication) {
      $qb->setParameters([
        'responseState' => [ResponseStateEnum::Verified],
        'tag' => [Problem::TAG_COMMUNITY_PUBLIC, Problem::TAG_SAMPLE],
      ]);
    } else {
      $qb->setParameters([
        'responseState' => ResponseStateEnum::cases(),
        'tag' => [Problem::TAG_COMMUNITY_PUBLIC, Problem::TAG_SAMPLE],
      ]);
    }

    return $qb->getQuery()
      ->getSingleColumnResult();
  }
}

// $qb->innerJoin(TextProblem::class, 'self', Join::WITH, $qb->expr()->eq('p.id', 'self.id'))
