<?php

declare(strict_types=1);

namespace Banque\Repository;

use Banque\Entity\Photo;
use Banque\Entity\PhotoProblem;
use Banque\Entity\Problem;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Repository\BaseRepository;

/** @extends BaseRepository<Problem> */
class ProblemRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, Problem::class);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function isAttachmentSpareBetweenMultipleProblem(string $attachment): bool
  {
    $qb = $this->entityManager->createQueryBuilder()
      ->from(Photo::class, 'p');

    $qb2 = $this->entityManager->createQueryBuilder()
      ->from(PhotoProblem::class, 'p');

    $expr = $qb->expr();

    $numberPhoto = $qb->select($expr->count('p.id'))
      ->where('p.attachment = (:attachment)')
      ->setParameter('attachment', $attachment)
      ->getQuery()
      ->getSingleScalarResult();

    $numberPhotoProblem = $qb2->select($expr->count('p.id'))
      ->where('p.attachment = (:attachment)')
      ->setParameter('attachment', $attachment)
      ->getQuery()
      ->getSingleScalarResult();

    return ($numberPhoto + $numberPhotoProblem) > 1;
  }

  #================================================================#

  /**
   * Get the number of likes and the number of problems liked for the user
   *
   * @param User $user
   * @return array<"nbrLikes"|"nbrProblems", int>
   */
  public function getProblemLiked(User $user): array
  {
    $qb = $this->createQueryBuilder('p');

    return $qb->select('COALESCE(SUM(p.counterLike), 0) as nbrLikes, COUNT(p.id) as nbrProblems')
      ->where('p.user = (:user)')
      ->setParameter('user', $user)
      ->andWhere('p.counterLike > 0')
      ->getQuery()
      ->getScalarResult()[0];
  }

  #================================================================#

  /**
   * Get the number of likes and the number of problems liked for the user
   *
   * @param User $user
   * @return array<"nbrComments"|"nbrProblems", int>
   */
  public function getProblemCommented(User $user): array
  {
    $qb = $this->createQueryBuilder('p');

    return $qb->select('COALESCE(SUM(p.counterComment), 0) as nbrComments, COUNT(p.id) as nbrProblems')
      ->where('p.user = (:user)')
      ->setParameter('user', $user)
      ->andWhere('p.counterComment > 0')
      ->getQuery()
      ->getScalarResult()[0];
  }

  #================================================================#

  /**
   * @return Problem[]
   */
  public function getNewMonthlyProblems(\DateTime $startDay, \DateTime $endDay): array
  {
    return $this->createQueryBuilder('p')
      ->where('p.tag = (:tag)')
      ->andWhere('p.verifiedDate > (:startDay)')
      ->andWhere('p.verifiedDate < (:endDay)')
      ->setParameters([
        'tag' => Problem::TAG_COMMUNITY_PUBLIC,
        'startDay' => $startDay,
        'endDay' => $endDay
      ])
      ->andWhere('p.isVerified = true')
      ->getQuery()
      ->getResult();
  }

  #================================================================#

  public function numberOfContributors(): int
  {
    return $this->createQueryBuilder('p')
      ->select('COUNT(DISTINCT(p.user))')
      ->getQuery()
      ->getSingleScalarResult();
  }

  #================================================================#

  public function getNumberOfDownloads(User $user): int
  {
    $qb = $this->createQueryBuilder('p');

    return (int) $qb->select('SUM(p.downloadCount)')
      ->where('p.user = (:user)')
      ->setParameter('user', $user)
      ->getQuery()
      ->getSingleScalarResult();
  }

  #================================================================#

  /**
   * Get problem from filters
   *
   * @template T of Problem
   *
   * @param class-string<T> $problemType
   * @param array $criteria [array $requiredParam, array $optionalParam]
   * @param boolean $justCount
   * @param integer|null $limit
   * @param integer|null $offset
   *
   * @return ($justCount is true ? int : T[])
   */
  public function getProblemsQuery(string $problemType, array $criteria, bool $justCount, int $limit = null, int $offset = null): int|array
  {
    $qb = $this->entityManager->createQueryBuilder()
      ->from($problemType, 'p');

    $expr = $qb->expr();

    if ($justCount) {
      $qb->select($expr->count('p.id'));
    } else {
      $qb->select('p');
    }

    $requiredAnd = $expr->andX();
    if ($criteria[0] !== []) {
      foreach ($criteria[0] as $key => $value) {
        if ($key === 'photoType') {
          $typeOr = $expr->orX();
          foreach ($value as $v) {
            $typeOr->add($expr->like('p.type', $qb->expr()->literal('%' . $v . '%')));
          }
          $requiredAnd->add($typeOr);
        } elseif ($key === 'textResearch') {
          $requiredAnd->add($expr->like('p.statement', $qb->expr()->literal('%' . $value . '%')));
        } else {
          $requiredAnd->add($expr->in($key, $value));
        }
      }
    }

    $optionalOr = $expr->orX();
    if ($criteria[1] !== []) {
      foreach ($criteria[1] as $key => $value) {
        $optionalOr->add($expr->in($key, $value));
      }
    }

    $noCriteria = true;
    if ($criteria[0] !== $criteria[1] || $criteria[0] !== []) {
      $all = $expr->andX($requiredAnd, $optionalOr);

      $qb->where($all);

      $qb->orderBy('p.counterLike', 'DESC');
      $qb->addOrderBy('p.id', 'ASC');

      $noCriteria = false;
    }

    if ($justCount) {
      if (!$noCriteria) {
        return $qb->getQuery()
          ->getSingleScalarResult();
      } else {
        return 0;
      }
    } else {
      if (false === is_null($offset)) {
        $qb->setFirstResult($offset);
      }
      if (false === is_null($limit)) {
        $qb->setMaxResults($limit);
      }

      return $qb->getQuery()
        ->getResult();
    }
  }
}
