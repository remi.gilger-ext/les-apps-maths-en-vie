<?php

declare(strict_types=1);

namespace Banque\Repository;

use Banque\Entity\MathWeekProblem;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Repository\BaseRepository;

/** @extends BaseRepository<MathWeekProblem> */
class MathWeekProblemRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, MathWeekProblem::class);
  }
}
