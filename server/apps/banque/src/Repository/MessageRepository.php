<?php

declare(strict_types=1);

namespace Banque\Repository;

use Banque\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Repository\BaseRepository;

/** @extends BaseRepository<Message> */
class MessageRepository extends BaseRepository
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(protected EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager, Message::class);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Get all messages of the user
   *
   * @return Message[]
   */
  public function findUserNotificationMessages(User $user): array
  {
    return $this->createQueryBuilder('m')
      ->andWhere('m.receiver = :user')
      ->orWhere('m.receiver is NULL')
      ->andWhere('m.createdAt >= :registerDate')
      ->setParameter('user', $user->getId())
      ->setParameter('registerDate', $user->getRegisterDate())
      ->orderBy('m.createdAt', 'DESC')
      ->getQuery()
      ->getResult();
  }
}
