<?php

declare(strict_types=1);

namespace Banque\Enum;

enum GenerateAiTypeEnum: string
{
  case Response = "response";
  case HelpSentence1 = "helpSentence1";
  case HelpSentence2 = "helpSentence2";
  case ResponseExplication = "responseExplication";
  case English = "english";
  case Italian = "italian";
  case Spanish = "spanish";
  case German = "german";
  case All = "all";
}
