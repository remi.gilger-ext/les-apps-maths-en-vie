<?php

declare(strict_types=1);

namespace Banque\Enum;

enum GradeEnum: int
{
  case NoGrade = 0;
  case Novice = 3;
  case Débutant = 10;
  case Confirmé = 25;
  case Expert = 50;
  case Certifié = 100;
}
