<?php

declare(strict_types=1);

namespace Banque\Enum;

enum NotificationTypeEnum: string
{
  case Like = "likes";
  case Comment = "comments";
  case Message = "messages";
}
