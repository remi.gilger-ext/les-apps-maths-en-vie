<?php

declare(strict_types=1);

namespace Banque\Enum;

enum DailyCounterTypeEnum: string
{
  case VISITS = 'visits';
  case USER_VISITS = 'user_visits';
  case EMAILS_SENT = 'emails_sent';
  case REGISTRATIONS = 'registrations';
  case RITUEL_CLICKS = 'rituel_clicks';
  case PROBLEMS_ADDED = 'problems_added';
  case GAMES_PLAYED = 'games_played';
}
