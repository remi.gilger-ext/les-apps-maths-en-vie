<?php

declare(strict_types=1);

namespace Banque\Enum;

enum ResponseStateEnum: string
{
  case NoResponse = "noResponse";
  case AnsweredByHuman = "answeredByHuman";
  case AnsweredByIa = "answeredByIa";
  case Conflict = "conflict";
  case Verified = "verified";
}
