<?php

declare(strict_types=1);

namespace Banque\Enum;

enum EnigmaTypeEnum: string
{
  case algebriques = "Problèmes algébriques";
  case algorithmiques = "Problèmes algorithmiques";
  case denombrement = "Problèmes de dénombrement";
  case optimisation = "Problèmes d'optimisation";
  case geometrie = "Problèmes de géométrie";
  case logique = "Problèmes de logique";
}
