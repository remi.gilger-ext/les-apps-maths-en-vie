<?php

declare(strict_types=1);

namespace Banque\Enum;

enum TextProblemTypeEnum: string
{
  case rdt = "Recherche du tout";
  case rdp = "Recherche d'une partie";
  case rdtpr = "Recherche du tout - parties réitérées";
  case rvp = "Recherche de la valeur d'une partie";
  case rnp = "Recherche du nombre de parties";
  case refa = "Recherche de l'état final";
  case reia = "Recherche de l'état initial";
  case rea = "Recherche de l'évolution";
  case refm = "Recherche de l'état final ";
  case reim = "Recherche de l'état initial ";
  case rem = "Recherche de l'évolution ";
  case rpqa = "Recherche de la petite quantité";
  case rgqa = "Recherche de la grande quantité";
  case re = "Recherche de l'écart";
  case rpqm = "Recherche de la petite quantité ";
  case rgqm = "Recherche de la grande quantité ";
  case rdr = "Recherche du rapport";
  case add = "Problème additif à étapes";
  case mult = "Problème multiplicatif à étapes";
  case mixed = "Problème mixte à étapes";
  case propor = "Problème de proportionnalité";
  case frac = "Problème avec des fractions";
  case cart = "Produits cartésiens";

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * @param bool $onlyName
   *
   * @return ($onlyName is true ? string[] : TextProblemTypeEnum[])
   */
  public static function getAdditionType(bool $onlyName = false): array
  {
    $result = [
      self::rdt,
      self::rdp,
      self::refa,
      self::reia,
      self::rea,
      self::rpqa,
      self::rgqa,
      self::re
    ];

    if (!$onlyName) {
      return $result;
    }

    return \array_map(function (TextProblemTypeEnum $type) {
      return $type->name;
    }, $result);
  }

  #================================================================#

  /**
   * @param bool $onlyName
   *
   * @return ($onlyName is true ? string[] : TextProblemTypeEnum[])
   */
  public static function getMultiplicationType(bool $onlyName = false): array
  {
    $result = [
      self::rdtpr,
      self::rvp,
      self::rnp,
      self::refm,
      self::reim,
      self::rem,
      self::rpqm,
      self::rgqm,
      self::rdr
    ];

    if (!$onlyName) {
      return $result;
    }

    return \array_map(function (TextProblemTypeEnum $type) {
      return $type->name;
    }, $result);
  }

  #================================================================#

  /**
   * @return string[]
   */
  public static function casesName(): array
  {
    return \array_map(function (TextProblemTypeEnum $type) {
      return $type->name;
    }, self::cases());
  }

  #================================================================#

  public static function fromName(string $name): self
  {
    foreach (self::cases() as $status) {
      if ($name === $status->name) {
        return $status;
      }
    }
    throw new \ValueError("$name is not a valid backing value for enum " . self::class);
  }

  #================================================================#

  public static function tryFromName(string $name): self|null
  {
    try {
      return self::fromName($name);
    } catch (\ValueError $error) {
      return null;
    }
  }
}
