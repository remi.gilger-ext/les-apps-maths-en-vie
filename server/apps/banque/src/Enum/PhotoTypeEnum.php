<?php

declare(strict_types=1);

namespace Banque\Enum;

enum PhotoTypeEnum: string
{
  case geom_1 = "Cercles, disques, sphères et cylindres";
  case geom_2 = "Polygones";
  case geom_3 = "Formes géométriques";
  case geom_4 = "Perpendiculaires";
  case geom_5 = "Parallèles";
  case geom_6 = "Solides";
  case geom_7 = "Symétrie";
  case geom_8 = "Algorithmes";
  case geom_9 = "Angles";

  case meas_1 = "Longueurs";
  case meas_2 = "Vitesses";
  case meas_3 = "Masses";
  case meas_4 = "Volumes";
  case meas_5 = "Temps";
  case meas_6 = "Températures";
  case meas_7 = "Prix";
  case meas_8 = "Aires et périmètres";

  case numb_1 = "Numéros";
  case numb_2 = "Quantités nombrables";
  case numb_3 = "Décimaux";
  case numb_4 = "Fractions";
  case numb_5 = "Proportionnalité";
  case numb_6 = "Pourcentages";
}
