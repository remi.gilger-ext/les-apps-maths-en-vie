<?php

declare(strict_types=1);

namespace Banque\Enum;

enum ProblemTypeEnum: string
{
  case TextProblem = "textProblem";
  case Photo = "photo";
  case PhotoProblem = "photoProblem";
}
