<?php

declare(strict_types=1);

namespace Banque\Service;

use Banque\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class MessageService
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private CacheService $serviceCache,
    private EntityManagerInterface $em,
    private TagAwareCacheInterface $cache
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function sendMessage(User $sender, ?User $receiver, string $title, string $content): void
  {
    $message = new Message();

    $message->setTitle($title);
    $message->setContent($content);
    $message->setReceiver($receiver);

    $sender->sendMessage($message);

    $this->em->persist($message);
    $this->em->flush();

    // We update the cache
    if ($receiver === null) {
      $this->cache->invalidateTags(['notifications']);
    } else {
      $this->serviceCache->getNotifications($receiver, true);
    }
  }
}
