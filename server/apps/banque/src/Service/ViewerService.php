<?php

declare(strict_types=1);

namespace Banque\Service;

use Banque\Entity\Viewer;
use Banque\Enum\DailyCounterTypeEnum;
use Banque\Repository\ViewerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

class ViewerService
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private DailyCounterService $dailyCounterService,
    private EntityManagerInterface $em,
    private ViewerRepository $viewerRepository,
    private Security $security
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function onControllerRequest(ControllerEvent $event): void
  {
    /** @var ?User $user */
    $user = $this->security->getUser();

    if ($user !== null) {
      $date = new \DateTime();
      $date->setTimestamp(\time());

      if ($date->format("d-m-Y") !== $user->getLastActivity()->format("d-m-Y") && \abs($date->getTimestamp() - $user->getLastActivity()->getTimestamp()) > 60 * 1) {
        // We update the counter for each user each day (he ghave so unique user visitor every day)
        $this->dailyCounterService->add(DailyCounterTypeEnum::USER_VISITS);
        $user->setLastActivity(new \DateTime('now', new \DateTimeZone('UTC')));
        $this->em->flush();
      } else {
        // Every minutes the last activity is upgraded
        if (\time() - $user->getLastActivity()->getTimestamp() > 60 * 1) {
          $user->setLastActivity(new \DateTime('now', new \DateTimeZone('UTC')));
          $this->em->flush();
        }
      }
    }

    $request = $event->getRequest();

    $ip = $request->getClientIp();

    if ($ip === null) {
      return;
    }

    if ($this->isBanned($ip)) {
      return;
    }

    $session = $request->getSession();

    if ($session->has('lastVisitedTime')) {
      $time = $session->get('lastVisitedTime');

      if ((\time() - $time) <= Viewer::TIME_MAX_VISIT) {
        # Nothing to do the visitor is already saved
      } else {
        # The time for a visit is already passed we need to add him on the list and udpate the session
        $this->addNewConnexion($ip);

        $session->set('lastVisitedTime', \time());
      }
    } else {
      /**
       * This is the first visit for this user so there is 2 cases :
       *
       *  - His address ip is not in the list so we add him
       *  - His address ip is in the list so he is not new
       */

      $viewer = $this->viewerRepository->findViewerByIp($ip);

      if ($viewer === null) {
        $this->addNewConnexion($ip);

        $session->set('lastVisitedTime', \time());
      } else {
        $session->set('lastVisitedTime', $viewer->getViewingDate()->getTimestamp());
      }
    }
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  private function addNewConnexion(string $ip): void
  {
    $date = new \DateTime();
    $date->setTimestamp(\time());

    $v = new Viewer();
    $v->setAddressIp($ip);
    $v->setUserId(null);
    $v->setViewingDate($date);

    $this->em->persist($v);
    $this->em->flush();

    $this->dailyCounterService->add(DailyCounterTypeEnum::VISITS);
  }

  #================================================================#

  private function isBanned(string $s_ip): bool
  {
    $ip = \ip2long($s_ip);

    if ($ip === false) {
      # Ipv6 address always accepted
      return false;
    }

    $bannedList = [
      [ip2long('17.0.0.0'), ip2long('17.255.255.255')],
      [ip2long('34.64.0.0'), ip2long('34.127.255.255')],
      [ip2long('40.74.0.0'), ip2long('40.125.127.255')],
      [ip2long('52.145.0.0'), ip2long('52.191.255.255')],
      [ip2long('63.64.0.0'), ip2long('63.127.255.255')],
      [ip2long('66.249.64.0'), ip2long('66.249.95.255')],
      [ip2long('78.46.161.64'), ip2long('78.46.161.95')],
      [ip2long('157.54.0.0'), ip2long('157.60.255.255')],
      [ip2long('159.65.0.0'), ip2long('159.65.255.255')],
      [ip2long('207.46.0.0'), ip2long('207.46.255.255')],
      [ip2long('209.143.0.0'), ip2long('209.143.63.255')],
      [ip2long('216.244.64.0'), ip2long('216.244.95.255')]
    ];

    foreach ($bannedList as $bannedRange) {
      if ($ip >= $bannedRange[0] && $ip <= $bannedRange[1]) {
        return true;
      }
    }

    return false;
  }
}
