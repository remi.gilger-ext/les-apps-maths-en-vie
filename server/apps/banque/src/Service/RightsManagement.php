<?php

declare(strict_types=1);

namespace Banque\Service;

use Banque\Enum\ProblemTypeEnum;
use Shared\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

class RightsManagement
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const AMOUT_PUBLIC_PROBLEM_MINIMUM = 3;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private Security $security,
    private CacheService $cacheService
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function isRestrictedForPublicProblem(ProblemTypeEnum $type, bool $isEdition = false): bool
  {
    /**
     * @var User $user
     */
    $user = $this->security->getUser();

    if ($user === null) {
      return true;
    }

    if ($this->security->isGranted('ROLE_ADHERENT') || $this->security->isGranted('ROLE_VIP')) {
      return false;
    } elseif ($isEdition) {
      return true;
    }

    $nbProblemPublic = $this->cacheService->nbProblemPublic($user);

    if ($nbProblemPublic === null) {
      return true;
    }

    if (!isset($nbProblemPublic[$type->value])) {
      throw new \LogicException("Type impossible.");
    }

    if ($nbProblemPublic[$type->value] >= self::AMOUT_PUBLIC_PROBLEM_MINIMUM) {
      return false;
    }

    return true;
  }
}
