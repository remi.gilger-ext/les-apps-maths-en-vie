<?php

declare(strict_types=1);

namespace Banque\Service;

use Dompdf\Dompdf;
use Dompdf\Options;

class PdfService
{
  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  private Dompdf $dompdf;

  /** @var string "portrait"|"landscape" */
  private string $orientation = 'portrait';
  private string $size = 'A4';

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct()
  {
    $this->dompdf = new Dompdf();

    $pdfOptions = new Options();

    $this->dompdf->setOptions($pdfOptions);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function generate(string $html, string $rootPath, bool $base64 = true): ?string
  {
    $this->dompdf->setPaper($this->size, $this->orientation);

    $this->dompdf->getOptions()->setChroot($rootPath);

    $this->dompdf->loadHtml($html);

    $this->dompdf->render();

    if ($base64) {
      return \base64_encode($this->dompdf->output() ?? "");
    }

    return $this->dompdf->output();
  }

  #================================================================#

  /**
   * @param 'portrait'|'landscape' $orientation
   */
  public function setOrientation(string $orientation): void
  {
    $this->orientation = $orientation;
  }

  #================================================================#

  public function setSize(string $size): void
  {
    $this->size = $size;
  }
}
