<?php

declare(strict_types=1);

namespace Banque\Service;

use Banque\Entity\Photo;
use Banque\Entity\PhotoProblem;
use Banque\Entity\Problem;
use Banque\Entity\ProblemComment;
use Banque\Entity\ProblemLike;
use Banque\Entity\TextProblem;
use Banque\Security\Voter\ProblemCommentVoter;
use Banque\Security\Voter\ProblemVoter;
use Shared\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

class ParseToArrayService
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private Security $security
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Parse a problem to an array
   *
   * @return array<string, mixed>
   */
  public function parseProblem(?Problem $problem, User $user = null): array
  {
    if ($problem === null) {
      return [];
    }

    if ($problem instanceof TextProblem) {
      $adminProperty = [];
      if ($this->security->isGranted("ROLE_ADMIN", $user)) {
        $adminProperty['response'] = $problem->getResponse();
        $adminProperty['responseState'] = $problem->getResponseState();
        $adminProperty['responseSentence'] = $problem->getResponseSentence();
        $adminProperty['helpSentence1'] = $problem->getHelpSentence1();
        $adminProperty['helpSentence2'] = $problem->getHelpSentence2();
        $adminProperty['responseExplication'] = $problem->getResponseExplication();
        $adminProperty['isExclude'] = $problem->isExclude();
      }

      $successRate = -1;
      if ($problem->getAmoutAnswered() >= 5) {
        $successRate = $problem->getTotalScore() / $problem->getAmoutAnswered();
      }

      $traductions = $problem->getTraductions();

      return [
        ...$this->parseProblemBase($problem, $user),
        'title' => $problem->getTitle(),
        'statement' => $problem->getStatement(),
        'nivel' => $problem->getNivel(),
        'type' => $problem->getType(),
        'traductions' => [
          "english" => [
            "title" => $traductions?->getEnglishTitle(),
            "statement" => $traductions?->getEnglishStatement(),
          ],
          "italian" => [
            "title" => $traductions?->getItalianTitle(),
            "statement" => $traductions?->getItalianStatement(),
          ],
          "spanish" => [
            "title" => $traductions?->getSpanishTitle(),
            "statement" => $traductions?->getSpanishStatement(),
          ],
          "german" => [
            "title" => $traductions?->getGermanTitle(),
            "statement" => $traductions?->getGermanStatement(),
          ],
        ],
        'successRate' => $successRate,
        ...$adminProperty
      ];
    }
    if ($problem instanceof Photo) {
      return [
        ...$this->parseProblemBase($problem, $user),
        'attachment' => $problem->getAttachment(),
        'type' => $problem->getType(),
      ];
    }
    if ($problem instanceof PhotoProblem) {
      return [
        ...$this->parseProblemBase($problem, $user),
        'statement' => $problem->getStatement(),
        'attachment' => $problem->getAttachment(),
        'nivel' => $problem->getNivel(),
        'type' => $problem->getType(),
      ];
    }

    throw new \LogicException("Problem type impossible");
  }

  #================================================================#

  /**
   * Parse a problem comment to an array
   *
   * @return array<string, mixed>
   */
  public function parseProblemComment(ProblemComment $comment): array
  {
    if ($comment === null) {
      return [];
    }

    return [
      'id' => $comment->getId(),
      'content' => $comment->getContent(),
      'createdAt' => $comment->getCreatedAt()->getTimestamp(),
      'username' => $comment->getUser()->getUsername(),
      'canRemove' => $this->security->isGranted(ProblemCommentVoter::REMOVE, $comment),
      'problemId' => $comment->getProblem()->getId()
    ];
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  /**
   * Parse a problem to an array
   *
   * @return array<string, mixed>
   */
  private function parseProblemBase(Problem $problem, User $user = null): array
  {
    $userProblem = $problem->getUser();

    return [
      'id' => $problem->getId(),
      'counterComment' => $problem->getCounterComment(),
      'counterLike' => $problem->getCounterLike(),
      'discr' => $problem->getDiscr(),
      'username' => $userProblem->getUsername(),
      'grade' => $userProblem->getGrade(),
      'canModify' => $this->security->isGranted(ProblemVoter::MODIFY, $problem),
      'canReport' => $this->security->isGranted(ProblemVoter::REPORT, $problem),
      'canPublish' => $this->security->isGranted(ProblemVoter::PUBLISH, $problem),
      'canRemove' => $this->security->isGranted(ProblemVoter::REMOVE, $problem),
      'isPrivateWait' => $problem->getTag() === Problem::TAG_COMMUNITY_PRIVATE_WAIT,
      'isPrivate' => \in_array($problem->getTag(), [Problem::TAG_COMMUNITY_PRIVATE_WAIT, Problem::TAG_COMMUNITY_PRIVATE]),
      'isLiked' => !empty(\array_filter($problem->getProblemLikes()->toArray(), function (ProblemLike $like) use ($user) {
        return $like->getUser() === $user;
      }))
    ];
  }
}
