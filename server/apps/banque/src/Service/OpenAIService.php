<?php

declare(strict_types=1);

namespace Banque\Service;

use Atelier\Enum\SchoolNivel;
use Banque\Entity\Problem;
use Banque\Entity\ProblemTraduction;
use Banque\Entity\TextProblem;
use Banque\Enum\GenerateAiTypeEnum;
use Banque\Enum\ResponseStateEnum;
use Doctrine\ORM\EntityManagerInterface;
use OpenAI;
use OpenAI\Client;
use OpenAI\Responses\Chat\CreateResponse as ChatCreateResponse;
use OpenAI\Responses\Completions\CreateResponse as CompletionsCreateResponse;

class OpenAIService
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const INPUT_GPT_4O_MINI = 0.000150 / 1000;
  public const OUTPUT_GPT_4O_MINI = 0.000600 / 1000;

  public const STRING_TRIM = " \n\r\t\v\x00\"";

  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  private Client $client;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    string $openaiApiKey,
    private EntityManagerInterface $em
  ) {
    $this->client = OpenAI::client($openaiApiKey);
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function generateIA(TextProblem $problem, GenerateAiTypeEnum $generateAiTypeEnum): void
  {
    // If the problem is not a public contribution or a sample we do nothing
    if (!\in_array($problem->getTag(), [Problem::TAG_COMMUNITY_PUBLIC, Problem::TAG_SAMPLE])) {
      return;
    }

    //==========================================================================
    // Traduction Generation
    if (
      ($generateAiTypeEnum === GenerateAiTypeEnum::English ||
        $generateAiTypeEnum === GenerateAiTypeEnum::Italian ||
        $generateAiTypeEnum === GenerateAiTypeEnum::Spanish ||
        $generateAiTypeEnum === GenerateAiTypeEnum::German ||
        $generateAiTypeEnum === GenerateAiTypeEnum::All) &&
      $problem->getTraductions() === null
    ) {
      $problemTraduction = new ProblemTraduction();
      $problemTraduction->setProblem($problem);
      $problem->setTraductions($problemTraduction);
      $this->em->persist($problemTraduction);
    }

    if (
      $generateAiTypeEnum === GenerateAiTypeEnum::English ||
      $generateAiTypeEnum === GenerateAiTypeEnum::All
    ) {
      $englishTraduction = $this->generateEnglishTraduction($problem);

      $problem->getTraductions()->setEnglishTitle($englishTraduction["title"]);
      $problem->getTraductions()->setEnglishStatement($englishTraduction["statement"]);
    }

    if (
      $generateAiTypeEnum === GenerateAiTypeEnum::Italian ||
      $generateAiTypeEnum === GenerateAiTypeEnum::All
    ) {
      $italianTraduction = $this->generateItalianTraduction($problem);

      $problem->getTraductions()->setItalianTitle($italianTraduction["title"]);
      $problem->getTraductions()->setItalianStatement($italianTraduction["statement"]);
    }

    if (
      $generateAiTypeEnum === GenerateAiTypeEnum::Spanish ||
      $generateAiTypeEnum === GenerateAiTypeEnum::All
    ) {
      $spanishTraduction = $this->generateSpanishTraduction($problem);

      $problem->getTraductions()->setSpanishTitle($spanishTraduction["title"]);
      $problem->getTraductions()->setSpanishStatement($spanishTraduction["statement"]);
    }

    if (
      $generateAiTypeEnum === GenerateAiTypeEnum::German ||
      $generateAiTypeEnum === GenerateAiTypeEnum::All
    ) {
      $germanTraduction = $this->generateGermanTraduction($problem);

      $problem->getTraductions()->setGermanTitle($germanTraduction["title"]);
      $problem->getTraductions()->setGermanStatement($germanTraduction["statement"]);
    }
    //==========================================================================

    // If the problem is excluded we just do the translation
    if ($problem->isExclude()) {
      $this->em->flush();
      return;
    }

    if ($generateAiTypeEnum === GenerateAiTypeEnum::Response || $generateAiTypeEnum === GenerateAiTypeEnum::All) {
      // Generate response and response sentence
      if (
        $problem->getResponseState() === ResponseStateEnum::AnsweredByHuman ||
        $problem->getResponseState() === ResponseStateEnum::NoResponse
      ) {
        $responseSentence = $this->generateResponseSentence($problem);
        $response = $this->generateResponse($problem);

        $goodResponse = null;
        if (
          $responseSentence !== null &&
          $this->responsesEqual($response, $responseSentence["response"])
        ) {
          $goodResponse = $response;
        } else {
          $responsesTry = [];
          for ($i = 0; $i < 3; $i++) {
            $responsesTry[] = $this->generateResponse($problem);
          }

          if (
            $this->responsesEqual($responsesTry[0], $responsesTry[1]) &&
            $this->responsesEqual($responsesTry[0], $responsesTry[2])
          ) {
            $goodResponse = $responsesTry[0];
          }
        }

        // If the IA found a good response
        if ($goodResponse !== null) {
          if ($responseSentence === null) {
            $responseSentence = $this->generateResponseSentence($problem, $goodResponse);
          }

          if ($responseSentence !== null) {
            if ($problem->getResponseState() === ResponseStateEnum::AnsweredByHuman) {
              if ($this->responsesEqual($goodResponse, $problem->getResponse())) {
                $problem->setResponseState(ResponseStateEnum::Verified);

                $this->generateIA($problem, GenerateAiTypeEnum::ResponseExplication);
              } else {
                $problem->setResponseState(ResponseStateEnum::Conflict);
                $problem->setResponse($problem->getResponse() . ";" . $goodResponse);
              }
            } elseif ($problem->getResponseState() === ResponseStateEnum::NoResponse) {
              $problem->setResponse($goodResponse);
              $problem->setResponseState(ResponseStateEnum::AnsweredByIa);
            }

            $problem->setResponseSentence($responseSentence["responseSentence"]);
          }
        }
      } elseif ($problem->getResponseState() === ResponseStateEnum::Verified) {
        $responseSentence = $this->generateResponseSentence($problem, $problem->getResponse());

        if ($responseSentence !== null) {
          $problem->setResponseSentence($responseSentence["responseSentence"]);
        }
      }
    }

    if (
      $generateAiTypeEnum === GenerateAiTypeEnum::HelpSentence1 ||
      $generateAiTypeEnum === GenerateAiTypeEnum::HelpSentence2 ||
      $generateAiTypeEnum === GenerateAiTypeEnum::All
    ) {
      $helpSentences = $this->generateHelpSentences($problem);

      if (
        $generateAiTypeEnum === GenerateAiTypeEnum::HelpSentence1 ||
        $generateAiTypeEnum === GenerateAiTypeEnum::All
      ) {
        $problem->setHelpSentence1($helpSentences["helpSentence1"]);
      }

      if (
        $generateAiTypeEnum === GenerateAiTypeEnum::HelpSentence2 ||
        $generateAiTypeEnum === GenerateAiTypeEnum::All
      ) {
        $problem->setHelpSentence2($helpSentences["helpSentence2"]);
      }
    }

    if (
      ($generateAiTypeEnum === GenerateAiTypeEnum::ResponseExplication ||
        $generateAiTypeEnum === GenerateAiTypeEnum::All) &&
      $problem->getResponseState() === ResponseStateEnum::Verified
    ) {
      $responseExplication = $this->generateResponseExplication($problem);

      $problem->setResponseExplication($responseExplication);
    }

    $this->em->flush();
  }

  #================================================================#

  /**
   * Generate the response sentence
   *
   * @param TextProblem $problem
   * @return array{response: string, responseSentence: string}|null
   */
  protected function generateResponseSentence(TextProblem $problem, ?string $response = null): ?array
  {
    $gpt = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
        ['role' => 'user', 'content' => 'Donne la phrase réponse (verbe sujet complément) avec le résultat écrit sous forme de chiffre et sans donner la réponse ou le calcul pour obtenir la réponse : ' . $problem->getStatement()],
      ],
      'max_tokens' => 100,
    ]);

    if ($gpt->choices[0]->finishReason === "length") {
      return null;
    }

    $this->countApiUsage($gpt);
    \preg_match_all('/\b\d+(?: ?\d{3})*(?:[,.]\d+|\/\d+)?\b/', $gpt->choices[0]->message->content, $matches);

    if ($response !== null && \count($matches[0]) > 1) {
      $allMatches = [...array_filter($matches[0], function ($match) use ($response) {
        return $this->responsesEqual($match, $response);
      })];

      if (\count($allMatches) === 1) {
        return [
          "response" => \str_replace([".", " "], [",", ""], $allMatches[0]),
          "responseSentence" => \trim(\preg_replace("/\b" . $allMatches[0] . "\b/", "[]", $gpt->choices[0]->message->content), self::STRING_TRIM)
        ];
      }

      return null;
    }

    if (\count($matches[0]) === 1) {
      return [
        "response" => \str_replace([".", " "], [",", ""], $matches[0][0]),
        "responseSentence" => \trim(\preg_replace("/\b\d+(?: ?\d{3})*(?:[,.]\d+|\/\d+)?\b/", "[]", $gpt->choices[0]->message->content), self::STRING_TRIM)
      ];
    }

    return null;
  }

  #================================================================#

  protected function generateResponse(TextProblem $problem): ?string
  {
    $response = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
        ['role' => 'user', 'content' => 'Résouds le problème sans latex et met le résultat sous forme de chiffre entre crochets à la toute fin : ' . $problem->getStatement()],
      ],
    ]);

    $this->countApiUsage($response);

    \preg_match_all(
      '/(?:\b\d+(?: ?\d{3})*(?:[,.]\d+|\/\d+)?\b)+(?=[^][]*\])/',
      $response->choices[0]->message->content,
      $matches
    );

    if (\count($matches[0]) === 1) {
      return \str_replace([".", " "], [",", ""], $matches[0][0]);
    }

    return null;
  }

  #================================================================#

  /**
   * Generate the help sentences
   *
   * @param TextProblem $problem
   * @return array{helpSentence1: string, helpSentence2: ?string}
   */
  protected function generateHelpSentences(TextProblem $problem): array
  {
    $responseHelpSentence1 = null;
    $responseHelpSentence2 = null;

    if ($problem->getNivel() === SchoolNivel::CP->value) {
      $responseHelpSentence1 = $this->client->chat()->create([
        'model' => 'gpt-4o-mini',
        'messages' => [
          ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
          ['role' => 'user', 'content' => "Tu doit aider un élève de CP qui est en difficulté pour résoudre ce problème : " . $problem->getStatement() . "Donne-lui une indication simple et courte afin qu'il puisse réaliser un dessin ou un schéma qui va l'aider à trouver la solution. Tu ne doit pas utiliser le concept de multiplication ou de division. Ta réponse doit commencer par \"Pour t'aider à trouver la solution tu peux dessiner...\""],
        ],
        'max_tokens' => 250,
      ]);
    } elseif ($problem->getNivel() === SchoolNivel::CE1->value) {
      $responseHelpSentence1 = $this->client->chat()->create([
        'model' => 'gpt-4o-mini',
        'messages' => [
          ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
          ['role' => 'user', 'content' => "Tu doit aider un élève de CE1 qui est en difficulté pour résoudre ce problème : " . $problem->getStatement() . "Donne-lui une indication simple et courte afin qu'il puisse réaliser un dessin ou un schéma qui va l'aider à trouver la solution. Tu ne doit pas utiliser le concept de division mais tu peux utiliser le concept de mutliplication. Ta réponse doit commencer par \"Pour t'aider à trouver la solution tu peux dessiner...\""],
        ],
        'max_tokens' => 250,
      ]);
    } elseif ($problem->getNivel() === SchoolNivel::CE2->value) {
      $responseHelpSentence1 = $this->client->chat()->create([
        'model' => 'gpt-4o-mini',
        'messages' => [
          ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
          ['role' => 'user', 'content' => "Tu ne doit pas utiliser le concept de division mais tu peux utiliser le concept de mutliplication. Indiquer, en une seule phrase qui commence par \"Tu dois utiliser\", les données qu'on doit utiliser pour résoudre ce problème : " . $problem->getStatement()],
        ],
        'max_tokens' => 250,
      ]);

      $responseHelpSentence2 = $this->client->chat()->create([
        'model' => 'gpt-4o-mini',
        'messages' => [
          ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
          ['role' => 'user', 'content' => "Tu ne doit pas utiliser le concept de division mais tu peux utiliser le concept de mutliplication. Donne une très simple indication sans donner la réponse pour résoudre ce problème sans latex ni gras : " . $problem->getStatement()],
        ],
        'max_tokens' => 250,
      ]);
    } else {
      $responseHelpSentence1 = $this->client->chat()->create([
        'model' => 'gpt-4o-mini',
        'messages' => [
          ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
          ['role' => 'user', 'content' => "Indiquer, en une seule phrase qui commence par \"Tu dois utiliser\", les données qu'on doit utiliser pour résoudre ce problème : " . $problem->getStatement()],
        ],
        'max_tokens' => 250,
      ]);

      $responseHelpSentence2 = $this->client->chat()->create([
        'model' => 'gpt-4o-mini',
        'messages' => [
          ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
          ['role' => 'user', 'content' => "Donne une très simple indication sans donner la réponse pour résoudre ce problème sans latex ni gras : " . $problem->getStatement()],
        ],
        'max_tokens' => 250,
      ]);
    }

    $this->countApiUsage($responseHelpSentence1);
    $this->countApiUsage($responseHelpSentence2);

    return [
      "helpSentence1" => $responseHelpSentence1 ? \trim($responseHelpSentence1->choices[0]->message->content, self::STRING_TRIM) : null,
      "helpSentence2" => $responseHelpSentence2 ? \trim($responseHelpSentence2->choices[0]->message->content, self::STRING_TRIM) : null,
    ];
  }

  #================================================================#

  /**
   * Generate the response explication
   *
   * @param TextProblem $problem
   * @return string
   */
  protected function generateResponseExplication(TextProblem $problem): string
  {
    if ($problem->getNivel() === SchoolNivel::CP->value) {
      $responseExplication = $this->client->chat()->create([
        'model' => 'gpt-4o-mini',
        'messages' => [
          ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
          ['role' => 'user', 'content' => "Tu ne doit pas utiliser le concept de multiplication ou de division. Explique par un court texte en faisant le moins de phrase possible à l'élève sans latex et sans gras comment résoudre ce problème pour trouver le résultat qui doit obligatoirement être " . $problem->getResponse() . " : " . $problem->getStatement()],
        ],
        'max_tokens' => 250,
      ]);
    } elseif (\in_array($problem->getNivel(), [SchoolNivel::CE1->value, SchoolNivel::CE2->value])) {
      $responseExplication = $this->client->chat()->create([
        'model' => 'gpt-4o-mini',
        'messages' => [
          ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
          ['role' => 'user', 'content' => "Tu ne doit pas utiliser le concept de division mais tu peux utiliser le concept de mutliplication. Explique par un court texte en faisant le moins de phrase possible à l'élève sans latex et sans gras comment résoudre ce problème pour trouver le résultat qui doit obligatoirement être " . $problem->getResponse() . " : " . $problem->getStatement()],
        ],
        'max_tokens' => 250,
      ]);
    } else {
      $responseExplication = $this->client->chat()->create([
        'model' => 'gpt-4o-mini',
        'messages' => [
          ['role' => 'system', 'content' => 'Tu es une enseignante de ' . $problem->getNivel() . ' en primaire'],
          ['role' => 'user', 'content' => "Explique par un court texte en faisant le moins de phrase possible à l'élève sans latex et sans gras comment résoudre ce problème pour trouver le résultat qui doit obligatoirement être " . $problem->getResponse() . " : " . $problem->getStatement()],
        ],
        'max_tokens' => 250,
      ]);
    }

    $this->countApiUsage($responseExplication);

    return \trim($responseExplication->choices[0]->message->content, self::STRING_TRIM);
  }

  #================================================================#

  /**
   * Generate the english traduction
   *
   * @return array{title: string, statement: string}
   */
  protected function generateEnglishTraduction(TextProblem $problem): array
  {
    $title = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es un traducteur de francais vers anglais'],
        ['role' => 'user', 'content' => "Traduit le titre suivant en anglais, donne seulement la traduction : " . $problem->getTitle()],
      ],
      'max_tokens' => 512,
    ]);

    $statement = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es un traducteur de francais vers anglais'],
        ['role' => 'user', 'content' => "Traduit le problème de mathématique suivant en anglais, donne seulement la traduction et fait un retour à la ligne pour la question : " . $problem->getStatement()],
      ],
      'max_tokens' => 512,
    ]);

    $this->countApiUsage($title);
    $this->countApiUsage($statement);

    return [
      "title" => \trim($title->choices[0]->message->content, self::STRING_TRIM),
      "statement" => \str_replace("\n\n", "\n", \trim($statement->choices[0]->message->content, self::STRING_TRIM))
    ];
  }

  #================================================================#

  /**
   * Generate the italian traduction
   *
   * @return array{title: string, statement: string}
   */
  protected function generateItalianTraduction(TextProblem $problem): array
  {
    $title = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es un traducteur de francais vers italien'],
        ['role' => 'user', 'content' => "Traduit le titre suivant en italien, donne seulement la traduction : " . $problem->getTitle()],
      ],
      'max_tokens' => 512,
    ]);

    $statement = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es un traducteur de francais vers italien'],
        ['role' => 'user', 'content' => "Traduit le problème de mathématique suivant en italien, donne seulement la traduction et fait un retour à la ligne pour la question : " . $problem->getStatement()],
      ],
      'max_tokens' => 512,
    ]);

    $this->countApiUsage($title);
    $this->countApiUsage($statement);

    return [
      "title" => \trim($title->choices[0]->message->content, self::STRING_TRIM),
      "statement" => \str_replace("\n\n", "\n", \trim($statement->choices[0]->message->content, self::STRING_TRIM))
    ];
  }

  #================================================================#

  /**
   * Generate the spanish traduction
   *
   * @return array{title: string, statement: string}
   */
  protected function generateSpanishTraduction(TextProblem $problem): array
  {
    $title = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es un traducteur de francais vers espagnol'],
        ['role' => 'user', 'content' => "Traduit le titre suivant en espagnol, donne seulement la traduction : " . $problem->getTitle()],
      ],
      'max_tokens' => 512,
    ]);

    $statement = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es un traducteur de francais vers espagnol'],
        ['role' => 'user', 'content' => "Traduit le problème de mathématique suivant en espagnol, donne seulement la traduction et fait un retour à la ligne pour la question : " . $problem->getStatement()],
      ],
      'max_tokens' => 512,
    ]);

    $this->countApiUsage($title);
    $this->countApiUsage($statement);

    return [
      "title" => \trim($title->choices[0]->message->content, self::STRING_TRIM),
      "statement" => \str_replace("\n\n", "\n", \trim($statement->choices[0]->message->content, self::STRING_TRIM))
    ];
  }

  #================================================================#

  /**
   * Generate the german traduction
   *
   * @return array{title: string, statement: string}
   */
  protected function generateGermanTraduction(TextProblem $problem): array
  {
    $title = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es un traducteur de francais vers allemand'],
        ['role' => 'user', 'content' => "Traduit le titre suivant en allemand, donne seulement la traduction : " . $problem->getTitle()],
      ],
      'max_tokens' => 512,
    ]);

    $statement = $this->client->chat()->create([
      'model' => 'gpt-4o-mini',
      'messages' => [
        ['role' => 'system', 'content' => 'Tu es un traducteur de francais vers allemand'],
        ['role' => 'user', 'content' => "Traduit le problème de mathématique suivant en allemand, donne seulement la traduction et fait un retour à la ligne pour la question : " . $problem->getStatement()],
      ],
      'max_tokens' => 512,
    ]);

    $this->countApiUsage($title);
    $this->countApiUsage($statement);

    return [
      "title" => \trim($title->choices[0]->message->content, self::STRING_TRIM),
      "statement" => \str_replace("\n\n", "\n", \trim($statement->choices[0]->message->content, self::STRING_TRIM))
    ];
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  private function countApiUsage(ChatCreateResponse | CompletionsCreateResponse $response = null): void
  {
    if ($response === null) {
      return;
    }

    $cost = 0;

    $cost += $response->usage->promptTokens * self::INPUT_GPT_4O_MINI;
    $cost += $response->usage->completionTokens * self::OUTPUT_GPT_4O_MINI;
  }

  #================================================================#

  public function responsesEqual(?string $response1, ?string $response2): bool
  {
    if ($response1 === null || $response2 === null) {
      return false;
    }

    if (\str_contains($response1, '/')) {
      $explode = explode('/', $response1);
      $response1f = floatval($explode[0] / $explode[1]);
    } else {
      $response1f = floatval(\str_replace([",", " "], [".", ""], $response1));
    }
    if (\str_contains($response2, '/')) {
      $explode = explode('/', $response2);
      $response2f = floatval($explode[0] / $explode[1]);
    } else {
      $response2f = floatval(\str_replace([",", " "], [".", ""], $response2));
    }

    return round($response1f, 2) === round($response2f, 2);
  }
}
