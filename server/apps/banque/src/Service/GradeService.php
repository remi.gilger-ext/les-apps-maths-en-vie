<?php

declare(strict_types=1);

namespace Banque\Service;

use Banque\Enum\GradeEnum;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Repository\UserRepository;
use Shared\Entity\User;

class GradeService
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const CERTIFIE_GRADE_AMOUNT = 100;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private CacheService $cacheService,
    private EntityManagerInterface $em,
    private MessageService $messageService,
    private UserRepository $userRepository
  ) {
  }

  #================================================================#
  # Static Methods                                                 #
  #================================================================#

  /**
   * Get the grade step
   *
   * @return array<int, array<int|string>|null> the grade
   */
  public static function getGradeStep(): array
  {
    return [
      GradeEnum::NoGrade->value => [GradeEnum::NoGrade->value, GradeEnum::Novice->value - 1, GradeEnum::Novice->name],
      GradeEnum::Novice->value => [GradeEnum::Novice->value, GradeEnum::Débutant->value - 1, GradeEnum::Débutant->name],
      GradeEnum::Débutant->value => [GradeEnum::Débutant->value, GradeEnum::Confirmé->value - 1, GradeEnum::Confirmé->name],
      GradeEnum::Confirmé->value => [GradeEnum::Confirmé->value, GradeEnum::Expert->value - 1, GradeEnum::Expert->name],
      GradeEnum::Expert->value => [GradeEnum::Expert->value, GradeEnum::Certifié->value - 1, GradeEnum::Certifié->name],
      GradeEnum::Certifié->value => null
    ];
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Get the grade based on the number of public problem
   *
   * @param int $numberOfPublicProblems
   * @return int the grade
   */
  public function getGrade(int $numberOfPublicProblems): int
  {
    if ($numberOfPublicProblems <= GradeEnum::NoGrade->value) {
      return GradeEnum::NoGrade->value;
    }

    foreach (self::getGradeStep() as $grade => $gap) {
      if ($gap === null || $numberOfPublicProblems >= $gap[0] && $numberOfPublicProblems <= $gap[1]) {
        return $grade;
      }
    }

    throw new \LogicException("The user need to have a grade");
  }

  #================================================================#

  /**
   * Upgrade the grade of the user based on the number of public problems
   *
   * @param User $user
   * @return void
   */
  public function updateGrade(User $user): void
  {
    $problems = $this->cacheService->nbProblemPublic($user);

    $nbPublicProblem = \array_sum($problems);

    $gradeBefore = $user->getGrade();
    $gradeNow = $this->getGrade($nbPublicProblem);

    if ($gradeNow > $gradeBefore) {
      $mathsenvie = $this->userRepository->findOneBy(['username' => 'Maths_en_vie']);
      $this->messageService->sendMessage($mathsenvie, $user, "Vous avez atteint un nouveau grade", "Félicitations {$user->getUsername()},\nGrace à vos contributions, vous avez atteint le grade \"" . GradeEnum::tryFrom($gradeNow)->name . "\".\n Merci pour votre participation.");
    }

    $user->setGrade($gradeNow);
    $this->em->flush();
  }
}
