<?php

declare(strict_types=1);

namespace Banque\Service;

use Banque\Enum\NotificationTypeEnum;
use Banque\Service\CacheService;
use Shared\Entity\User;

class NotificationService
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private CacheService $cache
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Get the number of new notifications in each type and in total
   *
   * @param User $user target
   *
   * @return array{total: int, types: array<value-of<NotificationTypeEnum>, int>}
   */
  public function getNumberNewNotification(User $user): array
  {
    $notificationsTypes = $this->cache->getNotifications($user);

    $lastTimeNotificationChecked = $user->getLastNotificationChecked()->getTimestamp();

    $nbNewNotification = 0;
    $nbNotificationEachType = [];
    foreach ($notificationsTypes as $type => $notifications) {
      $nbNotificationEachType[$type] = 0;

      foreach ($notifications as $notification) {
        if ($notification['date'] >= $lastTimeNotificationChecked) {
          $nbNotificationEachType[$type] += 1;
          $nbNewNotification += 1;
          continue;
        }

        break;
      }
    }

    return [
      'total' => $nbNewNotification,
      'types' => $nbNotificationEachType
    ];
  }
}
