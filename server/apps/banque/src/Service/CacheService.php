<?php

declare(strict_types=1);

namespace Banque\Service;

use Banque\Entity\Message;
use Banque\Entity\Problem;
use Banque\Entity\ProblemComment;
use Banque\Entity\ProblemLike;
use Banque\Enum\DailyCounterTypeEnum;
use Banque\Enum\NotificationTypeEnum;
use Banque\Enum\ProblemTypeEnum;
use Banque\Repository\MessageRepository;
use Banque\Repository\ProblemCommentRepository;
use Banque\Repository\ProblemLikeRepository;
use Banque\Repository\ProblemRepository;
use Shared\Entity\User;
use Shared\Repository\UserRepository;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class CacheService
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private string $dailyCounterFile,
    private string $directoryEnigma,
    private MessageRepository $messageRepository,
    private ProblemRepository $problemRepository,
    private ProblemCommentRepository $problemCommentRepository,
    private ProblemLikeRepository $problemLikeRepository,
    private TagAwareCacheInterface $cache,
    private UserRepository $userRepository
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  /**
   * Get the amout of public problem of the user
   *
   * @param User $user
   * @param boolean $remove
   * @return ($remove is true ? null : array<value-of<ProblemTypeEnum>, int>)
   */
  public function nbProblemPublic(User $user, bool $remove = false): ?array
  {
    if ($remove) {
      $this->cache->delete('nbProblemPublic' . $user->getId());
      return null;
    }

    return $this->cache->get('nbProblemPublic' . $user->getId(), function (ItemInterface $item) use ($user) {
      $item->expiresAfter(1800); // 30 min

      $problems = $this->problemRepository->findBy([
        'user' => $user->getId(),
        'tag' => Problem::TAG_COMMUNITY_PUBLIC,
        'isVerified' => true
      ]);

      return [
        ProblemTypeEnum::TextProblem->value => \count(Problem::getTextProblem($problems)),
        ProblemTypeEnum::Photo->value => \count(Problem::getPhoto($problems)),
        ProblemTypeEnum::PhotoProblem->value => \count(Problem::getPhotoProblem($problems))
      ];
    });
  }

  #================================================================#

  /**
   * To get all notification order by date, most recent is the first
   *
   * @param User $user
   * @param boolean $remove
   * @return ($remove is true ? null : array<value-of<NotificationTypeEnum>, mixed>)
   */
  public function getNotifications(User $user, bool $remove = false): ?array
  {
    if ($remove) {
      $this->cache->delete('notfications' . $user->getId());
      return null;
    }

    return $this->cache->get('notfications' . $user->getId(), function (ItemInterface $item) use ($user) {
      $item->expiresAfter(3600); // 1h
      $item->tag('notifications');

      $likes = $this->problemLikeRepository->findUserNotificationProblemsLiked($user);
      $likes = \array_map(function (ProblemLike $like) {
        return [
          "type" => NotificationTypeEnum::Like,
          "date" => $like->getCreatedAt()->getTimestamp(),
          "username" => $like->getUser()->getUsername(),
          "id" => $like->getProblem()->getId(),
          "p_discr" => $like->getProblem()->getDiscr()
        ];
      }, $likes);

      $comments = $this->problemCommentRepository->findUserNotificationProblemsCommented($user);
      $comments = \array_map(function (ProblemComment $comment) {
        return [
          "type" => NotificationTypeEnum::Comment,
          "date" => $comment->getCreatedAt()->getTimestamp(),
          "username" => $comment->getUser()->getUsername(),
          "id" => $comment->getId(),
          "p_id" => $comment->getProblem()->getId(),
          "p_discr" => $comment->getProblem()->getDiscr()
        ];
      }, $comments);

      $messages = $this->messageRepository->findUserNotificationMessages($user);
      $messages = \array_map(function (Message $message) {
        return [
          "type" => NotificationTypeEnum::Message,
          "date" => $message->getCreatedAt()->getTimestamp(),
          "username" => $message->getSender()->getUsername(),
          "title" => $message->getTitle(),
          "content" => $message->getContent(),
          "id" => $message->getId()
        ];
      }, $messages);

      return [
        NotificationTypeEnum::Like->value => $likes,
        NotificationTypeEnum::Comment->value => $comments,
        NotificationTypeEnum::Message->value => $messages
      ];
    });
  }

  #================================================================#

  /**
   * To get the banque stats
   *
   * @param boolean $remove
   * @return ($remove is true ? null : array<string, mixed>)
   */
  public function getBanqueStats(bool $remove = false): ?array
  {
    if ($remove) {
      $this->cache->delete('banque-stats');
      return null;
    }

    return $this->cache->get('banque-stats', function (ItemInterface $item) {
      $item->expiresAfter(300); // 5 min

      $totalContributions = $this->problemRepository->count([
        'isVerified' => 1,
        'tag' => [
          Problem::TAG_COMMUNITY_PUBLIC,
          Problem::TAG_SAMPLE
        ]
      ]);

      $totalContributions += $this->problemsCounter()["enigma"];

      $numberOfContributors = $this->problemRepository->numberOfContributors();

      $topUsers = $this->userRepository->getTopUsers(10);

      $numberOfVisit = 0;
      if (\file_exists($this->dailyCounterFile)) {
        $visits = \json_decode(\file_get_contents($this->dailyCounterFile), true);

        foreach ($visits as $visit) {
          $numberOfVisit += $visit[DailyCounterTypeEnum::VISITS->value];
        }
      }

      return [
        'totalContributions' => $totalContributions,
        'numberOfContributors' => $numberOfContributors,
        'topUsers' => $topUsers,
        'numberOfVisit' => $numberOfVisit,
      ];
    });
  }

  #================================================================#

  /**
   * To count every type of problem to show them in home page
   *
   * @param boolean $remove
   * @return ($remove is true ? null : array{textProblem: int, photo: int, photoProblem: int, enigma: int})
   */
  public function problemsCounter(bool $remove = false): ?array
  {
    if ($remove) {
      $this->cache->delete('problemsCounter');
      return null;
    }

    return $this->cache->get('problemsCounter', function (ItemInterface $item) {
      $item->expiresAfter(300); // 5 min

      $problems = $this->problemRepository->findBy([
        'tag' => [
          Problem::TAG_COMMUNITY_PUBLIC,
          Problem::TAG_SAMPLE
        ],
        'isVerified' => true
      ]);

      $enigma = 0;
      $baseDirectory = $this->directoryEnigma;
      if ($dh = \opendir($baseDirectory)) {
        while (($subDirectory = \readdir($dh)) !== false) {
          if (is_dir($baseDirectory . '/' . $subDirectory) && !\in_array($subDirectory, ['.', '..'])) {
            $enigma += (\count(\scandir($baseDirectory . '/' . $subDirectory)) - 2);
          }
        }
        \closedir($dh);
      }

      return [
        ProblemTypeEnum::TextProblem->value => \count(Problem::getTextProblem($problems)),
        ProblemTypeEnum::Photo->value => \count(Problem::getPhoto($problems)),
        ProblemTypeEnum::PhotoProblem->value => \count(Problem::getPhotoProblem($problems)),
        'enigma' => $enigma
      ];
    });
  }
}
