<?php

declare(strict_types=1);

namespace Banque\Service;

use Banque\Enum\DailyCounterTypeEnum;
use Banque\Repository\ViewerRepository;
use Shared\Utils\Utils;

class DailyCounterService
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private string $dailyCounterFile,
    private ViewerRepository $viewerRepository
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function add(DailyCounterTypeEnum $type): void
  {
    $date = new \DateTime();
    $date->setTimestamp(\time());

    // We create the file if it not exist
    Utils::fileExistsSafe($this->dailyCounterFile);

    $myfile = fopen($this->dailyCounterFile, 'rt');
    flock($myfile, \LOCK_SH);
    $content = \file_get_contents($this->dailyCounterFile);
    fclose($myfile);

    if ($content === false) {
      return;
    }

    $visits = \json_decode($content, true);

    $dayTime = $date->format("d-m-Y");

    if ($visits !== null) {
      if (isset($visits[$dayTime])) {
        // Increment counter
        if (!isset($visits[$dayTime][$type->value])) {
          // This is used just when a new counter is added for the first day
          $visits[$dayTime][$type->value] = 1;
        } else {
          $visits[$dayTime][$type->value] += 1;
        }

        $newData = $visits;
      } else {
        // Initialization
        $newVisit = [];

        foreach (DailyCounterTypeEnum::cases() as $case) {
          $newVisit[$dayTime][$case->value] = 0;
        }

        // Increment counter
        $newVisit[$dayTime][$type->value] += 1;

        $newData = $newVisit + $visits;

        /**
         * NOTE: Each of these action below will be executed each day if the counter is incremented (lot of chance to be right after midnight)
         */
        $this->viewerRepository->removeOldConnexions($date);
      }
    } else {
      // If the file is empty, initialization...
      foreach (DailyCounterTypeEnum::cases() as $case) {
        $newData[$dayTime][$case->value] = 0;
      }

      // Increment counter
      $newData[$dayTime][$type->value] += 1;
    }

    file_put_contents($this->dailyCounterFile, json_encode($newData), \LOCK_EX);
  }
}
