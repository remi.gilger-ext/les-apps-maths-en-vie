<?php

declare(strict_types=1);

namespace Banque\Service\Log;

use Psr\Log\LoggerInterface;
use Shared\Entity\User;

class UserLog
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private LoggerInterface $logger
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function granted(User $user, string $role): void
  {
    $this->logger->info("[GRANTED] : {$user->getUserIdentifier()} granted role {$role}");
  }

  #================================================================#

  public function subscribedToEmails(User $user): void
  {
    $this->logger->info("[SUBSCRIBE EMAIL] : {$user->getUserIdentifier()} subscribed to emails");
  }

  #================================================================#

  public function unsubscribedToEmails(User $user): void
  {
    $this->logger->info("[UNSUBSCRIBE EMAIL] : {$user->getUserIdentifier()} unsubscribed to emails");
  }
}
