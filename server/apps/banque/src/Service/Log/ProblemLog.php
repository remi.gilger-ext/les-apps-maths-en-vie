<?php

declare(strict_types=1);

namespace Banque\Service\Log;

use Shared\Entity\User;
use Psr\Log\LoggerInterface;

class ProblemLog
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private LoggerInterface $logger
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function logEvent(User $user, string $reason, string $message): void
  {
    $this->logger->info("[{$reason}] : {$user->getUserIdentifier()} {$message}");
  }
}
