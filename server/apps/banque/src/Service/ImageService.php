<?php

declare(strict_types=1);

namespace Banque\Service;

class ImageService
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const WIDTH_PERCENT = 0.20;

  #================================================================#
  # Private Attributs                                              #
  #================================================================#

  private \GdImage $image;
  private string $extension;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private string $photoDirectory,
    private string $logoPath
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function setImage(string $filePath): void
  {
    $this->extension = \explode('.', $filePath)[1];

    if ($this->extension === 'png') {
      $this->image = \imagecreatefrompng($this->photoDirectory . '/' . $filePath);
    } elseif ($this->extension === 'jpg' || $this->extension === 'jpeg') {
      $this->image = \imagecreatefromjpeg($this->photoDirectory . '/' . $filePath);

      # Jpeg image can have a default rotation that we nned to remove
      $exif = \exif_read_data($this->photoDirectory . '/' . $filePath);
      if ($exif !== false && isset($exif['Orientation'])) {
        $orientation = $exif['Orientation'];

        switch ($orientation) {
          case 2:
            imageflip($this->image, IMG_FLIP_HORIZONTAL);
            break;
          case 3:
            $this->image = imagerotate($this->image, 180, 0);
            break;
          case 4:
            imageflip($this->image, IMG_FLIP_VERTICAL);
            break;
          case 5:
            $this->image = imagerotate($this->image, -90, 0);
            imageflip($this->image, IMG_FLIP_HORIZONTAL);
            break;
          case 6:
            $this->image = imagerotate($this->image, -90, 0);
            break;
          case 7:
            $this->image = imagerotate($this->image, 90, 0);
            imageflip($this->image, IMG_FLIP_HORIZONTAL);
            break;
          case 8:
            $this->image = imagerotate($this->image, 90, 0);
            break;
          default:
        }
      }
    }
  }

  #================================================================#

  public function saveImage(string $filePath): void
  {
    if ($this->extension === 'png') {
      \imagepng($this->image, $this->photoDirectory . '/' . $filePath);
    } elseif ($this->extension === 'jpg' || $this->extension === 'jpeg') {
      \imagejpeg($this->image, $this->photoDirectory . '/' . $filePath);
    }
  }

  #================================================================#

  public function addLogo(): void
  {
    $source = \imagecreatefrompng($this->logoPath);

    $coeff = \imagesx($source) / \imagesy($source);

    $newWidth = (int) (\imagesx($this->image) * self::WIDTH_PERCENT);
    $newheight = (int) ($newWidth / $coeff);
    $thumb = \imagecreatetruecolor($newWidth, $newheight);

    \imagealphablending($source, true);
    \imagesavealpha($source, true);
    \imagecolortransparent($thumb, \imagecolorallocate($thumb, 0, 0, 0));

    \imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newheight, \imagesx($source), \imagesy($source));

    \imagecopy(
      $this->image,
      $thumb,
      (int) (\imagesx($this->image) - $newWidth - $newheight / 2),
      (int) (\imagesy($this->image) - 1.5 * $newheight),
      0,
      0,
      $newWidth,
      $newheight
    );
  }

  #================================================================#

  public function rotate(int $degrees): void
  {
    if ($this->enoughMemory(\imagesx($this->image), \imagesy($this->image))) {
      $this->image = imagerotate($this->image, -$degrees, 0);
    }
  }

  #================================================================#

  public function resize(string $datas): void
  {
    $numbers = \explode(';', $datas);

    $srcOffsetX = (int) (\imagesx($this->image) * ($numbers[0] < 0 ? 0 : $numbers[0]));
    $srcOffsetY = (int) (\imagesy($this->image) * ($numbers[1] < 0 ? 0 : $numbers[1]));

    $srcWidth = (int) (\imagesx($this->image) * ($numbers[2] > 1 ? 1 : $numbers[2]));
    $srcHeight = (int) (\imagesy($this->image) * ($numbers[3] > 1 ? 1 : $numbers[3]));

    if ($this->enoughMemory($srcWidth, $srcHeight)) {
      $thumb = \imagecreatetruecolor($srcWidth, $srcHeight);

      if ($this->enoughMemory($srcWidth, $srcHeight)) {
        imagefill($thumb, 0, 0, \imagecolorallocate($thumb, 255, 255, 255));

        if ($this->enoughMemory($srcWidth, $srcHeight)) {
          \imagecopyresized(
            $thumb,
            $this->image,
            0,
            0,
            $srcOffsetX,
            $srcOffsetY,
            $srcWidth,
            $srcHeight,
            $srcWidth,
            $srcHeight
          );
          $this->image = $thumb;
        }
      }
    }
  }

  #================================================================#

  public function enoughMemory(int $x, int $y, int $rgb = 3)
  {
    return $x * $y * $rgb * 1.7 < ((int) ini_get('memory_limit') * 1024 * 1024) - memory_get_usage();
  }
}
