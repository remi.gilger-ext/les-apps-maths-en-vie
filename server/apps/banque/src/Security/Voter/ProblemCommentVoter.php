<?php

declare(strict_types=1);

namespace Banque\Security\Voter;

use Banque\Entity\ProblemComment;
use Shared\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ProblemCommentVoter extends Voter
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const VIEW = 'PROBLEM_COMMENT_VIEW';
  public const REMOVE = 'PROBLEM_COMMENT_REMOVE';

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private Security $security
  ) {}

  #================================================================#
  # Protected Methods                                              #
  #================================================================#

  protected function supports(string $attribute, mixed $subject): bool
  {
    return in_array($attribute, [
      self::VIEW,
      self::REMOVE
    ]) && $subject instanceof ProblemComment;
  }

  #================================================================#

  protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if (!$user instanceof UserInterface) {
      return false;
    }

    /** @var ProblemComment $comment */
    $comment = $subject;

    return match ($attribute) {
      self::VIEW => $this->canView(),
      self::REMOVE => $this->canRemove($comment, $user),
      default => throw new \LogicException('This code should not be reached!')
    };
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  private function canView(): bool
  {
    return true;
  }

  #================================================================#

  private function canRemove(ProblemComment $comment, User $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    if ($user === $comment->getUser()) {
      return true;
    }

    return false;
  }
}
