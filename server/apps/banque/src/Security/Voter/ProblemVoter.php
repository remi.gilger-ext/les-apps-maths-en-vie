<?php

declare(strict_types=1);

namespace Banque\Security\Voter;

use Banque\Entity\Problem;
use Banque\Service\RightsManagement;
use Shared\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ProblemVoter extends Voter
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const VIEW = 'PROBLEM_VIEW';
  public const MODIFY = 'PROBLEM_MODIFY';
  public const EDIT = 'PROBLEM_EDIT';
  public const REPORT = 'PROBLEM_REPORT';
  public const PUBLISH = 'PROBLEM_PUBLISH';
  public const REMOVE = 'PROBLEM_REMOVE';
  public const LIKE = 'PROBLEM_LIKE';
  public const COMMENT = 'PROBLEM_COMMENT';

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private Security $security,
    private RightsManagement $rightsManagement
  ) {}

  #================================================================#
  # Protected Methods                                              #
  #================================================================#

  protected function supports(string $attribute, mixed $subject): bool
  {
    return in_array($attribute, [
      self::EDIT,
      self::VIEW,
      self::MODIFY,
      self::REPORT,
      self::PUBLISH,
      self::REMOVE,
      self::LIKE,
      self::COMMENT
    ]) && $subject instanceof Problem;
  }

  #================================================================#

  protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if (!$user instanceof UserInterface) {
      return false;
    }

    /** @var Problem $problem */
    $problem = $subject;

    return match ($attribute) {
      self::VIEW => $this->canView($problem, $user),
      self::MODIFY => $this->canModify($problem, $user),
      self::EDIT => $this->canEdit($problem, $user),
      self::REPORT => $this->canReport($problem, $user),
      self::PUBLISH => $this->canPublish($problem, $user),
      self::REMOVE => $this->canRemove($problem, $user),
      self::LIKE => $this->canLike($problem),
      self::COMMENT => $this->canComment($problem),
      default => throw new \LogicException('This code should not be reached!')
    };
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  private function canView(Problem $problem, User $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    if ($user === $problem->getUser()) {
      return true;
    }

    if ($problem->getTag() === Problem::TAG_SAMPLE) {
      return true;
    }

    if (
      $problem->getTag() === Problem::TAG_COMMUNITY_PUBLIC &&
      (
        $this->security->isGranted('ROLE_ADHERENT') ||
        $this->security->isGranted('ROLE_VIP') ||
        !$this->rightsManagement->isRestrictedForPublicProblem($problem->getDiscr())
      )
    ) {
      return true;
    }

    return false;
  }

  #================================================================#

  private function canModify(Problem $problem, User $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    if ($problem->getTag() === Problem::TAG_COMMUNITY_PRIVATE && $user === $problem->getUser()) {
      return true;
    }

    return false;
  }

  #================================================================#

  private function canEdit(Problem $problem, User $user): bool
  {
    if (
      $this->security->isGranted('ROLE_USER') &&
      ($problem->getTag() === Problem::TAG_SAMPLE || $user === $problem->getUser())
    ) {
      return true;
    }

    if (
      ($this->security->isGranted('ROLE_ADHERENT') || $this->security->isGranted('ROLE_VIP')) &&
      $problem->getTag() === Problem::TAG_COMMUNITY_PUBLIC
    ) {
      return true;
    }

    return false;
  }

  #================================================================#

  private function canReport(Problem $problem, User $user): bool
  {
    return $this->security->isGranted('ROLE_USER') && $problem->getUser() !== $user;
  }

  #================================================================#

  private function canPublish(Problem $problem, User $user): bool
  {
    if ($problem->getTag() === Problem::TAG_COMMUNITY_PRIVATE && $user === $problem->getUser()) {
      return true;
    }

    return false;
  }

  #================================================================#

  private function canRemove(Problem $problem, User $user): bool
  {
    return $this->canModify($problem, $user);
  }

  #================================================================#

  private function canLike(Problem $problem): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    return !$this->rightsManagement->isRestrictedForPublicProblem($problem->getDiscr());
  }

  #================================================================#

  private function canComment(Problem $problem): bool
  {
    return $this->canLike($problem);
  }
}
