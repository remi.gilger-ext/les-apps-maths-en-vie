<?php

declare(strict_types=1);

namespace Banque\Controller;

use Banque\Entity\Problem;
use Banque\Enum\NotificationTypeEnum;
use Banque\Repository\ProblemCommentRepository;
use Banque\Repository\ProblemRepository;
use Banque\Service\CacheService;
use Banque\Service\GradeService;
use Banque\Service\Log\UserLog;
use Banque\Service\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/profil')]
#[IsGranted('ROLE_USER')]
class ProfilController extends AbstractController
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const AMOUT_NOTIFICATION_PER_PAGE = 10;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private CacheService $cacheService,
    private EntityManagerInterface $em,
    private ProblemRepository $problemRepository,
    private ProblemCommentRepository $problemCommentRepository
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/activity', name: 'profil.activity', methods: 'GET')]
  public function profilActivity(): Response
  {
    /** @var User $user */
    $user = $this->getUser();

    $numberProblems = $this->problemRepository->count(['user' => $user]);

    $problems = $this->problemRepository->findBy([
      'user' => $user,
      'tag' => Problem::TAG_COMMUNITY_PUBLIC
    ]);

    $numberProblemComments = $this->problemCommentRepository->count(['user' => $user]);
    $numberProblemsWaiting = $this->problemRepository->count([
      'user' => $user,
      'tag' => [
        Problem::TAG_COMMUNITY_PRIVATE_WAIT,
        Problem::TAG_COMMUNITY_PUBLIC_WAIT
      ]
    ]);

    return new JsonResponse([
      'numberProblems' => $numberProblems,
      'numberTextProblemsPublic' => \count(Problem::getTextProblem($problems)),
      'numberPhotosPublic' => \count(Problem::getPhoto($problems)),
      'numberPhotoProblemsPublic' => \count(Problem::getPhotoProblem($problems)),
      'numberProblemComments' => $numberProblemComments,
      'numberProblemsWaiting' => $numberProblemsWaiting
    ]);
  }

  #================================================================#

  #[Route('/grade', name: 'profil.grade', methods: 'GET')]
  public function profilGrade(): Response
  {
    /** @var User $user */
    $user = $this->getUser();

    $steps = [...GradeService::getGradeStep()];
    \array_pop($steps);

    $gradeProgressBar = [
      'step' => $steps,
      'actual' => $this->problemRepository->count([
        'user' => $user->getId(),
        'tag' => [
          Problem::TAG_COMMUNITY_PUBLIC,
          Problem::TAG_COMMUNITY_PUBLIC_WAIT,
          Problem::TAG_COMMUNITY_PRIVATE_WAIT
        ]
      ]),
      'actualValidate' => \array_sum($this->cacheService->nbProblemPublic($user))
    ];

    $isCertifie = $gradeProgressBar['actualValidate'] >= GradeService::CERTIFIE_GRADE_AMOUNT;

    return new JsonResponse([
      'gradeProgressBar' => $gradeProgressBar,
      'isCertifie' => $isCertifie,
      'urlBadge' => $isCertifie ? 'https://www.mathsenvie.fr/badges/badge-contributeur.png' : ''
    ]);
  }

  #================================================================#

  #[Route('/emails-subscribe', name: 'profil.emailsSubscribe', methods: 'PUT')]
  public function emailsSubscribe(
    UserLog $userLog,
    Request $request,
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    $subscribed = $request->getPayload()->getBoolean('subscribed', true);

    $user->setIsEmailsSubscribed($subscribed);
    $this->em->flush();

    if ($subscribed) {
      $userLog->subscribedToEmails($user);
    } else {
      $userLog->unsubscribedToEmails($user);
    }

    return new JsonResponse([
      'isEmailsSubscribed' => $subscribed
    ]);
  }

  #================================================================#

  #[Route('/notifications/{type}/{page?1}', name: 'profil.notifications', requirements: ['page' => '\d+'], methods: 'GET')]
  public function notificationPageType(
    CacheService $cache,
    NotificationService $notificationService,
    NotificationTypeEnum $type,
    int $page = 1
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    $notifications = $cache->getNotifications($user);
    $totalPages = (int) ((\count($notifications[$type->value]) - 1) / self::AMOUT_NOTIFICATION_PER_PAGE) + 1;

    $notifications = \array_slice(
      $notifications[$type->value],
      ($page - 1) * self::AMOUT_NOTIFICATION_PER_PAGE,
      self::AMOUT_NOTIFICATION_PER_PAGE
    );

    /**
     * Not update the last notification time if the notification page requested do not contain new notifications
     *
     * NOTE: This is not perfect as if there is 2 pages containing new notifications (ex: "likes" and "comments") requesting just
     *  one will updated the other one.
     */
    if ($notificationService->getNumberNewNotification($user)['types'][$type->value] !== 0) {
      $user->setLastNotificationChecked(new \DateTime('now', new \DateTimeZone('UTC')));
      $this->em->flush();
    }

    return new JsonResponse([
      'page' => $page,
      'totalPage' => $totalPages,
      'notifications' => $notifications
    ]);
  }

  #================================================================#

  #[Route('/update-notifications', name: 'profil.updateNotifications', methods: 'GET')]
  public function updateNotification(
    NotificationService $notificationService
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    $notificationsNumber = $notificationService->getNumberNewNotification($user);

    return new JsonResponse($notificationsNumber);
  }
}
