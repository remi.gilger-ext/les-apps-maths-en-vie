<?php

declare(strict_types=1);

namespace Banque\Controller;

use Banque\Entity\Photo;
use Banque\Entity\PhotoProblem;
use Banque\Entity\Problem;
use Banque\Entity\TextProblem;
use Banque\Enum\DailyCounterTypeEnum;
use Banque\Enum\GenerateAiTypeEnum;
use Banque\Enum\TextProblemTypeEnum;
use Banque\Form\PhotoModel;
use Banque\Form\PhotoProblemModel;
use Banque\Form\TextProblemModel;
use Banque\Repository\ProblemRepository;
use Banque\Repository\TextProblemRepository;
use Banque\Service\CacheService;
use Banque\Service\GradeService;
use Banque\Service\ImageService;
use Banque\Service\Log\ProblemLog;
use Banque\Service\MessageService;
use Banque\Service\OpenAIService;
use Banque\Service\ParseToArrayService;
use Banque\Service\RightsManagement;
use Banque\Trans\Trans;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Repository\UserRepository;
use Shared\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/admin')]
#[IsGranted('ROLE_ADMIN')]
class AdminController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private AccessDecisionManagerInterface $accessDecisionManager,
    private EntityManagerInterface $em,
    private CacheService $cacheService,
    private MailerService $mailerService,
    private MessageService $messageService,
    private OpenAIService $openAIService,
    private ProblemRepository $problemRepository,
    private UserRepository $userRepository,
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('', name: 'api.admin', methods: 'GET')]
  public function index(): Response
  {
    $nbrProblemNotReviewed = $this->problemRepository->count([
      'isVerified' => 0,
      'tag' => [
        Problem::TAG_COMMUNITY_PRIVATE_WAIT,
        Problem::TAG_COMMUNITY_PUBLIC_WAIT
      ]
    ]);

    $stats = $this->cacheService->getBanqueStats();

    return new JsonResponse([
      'nbrProblemNotReviewed' => $nbrProblemNotReviewed,
      'numberOfVisit' => $stats['numberOfVisit']
    ]);
  }

  #================================================================#

  #[Route('/review/{id?0}', name: 'admin.reviewProblem', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'])]
  public function reviewProblem(
    ProblemLog $problemLog,
    GradeService $gradeService,
    ImageService $imageService,
    ParseToArrayService $parseToArrayService,
    Request $request,
    ValidatorInterface $validator,
    Problem $problem = null
  ): Response {
    $reject = $request->query->getBoolean('reject');
    $remove = $request->query->getBoolean('remove');

    /** @var User $user */
    $user = $this->getUser();

    $index = $request->query->getInt('index');
    # To select a problem to review
    if ($problem === null) {
      $problems = $this->problemRepository->findBy([
        'isVerified' => 0,
        'tag' => [
          Problem::TAG_COMMUNITY_PRIVATE_WAIT,
          Problem::TAG_COMMUNITY_PUBLIC_WAIT
        ]
      ], ['sendingDate' => 'DESC']);

      if (empty($problems)) {
        return new JsonResponse([
          'message' => 'Bravo, tous les problèmes ont été vérifiés !',
          'nbrProblemNotReviewed' => \count($problems)
        ], status: 200);
      }
      $index = $index % \count($problems);
      $problem = $problems[$index];

      return new JsonResponse([
        'problem' => [...$parseToArrayService->parseProblem($problem), 'email' => $problem->getUser()->getEmail()],
        'nbrProblemNotReviewed' => \count($problems),
        'prev' => $index > 0,
        'next' => $index < \count($problems) - 1
      ]);
    }

    # If the admin refuse to publish a private problem --> it will return to private
    if ($reject) {
      $problem->setTag(Problem::TAG_COMMUNITY_PRIVATE);
      $this->em->persist($problem);
      $this->em->flush();

      $problemLog->logEvent($user, 'REFUSE-PUBLISH', "Problem #{$problem->getId()} was refused to be published");

      return new JsonResponse([
        'message' => 'Le problème a été refusé.'
      ], status: 200);
    }

    if ($remove) {
      return $this->forward('Banque\Controller\ProblemController::removeProblem', [
        'id' => $problem->getId()
      ]);
    }

    # The problem is already verified
    if ($problem->isVerified()) {
      return new JsonResponse([
        'message' => 'Le problème a déjà été validé.',
        'variant' => 'warning'
      ], status: 200);
    }

    # The problem need to be verified
    $problemValidator = null;

    if ($problem instanceof TextProblem) {
      $problemValidator = new TextProblemModel($this->openAIService);
    } elseif ($problem instanceof Photo) {
      $problemValidator = new PhotoModel($imageService, $this->getParameter('directory.photos'));
    } elseif ($problem instanceof PhotoProblem) {
      $problemValidator = new PhotoProblemModel($imageService, $this->getParameter('directory.photos'));
    } else {
      throw new \LogicException('Problem instance impossible');
    }

    $problemValidator->fillInstance($request->getPayload());
    $errors = $validator->validate($problemValidator);

    $problemValidator->removeModifyingErrors($errors);

    if ($errors->count() === 0) {
      $problemValidator->updateInstance($problem, true);

      $problem->setIsVerified(true);
      $problem->setTag(Problem::TAG_COMMUNITY_PUBLIC);
      $problem->setVerifiedDate(new \DateTime('now', new \DateTimeZone('UTC')));

      if ($problem instanceof PhotoProblem && $problemValidator->getAddToPhoto()) {
        $photo = new Photo();
        $photo->setAttachment($problem->getAttachment());
        $photo->setType(\implode(";", $problemValidator->getPhotoType()));
        $photo->setIsVerified(true);
        $photo->setTag(Problem::TAG_COMMUNITY_PUBLIC);
        $photo->setVerifiedDate(new \DateTime('now', new \DateTimeZone('UTC')));

        $problem->getUser()->addProblem($photo);
        $this->em->persist($photo);
      }

      $this->em->flush();

      $this->accessPublicProblem($problem);

      if (isset($photo)) {
        $this->accessPublicProblem($photo);
      }

      $gradeService->updateGrade($problem->getUser());

      $problemLog->logEvent($user, 'ACCEPT', "{$problem->getDiscr()->value} #{$problem->getId()} was accepted");

      return new JsonResponse([
        'message' => 'Le problème vient d\'être accepté.'
      ], status: 200);
    }

    return new JsonResponse([
      'formErrors' => $problemValidator->getFormatErrors($errors)
    ], status: 400);
  }

  #================================================================#

  #[Route('/emails-subscribed-number', name: 'console.emailsSubscribedNumber', methods: 'GET')]
  public function emailsSubscribedNumber(): Response
  {
    $users = $this->userRepository->findBy([
      'isVerified' => true,
      'isEmailsSubscribed' => true
    ]);

    return new JsonResponse(\count($users));
  }

  #================================================================#

  #[Route('/send-email/{offset}/{limit}', name: 'admin.sendEmail', methods: 'POST', requirements: ['offset' => '\d+', 'limit' => '\d+'])]
  public function sendMail(
    Request $request,
    int $offset,
    int $limit
  ): Response {
    \set_time_limit(300);
    session_write_close();

    $payload = $request->getPayload();
    $subject = $payload->getString('subject', 'M@ths en-vie - Appenvie');
    $content = $payload->getString('content');

    if (\mb_strlen($subject) > 128 || \mb_strlen($content) > 1024) {
      return new JsonResponse([
        'message' => 'Sujet ou contenu trop long.'
      ], status: 400);
    }

    if (empty($content)) {
      return new JsonResponse([
        'message' => 'Vous devez renseigner le contenu du courriel.'
      ], status: 400);
    }

    $users = $this->userRepository->findBy([
      'isVerified' => true,
      'isEmailsSubscribed' => true
    ], limit: $limit, offset: $offset);

    if (\count($users) === 0) {
      return new JsonResponse([
        'message' => 'All emails have been sent.'
      ], status: 400);
    }

    $context = [
      'title' => $subject,
      'content' => $content
    ];

    $this->mailerService->sendEmailToList($users, $offset, $context, '@Banque/emails/globalEmail.html.twig', $subject, true);

    return new JsonResponse([
      'message' => \count($users) . ' mails ont bien été envoyés.'
    ]);
  }

  #================================================================#

  #[Route('/export-users-csv', name: 'admin.exportUsersCsv', methods: 'GET')]
  public function exportUsersCsv(): Response
  {
    $response = new StreamedResponse(function () {
      $fp = fopen('php://output', 'wb');

      if ($fp === false) {
        return new StreamedResponse();
      }

      $now = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
      echo 'Export,' . $now->format("Y-m-d H:i:s") . \PHP_EOL;

      \fputcsv($fp, [
        'id',
        'roles',
        'username',
        'email',
        'date d\'inscription',
        'date de dernière connexion',
        'utilisateur vérifié',
        'contributions totale',
        'contributions problèmes',
        'contributions photos',
        'contributions photo-problèmes',
        'grade'
      ]);
      \flush();

      foreach ($this->userRepository->userIterator() as $user) {
        $problems = $user->getProblems()->toArray();
        $textProblems = \count(Problem::getTextProblem($problems, true));
        $photos = \count(Problem::getPhoto($problems, true));
        $photoProblems = \count(Problem::getPhotoProblem($problems, true));
        \fputcsv($fp, [
          $user->getId(),
          \implode(" | ", $user->getRoles()),
          $user->getUsername(),
          $user->getEmail(),
          $user->getRegisterDate()->format('Y-m-d H:i:s'),
          $user->getLastLogin()->format('Y-m-d H:i:s'),
          $user->isVerified() ? "Oui" : "Non",
          $textProblems + $photos + $photoProblems,
          $textProblems,
          $photos,
          $photoProblems,
          $user->getGrade()
        ]);
        \flush();
      }
      fclose($fp);
    });

    $response->headers->set('Content-Type', 'text/csv');

    $disposition = HeaderUtils::makeDisposition(
      HeaderUtils::DISPOSITION_ATTACHMENT,
      'users-export.csv'
    );
    $response->headers->set('Content-Disposition', $disposition);

    return $response;
  }

  #================================================================#

  #[Route('/export-problems-csv', name: 'admin.exportProblemsCsv', methods: 'GET')]
  public function exportProblemsCsv(): Response
  {
    $response = new StreamedResponse(function () {
      $fp = fopen('php://output', 'wb');

      if ($fp === false) {
        return new StreamedResponse();
      }

      $now = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
      echo 'Export,' . $now->format("Y-m-d H:i:s") . \PHP_EOL;

      \fputcsv($fp, [
        'id',
        'titre',
        'enoncé',
        'niveau',
        'type',
        'problème est exclu',
        'réponse',
        'etat réponse',
        'phrase réponse',
        'aide 1',
        'aide 2',
        'response explication',
        'score total',
        'quantité résolu'
      ]);
      \flush();

      $problems = $this->problemRepository->findAll();
      $textProblems = Problem::getTextProblem($problems, true);

      foreach ($textProblems as $problem) {
        \fputcsv($fp, [
          $problem->getId(),
          $problem->getTitle(),
          $problem->getStatement(),
          $problem->getNivel(),
          TextProblemTypeEnum::fromName($problem->getType())->value,
          $problem->isExclude() ? "Oui" : "Non",
          $problem->getResponse() ? \str_replace(";", ":", $problem->getResponse()) : "",
          $problem->getResponseState()->value,
          $problem->getResponseSentence(),
          $problem->getHelpSentence1(),
          $problem->getHelpSentence2(),
          $problem->getResponseExplication(),
          $problem->getTotalScore(),
          $problem->getAmoutAnswered()
        ]);
        \flush();
      }
      fclose($fp);
    });

    $response->headers->set('Content-Type', 'text/csv');

    $disposition = HeaderUtils::makeDisposition(
      HeaderUtils::DISPOSITION_ATTACHMENT,
      'problems-export.csv'
    );
    $response->headers->set('Content-Disposition', $disposition);

    return $response;
  }

  #================================================================#

  #[Route('/visitor-diagram/{temporality}/{startDay}', name: 'admin.visitorDiagram', methods: 'GET')]
  public function visitorDiagram(
    string $temporality = '',
    string $startDay = ''
  ): Response {
    if (empty($temporality) || empty($startDay)) {
      return new JsonResponse(status: 400);
    }

    if (!\in_array($temporality, ['week', 'month', 'year', 'all'])) {
      return new JsonResponse(status: 400);
    }

    if (!\file_exists($this->getParameter('dailyCounterFilePath'))) {
      return new JsonResponse(status: 400);
    }

    $startDate = \DateTime::createFromFormat('d-m-Y', $startDay);

    if ($startDate === false) {
      return new JsonResponse(status: 400);
    }

    $visits = \json_decode(\file_get_contents($this->getParameter('dailyCounterFilePath')), true);

    $labels = [];
    $data = [];

    if ($temporality === 'week') {
      // $labels = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
      $date = $startDate;

      for ($i = 0; $i < 30; $i += 1) {
        $iDate = $date->format('d-m-Y');
        $labels[] = $iDate;
        foreach (DailyCounterTypeEnum::cases() as $case) {
          if (isset($visits[$iDate][$case->value])) {
            $data[$case->value][] = $visits[$iDate][$case->value];
          } else {
            $data[$case->value][] = 0;
          }
        }
        $date = $date->modify('+1 day');
      }
    }

    return new JsonResponse([
      'labels' => $labels,
      'data' => $data
    ]);
  }

  #================================================================#

  #[Route('/problems-ai/{type}', name: 'admin.problemsAi', methods: 'GET')]
  public function problemsAi(
    TextProblemRepository $textProblemRepository,
    GenerateAiTypeEnum $type
  ): Response {
    return new JsonResponse($textProblemRepository->getIdProblemsNeedToGenerateResponse($type));
  }

  #================================================================#

  #[Route('/generate-ai/{id?0}/{generateAiTypeEnum}', name: 'admin.generateAi', requirements: ['id' => '\d+'], methods: 'PUT')]
  public function generateAi(
    OpenAIService $openAIService,
    Problem $problem,
    GenerateAiTypeEnum $generateAiTypeEnum
  ): Response {
    if (!$problem instanceof TextProblem) {
      return new Response(status: 404);
    }

    session_write_close();
    $openAIService->generateIA($problem, $generateAiTypeEnum);

    return new JsonResponse([
      "responseState" => $problem->getResponseState(),
      "response" => $problem->getResponse(),
      "responseSentence" => $problem->getResponseSentence(),
      "helpSentence1" => $problem->getHelpSentence1(),
      "helpSentence2" => $problem->getHelpSentence2(),
      "responseExplication" => $problem->getResponseExplication()
    ]);
  }

  #================================================================#
  # Private Methods                                                #
  #================================================================#

  /**
   * If the user have the correct amount of problems needed to have access to all problems we send an email and a notification
   *
   * @param Problem $problem problem added
   * @return void
   */
  private function accessPublicProblem(Problem $problem)
  {
    $this->cacheService->nbProblemPublic($problem->getUser(), true);

    $token = new UsernamePasswordToken($problem->getUser(), 'none', $problem->getUser()->getRoles());

    if (!$this->accessDecisionManager->decide($token, ['ROLE_ADHERENT']) && !$this->accessDecisionManager->decide($token, ['ROLE_VIP'])) {
      if ($this->cacheService->nbProblemPublic($problem->getUser())[$problem->getDiscr()->value] === RightsManagement::AMOUT_PUBLIC_PROBLEM_MINIMUM) {
        $this->mailerService->grantedOnPublicProblem($problem->getUser(), $problem->getDiscr());
        $admin = $this->userRepository->findOneBy(['username' => 'Administrateur']);

        if ($admin === null) {
          throw new \LogicException('User \'Administrateur\' don\'t exist in the database');
        }

        $this->messageService->sendMessage($admin, $problem->getUser(), 'Accès à la banque collaborative', Trans::trans(Trans::ADMIN_GRANTED_PUBLIC_PROBLEM, $problem->getDiscr()));
      }
    }
  }
}
