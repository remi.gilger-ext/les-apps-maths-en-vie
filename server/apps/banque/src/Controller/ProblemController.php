<?php

declare(strict_types=1);

namespace Banque\Controller;

use Banque\Entity\Photo;
use Banque\Entity\PhotoProblem;
use Banque\Entity\Problem;
use Banque\Entity\ProblemLike;
use Banque\Entity\TextProblem;
use Banque\Enum\DailyCounterTypeEnum;
use Banque\Enum\GenerateAiTypeEnum;
use Banque\Enum\ProblemTypeEnum;
use Banque\Enum\ResponseStateEnum;
use Banque\Form\AbstractProblemFilters;
use Banque\Form\PhotoFilters;
use Banque\Form\PhotoModel;
use Banque\Form\PhotoProblemFilters;
use Banque\Form\PhotoProblemModel;
use Banque\Form\TextProblemModel;
use Banque\Form\TextProblemFilters;
use Banque\Repository\ProblemLikeRepository;
use Banque\Repository\ProblemRepository;
use Banque\Security\Voter\ProblemVoter;
use Banque\Service\CacheService;
use Banque\Service\DailyCounterService;
use Banque\Service\GradeService;
use Banque\Service\ImageService;
use Banque\Service\Log\ProblemLog;
use Banque\Service\OpenAIService;
use Banque\Service\ParseToArrayService;
use Banque\Service\PdfService;
use Banque\Service\RightsManagement;
use Banque\Trans\Trans;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Shared\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api')]
class ProblemController extends AbstractController
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const AMOUT_PROBLEM_PER_PAGE = 10;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private CacheService $cacheService,
    private DailyCounterService $dailyCounterService,
    private EntityManagerInterface $em,
    private ImageService $imageService,
    private MailerService $mailerService,
    private OpenAIService $openAIService,
    private ParseToArrayService $parseToArrayService,
    private ProblemLikeRepository $problemLikeRepository,
    private ProblemLog $problemLog,
    private ProblemRepository $problemRepository
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/banque/stats', name: 'banque.stats', methods: 'GET')]
  public function stats(
    CacheService $cacheService
  ): Response {
    $stats = $cacheService->getBanqueStats();

    return new JsonResponse([
      'totalContributions' => $stats['totalContributions'],
      'numberOfContributors' => $stats['numberOfContributors'],
      'topUsers' => $stats['topUsers'],
    ]);
  }

  #================================================================#

  #[Route('/problem/{id?0}', name: 'problem.get', requirements: ['id' => '\d+'], methods: 'GET')]
  public function get(
    Problem $problem = null
  ): Response {
    if ($problem === null) {
      return new JsonResponse([
        'message' => 'Le problème est inexistent.'
      ], status: 404);
    }

    if ($this->isGranted(ProblemVoter::VIEW, $problem)) {
      return new JsonResponse($this->parseToArrayService->parseProblem($problem, $this->getUser()));
    }

    return new JsonResponse([
      'message' => 'Vous n\'avez pas les droits de consulter ce problème.',
      'variant' => 'info'
    ], status: 403);
  }

  #================================================================#

  #[Route('/problem-counter', name: 'problems.counter', methods: 'GET')]
  /**
   * Get the count of each type of problem
   *
   * @return Response
   */
  public function problemsCounter(
    CacheService $cacheService
  ): Response {
    return new JsonResponse($cacheService->problemsCounter());
  }

  #================================================================#

  #[Route('/problems/{type}/{page?1}', name: 'problems.get', requirements: ['page' => '\d+'], methods: 'POST')]
  /**
   * Get problems based on type, page and filters
   *
   * @return Response
   */
  public function getProblems(
    Request $request,
    RightsManagement $rightsManagement,
    ValidatorInterface $validator,
    ProblemTypeEnum $type,
    int $page = 1
  ): Response {
    $user = $this->getUser();

    $canSeePublicProblem = !$rightsManagement->isRestrictedForPublicProblem(
      $type,
      $request->getPayload()->getBoolean('isEdition')
    );

    $filtersValidator = null;

    if ($type === ProblemTypeEnum::TextProblem) {
      $filtersValidator = new TextProblemFilters();
    } elseif ($type === ProblemTypeEnum::Photo) {
      $filtersValidator = new PhotoFilters();
    } elseif ($type === ProblemTypeEnum::PhotoProblem) {
      $filtersValidator = new PhotoProblemFilters();
    } else {
      throw new \LogicException('The type of the problem is invalid');
    }

    $filtersValidator->fillInstance($request->getPayload());

    $errors = $validator->validate($filtersValidator);

    if (\count($errors) === 0) {
      $paramQuery = $this->getParamsQuery($filtersValidator, $canSeePublicProblem);

      $amoutProblem = $this->problemRepository->getProblemsQuery(Problem::getClassFromType($type), $paramQuery, true);
      $totalPages = (int) (($amoutProblem - 1) / self::AMOUT_PROBLEM_PER_PAGE) + 1;

      $problems = $this->problemRepository->getProblemsQuery(Problem::getClassFromType($type), $paramQuery, false, self::AMOUT_PROBLEM_PER_PAGE, ($page - 1) * self::AMOUT_PROBLEM_PER_PAGE);

      $problems = \array_map(function (Problem $problem) use ($user): array {
        return $this->parseToArrayService->parseProblem($problem, $user);
      }, $problems);

      $result = [
        'page' => $page,
        'totalPage' => $totalPages,
        'totalProblems' => $amoutProblem,
        'problems' => $problems,
        'canSeePublicProblem' => $canSeePublicProblem
      ];

      return new JsonResponse($result);
    }

    return new JsonResponse([
      'formErrors' => $filtersValidator->getFormatErrors($errors)
    ], status: 400);
  }

  #================================================================#

  #[IsGranted('ROLE_USER')]
  #[Route('/problem/{type}/add', name: 'problem.add', methods: 'POST')]
  /**
   * Add a problem
   *
   * @return Response
   */
  public function addProblem(
    Request $request,
    ValidatorInterface $validator,
    ProblemTypeEnum $type,
  ): Response {
    /**
     * @var User user
     */
    $user = $this->getUser();

    $problemValidator = null;

    if ($type === ProblemTypeEnum::TextProblem) {
      $problemValidator = new TextProblemModel($this->openAIService);
    } elseif ($type === ProblemTypeEnum::Photo) {
      $problemValidator = new PhotoModel($this->imageService, $this->getParameter('directory.photos'));
    } elseif ($type === ProblemTypeEnum::PhotoProblem) {
      $problemValidator = new PhotoProblemModel($this->imageService, $this->getParameter('directory.photos'));
    } else {
      throw new \LogicException('The type of the problem is invalid');
    }

    $problemValidator->fillInstance($request->getPayload());

    $errors = $validator->validate($problemValidator);

    if ($errors->count() === 0) {
      $problem = $problemValidator->createInstance();
      $user->addProblem($problem);
      $this->em->persist($problem);
      $this->em->flush();

      if ($problem->getTag() === Problem::TAG_COMMUNITY_PUBLIC_WAIT) {
        $message = Trans::trans(Trans::PROBLEM_ADD_PUBLIC, $problem->getDiscr());
        $this->problemLog->logEvent($user, 'ADD', "Public {$type->value} #{$problem->getId()} added");
        $this->dailyCounterService->add(DailyCounterTypeEnum::PROBLEMS_ADDED);

        if ($this->getParameter('kernel.environment') === 'prod') {
          $this->mailerService->notifyAdminNewProblem($problem->getDiscr(), $user->getUserIdentifier());
        }
      } else {
        $message = Trans::trans(Trans::PROBLEM_ADD_PRIVATE, $problem->getDiscr());
        $this->problemLog->logEvent($user, 'ADD', "Private {$type->value} #{$problem->getId()} added");
      }

      return new JsonResponse([
        'message' => $message
      ], status: 200);
    }

    // To delete the temporary image if there is errors
    $problemValidator->clear();

    return new JsonResponse([
      'formErrors' => $problemValidator->getFormatErrors($errors)
    ], status: 400);
  }

  #================================================================#

  #[IsGranted('ROLE_USER')]
  #[Route('/problem/{id?0}/modify', name: 'problem.modify', requirements: ['id' => '\d+'], methods: 'PUT')]
  /**
   * Modify a problem
   *
   * @return Response
   */
  public function modifyProblem(
    Problem $problem,
    Request $request,
    ValidatorInterface $validator
  ): Response {
    if ($this->isGranted(ProblemVoter::MODIFY, $problem)) {
      $problemValidator = null;
      if ($problem instanceof TextProblem) {
        $problemValidator = new TextProblemModel($this->openAIService);
      } elseif ($problem instanceof Photo) {
        $problemValidator = new PhotoModel($this->imageService, $this->getParameter('directory.photos'));
      } elseif ($problem instanceof PhotoProblem) {
        $problemValidator = new PhotoProblemModel($this->imageService, $this->getParameter('directory.photos'));
      } else {
        throw new \LogicException('Problem instance impossible');
      }

      $problemValidator->fillInstance($request->getPayload());
      $errors = $validator->validate($problemValidator);

      $problemValidator->removeModifyingErrors($errors);

      if ($errors->count() === 0) {
        $problemValidator->updateInstance($problem);

        $this->em->flush();

        $this->problemLog->logEvent($this->getUser(), 'EDIT', "Problem #{$problem->getId()} edited");

        return new JsonResponse([
          'message' => 'Le problème vient d\'être modifié.'
        ], status: 200);
      }

      return new JsonResponse([
        'formErrors' => $problemValidator->getFormatErrors($errors)
      ], status: 400);
    }

    return new JsonResponse(status: 403);
  }

  #================================================================#

  #[IsGranted('ROLE_USER')]
  #[Route('/problem/{id?0}/like', name: 'problem.like', requirements: ['id' => '\d+'], methods: 'PUT')]
  public function likeProblem(
    Problem $problem = null
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    if ($problem === null) {
      return new Response(status: 404);
    }

    if ($this->isGranted(ProblemVoter::LIKE, $problem)) {
      if ($problem->getUser() !== $user) {
        $like = $this->problemLikeRepository->findOneBy(['user' => $user, 'problem' => $problem]);
        if ($like !== null) {
          if ($like->getValue() === 1) {
            $like->setValue(0);
            $problem->setCounterLike($problem->getCounterLike() - 1);
          } else {
            $like->setValue(1);
            $problem->setCounterLike($problem->getCounterLike() + 1);
          }
        } else {
          $like = new ProblemLike();
          $like->setCreatedAt(new \DateTime('now', new \DateTimeZone('UTC')));
          $like->setValue(1);

          $user->addProblemLike($like);
          $problem->addProblemLike($like);

          $problem->setCounterLike($problem->getCounterLike() + 1);

          $this->problemLog->logEvent($this->getUser(), 'LIKE', "Problem #{$problem->getId()} liked");
        }
        $this->cacheService->getNotifications($problem->getUser(), true);

        $this->em->persist($problem);
        $this->em->flush();

        return new JsonResponse([
          'isLiked' => $like->getValue() === 1,
          'nbrLikes' => $problem->getCounterLike()
        ], status: 200);
      }

      return new JsonResponse([
        'message' => 'Vous ne pouvez pas aimer vos propres contenus.'
      ], status: 403);
    }

    return new JsonResponse([
      'message' => 'Vous devez être connecté, et avoir contribué à 3 problèmes.'
    ], status: 403);
  }

  #================================================================#

  #[IsGranted('ROLE_USER')]
  #[Route('/problem/{id?0}/remove', name: 'problem.remove', requirements: ['id' => '\d+'], methods: 'DELETE')]
  public function removeProblem(
    GradeService $gradeService,
    Problem $problem = null
  ): Response {
    if ($this->isGranted(ProblemVoter::REMOVE, $problem)) {
      $problemId = $problem->getId();
      $problemUser = $problem->getUser();

      if ($problem instanceof Photo || $problem instanceof PhotoProblem) {
        // We remove the image just if the image is not split between differents problems
        if (!$this->problemRepository->isAttachmentSpareBetweenMultipleProblem($problem->getAttachment())) {
          \unlink($this->getParameter('directory.photos') . '/' . $problem->getAttachment());
        }
      }

      $this->problemRepository->remove($problem, true);

      $this->cacheService->nbProblemPublic($problemUser, true);
      $this->cacheService->getNotifications($problemUser, true);
      $gradeService->updateGrade($problemUser);

      $this->problemLog->logEvent($this->getUser(), 'REMOVE', "Problem #{$problemId} removed");

      return new JsonResponse([
        'message' => 'Le problème a bien été supprimé.'
      ], status: 200);
    }

    return new JsonResponse(status: 403);
  }

  #================================================================#

  #[IsGranted('ROLE_USER')]
  #[Route('/problem/{id?0}/publish', name: 'problem.publish', requirements: ['id' => '\d+'], methods: 'PUT')]
  public function publishProblem(
    Problem $problem = null
  ): Response {
    if ($this->isGranted(ProblemVoter::PUBLISH, $problem)) {
      $problem->setTag(Problem::TAG_COMMUNITY_PRIVATE_WAIT);

      $this->em->flush();

      $this->problemLog->logEvent($this->getUser(), 'PUBLISH', "Problem #{$problem->getId()} published");

      return new JsonResponse([
        'message' => 'Le problème a été soumis à un administrateur pour validation.'
      ], status: 200);
    }

    return new JsonResponse(status: 403);
  }

  #================================================================#

  #[IsGranted('ROLE_USER')]
  #[Route('/problem/{id?0}/report', name: 'problem.report', requirements: ['id' => '\d+'], methods: 'POST')]
  public function reportProblem(
    Request $request,
    Problem $problem = null
  ): Response {
    if ($this->isGranted(ProblemVoter::REPORT, $problem)) {
      $comment = $request->getPayload()->getString('comment', 'Aucune raison n\'a été donnée :(');

      if (\mb_strlen($comment) > 500) {
        return new JsonResponse([
          'message' => 'Commentaire trop long.'
        ], status: 400);
      }

      $this->mailerService->reportProblem($this->getUser(), $problem, $comment);

      $this->problemLog->logEvent($this->getUser(), 'REPORT', "Problem #{$problem->getId()} reported");

      return new JsonResponse([
        'message' => 'Le problème a bien été signalé.'
      ], status: 200);
    }

    return new JsonResponse(status: 403);
  }

  #================================================================#

  #[IsGranted('ROLE_ADMIN')]
  #[Route('/problem/{id?0}/resize', name: 'problem.resizePhoto', requirements: ['id' => '\d+'], methods: 'PUT')]
  public function resizePhoto(
    Request $request,
    Problem $problem = null
  ): Response {
    if ($problem === null) {
      return new JsonResponse([
        'message' => 'Le problème est introuvable.'
      ], status: 404);
    }

    if (!$problem instanceof Photo && !$problem instanceof PhotoProblem) {
      return new JsonResponse([
        'message' => 'Le problème doit être de type Photo ou PhotoProblem.'
      ], status: 404);
    }

    $payload = $request->getPayload();
    $addLogo = $payload->getBoolean('addLogo');
    $imageRotation = $payload->getInt('imageRotation', 0);
    $imageData = $payload->getString('imageData', '0;0;1;1');

    $fichier = $problem->getAttachment();

    $this->imageService->setImage($fichier);
    $this->imageService->rotate($imageRotation);
    $this->imageService->resize($imageData);
    if ($addLogo) {
      $this->imageService->addLogo();
    }
    $this->imageService->saveImage($fichier);

    return new JsonResponse([
      'message' => 'L\'image a été redimensionné.'
    ], status: 200);
  }

  #================================================================#

  #[IsGranted('ROLE_ADMIN')]
  #[Route('/problem/{id?0}/validate-response', name: 'problem.validateResponse', requirements: ['id' => '\d+'], methods: 'PUT')]
  public function validateResponse(
    Request $request,
    OpenAIService $openAIService,
    Problem $problem = null
  ): Response {
    if ($problem === null) {
      return new JsonResponse([
        'message' => 'Le problème est introuvable.'
      ], status: 404);
    }

    if (!$problem instanceof TextProblem) {
      return new JsonResponse([
        'message' => 'Le problème doit être de type TextProblem.'
      ], status: 404);
    }

    $response = $request->getPayload()->getString('response');

    if (
      !empty($response) &&
      $problem->getResponseState() === ResponseStateEnum::Conflict ||
      $problem->getResponseState() === ResponseStateEnum::AnsweredByIa
    ) {
      $r = \trim(\str_replace([".", " "], [",", ""], $response));
      $problem->setResponse($r);
      $problem->setResponseState(ResponseStateEnum::Verified);

      $openAIService->generateIA($problem, GenerateAiTypeEnum::ResponseExplication);

      $this->em->flush();
    }

    return new JsonResponse([
      'message' => 'La réponse a bien été validée.'
    ], status: 200);
  }

  #================================================================#

  #[Route('/edition/{problemType}/{editionType}/{index}/{shuffle?0}', name: 'problem.edition', requirements: ['editionype' => '(pdf|proj)', 'index' => '\d'], methods: ['POST'])]
  public function edition(
    PdfService $pdfService,
    Request $request,
    ValidatorInterface $validator,
    ProblemTypeEnum $problemType,
    string $editionType,
    int $index,
    bool $shuffle = false
  ): Response {
    $problems = [];

    if ($shuffle) {
      # Get the right amount of problem depending of the type choosen
      $numberProblemsWanted = 0;
      $type = (string) $editionType . $index;
      switch ($type) {
        case "pdf1":
          $numberProblemsWanted = $problemType === ProblemTypeEnum::TextProblem ? 10 : 2;
          break;
        case "pdf2":
          $numberProblemsWanted = $problemType === ProblemTypeEnum::TextProblem ? 20 : 2;
          break;
        case "pdf3":
          $numberProblemsWanted = $problemType === ProblemTypeEnum::TextProblem ? 2 : 4;
          break;
        case "pdf4":
          $numberProblemsWanted = 4;
          break;
        case "proj1":
          $numberProblemsWanted = 1;
          break;
        case "proj2":
          $numberProblemsWanted = 2;
          break;
        case "proj3":
          $numberProblemsWanted = 5;
          break;
        default:
          return new Response(status: 404);
      }

      $filtersValidator = null;

      if ($problemType === ProblemTypeEnum::TextProblem) {
        $filtersValidator = new TextProblemFilters();
      } elseif ($problemType === ProblemTypeEnum::PhotoProblem) {
        $filtersValidator = new PhotoProblemFilters();
      } else {
        throw new \LogicException('The type of the problem is invalid');
      }

      $filtersValidator->fillInstance(new InputBag($request->getPayload()->all('filters')));

      $errors = $validator->validate($filtersValidator);

      if (\count($errors) > 0) {
        return new JsonResponse([
          'formErrors' => $filtersValidator->getFormatErrors($errors)
        ], status: 400);
      }

      $paramQuery = $this->getParamsQuery($filtersValidator, $this->isGranted('ROLE_ADHERENT') || $this->isGranted('ROLE_VIP'));

      $allProblems = $this->problemRepository->getProblemsQuery(Problem::getClassFromType($problemType), $paramQuery, false);

      \srand(\time());

      if (!empty($allProblems)) {
        $idProblems = array_rand($allProblems, \count($allProblems) >= $numberProblemsWanted ? $numberProblemsWanted : \count($allProblems));

        if (\is_scalar($idProblems)) {
          $problems = [$allProblems[$idProblems]];
        } else {
          $problems = \array_values(\array_intersect_key($allProblems, \array_flip($idProblems)));
        }
      }
    } else {
      $problems = $this->problemRepository->findBy(['id' => $request->getPayload()->all('problemIds')]);
    }

    $problems = \array_filter($problems, function (Problem $problem) use ($problemType): array|bool {
      return $this->isGranted(ProblemVoter::EDIT, $problem) && $problem->getDiscr() === $problemType;
    });

    if (\count($problems) === 0) {
      return new JsonResponse([
        'message' => 'Veuillez sélectionner au moins 1 problème.'
      ], status: 400);
    }

    if (\count($problems) > 30) {
      return new JsonResponse([
        'message' => 'Vous ne pouvez pas séléctionner plus de 30 problèmes.'
      ], status: 400);
    }

    if ($editionType === 'pdf') {
      $view = $problemType === ProblemTypeEnum::TextProblem ? '@Banque/pdfs/textProblemEditionPdf.html.twig' : '@Banque/pdfs/photoProblemEditionPdf.html.twig';

      $html = $this->renderView($view, [
        'problems' => $problems,
        'type' => $index
      ]);

      $html = \preg_replace('/>\s+</', "><", $html);
      $rootPath = $this->getParameter('app.public_dir');

      if (
        ($problemType === ProblemTypeEnum::TextProblem && $index === 3) ||
        ($problemType === ProblemTypeEnum::PhotoProblem && \in_array($index, [1, 2]))
      ) {
        $pdfService->setOrientation('landscape');
      }

      foreach ($problems as $problem) {
        $problem->setDownloadCount($problem->getDownloadCount() + 1);
      }

      $this->em->flush();

      $result = $pdfService->generate($html, $rootPath);

      return new JsonResponse([
        'pdfData' => $result
      ]);
    } elseif ($editionType === 'proj') {
      $problems = \array_map(function (Problem $problem): array {
        return $this->parseToArrayService->parseProblem($problem);
      }, $problems);

      return new JsonResponse([
        'problems' => [...$problems],
        'type' => $index
      ]);
    } else {
      throw new \LogicException("This edition type is impossible");
    }
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  private function getParamsQuery(AbstractProblemFilters $filtersValidator, bool $canSeePublicProblem)
  {
    #=================================================
    # To get the problem that the user can see and use

    /**
     * @var User user
     */
    $user = $this->getUser();

    $optionalParam = [];
    $requiredParam = [];

    if ($user !== null) {
      $optionalParam['p.user'] = $user->getId();
    }

    $tag = [Problem::TAG_SAMPLE];
    if ($this->isGranted('ROLE_USER') && $canSeePublicProblem) {
      $tag[] = Problem::TAG_COMMUNITY_PUBLIC;
    }

    $optionalParam['p.tag'] = $tag;

    $filtersValidator->updateRequiredParam($requiredParam);

    if (count($filtersValidator->getTag())) {
      unset($optionalParam['p.user']);
      $tag = [];
      if (\in_array(Problem::TAG_COMMUNITY_PUBLIC, $filtersValidator->getTag())) {
        $tag[] = Problem::TAG_COMMUNITY_PUBLIC;
        $tag[] = Problem::TAG_SAMPLE;
      }

      $optionalParam['p.tag'] = array_intersect($optionalParam['p.tag'], $tag);

      // If the user choose their own problem
      if ($user !== null && \in_array(Problem::TAG_COMMUNITY_PRIVATE, $filtersValidator->getTag())) {
        $optionalParam['p.user'] = $user->getId();
      } else {
        if (empty($optionalParam['p.tag'])) {
          $optionalParam['p.tag'] = [Problem::TAG_SAMPLE];
        }
      }
    }

    if (empty($optionalParam['p.tag'])) {
      unset($optionalParam['p.tag']);
    }

    #=================================================

    return [$requiredParam, $optionalParam];
  }
}
