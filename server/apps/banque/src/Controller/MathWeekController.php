<?php

declare(strict_types=1);

namespace Banque\Controller;

use Banque\Entity\Photo;
use Banque\Form\MathWeekProblemModel;
use Banque\Repository\MathWeekProblemRepository;
use Banque\Repository\PhotoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/math-week')]
class MathWeekController extends AbstractController
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/counter', name: 'mathWeek.counter', methods: 'GET')]
  public function problemsCounter(
    MathWeekProblemRepository $mathWeekProblemRepository
  ): Response {
    return new JsonResponse($mathWeekProblemRepository->count([]));
  }

  #================================================================#

  #[Route('/photos', name: 'mathWeek.photos', methods: 'GET')]
  public function problemsPhotos(
    PhotoRepository $photoRepository,
  ): Response {
    // $photos = $photoRepository->findBy([
    //   'id' => [9956, 9951, 9978, 9979, 9980, 9981]
    // ]);

    $photos = $photoRepository->findBy([
      'id' => [2958, 2959, 1261, 1264, 1266, 1272, 1282, 1303, 1305, 1306, 1307, 1312, 1313, 1329, 1332, 1349, 1350, 1351, 1353, 1354, 1355, 1356, 1360, 1361, 1366, 1370, 1371, 1372, 1373, 1379, 1382, 1399, 1406, 1407, 1408, 1429, 1439, 1442, 1447, 1449, 1450, 1451, 1452, 1455, 1465, 1466, 1468, 1475, 1487, 1488, 1494, 1497, 1502, 1503, 1504, 1507, 1509, 1510, 1514, 1515, 1516, 1517, 1519, 1523, 1526, 1527, 1528, 1531, 1533, 1535, 1536, 1537, 1539, 1556, 1557, 1565, 1566, 1570, 1571, 1572, 1574, 1575, 1580, 1583, 1584, 1585, 1586, 1595, 1596, 1597, 1598, 1599, 1602, 1607, 1609, 1611, 1612, 1613, 1614, 1615, 1917, 1618, 1619, 1622, 1623, 1624, 1625, 1626, 1627, 1628, 1629, 1630, 1631, 1632, 1633, 1634, 1636, 1652, 1737, 1806, 1814, 1815, 1818, 1830, 1831, 1848, 1849, 1851, 1852, 1859, 1883, 1882, 1895, 1896, 1899, 1903, 1904, 1912, 1913, 1914, 1915, 1916, 1920, 1959, 1970, 1979, 1981, 1982, 1989, 1990, 1998, 1999, 2001, 2003, 2005, 2006, 2007, 2010, 2011, 2018, 2019, 2045, 2051, 2090, 2450, 2471, 2474, 2542, 2554, 2557, 2745, 2748, 2751, 2769, 2770, 3022, 3023, 3027, 3033, 3152, 3153, 3154, 3202, 3203, 3204, 3205, 3206, 3207, 3208, 3209, 3210, 3211, 3212, 3213, 3296, 3302, 3308, 3336, 3337, 3338, 3339, 3340, 3341, 3342, 3343, 3362, 3381, 3391, 3415, 3417, 3421, 3918, 3920, 3921, 3922, 3923, 4025, 4158, 4211, 4429, 4463, 5295, 5569, 5595, 5765, 5791, 5859, 5961, 6065, 6136, 6251, 6252, 6253, 6257, 6261, 6262, 6263, 6264, 6268, 6271, 6276, 6280, 6374, 6564, 6772, 6776, 6777, 6778, 6781, 6783, 6784, 6785, 6786, 6787, 6788, 6918, 6937, 19924, 19930, 19931, 20157, 20158, 20164, 20203, 20314, 20327, 20328, 20331, 20334, 20335, 20336, 20337, 20338, 20343, 20347, 20491, 20493, 20530, 20542, 20546, 20549, 20578, 20587, 20652, 20653, 20683, 20736, 20745, 20750, 20754, 20755, 20757, 20759, 20760, 20761, 20762, 20764, 20767, 20769, 20770, 21025, 21039, 21041, 21042, 21043, 21044, 21078, 21126, 21148, 21150, 21181, 21202, 21203, 21227, 21231, 21411, 21412, 22437, 22440, 22427, 22429, 22144, 22206, 22058, 21904, 21905, 21906, 21907, 21908, 21909, 21725, 21726, 21609, 21630, 21631, 21607, 21467]
    ]);

    \srand(time());
    $photoIds = \array_rand($photos, 10);

    $attachments = [];

    $photos = \array_values(\array_intersect_key($photos, \array_flip($photoIds)));

    /** @var Photo */
    foreach ($photos as $photo) {
      $attachments[] = [
        "attachment" => $photo->getAttachment(),
        "photoId" => $photo->getId()
      ];
    }

    return new JsonResponse($attachments);
  }

  #================================================================#

  #[Route('/add', name: 'mathWeek.add', methods: 'POST')]
  public function addProblem(
    EntityManagerInterface $em,
    Request $request,
    ValidatorInterface $validator,
  ): Response {
    $problemValidator = new MathWeekProblemModel();

    $problemValidator->fillInstance($request->getPayload());

    $errors = $validator->validate($problemValidator);

    if ($errors->count() === 0) {
      $problem = $problemValidator->createInstance();

      $em->persist($problem);
      $em->flush();

      return new JsonResponse([
        'message' => 'Le problème a été ajouté avec succès, merci pour votre participation.'
      ], status: 200);
    }

    return new JsonResponse([
      'formErrors' => $problemValidator->getFormatErrors($errors)
    ], status: 400);
  }
}
