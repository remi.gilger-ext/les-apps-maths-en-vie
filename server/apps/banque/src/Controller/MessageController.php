<?php

declare(strict_types=1);

namespace Banque\Controller;

use Banque\Service\MessageService;
use Shared\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/message')]
class MessageController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private MessageService $messageService
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[IsGranted('ROLE_ADMIN')]
  #[Route('/add', name: 'message.add', methods: 'POST')]
  public function add(
    Request $request
  ): Response {
    /** @var User $user */
    $user = $this->getUser();

    $payload = $request->getPayload();
    $title = $payload->getString('title');
    $content = $payload->getString('content');

    if (\mb_strlen($title) > 128 || \mb_strlen($content) > 1024) {
      return new JsonResponse([
        'message' => 'Titre ou contenu trop long.'
      ], status: 400);
    }

    if (empty($title) || empty($content)) {
      return new JsonResponse([
        'message' => 'Vous devez renseigner le titre et le contenu.'
      ], status: 400);
    }

    $this->messageService->sendMessage($user, null, $title, $content);

    return new JsonResponse([
      'message' => 'Le message a bien été posté.'
    ], status: 200);
  }
}
