<?php

declare(strict_types=1);

namespace Banque\Controller;

use Banque\Entity\Problem;
use Banque\Entity\ProblemComment;
use Banque\Repository\ProblemCommentRepository;
use Banque\Security\Voter\ProblemCommentVoter;
use Banque\Security\Voter\ProblemVoter;
use Banque\Service\CacheService;
use Banque\Service\Log\ProblemLog;
use Banque\Service\ParseToArrayService;
use Banque\Trans\Trans;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api')]
class ProblemCommentController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private CacheService $cacheService,
    private EntityManagerInterface $em,
    private ParseToArrayService $parseToArrayService,
    private ProblemCommentRepository $problemCommentRepository,
    private ProblemLog $problemLog
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/comment/{id?0}', name: 'comment.get', methods: 'GET')]
  public function get(
    ProblemComment $comment = null,
  ): Response {
    if ($this->isGranted(ProblemCommentVoter::VIEW, $comment)) {
      return new JsonResponse([
        'comment' => $this->parseToArrayService->parseProblemComment($comment)
      ], status: 200);
    }

    return new JsonResponse(status: 403);
  }

  #================================================================#


  #[Route('/comments/last', name: 'comments.last', methods: 'GET')]
  public function getLastComments(): Response
  {
    $comments = $this->problemCommentRepository->lastComments();

    $comments = \array_map(function (ProblemComment $comment) {
      return $this->parseToArrayService->parseProblemComment($comment);
    }, $comments);

    return new JsonResponse($comments);
  }

  #================================================================#

  #[Route('/problem/{id?0}/comments', name: 'comments.get', methods: 'GET')]
  public function getComments(
    ProblemCommentRepository $problemCommentRepository,
    Problem $problem = null
  ): Response {
    if ($problem !== null) {
      $comments = $problemCommentRepository->findBy([
        'problem' => $problem->getId()
      ], ['createdAt' => 'DESC']);

      $comments = \array_map(function (ProblemComment $comment) {
        return $this->parseToArrayService->parseProblemComment($comment);
      }, $comments);

      return new JsonResponse($comments);
    }

    return new JsonResponse(status: 404);
  }

  #================================================================#

  #[Route('/problem/{id?0}/comment', name: 'comment.add', methods: 'POST')]
  public function addComment(
    Request $request,
    Problem $problem = null
  ): Response {
    /**
     * @var User $user
     */
    $user = $this->getUser();
    if ($problem !== null) {
      if ($this->isGranted(ProblemVoter::COMMENT, $problem)) {
        $commentValue = $request->getPayload()->getString('comment');

        if (\mb_strlen($commentValue) > 512) {
          return new JsonResponse([
            'message' => 'Commentaire trop long.'
          ], status: 400);
        }

        if (empty($commentValue)) {
          return new JsonResponse([
            'message' => 'Vous devez saisir un commentaire.'
          ], status: 400);
        }

        $comment = new ProblemComment();
        $comment->setContent($commentValue);

        $user->addProblemComment($comment, $problem);
        $this->em->persist($comment);
        $this->em->flush();

        $this->cacheService->getNotifications($problem->getUser(), true);

        $this->problemLog->logEvent($this->getUser(), 'COMMENT', "Problem #{$problem->getId()} commented #{$comment->getId()}");

        return new JsonResponse([
          'message' => 'Nouveau commentaire ajouté avec succés.',
          'comment' => $this->parseToArrayService->parseProblemComment($comment),
          'commentCounter' => $problem->getCounterComment()
        ], status: 200);
      }

      return new JsonResponse([
        'message' => Trans::trans(Trans::PROBLEM_COMMENT_COMMENT_NOT_GRANTED, $problem->getDiscr()),
        'variant' => 'warning'
      ], status: 403);
    }

    return new JsonResponse(status: 403);
  }

  #================================================================#

  #[Route('/comment/{id?0}/remove', name: 'comment.remove', methods: 'DELETE')]
  public function removeComment(
    ProblemComment $comment = null,
  ): Response {
    if ($this->isGranted(ProblemCommentVoter::REMOVE, $comment)) {
      $problemUser = $comment->getProblem()->getUser();
      $commentId = $comment->getId();

      $comment->getUser()->removeProblemComment($comment);

      $this->problemCommentRepository->remove($comment, true);

      $this->cacheService->getNotifications($problemUser, true);

      $this->problemLog->logEvent($this->getUser(), 'COMMENT', "Comment #{$commentId} removed");

      return new JsonResponse([
        'message' => 'Le commentaire a bien été supprimé.',
        'commentCounter' => $comment->getProblem()->getCounterComment()
      ], status: 200);
    }

    return new JsonResponse(status: 403);
  }
}
