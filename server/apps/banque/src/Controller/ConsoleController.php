<?php

declare(strict_types=1);

namespace Banque\Controller;

use Banque\Enum\DailyCounterTypeEnum;
use Banque\Service\DailyCounterService;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Repository\UserRepository;
use Shared\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/console')]
#[IsGranted('ROLE_SUPER_ADMIN')]
class ConsoleController extends AbstractController
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private DailyCounterService $dailyCounterService,
    private EntityManagerInterface $em,
    private UserRepository $userRepository,
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/cache-clear', name: 'console.cacheClear', methods: 'PUT')]
  public function cacheClear(
    KernelInterface $kernel
  ): Response {
    $input = new ArrayInput([
      'command' => 'cache:clear'
    ]);

    return new JsonResponse([
      'message' => $this->runCommand($input, $kernel)
    ]);
  }

  #================================================================#

  #[Route('/sql', name: 'console.sql', methods: 'PUT')]
  public function sql(): Response
  {
    return new Response();
  }

  #================================================================#

  #[Route('/confirm-user', name: 'console.confirmUser', methods: 'PUT')]
  public function confirmUser(
    MailerService $mailerService,
    Request $request
  ): Response {
    $payload = $request->getPayload();
    $email = $payload->getString('email', '');
    $notifyUser = $payload->getBoolean('notifyUser', false);

    $user = $this->userRepository->findOneBy(['email' => $email]);

    if ($user !== null) {
      if (!$user->isVerified()) {
        $user->setIsVerified(true);
        $this->em->flush();

        if ($notifyUser) {
          $mailerService->confirmUser($user);
        }

        $mailerService->welcome($user);

        $this->dailyCounterService->add(DailyCounterTypeEnum::REGISTRATIONS);

        return new JsonResponse([
          'message' => 'L\'utilisateur à été accepté.'
        ], status: 200);
      }

      return new JsonResponse([
        'message' => 'Utilisateur déjà vérifié.'
      ], status: 400);
    }

    return new JsonResponse([
      'message' => 'Aucun compte pour cet email.'
    ], status: 400);
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  /**
   * Global function to lunch an command
   *
   * @param ArrayInput $input
   * @param KernelInterface $kernel
   *
   * @return string
   */
  private function runCommand(
    ArrayInput $input,
    KernelInterface $kernel
  ): string {
    $application = new Application($kernel);
    $application->setAutoExit(false);

    $output = new BufferedOutput();
    $application->run($input, $output);

    return $output->fetch();
  }
}
