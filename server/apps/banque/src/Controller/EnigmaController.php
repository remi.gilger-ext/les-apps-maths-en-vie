<?php

declare(strict_types=1);

namespace Banque\Controller;

use Banque\Enum\EnigmaTypeEnum;
use Banque\Form\EnigmaFilters;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api')]
class EnigmaController extends AbstractController
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const AMOUT_PROBLEM_PER_PAGE = 10;

  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private RequestStack $requestStack
  ) {}

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[Route('/enigmas/{page?1}', name: 'enigmas.get', requirements: ['page' => '\d+'], methods: 'POST')]
  /**
   * Get enigmas based on page and filters
   *
   * @return Response
   */
  public function getEnigmas(
    Request $request,
    ValidatorInterface $validator,
    int $page = 1
  ): Response {
    $requiredParam = [];

    $filtersValidator = new EnigmaFilters();

    $filtersValidator->fillInstance($request->getPayload());

    $errors = $validator->validate($filtersValidator);

    if (\count($errors) === 0) {
      #=================================================
      # To get the problem that the user can see and use

      $filtersValidator->updateRequiredParam($requiredParam);

      $enigmas = $this->getEnigmasFromParams($requiredParam);

      $this->mixTogether($enigmas);

      $totalPages = (int) ((\count($enigmas) - 1) / self::AMOUT_PROBLEM_PER_PAGE) + 1;

      $problems = \array_slice($enigmas, ($page - 1) * self::AMOUT_PROBLEM_PER_PAGE, self::AMOUT_PROBLEM_PER_PAGE);

      $result = [
        'page' => $page,
        'totalPage' => $totalPages,
        'totalProblems' => \count($enigmas),
        'problems' => $problems,
      ];

      return new JsonResponse($result);
    }

    return new JsonResponse([
      'formErrors' => $filtersValidator->getFormatErrors($errors)
    ], status: 400);
  }

  #================================================================#
  # Internal Private Methods                                       #
  #================================================================#

  private function getEnigmasFromParams(array $params): array
  {
    if (isset($params['enigmaType']) && !empty($params['enigmaType'])) {
      $typeDirectory = $params['enigmaType'];
    } else {
      $typeDirectory = \array_map(function ($o) {
        return $o->name;
      }, EnigmaTypeEnum::cases());
    }

    if (isset($params['enigmaNivel']) && !empty($params['enigmaNivel'])) {
      $nivelDirectory = $params['enigmaNivel'];
    } else {
      $nivelDirectory = ['1', '2', '3'];
    }

    if (\in_array('2', $nivelDirectory)) {
      $nivelDirectory[] = '22';
    }
    if (\in_array('3', $nivelDirectory)) {
      $nivelDirectory[] = '33';
    }

    $resultDirectory = [];

    foreach ($nivelDirectory as $nivel) {
      $resultDirectory = \array_merge(
        $resultDirectory,
        \array_map(function ($o) use ($nivel) {
          return $nivel . 'pb' . $o;
        }, $typeDirectory)
      );
    }

    $result = [];

    $baseDirectory = $this->getParameter('directory.enigma');
    foreach ($resultDirectory as $directory) {
      $absPathDirectory = $baseDirectory . '/' . $directory;
      if (\is_dir($absPathDirectory)) {
        if ($dh = \opendir($absPathDirectory)) {
          while (($file = \readdir($dh)) !== false) {
            if ($file !== '.' && $file !== '..') {
              \preg_match('/(\d\d?)pb(\w+)/', $directory, $match);
              if (empty($match)) {
                throw new \Exception('Error in preg match for enigma file');
              }

              $username = $match[1] === '1' || $match[1] === '2' ? 'JC Rolland IEN Epinay' : ($match[1] === '3' ? 'EurêkaMaths' : 'Maths_en_vie');
              $result[] = [
                'attachment' => $directory . '/' . $file,
                'type' => $match[2],
                'username' => $username
              ];
            }
          }
          \closedir($dh);
        }
      }
    }

    return $result;
  }

  #================================================================#

  private function mixTogether(array &$enigmas): void
  {
    $sessionId = $this->requestStack->getSession()->getId();

    $randId = 0;

    \array_map(function (string $char) use (&$randId) {
      $randId += \ord($char);
    }, \str_split($sessionId));

    \srand($randId);
    \shuffle($enigmas);
  }
}
