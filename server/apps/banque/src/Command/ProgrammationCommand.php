<?php

declare(strict_types=1);

namespace Banque\Command;

use Banque\Enum\TextProblemTypeEnum;
use Shared\Service\ProgrammationService as PS;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:programmation', description: 'Create a string to be used as the programmation for the application problems.')]
class ProgrammationCommand extends Command
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $result = [
      "cp" => [
        "1" => [
          PS::FILTER_TYPE => [
            ...array_fill(0, 3, TextProblemTypeEnum::rdt->name),
            TextProblemTypeEnum::rdp->name
          ],
          PS::FILTER_MAX_NUMBER => 9,
          PS::FILTER_CARACTERS_MAX => 150,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "1.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdt->name,
            TextProblemTypeEnum::rdp->name
          ],
          PS::FILTER_MAX_NUMBER => 9,
          PS::FILTER_CARACTERS_MAX => 150,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "2" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::refa->name,
            TextProblemTypeEnum::rea->name
          ],
          PS::FILTER_MAX_NUMBER => 19,
          PS::FILTER_CARACTERS_MAX => 200,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "2.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::reia->name,
            TextProblemTypeEnum::refa->name,
            TextProblemTypeEnum::rea->name
          ],
          PS::FILTER_MAX_NUMBER => 19,
          PS::FILTER_CARACTERS_MAX => 200,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "3" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rpqa->name,
            TextProblemTypeEnum::rgqa->name,
            TextProblemTypeEnum::re->name
          ],
          PS::FILTER_MAX_NUMBER => 49,
          PS::FILTER_CARACTERS_MAX => 250,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "4" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name
          ],
          PS::FILTER_MAX_NUMBER => 99,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "4.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name
          ],
          PS::FILTER_MAX_NUMBER => 99,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "5" => [
          PS::FILTER_TYPE => [
            ...TextProblemTypeEnum::getAdditionType(true),
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name
          ],
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "5.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::add->name,
            TextProblemTypeEnum::mult->name,
            TextProblemTypeEnum::mixed->name
          ],
          PS::FILTER_ALLOW_DECIMALS => false
        ],
      ],
      "ce1" => [
        "1" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::reia->name,
            TextProblemTypeEnum::refa->name,
            TextProblemTypeEnum::rea->name
          ],
          PS::FILTER_MAX_NUMBER => 19,
          PS::FILTER_CARACTERS_MAX => 200,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "2" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rpqa->name,
            TextProblemTypeEnum::rgqa->name,
            TextProblemTypeEnum::re->name
          ],
          PS::FILTER_MAX_NUMBER => 49,
          PS::FILTER_CARACTERS_MAX => 250,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "3" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdt->name,
            TextProblemTypeEnum::rdp->name
          ],
          PS::FILTER_MAX_NUMBER => 99,
          PS::FILTER_CARACTERS_MAX => 300,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "3.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name
          ],
          PS::FILTER_MAX_NUMBER => 99,
          PS::FILTER_CARACTERS_MAX => 300,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "4" => [
          PS::FILTER_TYPE => [
            ...TextProblemTypeEnum::getAdditionType(true),
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name
          ],
          PS::FILTER_MAX_NUMBER => 249,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::add->name,
            TextProblemTypeEnum::mult->name,
            TextProblemTypeEnum::mixed->name
          ],
          PS::FILTER_ALLOW_DECIMALS => false
        ],
      ],
      "ce2" => [
        "1" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::reia->name,
            TextProblemTypeEnum::refa->name,
            TextProblemTypeEnum::rea->name
          ],
          PS::FILTER_MAX_NUMBER => 49,
          PS::FILTER_CARACTERS_MAX => 250,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "1.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::add->name,
            TextProblemTypeEnum::mult->name,
            TextProblemTypeEnum::mixed->name
          ],
          PS::FILTER_MAX_NUMBER => 49,
          PS::FILTER_CARACTERS_MAX => 250,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "2" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdt->name,
            TextProblemTypeEnum::rdp->name,
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name
          ],
          PS::FILTER_MAX_NUMBER => 99,
          PS::FILTER_CARACTERS_MAX => 300,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "2.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::add->name,
            TextProblemTypeEnum::mult->name,
            TextProblemTypeEnum::mixed->name
          ],
          PS::FILTER_MAX_NUMBER => 99,
          PS::FILTER_CARACTERS_MAX => 300,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "3" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rpqa->name,
            TextProblemTypeEnum::rgqa->name,
            TextProblemTypeEnum::re->name
          ],
          PS::FILTER_MAX_NUMBER => 499,
          PS::FILTER_CARACTERS_MAX => 350,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "3.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rpqm->name,
            TextProblemTypeEnum::rgqm->name,
            TextProblemTypeEnum::rdr->name
          ],
          PS::FILTER_MAX_NUMBER => 499,
          PS::FILTER_CARACTERS_MAX => 350,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "4" => [
          PS::FILTER_TYPE => [
            ...TextProblemTypeEnum::getAdditionType(true),
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name
          ],
          PS::FILTER_MAX_NUMBER => 999,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "4.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::add->name,
            TextProblemTypeEnum::mult->name,
            TextProblemTypeEnum::mixed->name
          ],
          PS::FILTER_MAX_NUMBER => 999,
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "5" => [
          PS::FILTER_TYPE => [
            ...TextProblemTypeEnum::getAdditionType(true),
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name,
            TextProblemTypeEnum::rpqm->name,
            TextProblemTypeEnum::rgqm->name,
            TextProblemTypeEnum::rdr->name
          ],
          PS::FILTER_ALLOW_DECIMALS => false
        ],
        "5.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::add->name,
            TextProblemTypeEnum::mult->name,
            TextProblemTypeEnum::mixed->name,
          ],
          PS::FILTER_ALLOW_DECIMALS => false
        ],
      ],
      "cm1" => [
        "1" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdt->name,
            TextProblemTypeEnum::rdp->name
          ],
          PS::FILTER_MAX_NUMBER => 499,
          PS::FILTER_CARACTERS_MAX => 250,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3
        ],
        "1.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::reia->name,
            TextProblemTypeEnum::refa->name,
            TextProblemTypeEnum::rea->name
          ],
          PS::FILTER_MAX_NUMBER => 499,
          PS::FILTER_CARACTERS_MAX => 250,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3
        ],
        "2" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name
          ],
          PS::FILTER_MAX_NUMBER => 999,
          PS::FILTER_CARACTERS_MAX => 300,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3
        ],
        "3" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rpqa->name,
            TextProblemTypeEnum::rgqa->name,
            TextProblemTypeEnum::re->name,
            TextProblemTypeEnum::frac->name
          ],
          PS::FILTER_MAX_NUMBER => 4_999,
          PS::FILTER_CARACTERS_MAX => 350,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3
        ],
        "4" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name
          ],
          PS::FILTER_MAX_NUMBER => 49_999,
        ],
        "4.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::propor->name
          ],
          PS::FILTER_MAX_NUMBER => 49_999,
        ],
        "5" => [
          PS::FILTER_TYPE => [
            ...TextProblemTypeEnum::casesName(),
            ...array_fill(0, 6, TextProblemTypeEnum::add->name),
            ...array_fill(0, 6, TextProblemTypeEnum::mult->name),
            ...array_fill(0, 6, TextProblemTypeEnum::mixed->name)
          ]
        ],
      ],
      "cm2" => [
        "1" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdt->name,
            TextProblemTypeEnum::rdp->name,
            TextProblemTypeEnum::reia->name,
            TextProblemTypeEnum::refa->name,
            TextProblemTypeEnum::rea->name
          ],
          PS::FILTER_MAX_NUMBER => 4_999,
          PS::FILTER_CARACTERS_MAX => 300,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3
        ],
        "2" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rdtpr->name,
            TextProblemTypeEnum::rvp->name,
            TextProblemTypeEnum::rnp->name
          ],
          PS::FILTER_MAX_NUMBER => 49_999,
          PS::FILTER_CARACTERS_MAX => 350,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3
        ],
        "2.5" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::reim->name,
            TextProblemTypeEnum::refm->name,
            TextProblemTypeEnum::rem->name
          ],
          PS::FILTER_MAX_NUMBER => 49_999,
          PS::FILTER_CARACTERS_MAX => 350,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3
        ],
        "3" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::rpqa->name,
            TextProblemTypeEnum::rgqa->name,
            TextProblemTypeEnum::re->name,
            TextProblemTypeEnum::rpqm->name,
            TextProblemTypeEnum::rgqm->name,
            TextProblemTypeEnum::rdr->name,
            TextProblemTypeEnum::frac->name
          ],
          PS::FILTER_MAX_NUMBER => 499_999,
          PS::FILTER_CARACTERS_MAX => 400,
          PS::FILTER_STEP_PROBLEM_MAX_NUMBERS => 3
        ],
        "4" => [
          PS::FILTER_TYPE => [
            TextProblemTypeEnum::propor->name
          ],
        ],
        "5" => [
          PS::FILTER_TYPE => [
            ...TextProblemTypeEnum::casesName(),
            ...array_fill(0, 6, TextProblemTypeEnum::add->name),
            ...array_fill(0, 6, TextProblemTypeEnum::mult->name),
            ...array_fill(0, 6, TextProblemTypeEnum::mixed->name)
          ]
        ],
      ]
    ];

    file_put_contents("programmation.txt", json_encode($result));

    return Command::SUCCESS;
  }
}
