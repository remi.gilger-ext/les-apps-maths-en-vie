<?php

declare(strict_types=1);

namespace Banque\Command;

use Atelier\Repository\GameRepository;
use Atelier\Repository\StudentRepository;
use Banque\Entity\Problem;
use Banque\Repository\ProblemRepository;
use Shared\Repository\UserRepository;
use Shared\Service\MailerService;
use Shared\Utils\Utils;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand(name: 'app:send-montly-email', description: 'Send a montly email to every users.')]
class SendMontlyEmailCommand extends Command
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private UserRepository $userRepository,
    private ParameterBagInterface $parameterBag,
    private MailerService $mailerService,
    private GameRepository $gameRepository,
    private ProblemRepository $problemRepository,
    private StudentRepository $studentRepository,
  ) {
    parent::__construct();
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $hmacSecret = $this->parameterBag->get('hmacSecret');

    $users = $this->userRepository->findBy([
      'isVerified' => true,
      'isEmailsSubscribed' => true
    ]);

    $contexts = array();

    $startDay = new \DateTime('first day of previous month 00:00:00', new \DateTimeZone('UTC'));
    $endDay = new \DateTime('last day of previous month 00:00:00', new \DateTimeZone('UTC'));

    $month = Utils::convertMonthToFrench($startDay->format("F"));

    if ($month === null) {
      throw new \LogicException("This month don't exist", 1);
    }

    // New contribution this month
    $montlyProblems = $this->problemRepository->getNewMonthlyProblems($startDay, $endDay);

    $montlyTextProblems = \count(Problem::getTextProblem($montlyProblems));
    $montlyPhotos = \count(Problem::getPhoto($montlyProblems));
    $montlyPhotoProblems = \count(Problem::getPhotoProblem($montlyProblems));

    // Atelier stats
    $totalClassesAmout = $this->studentRepository->getNumberOfClass();
    $totalStudentsAmout = $this->studentRepository->count([]);
    $totalGamesAmout = $this->gameRepository->count([]);
    $totalProblemsResolvedAmouts = $totalGamesAmout * 5;

    $top10Users = $this->userRepository->getTopUsers(10);

    foreach ($users as $user) {
      // My class
      $studentsAmout = \count($user->getStudents());
      $gamesAmout = 0;
      foreach ($user->getStudents() as $student) {
        $gamesAmout += \count($student->getGames());
      }
      $problemsResolvedAmouts = $gamesAmout * 5;

      // My contributions
      $contributionProblems = $this->problemRepository->findBy([
        'user' => $user->getId(),
        'tag' => Problem::TAG_COMMUNITY_PUBLIC,
        'isVerified' => true
      ]);

      $contributionTextProblems = \count(Problem::getTextProblem($contributionProblems));
      $contributionPhotos = \count(Problem::getPhoto($contributionProblems));
      $contributionPhotoProblems = \count(Problem::getPhotoProblem($contributionProblems));

      // Interactions on my problems

      $likesData = $this->problemRepository->getProblemLiked($user);

      $likedOnMyProblems = (int) $likesData["nbrLikes"];
      $nbrProblemsLiked = (int) $likesData["nbrProblems"];

      $commentsData = $this->problemRepository->getProblemCommented($user);

      $commentedOnMyProblems = (int) $commentsData["nbrComments"];
      $nbrProblemsCommented = (int) $commentsData["nbrProblems"];

      $problemsDownloaded = $this->problemRepository->getNumberOfDownloads($user);

      $contexts[] = [
        'month' => $month,
        'studentsAmout' => $studentsAmout,
        'gamesAmout' => $gamesAmout,
        'problemsResolvedAmouts' => $problemsResolvedAmouts,
        'totalClassesAmout' => $totalClassesAmout,
        'totalStudentsAmout' => $totalStudentsAmout,
        'totalGamesAmout' => $totalGamesAmout,
        'totalProblemsResolvedAmouts' => $totalProblemsResolvedAmouts,
        'montlyTextProblems' => $montlyTextProblems,
        'montlyPhotos' => $montlyPhotos,
        'montlyPhotoProblems' => $montlyPhotoProblems,
        'contributionTextProblems' => $contributionTextProblems,
        'contributionPhotos' => $contributionPhotos,
        'contributionPhotoProblems' => $contributionPhotoProblems,
        'likedOnMyProblems' => $likedOnMyProblems,
        'nbrProblemsLiked' => $nbrProblemsLiked,
        'commentedOnMyProblems' => $commentedOnMyProblems,
        'nbrProblemsCommented' => $nbrProblemsCommented,
        'problemsDownloaded' => $problemsDownloaded,
        'top10Users' => $top10Users,
        'unsubscribeToken' => \hash_hmac('sha256', (string) $user->getId(), $hmacSecret),
        'userId' => $user->getId()
      ];
    }

    $subject = '[Appenvie] Résumé de l\'activité du mois ' . $month;

    if (!$this->mailerService->sendEmailToList($users, 0, $contexts, '@Banque/emails/montlyReport.html.twig', $subject)) {
      $output->writeln("Command failed : Mois " . $month);
      return Command::FAILURE;
    }

    $output->writeln("Command executed with success : Mois " . $month);
    return Command::SUCCESS;
  }
}
