<?php

declare(strict_types=1);

namespace Banque\Command;

use Shared\Enum\SchoolZone;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:create-period-date', description: 'Create a file with the period date for each zones.')]
class CreatePeriodDatesCommand extends Command
{
  #================================================================#
  # Const Definition                                               #
  #================================================================#

  public const HOLIDAYS_LABEL_API = [
    1 => [["Vacances d'Été", "Vacances d'Hiver austral", "Grandes Vacances"], -1, "Élèves"],
    2 => [["Vacances de la Toussaint", "Vacances après 1ère période", "Vacances de Novembre"], 0, "-"],
    3 => [["Vacances de Noël", "Vacances d'Été austral"], 0, "-"],
    4 => [["Vacances d'Hiver", "Vacances de Carnaval", "Vacances après 3ème période", "Vacances de Février"], 0, "-"],
    5 => [["Vacances de Printemps", "Vacances de Pâques", "Vacances après 4ème période"], 0, "-"],
    6 => [["Vacances d'Été", "Vacances d'Hiver austral", "Début des Vacances d'Été", "Grandes Vacances"], 0, "Élèves"]
  ];

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  protected function configure(): void
  {
    $this->addArgument('startYear', InputArgument::REQUIRED, 'The year to start in the form 20XX-20XX.');
  }

  #================================================================#

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $startYear = \explode("-", $input->getArgument('startYear'));
    $result = [];

    foreach (self::HOLIDAYS_LABEL_API as $period => $params) {
      $schoolYear = $startYear[0] + $params[1] . "-" . $startYear[1] + $params[1];

      foreach ($params[0] as $holidayLabel) {
        $url = "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-calendrier-scolaire/records?select=min(start_date)%20as%20start_date,max(end_date)%20as%20end_date&group_by=zones&refine=annee_scolaire:$schoolYear&refine=description:" . \urlencode($holidayLabel);

        $response = file_get_contents($url);

        $dates = \json_decode($response, true)["results"];

        foreach ($dates as $date) {
          $startDate = new \DateTime($date["start_date"], new \DateTimeZone("UTC"));
          $endDate = new \DateTime($date["end_date"], new \DateTimeZone("UTC"));
          $endDate->modify("+1 day");

          $startDate = $startDate->format("Y/m/d");
          $endDate = $endDate->format("Y/m/d");

          foreach (SchoolZone::cases() as $zone) {
            if ($date["zones"] === $zone->getDisplayName()) {
              if ($period === 1) {
                $result[$zone->value][(string) ($period)][0] = $endDate;
              } else {
                // Calculate the middle date
                $prevEndDate = \date_create($result[$zone->value][(string) ($period - 1)][0], new \DateTimeZone("UTC"));

                $diffDays = \date_diff($prevEndDate, \date_create($startDate, new \DateTimeZone("UTC")))->days;
                $middleDate = $prevEndDate->modify("+" . \ceil($diffDays / 2) . " days")->format("Y/m/d");
                // ==========================

                $result[$zone->value][(string) ($period - 1)][1] = $middleDate;
                $result[$zone->value][(string) ($period - 0.5)][0] = $middleDate;
                $result[$zone->value][(string) ($period - 0.5)][1] = $startDate;

                if ($period !== 6) {
                  $result[$zone->value][(string) ($period)][0] = $endDate;
                }
              }
            }
          }
        }
      }
    }

    $period = "1";

    for ($m = 9; $m <= 12; $m++) {
      $dt = $startYear[0] . "/" . $m . "/01";
      $result[SchoolZone::Default->value][$period] = [\date("Y/m/01", strtotime($dt)), date("Y/m/t", strtotime($dt))];

      $period = (string) ($period + 0.5);
    }

    for ($m = 1; $m <= 6; $m++) {
      $dt = $startYear[1] . "/" . $m . "/01";
      $result[SchoolZone::Default->value][$period] = [\date("Y/m/01", strtotime($dt)), date("Y/m/t", strtotime($dt))];

      $period = (string) ($period + 0.5);
    }

    $dt = $startYear[0] . "/07/01";
    $result[SchoolZone::Default->value]["1"][0] = date("Y/m/01", strtotime($dt));

    $dt = $startYear[1] . "/08/01";
    $result[SchoolZone::Default->value]["5.5"][1] = date("Y/m/t", strtotime($dt));

    file_put_contents("schoolZoneDates.json", json_encode($result));

    return Command::SUCCESS;
  }
}
