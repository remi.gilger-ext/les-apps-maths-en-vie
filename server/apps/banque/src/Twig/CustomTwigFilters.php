<?php

declare(strict_types=1);

namespace Banque\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class CustomTwigFilters extends AbstractExtension
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function getFilters(): array
  {
    return [
      new TwigFilter('htmldecode', [$this, 'htmldecode'], ['is_safe' => array('html')]),
      new TwigFilter('boldQuestion', [$this, 'addBoldToQuestion']),
      new TwigFilter('br', [$this, 'convertIntoBr']),
    ];
  }

  #================================================================#

  public function addBoldToQuestion(string $statement): string
  {
    return \preg_replace_callback('~([A-Z0-9ÀÉ][^.?!]+?\?)|((Calcule|Indique|Donne|Trouve)[^.?!]+?\.)~', function ($question) {
      return '<span style="font-weight: bold">' . $question[0] . '</span>';
    }, $statement) ?? $statement;
  }

  #================================================================#

  public function convertIntoBr(string $string): string
  {
    return str_replace(array("\r\n", "\r", "\n"), "<br>", $string);
  }

  #================================================================#

  public function htmldecode(string $string): string
  {
    return html_entity_decode($string, \ENT_QUOTES);
  }
}
