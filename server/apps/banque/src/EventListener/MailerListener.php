<?php

declare(strict_types=1);

namespace Banque\EventListener;

use Banque\Enum\DailyCounterTypeEnum;
use Banque\Service\DailyCounterService;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Mailer\Event\SentMessageEvent;

final class MailerListener
{
  #================================================================#
  # Constructor                                                    #
  #================================================================#

  public function __construct(
    private DailyCounterService $dailyCounterService
  ) {
  }

  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  #[AsEventListener(event: SentMessageEvent::class)]
  public function onMessage(SentMessageEvent $event): void
  {
    $this->dailyCounterService->add(DailyCounterTypeEnum::EMAILS_SENT);
  }
}
