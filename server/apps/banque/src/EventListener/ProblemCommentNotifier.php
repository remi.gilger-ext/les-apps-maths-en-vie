<?php

declare(strict_types=1);

namespace Banque\EventListener;

use Banque\Entity\ProblemComment;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::prePersist, method: 'prePersist', entity: ProblemComment::class)]
#[AsEntityListener(event: Events::preRemove, method: 'preRemove', entity: ProblemComment::class)]
class ProblemCommentNotifier
{
  #================================================================#
  # Public Methods                                                 #
  #================================================================#

  public function prePersist(ProblemComment $comment, PrePersistEventArgs $event): void
  {
    $problem = $comment->getProblem();
    $problem->setCounterComment($problem->getCounterComment() + 1);
  }

  #================================================================#

  public function preRemove(ProblemComment $comment, PreRemoveEventArgs $event): void
  {
    $problem = $comment->getProblem();
    $problem->setCounterComment($problem->getCounterComment() - 1);
  }
}
