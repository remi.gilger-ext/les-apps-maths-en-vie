<?php

declare(strict_types=1);

namespace Shared\Tests\Service;

use Banque\Entity\Problem;
use Banque\Entity\TextProblem;
use Banque\Enum\ResponseStateEnum;
use Banque\Enum\TextProblemTypeEnum;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shared\Entity\User;
use Shared\Repository\UserRepository;
use Shared\Service\ProgrammationService;

class ProgrammationServiceTest extends TestCase
{
  public static function paramsFilters(): array
  {
    return [
      [ // To test the FILTER_MAX_NUMBER filter
        [ProgrammationService::FILTER_MAX_NUMBER => 9],
        [0, 2]
      ],
      [ // To test the FILTER_STEP_PROBLEM_MAX_NUMBERS filter
        [ProgrammationService::FILTER_STEP_PROBLEM_MAX_NUMBERS => 2],
        [0, 2, 3, 4]
      ],
      [ // To test the FILTER_CARACTERS_MAX filter
        [ProgrammationService::FILTER_CARACTERS_MAX => 20],
        [0, 1, 2, 4]
      ],
      [ // To test the FILTER_ALLOW_DECIMALS filter
        [ProgrammationService::FILTER_ALLOW_DECIMALS => false],
        [1, 4]
      ],
      [ // To test the FILTER_ALLOW_DECIMALS filter
        [ProgrammationService::FILTER_ALLOW_DECIMALS => 'exclusif'],
        [0, 2, 3]
      ],
      [ // To test the FILTER_ALLOW_DECIMALS filter
        [ProgrammationService::FILTER_ALLOW_DECIMALS => true],
        [0, 1, 2, 3, 4]
      ],
    ];
  }

  #================================================================#

  /**
   * @return TextProblem[]
   */
  public static function problemsSample(): array
  {
    $problem0 = ProgrammationServiceTest::createProblem(TextProblemTypeEnum::mult);
    $problem0->setStatement("ee 9 sd 3,34 ss");
    $problem1 = ProgrammationServiceTest::createProblem(TextProblemTypeEnum::add);
    $problem1->setStatement("ee 10 sd 8 sd 5");
    $problem2 = ProgrammationServiceTest::createProblem(TextProblemTypeEnum::refa);
    $problem2->setStatement("ee 1 sd 3.34 1 ss");
    $problem3 = ProgrammationServiceTest::createProblem(TextProblemTypeEnum::refa);
    $problem3->setStatement("problem with more than 20 caracters");
    $problem3->setResponse("2.34");
    $problem4 = ProgrammationServiceTest::createProblem(TextProblemTypeEnum::refa);
    $problem4->setStatement("less than 20");

    return [
      $problem0,
      $problem1,
      $problem2,
      $problem3,
      $problem4,
    ];
  }

  #================================================================#

  public static function createProblem(TextProblemTypeEnum $type): TextProblem
  {
    $problem = new TextProblem();
    $problem->setTag(Problem::TAG_COMMUNITY_PUBLIC);
    $problem->setResponseState(ResponseStateEnum::Verified);
    $problem->setResponse("10");
    $problem->setType($type->name);

    return $problem;
  }

  #================================================================#

  /**
   * @dataProvider paramsFilters
   *
   * @param mixed[] $filters
   * @param int[] $initialProblemIndexes
   * @param int[] $expectedProblemIndexes
   */
  public function testFiltersProblems(
    array $filters,
    array $expectedProblemIndexes
  ): void {
    $mathsenvie = new User();
    $mathsenvie->setProgrammation("default");

    /** @var MockObject&UserRepository $userRepositoryMock */
    $userRepositoryMock = $this->createMock(UserRepository::class);
    $userRepositoryMock->method("findOneBy")
      ->willReturn($mathsenvie);

    $programmationService = new ProgrammationService("", "test", $userRepositoryMock);

    $filteredProblems = $programmationService->filtersProblems(
      ProgrammationServiceTest::problemsSample(),
      $filters
    );

    $resultStatements = \array_map(function (TextProblem $problem) {
      return $problem->getStatement();
    }, $filteredProblems);

    $expectedStatements = [];
    foreach ($expectedProblemIndexes as $index) {
      $expectedStatements[] = ProgrammationServiceTest::problemsSample()[$index]->getStatement();
    }

    $this->assertEqualsCanonicalizing($expectedStatements, $resultStatements);
  }
}
