<?php

declare(strict_types=1);

namespace Shared\Tests\Form;

use PHPUnit\Framework\TestCase;
use Shared\Form\RituelParameters;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validation;

class RituelParametersTest extends TestCase
{
  public static function paramsProvider(): array
  {
    return [
      [[
        "schoolZone" => "zoneA",
        "classNivel" => ["cp", "cm2", "ce2"],
      ], []],
      [[
        "schoolZone" => "zoneB",
        "classNivel" => ["nothing"],
      ], ["classNivel"]],
      [[
        "schoolZone" => "zoneA",
        "classNivel" => ["cp", "ce1", "ce2", "cm1"],
      ], ["classNivel"]]
    ];
  }

  #================================================================#

  /** @dataProvider paramsProvider */
  public function testValidation(array $params, array $errorsName): void
  {
    $rituelParametersValidator = new RituelParameters();

    $rituelParametersValidator->fillInstance(new InputBag($params));

    $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

    $errors = $validator->validate($rituelParametersValidator);

    /** @var ConstraintViolation $error */
    foreach ($errors as $error) {
      $this->assertContains($error->getPropertyPath(), $errorsName);
    }

    $this->assertSame(\count($errorsName), $errors->count());
  }

  #================================================================#

  public function testWithUnkownSchoolZone(): void
  {
    $rituelParametersValidator = new RituelParameters();

    $this->expectExceptionCode(0);
    $rituelParametersValidator->fillInstance(new InputBag([
      "schoolZone" => "zoneUnknow"
    ]));
  }

  #================================================================#

  public function testDuplicateClassNivel(): void
  {
    $rituelParametersValidator = new RituelParameters();

    $rituelParametersValidator->fillInstance(new InputBag([
      "classNivel" => ["cp", "ce1", "ce1"],
    ]));

    $this->assertEquals(["cp", "ce1"], $rituelParametersValidator->getClassNivel());
  }
}
