<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241114170240 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $games = $this->connection->fetchAllAssociative('SELECT * FROM game');

    foreach ($games as $game) {
      $datas = \json_decode($game["game_data"], true);

      $newDatas = [];
      foreach ($datas as $data) {
        $data["responseGiven"] = "";
        $newDatas[] = $data;
      }
      $newDatas = \json_encode($newDatas);
      //dd('UPDATE `game` SET `game_data` = \'' . $newDatas . '\' WHERE id_game = ' . $game["id_game"]);
      $this->addSql('UPDATE `game` SET `game_data` = \'' . $newDatas . '\' WHERE id_game = ' . $game["id_game"]);
    }
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
  }
}
