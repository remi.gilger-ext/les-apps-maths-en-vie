<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241116083934 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE textProblem ADD textProblem_englishTraduction VARCHAR(1024) DEFAULT NULL, ADD textProblem_italianTraduction VARCHAR(1024) DEFAULT NULL, ADD textProblem_spanishTraduction VARCHAR(1024) DEFAULT NULL, ADD textProblem_germanTraduction VARCHAR(1024) DEFAULT NULL');
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE `textProblem` DROP textProblem_englishTraduction, DROP textProblem_italianTraduction, DROP textProblem_spanishTraduction, DROP textProblem_germanTraduction');
  }
}
