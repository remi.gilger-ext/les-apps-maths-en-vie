<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Atelier\Enum\GameType;
use Atelier\Enum\ProblemStateEnum;
use Atelier\Enum\SchoolNivel;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Shared\Utils\Utils;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241111104223 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    /** @var array<int, array{totalScore: float, amoutAnswered: int}> $problems */
    $problems = [];

    $games = $this->connection->fetchAllAssociative('SELECT * FROM game');

    foreach ($games as $game) {
      $datas = \json_decode($game["game_data"], true);
      foreach ($datas as $data) {
        $problemNivelSql = $this->connection->fetchOne('SELECT textProblem_nivel FROM textProblem WHERE id_problem = ' . $data["id"]);

        $problemNivel = null;

        if (!empty($problemNivelSql) && SchoolNivel::tryFrom($problemNivelSql)) {
          $problemNivel = SchoolNivel::from($problemNivelSql);
        }

        $score = Utils::getScore(GameType::from($game["game_type"]), ProblemStateEnum::from($data["state"]), $data["aidsUsed"], SchoolNivel::from($game["game_nivel"]), $problemNivel);

        if ($score === null) {
          continue;
        }

        if (!\array_key_exists($data["id"], $problems)) {
          $problems[$data["id"]] = [
            "totalScore" => 0,
            "amoutAnswered" => 0
          ];
        }

        $problems[$data["id"]]["amoutAnswered"] += 1;
        $problems[$data["id"]]["totalScore"] += $score;
      }
    }

    $this->addSql('ALTER TABLE textProblem ADD textProblem_totalScore DOUBLE PRECISION DEFAULT \'0\' NOT NULL, ADD textProblem_amoutAnswered INT DEFAULT 0 NOT NULL');

    foreach (\array_column($this->connection->fetchAllAssociative('SELECT id_problem FROM textProblem'), 'id_problem') as $id) {
      if (\array_key_exists($id, $problems)) {
        $this->addSql('UPDATE textProblem SET textProblem_totalScore = ' . $problems[$id]["totalScore"] . ', textProblem_amoutAnswered = ' . $problems[$id]["amoutAnswered"] . ' WHERE id_problem = ' . $id);
      }
    }
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE `textProblem` DROP textProblem_totalScore, DROP textProblem_amoutAnswered');
  }
}
