<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241006140707 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE game ADD game_nivel VARCHAR(16) NOT NULL');
    $this->addSql('UPDATE game g LEFT JOIN student s ON g.id_student = s.id_student SET g.game_nivel = s.student_nivel WHERE g.id_student IS NOT NULL');
    $this->addSql('UPDATE game g LEFT JOIN textProblem p ON p.id_problem = JSON_EXTRACT(g.game_data, "$[0].id") SET g.game_nivel = p.textProblem_nivel WHERE g.id_student IS NULL');
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE `game` DROP game_nivel');
  }
}
