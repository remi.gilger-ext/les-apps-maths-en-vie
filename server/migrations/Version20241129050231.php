<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241129050231 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $this->addSql('CREATE TABLE problem_traduction (id_problem INT NOT NULL, id_problemTraduction INT AUTO_INCREMENT NOT NULL, problemTraduction_englishTitle VARCHAR(64) DEFAULT NULL, problemTraduction_englishStatement VARCHAR(1024) DEFAULT NULL, problemTraduction_italianTitle VARCHAR(64) DEFAULT NULL, problemTraduction_italianStatement VARCHAR(1024) DEFAULT NULL, problemTraduction_spanishTitle VARCHAR(64) DEFAULT NULL, problemTraduction_spanishStatement VARCHAR(1024) DEFAULT NULL, problemTraduction_germanTitle VARCHAR(64) DEFAULT NULL, problemTraduction_germanStatement VARCHAR(1024) DEFAULT NULL, UNIQUE INDEX UNIQ_87A143ED97A65B8 (id_problem), PRIMARY KEY(id_problemTraduction)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    $this->addSql('ALTER TABLE problem_traduction ADD CONSTRAINT FK_87A143ED97A65B8 FOREIGN KEY (id_problem) REFERENCES `textProblem` (id_problem)');

    $problems = $this->connection->fetchAllAssociative('SELECT * FROM textProblem');

    $sql = 'INSERT INTO problem_traduction (id_problem, problemTraduction_englishStatement, problemTraduction_italianStatement, problemTraduction_spanishStatement, problemTraduction_germanStatement) VALUES ';

    foreach ($problems as $problem) {
      $englishTraduction = \str_replace('\'', '\'\'', $problem["textProblem_englishTraduction"] ?? "") ?: null;
      $italianTraduction = \str_replace('\'', '\'\'', $problem["textProblem_italianTraduction"] ?? "") ?: null;
      $spanishTraduction = \str_replace('\'', '\'\'', $problem["textProblem_spanishTraduction"] ?? "") ?: null;
      $germanTraduction = \str_replace('\'', '\'\'', $problem["textProblem_germanTraduction"] ?? "") ?: null;

      if ($englishTraduction === null || $italianTraduction === null || $spanishTraduction === null || $germanTraduction === null) {
        continue;
      }

      $sql .= "({$problem["id_problem"]}, '{$englishTraduction}', '{$italianTraduction}', '{$spanishTraduction}', '{$germanTraduction}'),";
    }

    $this->addSql(\rtrim($sql, ","));

    $this->addSql('ALTER TABLE textProblem DROP textProblem_englishTraduction, DROP textProblem_italianTraduction, DROP textProblem_spanishTraduction, DROP textProblem_germanTraduction');
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE problem_traduction DROP FOREIGN KEY FK_87A143ED97A65B8');
    $this->addSql('DROP TABLE problem_traduction');
    $this->addSql('ALTER TABLE `textProblem` ADD textProblem_englishTraduction VARCHAR(1024) DEFAULT NULL, ADD textProblem_italianTraduction VARCHAR(1024) DEFAULT NULL, ADD textProblem_spanishTraduction VARCHAR(1024) DEFAULT NULL, ADD textProblem_germanTraduction VARCHAR(1024) DEFAULT NULL');
  }
}
