<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250209082829 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE tt_user ADD user_programmationType VARCHAR(16) NOT NULL');

    $this->addSql('UPDATE tt_user SET user_programmationType = \'mathsenvie\'');
    $this->addSql('UPDATE `tt_user` SET `user_programmationType` = \'custom\' WHERE user_programmation IS NOT NULL');

    $this->addSql('ALTER TABLE rituel ADD rituel_programmationType VARCHAR(16) NOT NULL');
    $this->addSql('UPDATE rituel SET rituel_programmationType = \'mathsenvie\'');
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE `tt_user` DROP user_programmationType');
    $this->addSql('ALTER TABLE `rituel` DROP rituel_programmationType');
  }
}
