<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241214162353 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    $users = $this->connection->fetchAllAssociative('SELECT id_user, user_programmation FROM tt_user WHERE user_programmation IS NOT NULL');
    foreach ($users as $user) {
      $programmation = \json_decode($user["user_programmation"], true);

      foreach ($programmation as $nivel => $nivelProgrammation) {
        foreach ($nivelProgrammation as $period => $periodProgrammation) {
          if (isset($periodProgrammation["f1"])) {
            if ($periodProgrammation["f1"] === ["rdt", "rdp", "rdtpr", "rvp", "rnp", "refa", "reia", "rea", "refm", "reim", "rem", "rpqa", "rgqa", "re", "rpqm", "rgqm", "rdr", "step", "propor", "frac", "english", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step"]) {
              $programmation[$nivel][$period]["f1"] = ["rdt", "rdp", "rdtpr", "rvp", "rnp", "refa", "reia", "rea", "refm", "reim", "rem", "rpqa", "rgqa", "re", "rpqm", "rgqm", "rdr", "add", "mult", "mixed", "propor", "frac", "add", "add", "add", "add", "add", "add", "mult", "mult", "mult", "mult", "mult", "mult", "mixed", "mixed", "mixed", "mixed", "mixed", "mixed"];
            } else {
              if (\in_array("step", $periodProgrammation["f1"])) {
                $newTypes = \array_diff($periodProgrammation["f1"], ["step"]);
                $newTypes[] = "add";
                $newTypes[] = "mult";
                $newTypes[] = "mixed";
                $programmation[$nivel][$period]["f1"] = $newTypes;
              }
            }
          }
        }
      }
      $this->addSql('UPDATE `tt_user` SET `user_programmation` = \'' . \json_encode($programmation) . '\' WHERE id_user = ' . $user["id_user"]);
    }
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
  }
}
